<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::Table('classes')->delete();
        /*$roles= array(

                'role' =>   'Administrator'

        );*/
        DB::Table('classes')->insert(
            [
                [
                    'class' => 'Montessori'
                ],
                [
                    'class' => 'Nursery'
                ],
                [ 'class' => 'LKG'],
                [ 'class' =>  'UKG'],
                [ 'class' => 'One'],
                [ 'class' => 'Two'],
                [ 'class' => 'Three'],
                [ 'class' => 'Four'],
                [ 'class' => 'Five'],
                [ 'class' => 'Six'],
                [ 'class' => 'Seven'],
                [ 'class' => 'Eight'],
                [ 'class' => 'Nine'],
                [ 'class' => 'Ten']
            ]
        );
    }
}
