<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WeekPlansTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('weekplans')->delete();

        DB::table('weekplans')->insert([
        [
        	'id' => '1',
            'weekplan' => '4 week',
            'mealplan' => '2 meal',
            'price' => '8.50'
        ],
        [
        	'id' => '2',
        	'weekplan' => '4 week',
            'mealplan' => '3 meal',
            'price' => '8.25'
        ],
        [
        	'id' => '3',
        	'weekplan' => '4 week',
            'mealplan' => '4 meal',
            'price' => '8.00'
        ],
        [
        	'id' => '4',
        	'weekplan' => '8 week',
            'mealplan' => '2 meal',
            'price' => '8.25'
        ],
        [
        	'id' => '5',
        	'weekplan' => '8 week',
            'mealplan' => '3 meal',
            'price' => '8.00'
        ],
        [
        	'id' => '6',
        	'weekplan' => '8 week',
            'mealplan' => '4 meal',
            'price' => '7.75'
        ],
        [
        	'id' => '7',
        	'weekplan' => '12 week',
            'mealplan' => '2 meal',
            'price' => '8.00'
        ],
        [
        	'id' => '8',
        	'weekplan' => '12 week',
            'mealplan' => '3 meal',
            'price' => '7.75'
        ],
        [
        	'id' => '9',
        	'weekplan' => '12 week',
            'mealplan' => '4 meal',
            'price' => '7.50'
        ]
        ]);
    }
}
