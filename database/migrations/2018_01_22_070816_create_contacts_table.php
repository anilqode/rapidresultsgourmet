<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->string('email');
            $table->string('phone_number')->nullable();
            $table->string('subject');
            $table->text('message')->nullable();
            $table->boolean('checked')->default('0');
            $table->boolean('replied')->default('0');
            $table->boolean('priority')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
