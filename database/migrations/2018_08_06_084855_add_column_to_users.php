<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('gender');
            $table->string('addressline1');
            $table->string('addressline2');
            $table->string('addressline3')->nullable;
            $table->string('postcode')->nullable;
            $table->string('user-description')->nullable;
            $table->integer('terms')->nullable;


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('gender');
            $table->dropColumn('addressline1');
            $table->dropColumn('addressline2');
            $table->dropColumn('addressline3');
            $table->dropColumn('postcode');
            $table->dropColumn('user-description');
            $table->dropColumn('terms');
        });
    }
}
