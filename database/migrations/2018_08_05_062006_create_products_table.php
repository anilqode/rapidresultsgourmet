<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cat_id')->unsigned();
            $table->string('title');
            $table->integer('price');
            $table->integer('saleprice')->nullable();
            $table->integer('quantity')->nullable();
            $table->integer('instock');
            $table->integer('week_id')->unsigned();
            $table->integer('food_id')->unsigned();
            $table->integer('discount')->nullable();
            $table->string('imagePath');
            $table->string('description')->nullable();
            $table->string('meta_desc')->nullable();
            $table->string('meta_keyword')->nullable();
            $table->timestamps();

            $table->foreign('week_id')->references('id')
                ->on('weeks')
                ->onDelete('cascade');
            $table->foreign('food_id')->references('id')
                ->on('foods')
                ->onDelete('cascade');

            $table->foreign('cat_id')->references('id')
                ->on('productcategories')
                ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
