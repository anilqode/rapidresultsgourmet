<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enroll_id')->unsigned();
            $table->string('email');
            $table->string('phone_number');
            $table->string('address');
            $table->integer('user_id')->nullable();
            $table->integer('amount')->nullable();
            $table->string('description')->nullable();
            $table->string('session_indicator')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->foreign('enroll_id')
                ->references('id')
                ->on('fees')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrolls');
    }
}
