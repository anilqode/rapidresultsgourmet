<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomPlanUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customplanusers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('weekplans_id');
            $table->string('meal_name');
            $table->integer('meal_quantity');
            $table->string('vegcarb');
            $table->integer('plan_no');
            $table->integer('noOfDelivery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customplanusers');
    }
}
