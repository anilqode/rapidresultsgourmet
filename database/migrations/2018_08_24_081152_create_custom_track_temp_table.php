<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomTrackTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customtracktemp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('temp_user_id');
            $table->integer('weekplans_id');
            $table->string('meal_name');
            $table->integer('meal_quantity');
            $table->string('vegcarb');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customtracktemp');
    }
}
