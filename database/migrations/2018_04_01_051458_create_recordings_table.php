<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recordings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('dict_cat_id')->unsigned();
            $table->integer('set_id')->unsigned();
            $table->text('question');
            $table->string('audio');
            $table->text('text_answer');
            $table->string('audio_answer');

            $table->timestamps();
            $table->foreign('set_id')
                ->references('id')
                ->on('question_sets')
                ->onDelete('cascade');

            $table->foreign('dict_cat_id')
                ->references('id')
                ->on('dictionary_categories')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recordings');
    }
}
