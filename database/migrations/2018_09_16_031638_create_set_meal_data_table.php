<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetMealDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('setmealdata', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('plan_name');
            $table->string('vegan');
            $table->decimal('vegan_price',10,2);
            $table->string('complementary_snacks');
            $table->string('extra_snacks');
            $table->float('extra_price');
            $table->string('gourment_snacks');
            $table->float('gourment_price');
            $table->float('total_price');
            $table->integer('plan_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setmealdata');

    }
}

