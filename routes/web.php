<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Auth::routes();
Route::get('/blog', 'MainController@blog');
Route::post('/paypal-checkout', 'PaymentController@payWithPaypal');
Route::get('/thank-you', 'PaymentController@thankyou');
Route::get('/paypal-checkout-status', 'PaymentController@getPaymentStatus')->name('status');
Route::post('/loadOrderDetail', 'OrderController@loadOrderDetail');

Route::get('/clear-basket', 'ProductController@clearBasket');
Route::get('/guest-checkout', 'GuestCheckoutController@guestCheckout');

Route::get('/post/{slug}/show', 'PostController@display');
Route::get('/fitreats/{title}', 'ProductController@fitreatsDetail');
Route::get('/payGoCardless', 'PaymentController@goCardless')->name('payGoCardless');

Route::get('/success', 'PaymentController@goCardlessSuccess');
Route::get('/subscriptionMonthly', 'PaymentController@subscriptionMonthly');
Route::get('/subscription', 'PaymentController@goCardlessSubscription');

Route::get('/cancelSubscription', 'PaymentController@cancelCardPayment');
Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::resource('/create-breakingnews', 'BreakingNewsController');
    Route::get('/news/{id}/edit', 'BreakingNewsController@edit');
    Route::get('/display-breakingnews', 'BreakingNewsController@display');
    Route::get('/menu', 'MainController@menudisplay');
    Route::get('/show', 'MainController@showmenu');
    Route::get('/enrol-now/{id}', 'MainController@enrolnow');
    Route::get('/show-all-order', 'OrderController@displayAllOrders');
    Route::get('/show-all-custom-order', 'CustomOrderController@displayAllOrders');
    Route::resource('/create-user', 'ProfileController');
    Route::resource('/create-post', 'PostController');
    Route::get('/posts', 'PostController@show');
    Route::get('/post//dictionary-category/{id}/edit', 'PostController@edit');
    Route::get('/post/{id}/delete', 'PostController@delete');
    Route::resource('/create-page', 'PageController');
    Route::get('/pages', 'PageController@displayPages');
    Route::get('/pages/{slug}/show', 'PageController@display');
    Route::get('/pages/{id}/edit', 'PageController@edit');
    Route::get('/pages/{id}/delete', 'PageController@delete');
    Route::resource('/create-categories', 'CategoriesController');
    Route::get('/category/{id}/edit', 'CategoriesController@edit');
    Route::get('/category/{id}/delete', 'CategoriesController@delete');
    Route::resource('/add-slider', 'SliderController');
    Route::get('/display-slider', 'SliderController@displayslider');
    Route::get('/edit/{id}/slider', 'SliderController@edit');
    Route::get('/delete/{id}/slider', 'SliderController@delete');
    Route::resource('/add-fee', 'FeeController');
    Route::get('/display-fee', 'FeeController@displayfee');
    Route::get('/add-fee-description', 'FeeController@feeDescription');
    Route::post('/post-fee-description', 'FeeController@postDescription');
    Route::get('/display-description', 'FeeController@displayDescription');
    Route::get('/description/{fee_id}/edit', 'FeeController@editDescription');
    Route::post('/update-fee-description/{id}', 'FeeController@updateDescription');
    Route::get('/fee/{id}/edit', 'FeeController@edit');
    Route::get('/fee/{id}/delete', 'FeeController@delete');
    Route::resource('/add-recording-question-answer', 'RecordingController');
    Route::get('/recording-test', 'RecordingController@recordingtest');
    Route::post('/recording-test', 'RecordingController@recordingteststore');
    Route::resource('/add-testinomial', 'TestinomialController');
    Route::get('/display-testinomial', 'TestinomialController@show');
    Route::get('/edit/{id}/testinomial', 'TestinomialController@edit');
    Route::get('/delete/{id}/testinomial', 'TestinomialController@delete');
    Route::post('/contact-checked', 'ContactController@checked');
    Route::post('/contact-replied', 'ContactController@replied');
    Route::post('/contact-priority', 'ContactController@priority');
    Route::get('/contact-display', 'ContactController@displaycontact');
    Route::get('/mail-replied/{id}', 'ContactController@mailChecked');
    Route::get('/take-quiz', 'ClientQuestionAnswerController@takequiz');
    Route::post('/take-quiz', 'ClientQuestionAnswerController@takequizstore');
});


Route::get('/shopping-basket', 'ProductController@displayCheckout');
Route::post('/proceed-checkout', 'ProductController@addToCheckoutanil');
Route::get('/continue-shopping', 'ProductController@goToSetMealPlan');
Route::get('/proceed-checkout-excluding-paid-desert', 'ProductController@addToCheckoutwithoutpaiddessert');


Route::group(['middleware' => ['auth', 'user']], function () {
    Route::get('/add-testinomial', 'TestinomialController@index');
    Route::post('/add-testinomial', 'TestinomialController@store');

    Route::get('display-user-order/{user_id}', 'OrderController@displayUserOrders');

    Route::get('display-user-custom-order/{user_id}', 'CustomOrderController@displayUserOrders');

    Route::get('/dashboard', 'CustomPlanController@dataToDashboard')->name('dashboard');

    Route::get('/home', 'CustomPlanController@dataToDashboard');

    Route::get('/register-user', 'RegisterController@index');



    Route::get('/logout', 'Auth\LoginController@logout');
    Route::post('/user-question-set/store', 'ClientQuestionAnswerController@answerstore');
    Route::get('/user-question-set/{dict_cat_id}/{set_id}', 'ClientQuestionAnswerController@questionanswerset');
    Route::get('/userinfo', 'ProfileController@displayUser');
    Route::get('/display/{userId}/user-question-set/{dic_id}/{set_id}', 'ClientQuestionAnswerController@displayquestionanswerset');
    Route::get('/userinfo/{id}/edit', 'ProfileController@edit');
    Route::get('/userinfo/{id}/delete', 'ProfileController@delete');

    Route::get('/change-password', 'ProfileController@changePassword');
    Route::post('/update-password', 'ProfileController@updatePassword');
    Route::post('/update-user-info/{id}', 'ProfileController@update');
    // order history
    Route::get('/fitreatsOrderDetails', 'OrderController@displayFitreatsDetail');
    Route::get('/fitreatsOrderDetails/{id}', 'OrderController@displayFitreatUserOrders');




    // Route::get('/cancel', 'PaymentController@cancel');
    Route::get('/exportCSV', 'ProfileController@exportCSV');
});



/* Additional Work */
Route::get('/fitreats-and-supplements', 'FacilityController@fitreatsAndSupplements');
Route::post('/cart/{id}', 'CartController@store');
Route::get('/cart', 'CartController@index');
Route::get('/cart/delete/{rowId}/{id}', 'CartController@destroy');
Route::post('/cart/update/{rowId}/{id}', 'CartController@update');
Route::post('/clearCart', 'CartController@clearCart');
Route::get('/checkout', 'CheckoutController@index')->middleware('auth');
Route::post('/paypal-checkout-fitreats', 'FitreatsPayment@payWithPaypal');
Route::get('/paypal-checkout-status-fitreats', 'FitreatsPayment@getPaymentStatus')->name('statusFitreats');


// new routes for food begins here

Route::get('/', 'MainController@index');



Route::get('/contact', 'ContactController@index');
Route::post('/contact-store', 'ContactController@storing');

Route::post('/enrol-now/store', 'MainController@enrolnowstore');
Route::get('/user-register', 'ProfileController@userregister');
Route::post('/user-register', 'ProfileController@userregisterstore');
Route::post('/user-register-checkout', 'ProfileController@userRegisterCheckout');


Route::get('/about_us', 'HomeController@aboutRedirect');
Route::get('/about-us', 'HomeController@about');
// Route::get('/affiliates','HomeController@affiliates');
Route::get('/courses', 'HomeController@courses');
Route::get('/fee', 'HomeController@fee');
Route::get('/testimonial', 'HomeController@testinomial');
Route::post('/proceed-payment', 'FeeController@proceedpayment');


Route::group(['middleware' => 'auth'], function () {

    Route::get('/redirect_to_register_for_auth', 'ProductController@redirectToRegister');

    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
    // list all lfm routes here...
});

Route::get('/checkout/{id}', 'MainController@checkout');
Route::get('/fetch', function () {
    return view('checkout.fetch');
});
Route::get('/checkoutstore', 'MainController@checkoutindex');
Route::get('/verify', 'MainController@checkoutupdate');
Route::get('/invoice', 'FeeController@invoice');

Route::get('/invoice', 'FeeController@invoice');

Route::get('/shop', [
    'uses' => 'ProductController@getproductindex',
    'as' => 'products.index'
]);
Route::get('/create-product-categories', [
    'uses' => 'ProductcategoryController@index',
    'as' => 'productcategory.index'
]);

Route::post('/store-product-categories', [
    'uses' => 'ProductcategoryController@store',
    'as' => 'productcategory.store'
]);
Route::get('/product-category/{id}/edit', [
    'uses' => 'ProductcategoryController@edit',
    'as' => 'productcategory.edit'
]);
Route::get('/product-category/{id}/delete', [
    'uses' => 'ProductcategoryController@delete',
    'as' => 'productcategory.delete'
]);
Route::patch('/product-category/{id}/update', [
    'uses' => 'ProductcategoryController@update',
    'as' => 'productcategory.update'
]);
Route::get('/create-product', [
    'uses' => 'ProductController@index',
    'as' => 'product.index'
]);
Route::post('/store-product', [
    'uses' => 'ProductController@store',
    'as' => 'product.store'
]);
Route::get('/show-product-report', 'ProductController@show');

Route::get('/edit/{id}/product/{cat_id}', 'ProductController@edit');
Route::patch('/update-product/{id}', 'ProductController@update');
Route::get('/delete-product/{id}', 'ProductController@delete');
Route::get('/shop', 'ProductController@shop');
Route::get('/delete-cart/{id}', 'ProductController@deleteCart');
Route::get('/weight-plan/{id}', 'ProductController@weightPlan');

Route::get('/weekplan/{cat_id}/{week_id}', 'ProductController@weekPlan');

Route::get('/add_vegan_to_checkout/{id}', 'ProductController@getAddVeganToCheckout');

Route::get('add_review_order_to_checkout/{id}', 'ProductController@getReviewOrder');
Route::post('/review-order-store', 'ProductController@StoreReviewOrder');
Route::get('/review-order-store', 'ProductController@getStoreReviewOrder');
Route::get('/add-snacks-to-checkout/{id}', 'ProductController@addSnacksToCheckout');
Route::get('/add_free_snacks_to_checkout', 'ProductController@getAddFreeSnacksToCheckout');
Route::get('/add_extra_snacks_to_checkout', 'ProductController@getAddExtraSnacksToCheckout');
Route::post('/add_extra_snacks_to_checkout', 'ProductController@addExtraSnacksToCheckout');
Route::get('/redirect_to_register', 'ProductController@redirectToRegister');
// Route::post('/delivery_info', 'ProductController@storeDeliveryInfo');


Route::post('/add_to_checkout', 'ProductController@addToCheckoutanil');
Route::get('/set-meal-plan', 'FacilityController@setMealPlan');
Route::get('/meals', 'FacilityController@showmeals');
Route::get('/deleteSetMeal/{id}', 'ProductController@deleteSetMeal');


// paypal routes
Route::get('/add_to_checkout', 'ProductController@getAddToCheckout')->name('add_to_checkout');

Route::post('/custom-paypal-checkout', 'CustomPaymentController@payWithPaypal');

Route::get('/custom-paypal-checkout-status', 'CustomPaymentController@getPaymentStatus')->name('customstatus');
// ends paypal routes


Route::post('/add-to-cart/{id}', [
    'uses' => 'ProductController@getAddToCart',
    'as' => 'product.addToCart'
]);
Route::get('/shopping-cart', [
    'uses' => 'ProductController@getCart',
    'as' => 'product.shoppingcart'
]);

Route::get('/tinymce_example', function () {
    return view('mceImageUpload::example');
});
Route::get('/tiny', function () {
    return view('test');
});



Route::get('register/verify/{api_token}/{email}', [
    'as' => 'confirmation_path',
    'uses' => 'ProfileController@confirm'
]);


//for naati dictionary

/*
Auth::routes();

Route::get('/', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');*/

Route::get('/terms-and-condition', function () {
    return view('T&C');
});

Route::get('/privacy-policy', function () {
    return view('privacy-policy');
});

Route::get('/faqs', function () {
    return view('faqs');
});

// Route::get('/custom-meal-plans', function(){
//     return view('customMealPlans');
// });

// Route::get('/custom-meal-plans', 'CustomPlanController@customIndex');
// Route::get('/custom-meal-plans/{weekplanid}/{id}/{userId}', 'CustomPlanController@storeCustomPlan');
Route::get('/main-meal-page/{weekplanid}/{id}/{temp_user_id}', 'CustomPlanController@getMainMealPage');
Route::post('/add_to_basket', 'CustomPlanController@addToBasket');
Route::post('/update_basket_data/{temp_id}/{meal}/{vegcarb}/{sn}', 'CustomPlanController@updateBasketData');
Route::get('/read-data/{temp}', 'CustomPlanController@readData');
Route::get('/redirect-to-register/{id}', 'CustomPlanController@redirectToRegister');
Route::get('/redirect-from-custom/{week_id}/{id}', 'CustomPlanController@redirectFromCustom');
Route::get('/redirect-from-custom-to-register', 'CustomPlanController@redirectFromCustomToRegister');
// Route::get('/custom-edit/{userId}/{planNo}', 'CustomPlanController@customEdit');
// Route::get('/custom-edit-email/{order_id}/{plan_no}', 'CustomPlanController@customEditEmail');
// Route::post('/custom-delete/{userId}/{planNo}', 'CustomPlanController@customDelete');
Route::get('/set-delete/{id}', 'CustomPlanController@setDelete');
Route::post('/update_edit_basket_data/{user_id}/{planNo}/{meal}/{vegcarb}/{sn}', 'CustomPlanController@updateEditBasketData');
Route::get('/read-edit-data/{userId}/{planNo}', 'CustomPlanController@readEditData');
Route::post('/add_to_edit_basket', 'CustomPlanController@addToEditBasket');


Route::get('/menu/{id}', 'MenuUploaderController@pdf');



Route::group(['middleware' => ['auth', 'admin']], function () {

    Route::get('/show-custom-plan', 'AdminCustomController@index');
    Route::get('/create-custom-plan', 'AdminCustomController@create');
    Route::post('/store-custom-plan', 'AdminCustomController@store');
    Route::get('/show-custom-plan/{id}', 'AdminCustomController@show');
    Route::get('/edit-custom-plan/{id}', 'AdminCustomController@edit');
    Route::post('/update-custom-plan/{id}', 'AdminCustomController@update');
    Route::post('/delete-custom-plan/{id}', 'AdminCustomController@delete');

    Route::get('/show-week-plan', 'WeekPlanController@index');
    Route::get('/create-week-plan', 'WeekPlanController@create');
    Route::post('/store-week-plan', 'WeekPlanController@store');
    Route::get('/show-week-plan/{id}', 'WeekPlanController@show');
    Route::get('/edit-week-plan/{id}', 'WeekPlanController@edit');
    Route::post('/update-week-plan/{id}', 'WeekPlanController@update');
    Route::post('/delete-week-plan/{id}', 'WeekPlanController@delete');

    Route::get('/show-meal-plan', 'MealPlanController@index');
    Route::get('/create-meal-plan', 'MealPlanController@create');
    Route::post('/store-meal-plan', 'MealPlanController@store');
    Route::get('/show-meal-plan/{id}', 'MealPlanController@show');
    Route::get('/edit-meal-plan/{id}', 'MealPlanController@edit');
    Route::post('/update-meal-plan/{id}', 'MealPlanController@update');
    Route::post('/delete-meal-plan/{id}', 'MealPlanController@delete');

    Route::resource('menu_uploader', 'MenuUploaderController');

    Route::resource('coupon', 'CouponController');
});
Route::post('/applyCoupon', 'CouponController@applyCoupon');

Route::get('/{seite}', 'PageController@errorPage')
    ->where('seite', '^([0-9A-Za-z\-]+)?');
