<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//test for demo
use  App\Http\Kernel;
 Route::get('/test-nav', function () {
    return view('test');
});
 Route::get('/blog', 'MainController@blog');
  Route::get('/post/{slug}/show', 'PostController@display');
Route::group(['middleware' => ['auth', 'admin']], function() {
 Route::resource('/create-breakingnews','BreakingNewsController');
 Route::get('/news/{id}/edit','BreakingNewsController@edit');
 Route::get('/display-breakingnews','BreakingNewsController@display');
 Route::get('/menu','MainController@menudisplay');
 Route::get('/show','MainController@showmenu');
 Route::get('/enrol-now/{id}','MainController@enrolnow');
 Route::get('/show-all-order','OrderController@displayAllOrders');


 Route::resource('/create-user','ProfileController');
 Route::resource('/create-post','PostController');
 Route::get('/posts', 'PostController@show');

 Route::get('/post//dictionary-category/{id}/edit', 'PostController@edit');
 Route::get('/post/{id}/delete', 'PostController@delete');
 Route::resource('/create-page','PageController');
 Route::get('/pages', 'PageController@displayPages');
 Route::get('/pages/{slug}/show', 'PageController@display');
 Route::get('/pages/{id}/edit', 'PageController@edit');
 Route::get('/pages/{id}/delete', 'PageController@delete');
 Route::resource('/create-categories','CategoriesController');
 Route::get('/category/{id}/edit', 'CategoriesController@edit');
 Route::get('/category/{id}/delete', 'CategoriesController@delete');
 



 Route::resource('/add-slider','SliderController');
 Route::get('/display-slider','SliderController@displayslider');
 Route::get('/edit/{id}/slider','SliderController@edit');
 Route::get('/delete/{id}/slider','SliderController@delete');
 Route::resource('/add-fee','FeeController');
 Route::get('/display-fee','FeeController@displayfee');
 Route::get('/add-fee-description','FeeController@feeDescription');
 Route::post('/post-fee-description','FeeController@postDescription');
 Route::get('/display-description','FeeController@displayDescription');
 Route::get('/description/{fee_id}/edit','FeeController@editDescription');
 Route::post('/update-fee-description/{id}','FeeController@updateDescription');

 Route::get('/fee/{id}/edit','FeeController@edit');
 Route::get('/fee/{id}/delete','FeeController@delete');
 Route::resource('/add-recording-question-answer','RecordingController');
 Route::get('/recording-test','RecordingController@recordingtest');
 Route::post('/recording-test','RecordingController@recordingteststore');
 Route::resource('/add-testinomial', 'TestinomialController');
 Route::get('/display-testinomial', 'TestinomialController@show');
 Route::get('/edit/{id}/testinomial', 'TestinomialController@edit');
 Route::get('/delete/{id}/testinomial', 'TestinomialController@delete');
 Route::post('/contact-checked','ContactController@checked');
 Route::post('/contact-replied','ContactController@replied');
 Route::post('/contact-priority','ContactController@priority');
 Route::get('/contact-display','ContactController@displaycontact');
 Route::get('/mail-replied/{id}','ContactController@mailChecked');
 Route::get('/take-quiz','ClientQuestionAnswerController@takequiz');
 Route::post('/take-quiz','ClientQuestionAnswerController@takequizstore');


});

Route::get('/proceed-checkout','ProductController@addToCheckoutanil');
Route::group(['middleware' => ['auth', 'user']], function() {
    Route::get('/add-testinomial', 'TestinomialController@index');
    Route::post('/add-testinomial/', 'TestinomialController@store');
Route::get('display-user-order/{user_id}','OrderController@displayUserOrders');
    Route::get('/dashboard', 'CustomPlanController@dataToDashboard');

    Route::get('/home', 'CustomPlanController@dataToDashboard');

    Route::get('/register-user', 'RegisterController@index');
    Route::get('logout', 'Auth\LoginController@logout');
    Route::post('/user-question-set/store','ClientQuestionAnswerController@answerstore');
    Route::get('/user-question-set/{dict_cat_id}/{set_id}','ClientQuestionAnswerController@questionanswerset');
    Route::get('/userinfo', 'ProfileController@displayUser');
    Route::get('/display/{userId}/user-question-set/{dic_id}/{set_id}','ClientQuestionAnswerController@displayquestionanswerset');
    Route::get('/userinfo/{id}/edit', 'ProfileController@edit');
    Route::get('/userinfo/{id}/delete', 'ProfileController@delete');

    Route::get('/change-password', 'ProfileController@changePassword');
    Route::post('/update-password','ProfileController@updatePassword');
    Route::post('/update-user-info/{id}','ProfileController@update');


});


// new routes for food begins here

Route::get('/', 'MainController@index');



Route::get('/contact','ContactController@index');
Route::post('/contact-store','ContactController@storing');

Route::post('/enrol-now/store','MainController@enrolnowstore');
Route::get('/user-register','ProfileController@userregister');
Route::post('/user-register','ProfileController@userregisterstore');
Route::get('/about_us','HomeController@about');
Route::get('/affiliates','PartnerController@show');
Route::get('/courses','HomeController@courses');
Route::get('/fee','HomeController@fee');
Route::get('/testimonial','HomeController@testinomial');
Route::post('/proceed-payment','FeeController@proceedpayment');

Route::group(['middleware' => 'auth'], function () {

Route::get('/redirect_to_register_for_auth', 'ProductController@redirectToRegister');

    Route::get('/laravel-filemanager', '\Unisharp\Laravelfilemanager\controllers\LfmController@show');
    Route::post('/laravel-filemanager/upload', '\Unisharp\Laravelfilemanager\controllers\UploadController@upload');
    // list all lfm routes here...
});




Route::get('/checkout/{id}','MainController@checkout');


Route::get('/fetch', function () {
    return view('checkout.fetch');
});

Route::get('/checkoutstore', 'MainController@checkoutindex');

Route::get('/verify', 'MainController@checkoutupdate');

Route::get('/invoice', 'FeeController@invoice');

Route::get('/invoice', 'FeeController@invoice');

Route::get('/shop',[
 'uses'=>'ProductController@getproductindex',
 'as'=>'products.index'
 ]);
Route::get('/create-product-categories',[
    'uses'=>'ProductcategoryController@index',
    'as'=>'productcategory.index'
    ]);

Route::post('/store-product-categories',[
    'uses'=>'ProductcategoryController@store',
    'as'=>'productcategory.store'
    ]);
Route::get('/product-category/{id}/edit',[
    'uses'=>'ProductcategoryController@edit',
    'as'=>'productcategory.edit'
    ]);
Route::get('/product-category/{id}/delete',[
    'uses'=>'ProductcategoryController@delete',
    'as'=>'productcategory.delete'
    ]);


Route::patch('/product-category/{id}/update',[
    'uses'=>'ProductcategoryController@update',
    'as'=>'productcategory.update'
    ]);

Route::get('/create-product',[
    'uses'=>'ProductController@index',
    'as'=>'product.index'
    ]);
Route::post('/store-product',[
    'uses'=>'ProductController@store',
    'as'=>'product.store'
    ]);

Route::get('/show-product-report/','ProductController@show');
Route::get('/edit/{id}/product/{cat_id}','ProductController@edit');
Route::patch('/update-product/{id}','ProductController@update');
Route::get('/delete-product/{id}','ProductController@delete');
Route::get('/shop','ProductController@shop');
Route::get('/delete-cart/{id}','ProductController@deleteCart');
Route::get('/weight-plan/{id}','ProductController@weightPlan');

Route::get('/weekplan/{cat_id}/{week_id}','ProductController@weekPlan');

Route::get('/add_vegan_to_checkout/{id}','ProductController@getAddVeganToCheckout');

Route::get('add_review_order_to_checkout/{id}','ProductController@getReviewOrder');
Route::post('/review-order-store','ProductController@StoreReviewOrder');
Route::get('/review-order-store','ProductController@getStoreReviewOrder');

Route::get('/add_free_snacks_to_checkout','ProductController@getAddFreeSnacksToCheckout');

Route::get('/add_extra_snacks_to_checkout','ProductController@getAddExtraSnacksToCheckout');
Route::post('/add_extra_snacks_to_checkout/','ProductController@addExtraSnacksToCheckout');


Route::get('/redirect_to_register', 'ProductController@redirectToRegister');
// Route::post('/delivery_info', 'ProductController@storeDeliveryInfo');


Route::post('/add_to_checkout','ProductController@addToCheckoutanil');

Route::get('/set-meal-plan','FacilityController@setMealPlan');
Route::get('/meals','FacilityController@showmeals');

// paypal routes
Route::get('/add_to_checkout','ProductController@getAddToCheckout')->name('add_to_checkout');
Route::post('/paypal-checkout','PaymentController@payWithPaypal');
Route::get('/paypal-checkout-status','PaymentController@getPaymentStatus')->name('status');
// ends paypal routes


Route::post('/add-to-cart/{id}',[
    'uses'=>'ProductController@getAddToCart',
    'as'=>'product.addToCart'
    ]);
Route::get('/shopping-cart',[
    'uses'=>'ProductController@getCart',
    'as'=>'product.shoppingcart'
    ]);
Auth::routes();





Route::get('/tinymce_example', function () {
    return view('mceImageUpload::example');
});
Route::get('/tiny', function () {
    return view('test');
});



Route::get('register/verify/{api_token}/{email}', [
    'as' => 'confirmation_path',
    'uses' => 'ProfileController@confirm'
    ]);


//for naati dictionary







/*
Auth::routes();

Route::get('/', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');*/

Route::get('/T&C', function(){
    return view('T&C');
});

Route::get('/privacy-policy', function(){
    return view('privacy-policy');
});

Route::get('/faqs', function(){
    return view('faqs');
});

// Route::get('/custom-meal-plans', function(){
//     return view('customMealPlans');
// });

Route::get('/custom-meal-plans', 'CustomPlanController@customIndex');
Route::get('/custom-meal-plans/{weekplanid}/{id}', 'CustomPlanController@storeCustomPlan');
Route::get('/main-meal-page/{weekplanid}/{id}/{temp_user_id}', 'CustomPlanController@getMainMealPage');
Route::post('/add_to_basket', 'CustomPlanController@addToBasket');
Route::post('/update_basket_data/{temp_id}/{meal}/{sn}', 'CustomPlanController@updateBasketData');
Route::get('/read-data/{temp}', 'CustomPlanController@readData');
Route::get('/redirect-to-register/{id}', 'CustomPlanController@redirectToRegister');
Route::get('/redirect-from-custom/{week_id}/{id}', 'CustomPlanController@redirectFromCustom');
Route::get('/redirect-from-custom-to-register', 'CustomPlanController@redirectFromCustomToRegister');
Route::get('/custom-edit/{userId}/{planNo}', 'CustomPlanController@customEdit');
Route::post('/custom-delete/{userId}/{planNo}', 'CustomPlanController@customDelete');
Route::get('/set-delete/{id}', 'CustomPlanController@setDelete');
Route::post('/update_edit_basket_data/{user_id}/{planNo}/{meal}/{sn}', 'CustomPlanController@updateEditBasketData');
Route::get('/read-edit-data/{userId}/{planNo}', 'CustomPlanController@readEditData');
Route::post('/add_to_edit_basket', 'CustomPlanController@addToEditBasket');



Route::get('/partners', 'PartnerController@index');
Route::get('/partners/create', 'PartnerController@create');
Route::post('/partners/create', 'PartnerController@store');
Route::get('/partners/edit/{id}', 'PartnerController@edit');
Route::post('/partners/edit/{id}', 'PartnerController@update');
Route::get('/partners/delete/{id}', 'PartnerController@destroy');
Route::get('/partners/show', 'PartnerController@show');
