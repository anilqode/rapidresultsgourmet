@extends('layouts.app')

@section('content')
  <section class="xyz">
    <div class="intro-custom">
      <p class="textcenter">Welcome to your Custom Meal Plans!</p>
           <p class="textcenter">
        Here you can customise your meals exactly how you would like them prepared and cooked - down to the portion-size and level of spiciness! </p>

        <p class="textcenter">To get started, simply create your RRG account and select how many meals a day you want below, and for how long you would like your meal plan to last. If you have to go on holiday, you can pause and restart your deliveries anytime. </p>

        <p class="textcenter">Follow each prompt of selecting your meals and each of the sides. Once you’ve selected the maximum amount of meals for the meal plan you’ve chosen, you can then review your basket and make any changes you need to. </p>

        <p class="textcenter">Give us any other requests, allergies and preferences and we’ll make sure your meals arrive to your door exactly how you want them. Don’t forget to let us know any specific delivery instructions so we can ensure we know where or who to drop your meals to! Easy as that <i class="fa fa-smile"></i></p>

        <p class="textcenter">Any questions? We’re only a call or message away and we will get back to you ASAP. </p>

        <p class="textcenter">Happy Ordering!</p>
    </div>
    </section>
  
@foreach($mealPlan as $key=> $plan)

  <section class="eight_week">
  <h3>
  {{$plan['week']}} payment plan ({{$plan['days']}}  days)
   </h3>
   <div class="container">
    <div class="row">
     <div class="col-sm-4">
      <div class="content_wrap_day">
        <h5>
        {{$plan['1']}}
        </h5>
        <a href="/redirect-from-custom/{{$plan['week_id']}}/1"><div class="product_content">
        
       </div></a>
      </div>
     </div>
     <div class="col-sm-4">
      <div class="content_wrap_day">
        <h5>
        {{$plan['2']}}
        </h5>
       <a href="/redirect-from-custom/{{$plan['week_id']}}/2"><div class="product_content">
       
       </div></a>
      </div>
     </div>
     <div class="col-sm-4">
      <div class="content_wrap_day">
        <h5>
        {{$plan['3']}}
        </h5>
       <a href="/redirect-from-custom/{{$plan['week_id']}}/3"><div class="product_content">
        
       </div></a>
      </div>
     </div>
    </div>
   </div>
  </section>
@endforeach

  
@endsection
