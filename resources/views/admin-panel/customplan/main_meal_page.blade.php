@extends('layouts.app')

@section('content')

<script>
// Get the modal
// var modal = document.getElementById('popupModal');
// var modal1 = document.getElementById('popupModalVegan');

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//     if (event.target == modal) {
//         modal.style.display = "none";
//         model1.style.display = "none";
//     }
// }

function showModal(i, j){
	document.getElementById('body_modal2').style.display = "block";
	document.getElementById('title_vegCarb').style.display = "block";
	document.getElementById('popupModal').style.display='block';
	document.getElementById('modal_item').value = i;

	if(j == "Vegan"){
		document.getElementById('body_modal2').style.display = "none";
		document.getElementById('title_vegCarb').style.display = "none";
		document.getElementById("add_to_basket_button").disabled = false;
	}
}

function cancelModal(){
	document.getElementById('popupModal').style.display='none';
	document.getElementById('count_quantity').value = 0;
}

//disable checkbox 
function onlyTwoCheckBox() {
	
    var checkboxgroup = document.getElementById('checkboxes').getElementsByTagName("input");
    var limit = 2;
    for (var i = 0; i < checkboxgroup.length; i++) {
        checkboxgroup[i].onclick = function() {
            var checkedcount = 0;
            document.getElementById("add_to_basket_button").disabled = true;
            for (var i = 0; i < checkboxgroup.length; i++) {
                checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
            }

            if (checkedcount > limit) {
            	document.getElementById("add_to_basket_button").disabled = false;
                alert("You can only select a maximum of " + limit + " checkbox.");
                this.checked = false;
            } else if (checkedcount == limit) {
            	document.getElementById("add_to_basket_button").disabled = false;
            }
        }
    }
}

//smooth scroll and on click 

$(document).ready(function(){
	 $(document).on("scroll", onScroll);

  $(".navbar a").on('click', function(event) {
	if (this.hash !== "") {
	  event.preventDefault();
	  var hash = this.hash;

	  $('html, body').animate({
		scrollTop: $(hash).offset().top
	  }, 500, function(){

	  window.location.hash = hash;
	  });
	} 
  });
});

function onScroll(event){
	var scrollPos = $(document).scrollTop()+120;
	$('.meat_fish a').each(function () {
    	var currLink = $(this);
    	var refElement = $(currLink.attr("href"));
    	if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
        $('.meat_fish ul li a').removeClass("custom_active");
        currLink.addClass("custom_active");
    	}
    	else{
        	currLink.removeClass("custom_active");
    	}
});
}

//stick to the top
$(window).scroll(function(){
$(".navbar-fixed-chicken, .basket_table").css("top",Math.max(0,108-$(this).scrollTop()));
 });

//increase and decrease function
// quantity = document.getElementById('count_quantity').value;


function decreaseCount(event, inputId){
	if(event == 'quantity'){
		var check = document.getElementById('count_quantity').value;
		if(check > 0){
			document.getElementById('count_quantity').value = parseInt(check) - 1;
		}
		else
			alert("Value is One, check out!!!");
	} else if(event == 'edit') {
		var trackInputId = 'count_edit'+inputId;
		var check = document.getElementById(trackInputId).value;
		if(check > 0){
			document.getElementById(trackInputId).value = parseInt(check) - 1;
			var checkForDelete = document.getElementById(trackInputId).value;
			if(checkForDelete == 0) {
				var deleteElement = 'forDelete'+inputId;
				document.getElementById(deleteElement).style.display = "none";
			}
		}
		else {
			alert("Value is One, check out!!!");
		}
	}
}


function increaseCount(event, inputId){ 	
	// var values = $("input[name='count_edit[]']")
 //              .map(function(){return $(this).val();}).get();
 	var weekplan = document.getElementById('weekplanid').value;
 	var meal_id = document.getElementById('meal_id').value;
 	var trackMealPlan; 
 	if(meal_id == 1 ){
 		trackMealPlan = 6;
 	} else if(meal_id == 2){
 		trackMealPlan = 9;
 	} else if(meal_id == 3){
 		trackMealPlan = 12;
 	}

	var ra = document.getElementById('temp_user_id').value;
	var url = '/read-data/'+ra;
	var limitingValue;
	 $.ajax({
            type: "GET",
            url: url,
            async: false,
            success: function( data ) {
                limitingValue = data;
            }
        });
	
		if(event == 'quantity'){
			var check = document.getElementById('count_quantity').value;
			var limitValue = trackMealPlan - limitingValue; //document.getElementById('quantiyInBasket').value;
			if(check < limitValue){
				document.getElementById('count_quantity').value = parseInt(check) + 1;
			}
			else
				alert("Reached maximum quantity");
		} else if(event == 'edit') {
			var trackInputId = 'count_edit'+inputId;
			var check = document.getElementById(trackInputId).value;
			var limitValue = trackMealPlan - limitingValue + parseInt(check); //document.getElementById('quantiyInBasket').value;
			if(check < limitValue){
				document.getElementById(trackInputId).value = parseInt(check) + 1;
			}
			else
				alert("Reached maximum quantity");
		}
}

// function vegcarbClick(idName){
// 		var id = '#v'+idName;
// 		if ($(id).prop('checked')) 
// 	 {
// 	   $(id).prop('checked', false);
// 	 }
// 	else 
// 	 {
// 	  $(id).prop('checked', true);
// 	 }
// }
	
</script>
<script type="text/javascript">

     $(document).ready(function() {
       $("#count_edit_btn1, #count_edit_btn2").click(function() {   //button id
          var loginForm = $(this).closest('form');  //form id
          loginForm.submit(function(e){
              e.preventDefault();

			    $.ajax({
			       type: 'post',
			       url: $(this).attr('action'),
			       data : $(this).serialize()
			   });
			});
      });

	if ($(window).width() < 600){
		$(".heading" ).click(function() {
   			$('.basket_table').toggleClass("top_410 top_144");
			// $(".mobile_table").toggle();
			// $(".mobile_head").toggle();
			// $(".mobile_foot").toggle();
		});
	}	


       $("#tableSubmit").click(function(e) {

			var weekplan = document.getElementById('weekplanid').value;
		 	var meal_id = document.getElementById('meal_id').value;
		 	var trackMealPlan; 
		 	if(meal_id == 1 ){
		 		trackMealPlan = 6;
		 	} else if(meal_id == 2){
		 		trackMealPlan = 9;
		 	} else if(meal_id == 3){
		 		trackMealPlan = 12;
		 	}

		 	var ra = document.getElementById('temp_user_id').value;
			var url = '/read-data/'+ra;
			var limitingValue;
			 $.ajax({
		            type: "GET",
		            url: url,
		            async: false,
			            success: function( data ) {
			                limitingValue = data;
			            }
			        });

			var popMsg = 'Select '+trackMealPlan+' items';

			if(limitingValue == trackMealPlan) {
		        return true;
		    } 
		    else {
		    	alert(popMsg);
		    	return false;
		    }
		});
   });
</script>

<div id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" >
	<article class="meat_fish">
		<section class="container">
			<nav class="navbar navbar-default navbar-fixed-chicken">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>

					<div class="collapse navbar-collapse" id="myNavigation">
						<ul class="nav navbar-nav navbar-left">
							@foreach($categoryData as $categoryDatum)
							@foreach(explode(' ', $categoryDatum->name) as $categoryDatums)
							@if (explode(' ', $categoryDatum->name)[0] == 'Custom')
							<li><a href="#{{ $categoryDatum->id }}">{{ preg_replace("/^(\w+\s)/", "", $categoryDatum->name) }}</a></li>
							@break
							@endif
							@endforeach
							@endforeach
						</ul>
					</div>
				</div>
			</nav>
		</section>
	
	@foreach($categoryData as $categoryDatum)
		@foreach(explode(' ', $categoryDatum->name) as $categoryDatums)
		@if (explode(' ', $categoryDatum->name)[0] == 'Custom')
		@php
		    $products = \App\Product::where([
		    				['cat_id', $categoryDatum->id],
		    				['meal_id', $meal_id]
		    				])->get();
		@endphp  
		<section id="{{ $categoryDatum->id }}" class="chicken">
			<div class="container">
				<h2>
					{{ preg_replace("/^(\w+\s)/", "", $categoryDatum->name) }}
				</h2>
				@foreach($products as $product)
				@if($product->week_id == $weekplanid)
				@php 
				  $jack = preg_replace("/^(\w+\s)/", "", $categoryDatum->name)
				@endphp
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('{{$product->title}}', '{{$jack}}')" class="content_wrap generate_Popup" >
							<img src = "/{{$product->imagePath}}" width="172px"/>
							<div class="product_content">
								<h5>
									{{$product->title}}<br>
								</h5>
								<em>{!! $product->description !!}</em>
							</div>
						</div>
					</div>
				</div>
				@endif
				@endforeach
			</div>
		</section>
		@break;
		@endif
		@endforeach
	@endforeach
		<!-- <section id="chicken">
			<div class="container">
				<h2>
					Chicken
				</h2>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Cajun Chicken')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Cajun Chicken
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Jerk Chicken')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Jerk Chicken.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Jerk Chicken
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Thai Sweet Chill Chicken')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Thai Sweet Chilli Chicken.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Thai Sweet Chilli Chicken
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
		<!-- <section id="2" class="chicken">
			<div class="container">
				<h2>
					Turkey
				</h2>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Cajun Turkey')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Cajun Turkey
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Siracha Hot Turkey')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Siracha Hot Turkey
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Thai Sweet Turkey')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Thai Sweet Turkey
								</h5>
								<em>Description</em>

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="3" class="chicken">
			<div class="container">
				<h2>
					Beef
				</h2>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Mexican Beef')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Mexican Beef
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Peppercorn Beef')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Peppercorn Beef.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Peppercorn Beef
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Cajun Beef')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Cajun Beef.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Cajun Beef
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="4" class="chicken">
			<div class="container">
				<h2>
					Salmon
				</h2>
				<div class="row">
					<div class="col-sm-8" >
						<div onclick="showModal('Garlic Butter Salmon')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Garlic Butter Salmon.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Garlic Butter Salmon
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8" >
						<div onclick="showModal('Aromatic Salmon')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Aromatic Salmon.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Aromatic Salmon
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8" >
						<div onclick="showModal('Lemon Herb Salmon')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Lemon Herb Salmon
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="5" class="chicken">
			<div class="container">
				<h2>
					Sea Bass
				</h2>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Garlic Butter Sea Bass')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Garlic Butter Sea Bass
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Aromatic Sea Bass')" class="content_wrap generate_Popup" >
							<img src = "/images/rrgimages/Aromatic Sea Bass.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Aromatic Sea Bass
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Lemon and Herb Sea Bass')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Lemon and Herb Sea Bass
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section id="6" class="chicken">
			<div class="container">
				<h2>
					Vegan
				</h2>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Asian Veggie Protein Noodle')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Asian Veggie Protein Noodle
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Buddha Bowl')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Buddha Bowl
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Loaded Sweet Potato')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Loaded Sweet Potato
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('The Easy Pea-Sy')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									The Easy Pea-Sy
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div onclick="showModal('Smashed Chickpea Avo Wrap')" class="content_wrap generate_Popup" >
							<img src = "https://stmed.net/sites/default/files/meal-wallpapers-28271-6149306.jpg" class="product_image"/>
							<div class="product_content">
								<h5>
									Smashed Chickpea Avo Wrap
								</h5>
								<em>Description</em>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
	</article>
	
	<article class="modal" id="popupModal">
		<section class="modelcontent_wrap">
			<form action="/add_to_basket" method="POST"> 
			{!! csrf_field() !!}
			<input type="text" class="count_edit" style="font-size: 50px; width: 100%; text-align: center;" name="food_item" id="modal_item" readonly>
			<input type="hidden" name="weekplanid" id="weekplanid" value="{{ $weekplanid }}">
			<input type="hidden" name="meal_id" id="meal_id" value="{{ $meal_id }}">

			<input type="hidden" name="temp_user_id" id="temp_user_id" value="{{ $temp_user_id }}">
			<input type="hidden" id="quantiyInBasket" value="{{ Session::get('count_quantity') }}">
			@php
				session()->forget('count_quantity');
			@endphp
			<div class="header_modal">
				<h5 class="title_vegCarb" id="title_vegCarb">
					Select two Veg and Carb
					<!-- <span class="close_popup">&times;</span> -->
					<span onclick="cancelModal();" class="close_popup">&times;</span>
				</h5>
			</div>
			<div class="body_modal2" id="body_modal2">
				<ul class="veg_varb_list2" id="checkboxes">
					@foreach($vegCarbProducts as $vegCarbProduct)
					<li>
						<label>
							{{$vegCarbProduct->title}}
						</label>
						<input type="checkbox" value="{{$vegCarbProduct->title}}" readonly  name="select_veg_and_carbs[]" />
					</li>
					@endforeach
					<!-- <li>
						<label id="product_title_8">
							Quinoa
						</label>
						<input type="checkbox" value="Quinoa" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Sweet Potato
						</label>
						<input type="checkbox" value="Sweet Potato" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Brown Rice
						</label>
						<input type="checkbox" value="Brown Rice" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Lentils
						</label>
						<input type="checkbox" value="Lentils" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Asparagus
						</label>
						<input type="checkbox" value="Asparagus" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Broccoli
						</label>
						<input type="checkbox" value="Broccoli" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Green Beans
						</label>
						<input type="checkbox" value="Green Beans" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Kale
						</label>
						<input type="checkbox" value="Kale" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li>
					<li>
						
						<label id="product_title_8">
							Spinach
						</label>
						<input type="checkbox" value="Spinach" readonly id="oneTry8"  name="select_veg_and_carbs[]" />
					</li> -->
				</ul>
				<script type="text/javascript">
    				onlyTwoCheckBox()
				</script>
			</div>
				<div class="counter_inc">
					<label>Meal Quantity: </label><div class="clearBoth"></div>
					<span class="fa fa-minus" onclick="decreaseCount('quantity');"></span>
					<input type="text" name="count_quantity" id="count_quantity" value="0" readonly/>
					<span class=" fa fa-plus" onclick="increaseCount('quantity');"></span>
				</div>
			<hr>
			<div class="footer_modal">
				<a class="btn btn-primary close_popup" onclick="cancelModal();">
					Cancel
				</a>
				<button type="submit" class="btn btn-primary add_to_basket_button" id ="add_to_basket_button" disabled>
					Add to Basket
				</button>
			</div>
			</form>  
		</section>
	</article>	
	
	<section class="basket_table top_410">
			<div class="heading">
				<h3>
					BASKET
				</h3>
			</div>
				<table id="table_product" width="320" class="text-center mobile_table">
					<tr>
						<th>Quantity</th>
						<th>Product</th>
					</tr>
					@if(Session::has('food_item'))
					@for ($i=0; $i<(Session::get('basket_item_count')); $i++)
					<tr id="forDelete{{ $i }}">
						<td>
							<div class="counter_inc">
								<form method="post" id="edit_product" action="/update_basket_data/{{ Session::get('food_item')[$i]->temp_user_id }}/{{ Session::get('food_item')[$i]->meal_name }}/{{ Session::get('food_item')[$i]->vegcarb }}/{{ $i }}">
									{{ csrf_field() }}
									<button type="submit" id="count_edit_btn1" class="fa fa-minus" onclick="decreaseCount('edit', '{{ $i }}'); "></button>
									<input type="text" class="count_edit" name="count_edit{{ $i }}" id="count_edit{{ $i }}" value="{{ Session::get('food_item')[$i]->meal_quantity }}" readonly />
									<button type="submit" id="count_edit_btn2" class=" fa fa-plus" onclick="increaseCount('edit', '{{ $i }}');"></button>
								</form>
							</div>
						</td>
						<td>{{ Session::get('food_item')[$i]->meal_name }}<br>
							@foreach (explode('+', Session::get('food_item')[$i]->vegcarb) as $vegcarb)
						    	@if ($loop->first) @continue @endif
						    	({{ $vegcarb }})<br>
							@endforeach
						</td>
					</tr>
					@endfor
					@php
						session()->forget('basket_item_count');
					@endphp
					@php
						session()->forget('food_item');
					@endphp
					@else
					<tr>
						<div class="text-center mobile_head">
							<h4>Nothing selected</h4>
						</div>
					</tr>
					
					@endif
				</table>
			<div class="table_foot mobile_foot">
				<a href="#" class="btn-cancel">CANCEL</a>
				<a href="/redirect-to-register/{{ $temp_user_id }}" id="tableSubmit" class="btn-submit">SUBMIT</a>
			</div>
		</section>

		
</div>
	
@endsection