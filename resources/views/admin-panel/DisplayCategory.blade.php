<?php
if(!isset($_SESSION))
{
    session_start();
}
?>

<div class="home-section display-category" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-sm-6  col-md-4 text-left">
            <div class="manage-users">Categories List</div>

        </div>


    </div>
    <div class="table-header">
        <div class="row">


            <div class="col-sm-12 col-md-12 text-left margin-bottom-8">
                <h4>Category List</h4>
            </div>




            <div class="clear-both" style="clear:both;"></div>
            <div class="col-md-1 col-sm-1  margin-bottom-0">
                <p class="category-info-head">Id</p>
            </div>
            <div class="col-md-2 col-sm-2  margin-bottom-0">
                <p class="category-info-head">Name</p>
            </div>
            <div class="col-md-2 col-sm-2 margin-bottom-0">
                <p class="category-info-head">Slug</p>
            </div>
            <div class="col-sm-3 margin-bottom-0">
                <p class="category-info-head">Description</p>
            </div>
            <div class="col-sm-4 margin-bottom-0">
                <p class="Page-info-head">Edit/Delete</p>
            </div>
        </div>
    </div>
    <div class="content-body">
        @foreach($category_infos as $pageinfo)
            <div class="user-row">
                <div class="row">

                        <div class="col-md-1 col-sm-1 margin-bottom-0">
                            <span class="user-red"><b>{{ $pageinfo->id}}</b></span>

                        </div>
                    <div class="col-md-2 col-sm-2  margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->name}}</b></span>

                    </div>

                    <div class="col-md-2 col-sm-2 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->slug}}</b></span>

                    </div>
                    <div class="col-md-3 col-sm-3 margin-bottom-0">
                        <span class="user-red"><b>{{ $pageinfo->body}}</b></span>

                    </div>


                    <div class="col-sm-4 margin-bottom-0">
                        <a href="/category/{{$pageinfo->id}}/edit" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                        <a href="/category/{{$pageinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>

                    </div>
                </div>

            </div>

        @endforeach()
    </div>
</div>