





<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         true,
                } );
            } );

        </script>

        {{--  <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>DESCRIPTION </th>
                <th>Edit/Delete</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>DESCRIPTION </th>
                <th>Edit/Delete</th>

            </tr>
            </tfoot>
            <tbody>


            @foreach($record as $fee)

                <tr>

                    <td>{{$fee->id}}</td>
                    <td>{{$fee->title}}</td>
                    <td>{{$fee->description}}</td>

                    <td><a href="/edit/{{$fee->id}}/slider">Edit</a>/<a href="/delete/{{$fee->id}}/slider"  onclick="return confirm('Are you sure?')" class="user-delete">Delete</a></td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>