<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

    <script src="https://paymentgateway.commbank.com.au/checkout/version/47/checkout.js"
            data-error="errorCallback"
            data-cancel="cancelCallback">
    </script>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script type="text/javascript">
        var amt = '';
        var id='';
        var desc = '';

        $(document).ready(function(){
            $('#amount').on('keyup', function(){
                amt = $('#amount').val();

            });

            $('#description').on('keyup', function(){
                desc = $('#description').val();

            });
            $('#id').on('keyup', function(){
                id = $('#id').val();

            });

        });

        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }

        function cancelCallback() {
            console.log('Payment cancelled');
        }


        Checkout.configure({
            merchant: 'TESTPROSTUCOM201',
            order: {
                amount: function () {


                    //Dynamic calculation of amount
                    return amt;
                },
                currency: 'AUD',
                description: function () {


                    //Dynamic calculation of amount
                    return desc;
                },
                id: id
            },
            interaction: {
                merchant: {
                    name: 'ericshrestha',
                    address: {
                        line1: '200 Sample St',
                        line2: '1234 Example Town'
                    }
                }
            }
        });

    </script>
</head>
<body>

<input type="text" name="amount" id="amount">
<input type="text" name="id" id="id">
<input type="text" name="description" id="description">

<input type="button" value="Pay with Lightbox" onclick="Checkout.showLightbox();" />
<input type="button" value="Pay with Payment Page" onclick="Checkout.showPaymentPage();" />

</body>
</html>