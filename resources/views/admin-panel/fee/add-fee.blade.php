<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="add-fee-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>ADD FEE</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/add-fee/','method'=>'POST','files' => true )) !!}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">




                                    <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Amount</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus>

                                            @if ($errors->has('amount'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('course') ? ' has-error' : '' }}">
                                        <label for="course" class="col-md-4 control-label">Course</label>

                                        <div class="col-md-6">
                                            <input id="course" type="text" class="form-control" name="course" value="{{ old('course') }}" required autofocus>

                                            @if ($errors->has('course'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('course') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="description" class="col-md-4 control-label">Description</label>

                                        <div class="col-md-6">
                                            <input id="description" type="textarea" class="form-control" name="description" value="{{ old('description') }}" required autofocus>

                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>





                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Add Fee
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>