
<div class="fee-edit-section">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="table-header"><h4 class="panel-heading ">UPADATE FEE</h4></div>

    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($record,['method'=>'PATCH','files' => true,'action'=>['FeeController@update',$record->id]]) !!}

        <div class="col-sm-12 ">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group{{ $errors->has('amount') ? 'has-error' : '' }}">
                        <label for="amount" class="col-md-4 control-label">Amount</label>
                        <div class="col-md-10 col-sm-12">

                            {{ Form:: text('amount', $record->amount,  array("class" => "form-control margin-bottom-12", 'id' => 'amount')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="course" class="col-md-4 control-label">Course</label>
                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('course',$record->course, array("class" => "form-control margin-bottom-12", 'id' => 'course')) }}


                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">

                        <label for="description" class="col-md-4 control-label">Description</label>
                        <div class="col-md-10 col-sm-12">

                            {{Form::textarea('description',$record->description, array("class"=>"form-control margin-bottom-12",'id'=>'description'))}}

                        </div>
                    </div>







                    <div class="form-group">
                        <div class="col-sm-12">

                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form:: close() }}
    </div>


</div>