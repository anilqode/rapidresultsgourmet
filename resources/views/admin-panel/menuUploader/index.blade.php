
@extends('layouts.app')

@section('content')

<!-- <div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div> -->
<div class="container-fluid progressive-natti-body">

    <div class="row">
        <div class="col-sm-3">
                @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>MENU UPLOADS</h4></div>
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <th>S.N</th>
                            <th>Menu</th>
                            <th class="menuuploaddesc">Description</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($menuploads  as $menuupload)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        <a href="/menu/{{$menuupload->id}}" target="_blank" class="btn">View Menu</a>
                                    </td>
                                    <td class="menuuploaddesc">{{ $menuupload->description }}</td>
                                    <td>
                                        <a href="{{ route('menu_uploader.edit', $menuupload->id) }}" class="btn btn-block">Edit</a>
                                        <form action="{{ route('menu_uploader.destroy', $menuupload->id) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button class="btn btn-danger btn-block" style="background: #d9534f !important; margin-top: 7px;" onclick="alertPromt(event)">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function alertPromt(evt){
        var result = confirm('Are you sure to delete?');
        if(! sssresult){
            evt.stopPropagation();
            evt.preventDefault();
        }
    }
</script>
@endsection