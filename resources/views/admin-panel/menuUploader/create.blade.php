x` @extends('layouts.app')

@section('content')
<!-- <div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div> -->
<div class="create-post-form container-fluid">
    <div class="row">
        <div class="col-sm-3">
                @include('admin-panel.left-nav')
            </div>
        <div class="col-sm-9 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>ADD NEW MENU</h4></div>

                <div class="panel-body">
                    <form action="{{ route('menu_uploader.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="menu">Upload a Menu (Only PDF Files Enabled | Max: 10000KB)</label>
                            <input type="file" class="form-control" name="menu" accept="application/pdf">
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <textarea name="description" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection