<?php

use App\Coupon;

if (!isset($_SESSION)) {
    session_start();
}
$price = 0;
?>

<div class="text-center"><a href="/set-meal-plan" class="btn btn-primary menuCheckButton center">Continue Shopping</a></div>
@if(isset($orderDetail))
@if(sizeof($orderDetail)>0)

<div class="col-sm-12">

    <div class="panel-body guestcheckoutbox">
        <label class="yourOrder">YOUR PAYMENT DETAILS</label>
        <div class="panel-body ">
            <div class="row">

                <div class="col-sm-6 col-xs-5"><strong>Product</strong></div>
                <div class="col-sm-2 col-xs-3 align-center"><strong>Qty</strong></div>
                <div class="col-sm-3 col-xs-2 align-center"><strong>Price</strong></div>
                <div class="col-sm-1 col-xs-1 align-center"><strong><span class="glyphicon glyphicon-trash"></span></strong></div>
            </div>
            <hr>

            @if(session()->get('first_comp') == 0)

            @else

            <div class="row giveborder">

                <div class="col-sm-6 col-xs-5">
                    <h4 class="product-name"><strong>{{$freesnacks->title}}({{$fitreatss}})</strong></h4>
                    <h4><small>{!! $freesnacks->description !!}</small></h4>
                </div>
                <div class="col-sm-4 price-vegan col-xs-4">
                    <div class="col-sm-8 text-right">
                        <h6><strong>Complementary </strong></h6>
                    </div>
                </div>
            </div>
            <hr>
            @endif
            <div class="extrasnacks-checkout-box">
                @if($orderDetail)

                @foreach($orderDetail as $key=>$order)
                <div class="row giveborder">
                    <div class="col-sm-6 col-xs-5">
                        {{$order->plan_name}}
                        @if($order->plan_name!='Fitreats')
                        ({{$order->vegan}})

                        @endif
                    </div>

                    <div class="col-sm-2 col-xs-3 clean text-center">
                        @if($order->plan_name!='Fitreats') <span class="shop-quantity">1</span> @endif
                    </div>


                    <div class="col-sm-3 col-xs-3 del text-center mb10">
                        @if($order->plan_name!='Fitreats')
                        <p class="mealprice">Monthly £{{$order->vegan_price}}</p>
                        <p class="mealprice">Weekly £{{$order->weekly_price}}</p>
                        <p class="mealprice">Per Meal £{{$order->price_per_meal}}</p>
                        @endif

                    </div>


                    <div class="col-sm-1 price-vegan col-xs-1 text-center">
                        <a class="deleteProduct" data-id="{{ $order->id }}" data-token="{{ csrf_token() }}">✖</a>
                    </div>
                    @if($order->plan_name!='Fitreats')
                    @if(@isset($order->extra_snacks))


                    <div class="meal-spacing">
                        @foreach($extra_snacks_detail as $key=>$extra_order)
                        <div class="row">
                            <div class="col-sm-6 col-xs-5">
                                <p class="product-name">
                                    {{$extra_order->extra_snack}}
                                </p>

                            </div>
                            <div class="col-sm-2 col-xs-3 align-center">
                                @if($order->plan_name!='Fitreats')
                                {{$extra_order->quantity}}
                                @endif
                            </div>
                            <div class="col-sm-3 price-vegan col-xs-3 text-center">
                                <p class="product-name">£{{$extra_order->rate}}</p>

                            </div>
                        </div>
                        @endforeach
                    </div>

                    @endif
                    @else
                    <div class="col-sm-6 col-xs-5">
                        <p class="product-name"> {{$order->vegan}} </p>

                    </div>
                    <div class="col-sm-2 col-xs-3 align-center">
                        <p class="product-name">{{$order->quantity}}</p>
                    </div>
                    <div class="col-sm-3 price-vegan col-xs-3 text-center">
                        <p class="product-name">£{{$order->extra_price}}</p>
                    </div>
                    @endif

                </div>

                @endforeach
                @endif
            </div>



        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12">
                    <table width="100%">
                        <tr>
                            <td>
                                Sub-total:
                            </td>
                            <td class="text-right">
                                £ {{$sub_total_in_decimal}}
                            </td>
                        </tr>
                    </table>

                    <hr>
                </div>

                <div class="col-sm-12">


                    <table width="100%">
                        <tr>
                            @if(isset($delivery_info))
                            <td> Delivery Charge : {{$delivery_info[0]->deliveries}} deliveries x £ {{$delivery_info[0]->delivery_rate}}</td>
                            <td>
                            </td>

                            <td class="text-right"> £{{number_format((float)$delivery_info[0]->total_delivery_cost, 2, '.', '')}}</td> @endif

                        </tr>
                    </table>

                    <hr>
                </div>

                @if(session()->get('coupon'))
                <div class="col-sm-12">
                    <table width="100%">
                        <tr>
                            <td> <span style="color:green">Discount:</span> </td>
                            <td class="text-right"> <span id="discountAmt">
                                    <?php
                                    $totAmt = $sub_total_in_decimal;
                                    if (session()->get('coupon')) {
                                        if (session()->get('coupon_type') == 1) {

                                            $disc = session()->get('coupon') * 0.01 * $totAmt;
                                            $disc = number_format((float)$disc, 2, '.', '');
                                        } else {
                                            $disc = session()->get('coupon');
                                        }
                                    }
                                    ?>
                                </span>
                                <?php echo '£ ' . $disc; ?></td>
                    </table>
                    <hr>

                </div>

                @endif

                <div class="col-sm-12">
                    <table width="100%">
                        <tr>
                            <td> <b>TOTAL PRICE:</b> </td>
                            <td class="text-right">

                                @if(isset($delivery_info))
                                <span id="totalAmt">
                                    <?php
                                    $totalAmount = $sub_total_in_decimal;
                                    if (session()->get('coupon')) {
                                        if (session()->get('coupon_type') == 1) {

                                            $totalAmount = $totalAmount - session()->get('coupon') * 0.01 * $totalAmount;
                                            $totalAmount = number_format((float)$totalAmount, 2, '.', '');
                                            $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                        } else {
                                            $totalAmount = $totalAmount - session()->get('coupon');
                                            $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                        }
                                    } else {

                                        $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                    }
                                    ?>
                                </span>
                                £{{ number_format((float)$totAmt, 2, '.', '')  }}

                                @endif
                                </strong>

                            </td>
                        </tr>

                    </table>
                    <hr>
                </div>
                @if($setMealCount==1)
                <div class="col-sm-12">
                    <table width="100%">
                        <tr>
                            <td>
                                Monthly Total Price:
                            </td>
                            <td class="text-right">£ {{ number_format((float)$totAmt, 2, '.', '')  }}</td>
                        </tr>
                        <tr>
                            <td>
                                Weekly Total Price:
                            </td>
                            <td class="text-right">£ {{ number_format((float)$totAmt/4, 2, '.', '')  }}</td>
                        </tr>
                    </table>
                </div>
                <hr>
                @endif



               
                {!! Form::open(array('url'=>'/paypal-checkout','method'=>'POST','files' => true )) !!}
                {!! csrf_field() !!}

                <div class="col-sm-12">
                    <p class="extra-added"> Order Notes </p>
                </div>
              
                <div class="col-sm-12">

                  
                    @if(session()->get('order_review'))
                    <textarea id="order_notes"  class="order-notes" type="textarea" name="review" placeholder="Notes about your order, eg special notes for delivery" spellcheck="false">{{session()->get('order_review')}}</textarea>
                    @else
                    <textarea id="order_notes" class="order-notes" type="textarea" name="review" placeholder="Notes about your order, eg special notes for delivery" spellcheck="false"></textarea>
                    @endif
                </div>
            </div>
        </div>
        <div class="cols-sm-12">
                    <div class="col-sm-12 couponDiv">
                        <p class="extra-added"><b>How would you like to pay?</b>

                            You can pay weekly, monthly or your total amount. Please select below: </p>



                    </div>
                </div>
  
            <div class="col-sm-12 couponDiv">
                <div class="row">
                    <div class="col-sm-12">
          
                    </div>

                    <div class="col-sm-3">
                      
                        <input type="hidden" name="order_id" value="{{$plan_no}}">
                        <input type="hidden" name="amount" value="{{ $totAmt }}">
                        <input type="hidden" name="vegan_title" value="RRG Order">

                     
                        <div class="text-center"></div>

                    </div>
                    <div class="col-sm-3 text-right">

                        <div id="paypal">
                            <button type="submit" class="btn btn-primary btn-o" name="paymentMode" value="gocardlessWeekly">Pay weekly</button>
                            <!-- <a href="/redirect_to_register">Place My Order</a> -->
                        </div>

                    </div>
                    <div class="col-sm-3 text-right">

                        <div id="paypal">
                            <button type="submit" class="btn btn-primary btn-o" name="paymentMode" value="gocardlessMonthly">Pay Monthly</button>
                            <!-- <a href="/redirect_to_register">Place My Order</a> -->
                        </div>

                    </div>
                    <div class="col-sm-3">
                    <button type="submit" class="btn btn-primary btn-o" name="paymentMode" value="paypal">
                    Pay  Total £{{ number_format((float)$totAmt, 2, '.', '')  }}
                           
                        </button>
                        <img src="images/rrgimages/pay-pal.png" width="100%">
                    </div>

                </div>

            </div>
   

        {{ Form:: close() }}
    </div>



</div>


<script>
    $(".deleteProduct").click(function() {
        var id = $(this).data("id");
        target = $(this);


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "https://rapidresultsgourmet.co.uk/deleteSetMeal/" + id,
            type: 'get', // replaced from put
            dataType: "JSON",
            data: {

                "id": id // method and token not needed in data
            },
            success: function(response) {

                //target.remove();

                target.closest('.row').remove();
                location.reload(true);

                console.log(response); // see the reponse sent
            },
            error: function(xhr) {
                console.log(xhr.responseText); // this line will save you tons of hours while debugging
                // do something here because of error
            }
        });
    });
</script>
@endif

@endif