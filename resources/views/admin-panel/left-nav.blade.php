<?php
use Illuminate\Support\Facades\Auth;
$userInfo=Auth::User();
if($userInfo)
{
 if($userInfo->role_id==1){
    ?>
    <div class="panel-group" id="accordion">
       
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                    </span>User</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>
                                <i class="fa fa-address-card"></i><a href="/userinfo">Userinfo</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-users"></i>  <a href="/create-user">Create User</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <i class="fa fa-check-circle"></i><a href="/change-password">Change Password</a>
                            </td>
                        </tr>


                    </table>
                </div>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOrderList"><span class="glyphicon glyphicon-th">
                    </span>Orders</a>
                </h4>
            </div>
        </div>
        <div id="collapseOrderList" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-address-card"></i><a href="/show-all-order">Order Summary</a>
                        </td>
                    </tr>
                   
                </table>
            </div>
        </div>
   

    <!-- <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span class="glyphicon glyphicon-user">
                </span>Page</a>
            </h4>
        </div>
        <div id="collapseThree" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-file"></i>   <a href="/pages">Page</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-plus"></i> <a href="/create-page">Create Page</a>
                        </td>
                    </tr>


                </table>
            </div>
        </div>
    </div> -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour"><span class="glyphicon glyphicon-file">
                </span>Posts</a>
            </h4>
        </div>
        <div id="collapseFour" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-file"></i><a href="posts">Post</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-plus"></i> <a href="/create-post">Create Post</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-tasks"></i><a href="/create-categories">Category</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseProduct"><span class="glyphicon glyphicon-file">
                </span>Product</a>
            </h4>
        </div>
        <div id="collapseProduct" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-file"></i><a href="/show-product-report">Product</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-plus"></i> <a href="/create-product">Create Product</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-tasks"></i><a href="/create-product-categories">Create Product Category</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <!-- <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomPlan"><span class="glyphicon glyphicon-file">
                </span>Custom Plan</a>
            </h4>
        </div>
        <div id="collapseCustomPlan" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-plus"></i><a href="/create-custom-plan">Create Custom Plan</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-file"></i> <a href="/show-custom-plan">Show Custom Plan</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseWeekPlan"><span class="glyphicon glyphicon-file">
                </span>Week Plan</a>
            </h4>
        </div>
        <div id="collapseWeekPlan" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-plus"></i><a href="/create-week-plan">Create Week Plan</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-file"></i> <a href="/show-week-plan">Show Week Plan</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseMealPlan"><span class="glyphicon glyphicon-file">
                </span>Meal Plan</a>
            </h4>
        </div>
        <div id="collapseMealPlan" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-plus"></i><a href="/create-meal-plan">Create Meal Plan</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-file"></i> <a href="/show-meal-plan">Show Meal Plan</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div> -->


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><span class="glyphicon glyphicon-file">
                </span>Testinomial</a>
            </h4>
        </div>
        <div id="collapseSix" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-book"></i><a href="/add-testinomial">Add Testinomial</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-book"></i><a href="/display-testinomial">Display Testinomial</a>
                        </td>
                    </tr>



                </table>
            </div>
        </div>
    </div>


    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven"><span class="glyphicon glyphicon-file">
                </span>Slider</a>
            </h4>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-book"></i><a href="/add-slider">Add Slider</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-book"></i><a href="/display-slider">Display Slider</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseMenuUploader"><span class="glyphicon glyphicon-file">
                </span>Menu Uploader</a>
            </h4>
        </div>
        <div id="collapseMenuUploader" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-book"></i><a href="{{ route('menu_uploader.create') }}">Add New Menu</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-book"></i><a href="{{ route('menu_uploader.index') }}">Display All Menu</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCoupon"><span class="glyphicon glyphicon-list-alt">
                </span>Coupon</a>
            </h4>
        </div>
        <div id="collapseCoupon" class="panel-collapse collapse">
            <div class="panel-body">
                <table class="table">
                    <tr>
                        <td>
                            <i class="fa fa-code"></i><a href="{{ route('coupon.create') }}">Add New Coupon</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-code"></i><a href="{{ route('coupon.index') }}">Display All Coupon</a>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="/logout"><span class="fa fa-power-off">
                </span> Logout</a>
            </h4>
        </div>
    </div>
   

</div>

<?php } else {

}?>
<?php } else {
    return redirect('/login');
}
?>


