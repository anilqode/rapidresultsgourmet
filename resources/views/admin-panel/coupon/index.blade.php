@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-post-form">

    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>COUPONS</h4>
                </div>

                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <th>S.N</th>
                            <th>Type</th>
                            <th>Code</th>
                            <th>Coupon Value</th>
                            <th>Time Of Use</th>
                            <th>Used Times</th>
                            <th>Promoter's Email</th>
                            <th>Status</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach($coupons as $coupon)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    @if($coupon->type == 0)
                                    {{ 'Flat Discount' }}
                                    @else
                                    {{ 'Percentage Discount' }}
                                    @endif
                                </td>
                                <td>{{ $coupon->code }}</td>
                                <td>
                                    @if($coupon->type == 0)
                                    {{ '£ ' . $coupon->value }}
                                    @else
                                    {{ $coupon->value . ' %' }}
                                    @endif
                                </td>
                                <td>
                                    {{ $coupon->time_of_use }}
                                </td>
                                <td>
                                    @if($coupon->used_times)
                                    {{ $coupon->used_times }}
                                    @else
                                    {{ '0' }}
                                    @endif
                                </td>
                                <td>
                                    {{ $coupon->email }}
                                </td>
                                <td>
                                    @if($coupon->status == 0)
                                    {{ 'Inactive' }}
                                    @else
                                    {{ 'Active' }}
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('coupon.edit', $coupon->id) }}" class="btn btn-block">Edit</a>
                                    <form action="{{ route('coupon.destroy', $coupon->id) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-block"
                                            style="background: #d9534f !important; margin-top: 7px;"
                                            onclick="alertPromt(event)">Delete</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function alertPromt(evt) {
    var result = confirm('Are you sure to delete?');
    if (!sssresult) {
        evt.stopPropagation();
        evt.preventDefault();
    }
}
</script>
@endsection