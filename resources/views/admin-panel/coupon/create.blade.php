@extends('layouts.app')

@section('styles')
<style>
button#couponValue {
    background-color: white !important;
    border: 1px solid #c6c5c5;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-post-form container-fluid">
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>ADD NEW COUPON</h4>
                </div>

                <div class="panel-body">
                    <form action="{{ route('coupon.store') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="form-group">
                                <label for="type" class="col-md-4">Coupon Type</label>
                                <div class="col-md-6">
                                    <select name="type" class="form-control" id="type" onchange="setType(event)">
                                        <option value="">Select the Coupon Type</option>
                                        <option value="0">Flat Discount</option>
                                        <option value="1">Percentage Discount</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="code" class="col-md-4">Code</label>
                                <div class="col-md-4">
                                    <input type="text" name="code" class="form-control" id="couponCode" required>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-primary no-m" onclick="generateCode(event)">Generate
                                        Code</button>
                                </div>
                                <script>
                                function generateCode(evt) {
                                    evt.preventDefault();
                                    var result = '';
                                    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                                    var charactersLength = characters.length;
                                    for (var i = 0; i < 6; i++) {
                                        result += characters.charAt(Math.floor(Math.random() * charactersLength));
                                    }
                                    result = 'RRG-' + result;
                                    document.getElementById('couponCode').value = result;
                                }

                                function setType(evt) {
                                    if (evt.target.value == 1) {
                                        document.getElementById('couponValue').innerHTML = 'Percentage';
                                    } else {
                                        document.getElementById('couponValue').innerHTML = 'Amount';
                                    }
                                }
                                </script>
                            </div>
                            <div class="form-group">
                                <label for="value" class="col-md-4">Coupon Value</label>
                                <div class="col-md-4">
                                    <input type="text" name="value" class="form-control" required>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn no-m btn-differ" id="couponValue" disabled>
                                        Amount
                                    </button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="time_of_use" class="col-md-4">Time of Use</label>
                                <div class="col-md-6">
                                    <input type="text" name="time_of_use" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-md-4">Status</label>
                                <div class="col-md-6">
                                    <select name="status" class="form-control" required>
                                        <option value="">Select the Status</option>
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-4">Promoter's Email</label>
                                <div class="col-md-6">
                                    <input type="email" name="email" class="form-control" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection