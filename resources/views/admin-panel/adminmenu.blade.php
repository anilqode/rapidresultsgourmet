<?php
if(!isset($_SESSION))
{
    session_start();
}
?>

@extends('layouts.app')

@section('contents')
    {!! Menu::render() !!}
@endsection

//YOU MUST HAVE JQUERY LOADED BEFORE menu scripts
@push('scripts')
{!! Menu::scripts() !!}
@endpush