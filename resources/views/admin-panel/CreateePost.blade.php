<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-post-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>CREATE NEW POST</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/create-post/','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">





                                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <label for="title" class="col-md-12 control-label">Title</label>

                                        <div class="col-md-12">
                                            <input id="title" type="text" class="form-control input-lg" name="title" value="{{ old('title') }}" required autofocus>

                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="slug" class="col-md-12 control-label">Slug</label>

                                        <div class="input-group  col-md-12">
                                            <input id="slug" type="text" class="form-control input-lg" name="slug" >


                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="catag_id" class="col-md-12 control-label">Category</label>
                                        <div class="col-md-12 col-sm-12">

                                            {{ Form:: select('category_id',$categories,null,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any catag', 'id' => 'catag_id')) }}


                                        </div>
                                    </div>



                                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                        <label for="body" class="col-md-12 control-label">body</label>

                                        <div class="col-md-12">

                                            <script src="http://cdn.tinymce.com/4/tinymce.min.js"></script>
                                            <textarea name="body" class="tinymce form-control my-editor"></textarea>

                                            <script>
                                                var editor_config = {
                                                    path_absolute : "/",
                                                    selector: "textarea.my-editor",
                                                    plugins: [
                                                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                                                        "insertdatetime media nonbreaking save table contextmenu directionality",
                                                        "emoticons template paste textcolor colorpicker textpattern"
                                                    ],
                                                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                                                    relative_urls: false,
                                                    file_browser_callback : function(field_name, url, type, win) {
                                                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                                        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                                        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                                        if (type == 'image') {
                                                            cmsURL = cmsURL + "&type=Images";
                                                        } else {
                                                            cmsURL = cmsURL + "&type=Files";
                                                        }

                                                        tinyMCE.activeEditor.windowManager.open({
                                                            file : cmsURL,
                                                            title : 'Filemanager',
                                                            width : x * 0.8,
                                                            height : y * 0.8,
                                                            resizable : "yes",
                                                            close_previous : "no"
                                                        });
                                                    }
                                                };

                                                tinymce.init(editor_config);
                                            </script>


                                            @if ($errors->has('body'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>









                                    <div class="form-group{{ $errors->has('meta_description') ? ' has-error' : '' }}">
                                        <label for="meta_description" class="col-md-12 control-label">Meta Description</label>

                                        <div class="col-md-12">
                                            <input id="meta_description" type="text" class="form-control" name="meta_description" value="{{ old('meta_description') }}" required autofocus>

                                            @if ($errors->has('meta_description'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('meta_description') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('meta_keywords') ? ' has-error' : '' }}">
                                        <label for="meta_keywords" class="col-md-12 control-label">Meta Keywords</label>

                                        <div class="col-md-12">
                                            <input id="meta_keywords" type="text" class="form-control" name="meta_keywords" value="{{ old('meta_keywords') }}" required autofocus>

                                            @if ($errors->has('meta_keywords'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('meta_keywords') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label for="status" class="col-md-12 control-label">Status</label>
                                        <div class="col-md-12 col-sm-12">

                                            {{ Form:: select('status', $status, array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Status', 'id' => 'status')) }}


                                        </div>
                                    </div>

                                    <div class="col-sm-6 margin-left-20">
                                        {!! Form::label('featured_image','Select Featured Image',array('id'=>'','class'=>'margin-top-20')) !!}
                                        {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}
                                        <div class="form-group userprofile-bg" id="imagePreview">

                                        </div>    </div>



                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Create
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>

    $("#title").keyup(function(){

        var str = $(this).val();
        str= str.toLowerCase();
        str = str.replace('/\s/g','-');
        $("#slug").val(str);



    });


</script>
@include('mceImageUpload::upload_form')

