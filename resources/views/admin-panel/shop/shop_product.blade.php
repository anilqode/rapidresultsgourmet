<Section class="shoping-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @foreach($product as $shop)
                <div class="col-sm-4 shop-box">
                    <div class="shop-box-in">
                        <div class="shop-box-in-image">
                        <img src="{{$shop->imagePath}}">



                    </div>
                       <?php  $title=substr($shop->title, 0, 100).'..'; ?>
                    <h4>{{$title}}</h4>
                    </div>
                    <div class="col-sm-12 shop-pricing-qty">
                        <div class="col-sm-2">
                            <h4>${{$shop->price}}</h4>
                        </div>
                        <div class="col-sm-2">
                            <div class="counter_inc">
                                <span class="fa fa-minus" onclick="decreaseCount('<?php echo $shop->id?>','<?php echo $shop->quantity ?>');"></span><input type="text" id="count_quantity{{$shop->id}}" name="qty" /><span class=" fa fa-plus" onclick="increaseCount('<?php echo $shop->id?>','<?php echo $shop->quantity ?>');"></span>
                                <input type="text" id="max" name="max" value="{{$shop->quantity}}"/>
                                <input type="text" id="id" name="id" value="{{$shop->id}}"/>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            {!! Form::open(array('url'=>'/add-to-cart/'.$shop->id,'method'=>'POST','files' => true )) !!}
                            <input type="text" id="count<?php echo $shop->id?>" name="qtty" value=""/>
                            <button type="submit" class="btn btn-primary">
                                Add to Cart
                            </button>
                            {{ Form:: close() }}

                        </div>
                    </div>
                </div>
                    @endforeach
            </div>
        </div>
    </div>

</Section>
<script >





    function decreaseCount(id,qty){
        var displayQuantity = document.getElementById('count_quantity'+id).value;
        // alert(displayQuantity);
        if(displayQuantity <= 1){
            alert('Cant Decrease anymore Bro!');
        }
        else{
            displayQuantity -= 1;
            document.getElementById('count_quantity'+id).value = displayQuantity;
            document.getElementById('count'+id).value = displayQuantity;
        }
    }

    function increaseCount(id,qty){
        var displayQuantity = document.getElementById('count_quantity'+id).value;
        // displayQuantity = parseInt(displayQuantity);
        if(displayQuantity >= qty){
            alert('Max stock value reached bro!!!');
        }
        else{
            displayQuantity ++;
            document.getElementById('count_quantity'+id).value = displayQuantity;
            document.getElementById('count'+id).value = displayQuantity;
        }
    }


</script>
