<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<div class="side-bar">
    <h4>Recent Posts</h4>

    @foreach($sidebar as $bar)
        <li><a href="/post/{{$bar->slug}}/show">{{str_limit($bar->title, $limit = 30, $end = '...') }}</a></li>
        @endforeach

    </div>