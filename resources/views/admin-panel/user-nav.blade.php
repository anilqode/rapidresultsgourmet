<div class="panel-group user-nav" id="accordion" >
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-laptop">
</i> User Menu</a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse in">
            <div class="panel-body">
                <table class="table">

                    <tr>
                        <td>
                            <i class="fa fa-user"></i><a href="/dashboard">My Account</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-clipboard"></i><a href="/display-user-order/{{ Auth::user()->id }}">Order History</a>
                        </td>
                    </tr>
                   <!--  <tr>
                        <td>
                            <i class="fa fa-dashboard"></i><a href="/display-user-custom-order/{{ Auth::user()->id }}">Custom Order History</a>
                        </td>
                    </tr> -->
                   


                    <tr>
                        <td>
                            <i class="fa fa-address-card"></i><a href="/userinfo">User Info</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <i class="fa fa-lock"></i><a href="/change-password">Change Password</a>
                        </td>
                    </tr>



                </table>
            </div>
        </div>
    </div>

</div>




