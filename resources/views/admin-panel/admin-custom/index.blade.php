@extends('layouts.app')


@section('content')

<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "scrollY":        "200px",
            "scrollCollapse": true,
            "paging":         true,
        } );
    } );

</script>

<div class="container-fluid progressive-natti-body">
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            @if(Session::has('delete'))
            <div class="row">
                <h4 class="text-danger">{{Session::get('delete')}}</h4>
            </div>
            @endif
            <div class="tab-content">
                <div id="class1" class="tab-pane fade in active">
                    <table id="example" class="display" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Week Plan</th>
                                <th>Meal Plan</th>
                                <th>Price</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>Week Plan</th>
                                <th>Meal Plan</th>
                                <th>Price</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($customplans as $customplan)
                            <tr>
                                <td>{{$customplan->week_id}}</td>
                                <td>{{$customplan->meal_id}}</td>
                                <td>{{$customplan->price}}</td>
                                <td><a href="/edit-custom-plan/{{$customplan->id}}">Edit</a>/
                                    <form action="/delete-custom-plan/{{$customplan->id}}" method="post">
                                        {{ csrf_field() }}
                                        <button type="submit" onclick="return confirm('Are you sure?')" class="user-delete" style="background: none;">Delete</button></form>
                                    </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection