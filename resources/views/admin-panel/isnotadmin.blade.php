@extends('layouts.app')

@section('content')
    <div class="not-an-admin">
    <div class="container">
    <div class="row">
       <div class="col-sm-12">

           <h4 class="not-admin">Sorry You are not Admin</h4>
           <p  class="not-admin">This page is only acessible to admin</p>
       </div>
    </div>
    </div>
    </div>
@endsection