@extends('layouts.app')
@section('content')

<div class="container-fluid progressive-natti-body">
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            @if(Session::has('store'))
            <div class="row">
                <h4 class="text-success">{{Session::get('store')}}</h4>
            </div>
            @endif
            <div class="create-category-form">
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="panel panel-default">
                            <div class="panel-heading"><h4>CREATE Meal PLAN</h4></div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" method="POST" action="/store-meal-plan">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-sm-12 ">
                                        <div class="panel panel-default ">
                                            <div class="panel-body">
                                                <div class="form-group{{ $errors->has('meal_plan') ? ' has-error' : '' }}">
                                                    <label for="meal_plan" class="col-md-4 control-label">Meal Plan</label>
                                                    <div class="col-md-6">
                                                        <input id="meal_plan" type="text" class="form-control" name="meal_plan" value="{{ old('meal_plan') }}" required autofocus>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <button type="Create" class="btn btn-primary">
                                                            Create
                                                        </button>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>



@endsection