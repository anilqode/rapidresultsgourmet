
<div class="fee-edit-section">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="table-header"><h4 class="panel-heading ">UPADATE TESTINOMIAL</h4></div>

    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($record,['method'=>'PATCH','files' => true,'action'=>['TestinomialController@update',$record->id]]) !!}

        <div class="col-sm-12 ">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group{{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name" class="col-md-4 control-label">Name</label>
                        <div class="col-md-10 col-sm-12">

                            {{ Form:: text('name', $record->name,  array("class" => "form-control margin-bottom-12", 'id' => 'name')) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="desg" class="col-md-4 control-label">Designation</label>
                        <div class="col-md-10 col-sm-12">
                            {{ Form::text('desg',$record->desg, array("class" => "form-control margin-bottom-12", 'id' => 'desg')) }}


                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('image_path') ? ' has-error' : '' }}">

                        <label for="image_path" class="col-md-4 control-label">Image Path</label>
                        <div class="col-md-10 col-sm-12">

                            {{Form::textarea('image_path',$record->image_path, array("class"=>"form-control margin-bottom-12",'id'=>'image_path'))}}

                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">

                        <label for="description" class="col-md-4 control-label">Description</label>
                        <div class="col-md-10 col-sm-12">

                            {{Form::textarea('description',$record->description, array("class"=>"form-control margin-bottom-12",'id'=>'description'))}}

                        </div>
                    </div>







                    <div class="form-group">
                        <div class="col-sm-12">

                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form:: close() }}
    </div>


</div>