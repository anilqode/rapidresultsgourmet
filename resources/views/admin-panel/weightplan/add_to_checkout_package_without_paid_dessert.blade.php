<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="text-center">
    @if ($message = Session::get('success'))
    <div class="alert alert-info">
        <h3>{!! $message !!}&nbsp;&nbsp;&nbsp;<span onclick="this.parentElement.style.display='none'">&times;</span>
        </h3>
    </div>
    <?php Session::forget('success');
    $order_id = Session()->get('order_id');
    echo "for order id" . $order_id;
    ?>
    @endif
    @if ($message = Session::get('error'))
    <div class="alert alert-danger">
        <h3>{!! $message !!}&nbsp;&nbsp;&nbsp; <span onclick="this.parentElement.style.display='none'">&times;</span>
        </h3>
    </div>
    <?php Session::forget('error'); ?>
    @endif
</div>
<?php

use App\Productcategory;

$cat = Productcategory::findOrFail(Session::get('prod_cat_id'));
?>
<div class="container">
    <div class="row">
        <div class="text-center stage-tracker ">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/set-meal-plan">Your Plan</a></li>
                <li class="breadcrumb-item "><a href="/weekplan/{{ $cat->id }}/1">{{ $cat->name }}</a></li>
                @if($cat->comp_dess == 1)
                <li class="breadcrumb-item">
                    <a href="/add_vegan_to_checkout/{{ Session::get('vegan_id') }}">Add a
                        Complimentary</a>
                </li>
                @endif
                <li class="breadcrumb-item ">
                    <a href="/add_review_order_to_checkout/{{ Session::get('vegan_id') }}">Review Order</a>
                </li>
                @if($cat->paid_dess == 1)
                <li class="breadcrumb-item "><a href="/add_free_snacks_to_checkout">Snacks</a></li>
                @endif
                <li class="breadcrumb-item active">Shopping Cart</li>
            </ol>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">
                <div class="row">
                    <div class="col-sm-12">
                        <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                    </div>
                </div>
            </div>
        </div>



        <div class="panel-body">
            <div class="row">
                <div class="col-sm-2"><img class="img-responsive" src="{{$vegan->imagePath}}">
                </div>
                <div class="col-sm-6">
                    <h4 class="product-name"><strong>{{$vegan->title}}</strong></h4>
                    <h4><small>
                            <html>

                            <body>{!! $vegan->short_intro !!}</body>

                            </html>
                        </small></h4>
                </div>

                <div class="col-sm-4 price-vegan">
                    <div class="varPrice">
                        <h4><span class="more_prices">( Monthly Price: </span> £{{ $vegan->price }} )</h4>

                    </div>

                    <div class="varPrice">
                        <h4><span class="more_prices">( Weekly Price: </span> £<?php if ($vegan->weekly_price)
                                                                                    echo $vegan->weekly_price;
                                                                                else
                                                                                    echo "--";
                                                                                ?> )
                        </h4>

                    </div>

                    <div class="varPrice">
                        <h4><span class="more_prices">( Price Per Meal: </span>£<?php if ($vegan->price_per_meal)
                                                                                    echo $vegan->price_per_meal;
                                                                                else
                                                                                    echo "--";
                                                                                ?> )
                        </h4>

                    </div>
                </div>
            </div>
            <hr>

            @if(session()->get('first_comp') == 1)
            <div class="row">
                <div class="col-sm-2"><img class="img-responsive" src="{{$freesnacks->imagePath}}">
                </div>
                <div class="col-sm-6">
                    <h4 class="product-name"><strong>{{$freesnacks->title}}({{$fitreatss}})</strong></h4>
                    <h4><small>
                            <p>{!! $freesnacks->description !!}</p>
                        </small></h4>
                </div>
                <div class="col-sm-4 price-vegan">
                    <div class="col-sm-8 text-right">
                        <h6><strong>Complementary </strong></h6>
                    </div>
                </div>
            </div>
            @endif




        </div>
        <div class="panel-footer">
            <div class="row text-center">
                <div class="col-sm-4">

                    <h4 class="text-left">
                        @if(isset($delivery_info))
                        Delivery Charge: {{$delivery_info[0]->deliveries}} deliveries at
                        £{{$delivery_info[0]->delivery_rate}} = £{{$delivery_info[0]->total_delivery_cost}} @endif</h4>
                    <p class="extra-added">You can pay Weekly or Monthly. Please select your preferred payment frequency
                        and payment method on the final Payment page after checking out.
                    </p>
                </div>

                <div class="col-sm-4">
                    @if(session()->get('coupon'))
                    <h4>Discount:
                        <strong>
                            <span id="discountAmt">
                                <?php
                                $totAmt = $vegan->price;
                                if (session()->get('coupon')) {
                                    if (session()->get('coupon_type') == 1) {

                                        $disc = session()->get('coupon') * 0.01 * $totAmt;
                                        $disc = number_format((float)$disc, 2, '.', '');
                                    } else {
                                        $disc = session()->get('coupon');
                                    }
                                }
                                ?>
                            </span>
                            <?php echo '£ ' . $disc; ?>
                        </strong>
                    </h4>
                    @endif
                    <h4>Monthly Price: <strong>
                            @if(isset($delivery_info))
                            £<span id="totalAmt">
                                <?php
                                $totalAmount = $vegan->price;
                                if (session()->get('coupon')) {
                                    if (session()->get('coupon_type') == 1) {
                                        $totalAmount = $totalAmount - session()->get('coupon') * 0.01 * $totalAmount;
                                        $totalAmount = number_format((float)$totalAmount, 2, '.', '');
                                        $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                    } else {
                                        $totalAmount = $totalAmount - session()->get('coupon');
                                        $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                    }
                                } else {
                                    $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                }
                                ?>
                            </span>
                            {{ $totAmt }}<span class="spacing">Space</span>Weekly Price: {{ '£ ' . $totAmt/4 }}
                            <br>
                            @endif
                        </strong>
                    </h4>
                </div>
                <div class="col-sm-3 text-right couponDiv">

                    <button type="button" class="btn btn-primary btn-o" data-toggle="modal" data-target="#haveCoupon">
                        Have a Coupon ?
                    </button>
                    <div class="modal fade text-center" id="haveCoupon" tabindex="-1" role="dialog"
                        aria-labelledby="couponTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Enter the Coupon Code</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST">
                                        {{ csrf_field() }}
                                        <div class="form-group no-pb row">
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-5">
                                                <input type="text" class="form-control no-border" name="coupon"
                                                    required>
                                            </div>
                                            <button class="btn btn-primary ajaxCall no-m col-sm-3">Use Coupon</button>
                                        </div>
                                    </form>
                                </div>
                                <script type="text/javascript">
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });
                                $(".ajaxCall").click(function(e) {
                                    e.preventDefault();
                                    var coupon = $("input[name=coupon]").val();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/applyCoupon',
                                        data: {
                                            coupon: coupon
                                        },
                                        success: function(data) {
                                            switch (data.status) {
                                                case 1:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                                case 2:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                                case 3:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                                case 4:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                            }
                                            window.location.reload();
                                        },
                                        error: function(e) {
                                            console.log(e);
                                        }
                                    });
                                });
                                </script>
                            </div>
                        </div>
                    </div>
                    <div id="paypal-button">
                        <a href="/redirect_to_register" class="btn btn-primary">Place My Order</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>