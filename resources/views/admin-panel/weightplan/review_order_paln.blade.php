@extends('layouts.app')

@section('content')
<div class="error-message-contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                @include('layouts.error_message')
                @include('layouts.sucess_message')
            </div>
        </div>
    </div>
</div>

<div class="contact-parallaxxx"
    style="background-image: url(/images/rrgimages/review-order.jpeg);height: 540px;background-size: cover;background-position: center center;">
</div>

<section class="review-order">
    <div class="container">
        <div class="row">
            <div class="text-center stage-tracker">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/set-meal-plan">Your Plan</a></li>
                    <li class="breadcrumb-item "><a href="/weekplan/{{ $productCat->id }}/1">{{ $productCat->name }}</a>
                    </li>
                    @if($productCat->comp_dess == 1)
                    <li class="breadcrumb-item">
                        <a href="/add_vegan_to_checkout/{{ Session::get('vegan_id') }}">Add a
                            Complimentary</a>
                    </li>
                    @endif
                    <li class="breadcrumb-item active">Review Order</li>
                    @if($productCat->paid_dess == 1)
                    <li class="breadcrumb-item"><a href="#">Add a Snack</a></li>
                    @endif
                    <li class="breadcrumb-item "><a href="#">Shopping Cart</a></li>
                </ol>
            </div>
            <div class="col-sm-offset-4 col-sm-4 parallax-inside-contact">
                <h4>
                    Review Order
                </h4>
            </div>
        </div>
        <div class="row set-meal-detail">
            <div class="col-md-offset-2 col-sm-offset-1 col-md-5 col-sm-6">
                <div class="img_Wrapper">
                    <img src="/{{ $productCat->image_path }}" alt="">
                    <p>{{ $productCat->name }}</p>
                </div>
                <div class="img_Wrapper">
                    <img src="/{{ $vegan->imagePath }}" alt="">
                    <p>{{ $vegan->title }}</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="varPrice">
                    <p>£{{ $vegan->price }}</p>
                    <span class="more_prices">( Monthly Price )</span>
                </div>

                <div class="varPrice">
                    <p>£<?php if ($vegan->weekly_price)
                            echo $vegan->weekly_price;
                        else
                            echo "--";
                        ?>
                    </p>
                    <span class="more_prices">( Weekly Price )</span>
                </div>

                <div class="varPrice">
                    <p>£<?php if ($vegan->price_per_meal)
                            echo $vegan->price_per_meal;
                        else
                            echo "--";
                        ?>
                    </p>
                    <span class="more_prices">( Price Per Meal )</span>
                </div>

            </div>
        </div>
        <div class="row set-meal-detail">
            <div class="col-sm-offset-2 col-sm-5">
                @if(Session::get('first_comp')==0) @else <p>{{ $freesnacks->title }} ({{$fitreatss}})</p>@endif</p>


            </div>
            <div class="col-sm-offset-2 col-sm-3">
                <p>@if(session()->get('first_comp')==0) @else Complimentary @endif</p>
            </div>
        </div>
        <div class="col-md-12 contact-left">
            <div class="well well-sm back-ground-none ">
                <form action="/review-order-store" method="POST">
                    {!! csrf_field() !!}
                    <div class="row">

                        <div class="col-md-12">
                            <div class="col-sm-10 offset-sm-2 set-meal-detail">
                                <p>
                                    Let us know your preferences by answering the questions below!
                                </p>
                                <div class="row">
                                    <ul class="col-md-offset-3 col-sm-offset-1 col-md-8 col-sm-10">
                                        <li>What is your preferred spice level? No spice, mild, medium or hot</li>
                                        <li>What are any vegetables or carbohydrates that you dislike?</li>
                                        <li>What are your goals? Lose weight, maintain muscle, or gain weight</li>
                                        <li>What is your current bodyweight?</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="name">
                                    Message</label>
                                <textarea name="message" id="message" class="form-control" rows="9" cols="25"
                                    placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-offset-9 col-sm-3">
                            <button type="submit" class="btn btn-primary" id="btnContactUs">Confirm Order</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    </div>
    </div>
</section>
@endsection