<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
<!-- <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script> -->
<!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!--  -->
<div class="text-center" style="display:none;">
    @if ($message = Session::get('success'))
    <div class="alert alert-info">
        <h3>{!! $message !!}&nbsp;&nbsp;&nbsp;<span onclick="this.parentElement.style.display='none'">&times;</span>
        </h3>
    </div>
    <?php Session::forget('success');
    $order_id = Session()->get('order_id');
    echo "for order id" . $order_id;
    ?>
    @endif
    @if ($message = Session::get('error'))
    <div class="alert alert-danger">
        <h3>{!! $message !!}&nbsp;&nbsp;&nbsp; <span onclick="this.parentElement.style.display='none'">&times;</span>
        </h3>
    </div>
    <?php Session::forget('error'); ?>
    @endif
</div>

<div class="container">
    <div class="row">
        <div class="text-center stage-tracker ">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/set-meal-plan">Your Plan</a></li>
                <li class="breadcrumb-item "><a href="#">Snacks</a></li>
                <li class="breadcrumb-item active">Shopping Cart</li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-7">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">

                            <div class="col-sm-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                            </div>
                            <div class="col-sm-6 text-right"> <a href="/clear-basket" class="continueshopping btn btn-danger " style="background: black;color: #fff;border: none;">Clear Cart
                                </a></div>

                        </div>
                    </div>
                </div>
                <div class="panel-body ">
                    <div class="row">

                        <div class="col-sm-5 col-xs-4"><strong>Product</strong></div>
                        <div class="col-sm-2 col-xs-2 align-left"><strong>Qty</strong></div>
                        <div class="col-sm-2 col-xs-2 align-left"><strong></strong></div>
                        <div class="col-sm-2 col-xs-2 align-left"><strong>Price</strong></div>
                        <div class="col-sm-1 col-xs-2 align-left"><strong><span class="glyphicon glyphicon-trash"></span></strong></div>
                    </div>
                    <hr>

                    @if(session()->get('first_comp') == 0)

                    @else

                    <div class="row giveborder">

                        <div class="col-sm-4 col-xs-4">
                            <h4 class="product-name"><strong>{{$freesnacks->title}}({{$fitreatss}})</strong></h4>
                            <h4><small>{!! $freesnacks->description !!}</small></h4>
                        </div>
                        <div class="col-sm-2 price-vegan col-xs-2">
                           
                                <h6><strong>Complementary </strong></h6>
                           
                        </div>
                        <div class="col-sm-2 price-vegan col-xs-2">
                           
                        </div>
                        <div class="col-sm-1 price-vegan col-xs-2">
                            
                        </div>
                    </div>

                    @endif
                    <div class="extrasnacks-checkout-box">

                        @if($orderDetail)

                        @foreach($orderDetail as $key=>$order)
                        <div class="row ">
                            <div class="col-sm-5 col-xs-4">
                              {{$order->plan_name}}
                                @if($order->plan_name!='Fitreats')
                                ({{$order->vegan}})

                                @endif
                            </div>

                            <div class="col-sm-2 col-xs-2 clean text-left ">
                            @if($order->plan_name!='Fitreats') 1 @endif
                            </div>

                            
                            <div class="col-sm-2 col-xs-2 clean text-left">
                            @if($order->plan_name!='Fitreats')
                            <p class="mealprice">Monthly </p>
                                <p class="mealprice">Weekly</p>
                                <p class="mealprice">Per Meal </p>
                                @endif
                            </div>


                            <div class="col-sm-2 col-xs-2 del text-left mb10">

                                @if($order->plan_name!='Fitreats')

                                £{{ number_format((float)$order->vegan_price, 2, '.', '')}}
                                £{{number_format((float)$order->weekly_price, 2, '.', '')}}
                                £{{number_format((float)$order->price_per_meal, 2,'.', '')}}
                                @endif

                            </div>


                            <div class="col-sm-1 price-vegan col-xs-2 text-left">
                                <a class="deleteProduct" data-id="{{ $order->id }}" data-token="{{ csrf_token() }}">✖</a>
                            </div>
                        </div>
                            @if($order->plan_name!='Fitreats')
                            
                            @if(@isset($order->extra_snacks))

                 
                                @foreach($extra_snacks_detail as $key=>$extra_order)
                                <div class="row">
                                    <div class="col-sm-5 col-xs-4">
                                        <p class="product-name">
                                            {{$extra_order->extra_snack}}
                                        </p>

                                    </div>
                                    <div class="col-sm-2 col-xs-2 align-left">
                                        @if($order->plan_name!='Fitreats')
                                        {{$extra_order->quantity}}
                                        @endif
                                    </div>
                                    <div class="col-sm-2 col-xs-2 align-left">
                                    
                                    </div>
                                    <div class="col-sm-2 price-vegan col-xs-2 text-left" style="margin-left: -8px;">£{{ number_format((float)$extra_order->rate, 2, '.', '')}}    
                                      
                                    </div>
                                    <div class="col-sm-2 price-vegan col-xs-1 text-left">
                                        

                                    </div>
                                </div>
                                @endforeach
                     

                            @endif
                            @else
                            <div class="row">
                            <div class="col-sm-5 col-xs-4">
                                <p class="product-name">{{$order->vegan}} </p>

                            </div>
                            <div class="col-sm-2 col-xs-2 align-left">
                               {{$order->quantity}}
                            </div>
                            <div class="col-sm-2 col-xs-2 align-left">
                            
                            </div>

                            <div class="col-sm-2 price-vegan col-xs-2 text-left">£{{ number_format((float)$order->extra_price, 2, '.', '')}}
                            </div>
                            <div class="col-sm-1 col-xs-2 align-center">
                            
                            </div>
                            </div>
                            @endif

                       

                        @endforeach
                        @endif
                    </div>
                    <div class="row">

                    <div class="col-sm-9 col-xs-8" style="padding-top: 14px;">
                       
                                    Sub Total:
                              
                            
                             
                    </div>

                    <div class="col-sm-3 col-xs-4 text-left" style="padding-top: 14px; margin-left:-15px;font-size:13px;">
                       
                   

                       £ {{$sub_total_in_decimal}}
                
       </div>
</div>
                 

                </div>




            </div>
            <div class="row">
                <div class="col-sm-12">
                    Once you've placed your order, you'll receive a form to select your meals, let us know of any allergies / preferences and your delivery instructions.
                    You can change your meals every week.
                </div>

                <div class="col-sm-12 clearandcontinue">
                    <a href="/fitreats-and-supplements" class="continueshopping continue-shopping-btn">
                         Continue Shopping <br> <span style="font-size:16px;">Fitreats</span></a> </div> </div> </div> <div class="col-sm-5">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> SHOPPING CART TOTAL</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                    Sub-total:
                                                    </td>
                                                    <td class="text-right">
                                                        £ {{$sub_total_in_decimal}}
                                                    </td>
                                                </tr>
                                            </table>

                                            <hr>
                                        </div>

                                        <div class="col-sm-12">

                                        @if( $sub_total_in_decimal!=0)

                                            <table width="100%">
                                                <tr>
                                                    @if(isset($delivery_info))
                                                    <td> Delivery Charge : {{$delivery_info[0]->deliveries}} deliveries  x £{{$delivery_info[0]->delivery_rate}}</td>
                                                    <td>
                                                       </td>
                                                    <td class="text-right"> £{{number_format((float)$delivery_info[0]->total_delivery_cost, 2, '.', '')}}</td> @endif

                                                </tr>
                                            </table>
                                            @endif

                                            <hr>
                                        </div>
                                        @if(session()->get('coupon'))
                                        <div class="col-sm-12">
                                   
                                        <table width="100%">
                                                <tr>
                                           
                                                    <td><span style="color:green;">Discount</span></td>

                                                    <td>
                                                       </td>
                                                    <td class="text-right"> <span id="discountAmt" style="color:green;">
                                                        <?php
                                                        $totAmt = $sub_total_in_decimal;
                                                        if (session()->get('coupon')) {
                                                            if (session()->get('coupon_type') == 1) {

                                                                $disc = session()->get('coupon') * 0.01 * $totAmt;
                                                                $disc = number_format((float)$disc, 2, '.', '');

                                                            } else {

                                                                $disc = session()->get('coupon');

                                                            }
                                                        }
                                                        ?>
                                                    </span>
                                            
                                                    <span style="color:green;"><?php echo '£ ' . $disc; ?></span></td>

                                                </tr>
                                            </table>
                                          
                                           
                                        <hr>



                                        </div>
                                        @endif
                                        <div class="col-sm-12">
                                            <table width="100%">
                                                <tr>
                                                    <td> <strong>TOTAL PRICE: </strong></td>
                                                    <td class="text-right">

                                                    @if( $sub_total_in_decimal!=0)

                                                        @if(isset($delivery_info))
                                                        <span id="totalAmt">
                                                            <?php
                                                            $totalAmount = $sub_total_in_decimal;
                                                            if (session()->get('coupon')) {
                                                                if (session()->get('coupon_type') == 1) {

                                                                    $totalAmount = $totalAmount - session()->get('coupon') * 0.01 * $totalAmount;
                                                                    $totalAmount = number_format((float)$totalAmount, 2, '.', '');
                                                                    $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                                                
                                                                } else {

                                                                    $totalAmount = $totalAmount - session()->get('coupon');
                                                                    $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                                               
                                                                }
                                                            } else {

                                                                $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                                           
                                                            }
                                                            ?>
                                                        </span>
                                                       <strong> £{{ number_format((float)$totAmt, 2, '.', '') }} </strong>

                                                        @endif
                                                        @else
                                                        <strong> £0.00 </strong>
                                                        @endif
                                                        </strong>

                                                    </td>
                                                </tr>

                                            </table>
                                            <hr>
                                        </div>
                                        @if($setMealCount==1)
                                        <div class="col-sm-12">
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        Monthly Total Price:
                                                    </td>
                                                    <td class="text-right">£ {{  number_format((float)$totAmt, 2, '.', '') }}</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Weekly Total Price:
                                                    </td>
                                                    <td class="text-right">£ {{ number_format((float)$totAmt/4, 2, '.', '')}}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <hr>
                                        @endif


                                                                              <div class="cols-sm-12">
                                            <div class="col-sm-12 couponDiv">
                                                <div class="row">
                                                    <div class="col-sm-6"><button type="button" class="btn btn-primary btn-o" data-toggle="modal" data-target="#haveCoupon">
                                                            <b>Have a Coupon ?</b>
                                                        </button></div>
                                                    <div class="col-sm-6">
                                                        <div id="paypal-button">
                                                            @auth
                                                            <a href="/dashboard">Place My Order</a>
                                                            @endauth
                                                            @guest
                                                            <a href="/guest-checkout">Place My Order</a>
                                                            @endguest
                                                          
                                                            <!-- <a href="/redirect_to_register">Place My Order</a> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <p class="extra-added" ><span style="font-weight:900;font-size:18px;">How Delivery Works?:</span>
                                                <p><b><i>South London and London deliveries:</i></b></p>
                                                <ul style="margin-left: 16px;line-height: 30px;">
                                                    <li>
                                                        Your delivery will be on MONDAYS
                                                    </li>
                                                    <li>
                                                        You will receive a 1-hour delivery slot on Sunday evening.
                                                    </li>
                                                </ul>

                                                <p><b><i> Anywhere outside of London:</i></b></p>

                                                <ul style="margin-left: 16px;line-height: 30px;">
                                                    <li> Your delivery will be on TUESDAYS </li>
                                                    <li> For deliveries well outside of London, you will receive a 1-hour delivery slot on Tuesday morning. </li>
                                                    <li> When you receive your email/text, please click the link to assign a safe place for the courier to leave your meals in case you're not in. </li>

                                                    <li> Kindly note that it is your responsibility to coordinate this with the courier (DPD outside of London/our RRG delivery driver inside of london) if you will be unavailable to receive your meals or need a change of delivery instructions. </li>

                                                    <li> We recommend on delivery, you freeze the meals you plan to have at the end of the week to preserve its freshness, as all the meals are cooked fresh with no preservatives. </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                </div>

                <!--Code for coupon code popup -->

                <div class="modal fade text-center" id="haveCoupon" tabindex="-1" role="dialog" aria-labelledby="couponTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Enter the Coupon Code</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/applyCoupon" method="POST">
                                    {{ csrf_field() }}
                                    <div class="form-group no-pb row">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-5">
                                            <input type="text" class="form-control no-border" name="coupon" required>
                                        </div>
                                        <button class="btn btn-primary coupon-popup-btn ajaxCall no-m col-sm-3">Use Coupon</button>
                                    </div>
                                </form>
                            </div>
                            <script type="text/javascript">
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    }
                                });

                                $(".ajaxCall").click(function(e) {
                                    e.preventDefault();
                                    var coupon = $("input[name=coupon]").val();
                                    $.ajax({
                                        type: 'POST',
                                        url: '/applyCoupon',
                                        data: {
                                            coupon: coupon
                                        },
                                        success: function(data) {
                                            switch (data.status) {
                                                case 1:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                                case 2:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                                case 3:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                                case 4:
                                                    alert(data.message);
                                                    $("#haveCoupon").modal('hide');
                                                    break;
                                            }
                                            window.location.reload();
                                        },
                                        error: function(e) {
                                            console.log(e);
                                        }
                                    });
                                });
                            </script>

                            <script>
                                $(".deleteProduct").click(function() {
                                    var id = $(this).data("id");
                                    target = $(this);

                                    $.ajaxSetup({
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        }
                                    });
                                    $.ajax({
                                        url: "deleteSetMeal/" + id,
                                        type: 'delete', // replaced from put
                                        dataType: "JSON",
                                        data: {

                                            "id": id // method and token not needed in data
                                        },
                                        success: function(response) {

                                            //target.remove();

                                            target.closest('.row').remove();
                                            location.reload(true);

                                            console.log(response); // see the reponse sent
                                        },
                                        error: function(xhr) {
                                            console.log(xhr.responseText); // this line will save you tons of hours while debugging
                                            // do something here because of error
                                        }
                                    });
                                });
                            </script>


                        </div>
                    </div>
                </div>