<script type="text/javascript">
//appends an "active" class to .popup and .popup-content when the "Open" button is clicked
$(".open").on("click", function() {
    $(".popup-overlay, .popup-content").addClass("active");
});

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
$(".close, .popup-overlay").on("click", function() {
    $(".popup-overlay, .popup-content").removeClass("active");
});
</script>
<?php

use App\Productcategory;

$cat = Productcategory::findOrFail(Session::get('prod_cat_id'));
?>
<div class="send-product-form-radio-button">
    <div class="set-meal-parallax">
        <div class="container">
            <div class="row">
                <div class="text-center stage-tracker">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/set-meal-plan">Your Plan</a></li>
                        <li class="breadcrumb-item"><a href="/weekplan/{{ $cat->id }}/1">{{ $cat->name }}</a></li>
                        @if($cat->comp_dess == 1)
                        <li class="breadcrumb-item active">Add a Complimentary</li>
                        @endif
                        <li class="breadcrumb-item"><a href="#">Review Order</a></li>
                        @if($cat->paid_dess == 1)
                        <li class="breadcrumb-item"><a href="#">Add a Snack</a></li>
                        @endif
                        <li class="breadcrumb-item "><a href="#">Shopping Cart</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="">
                <div class="set-meal-parallax">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 parallax-inside-set-meal">
                                <div class=" col-sm-12">
                                    <h3>
                                        {{-- With your chosen <strong> {{$vegan->title}} </strong> meal plan, you will
                                        receive a free serving of a high-protein, low-carb FiTreats a day! <br> Please
                                        select the FiTreats of your choice below:--}}

                                        With your chosen meal plan you will receive <strong> ({{ $fitreatss}}) </strong>
                                        fiTreats. These are tasty high protein, low carb vegan snacks that will control
                                        your sweet tooth in between meals or through the day! Please select your
                                        FiTreats below:
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="col-sm-12 product-vegan-page">
                                @foreach($snacks as $prod)
                                <div class="col-sm-3 product-vegan-radio">
                                    <div id="ck-button" style="background-image:url('/{{$prod->imagePath}}')">
                                        <label>
                                            <input id='testName' name="vegan" type="radio" style="display:none;"
                                                value="{{$prod->id}}">
                                            <a href="/add_review_order_to_checkout/{{$prod->id}}">
                                                <span>
                                                    <img src="/{{$prod->imagePath}}">
                                                </span>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="product-vegan-item-desc">
                                        <label for="prod-title"
                                            class="col-md-12 control-label product-title-checkout">{{$prod->title}}</label>
                                        <p><?php echo strip_tags($prod->description) ?></p>

                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-center" style="font-size: 20px; margin-top: 35px;">
    <a href="/weekplan/{{$vegan->cat_id}}/{{$vegan->week_id}}" class="back-btn"><i class="fa fa-arrow-left"></i> </a>
</div>