@if(session()->get('coupon'))
<?php 
session()->forget('coupon');
session()->forget('coupon_type');
session()->forget('coupon_code');
?>
@endif
<div class="send-product-form-radio-button">


    <div class="row">
        <div class="col-md-12 ">
            <div class="set-meal-parallax ">
                <div class="container">
                    <div class="row">
                        <div class="text-center stage-tracker">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/set-meal-plan">Your Plan</a></li>
                                <li class="breadcrumb-item "><a href="/weekplan/{{ $plan->id }}/1">{{ $plan->name }}</a></li>
                                <li class="breadcrumb-item active">Add a Snack</li>
                                <li class="breadcrumb-item "><a href="#">Shopping Cart</a></li>
                            </ol>
                        </div>
                        <div class="instruction col-sm-12">
                            <h4>
                            You can also purchase FiTreats, our bestselling high-protein vegan snacks!  Please choose below 
                            <i class="fa fa-smile"></i>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                {!! Form::open(array('url'=>'/proceed-checkout','method'=>'post','files' => true )) !!}
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="col-sm-12 product-vegan-page">
                            <input  name="vegan_id" type="hidden" value="<?php echo $vegan_id; ?>">
                            <input  name="freesnacks_id" type="hidden" value="<?php echo $freesnacks_id; ?>">
                            @foreach($snacks as $prod)
                            <div class="col-sm-4 product-vegan-radio ">
                                <div id="ck-button" style="background-image:url('/{{$prod->imagePath}}')">
                                    <label>
                                        <input id='testName' name="extrasnacks_id[]" type="checkbox" style="display:none;" value="{{$prod->id}}">
                                        <span>
                                            <img src="/{{$prod->imagePath}}">
                                            <h4 class="fromprice">&pound;{{number_format((float)$prod->price, 2, '.', '') }} for 12</h4>
                                        </span>
                                    </label>
                                </div>
                                <div class="product-vegan-item-desc">
                           
                                    <label for="prod-title" class="col-md-12 control-label product-title-checkout">{{$prod->title}}</label>
                                    <p><?php echo strip_tags($prod->description) ?></p>
                                </div>        
                            </div>
                           @endforeach
                        </div>
                        <div class="form-group ">
                            <div class="text-cen col-sm-12" style="font-size: 20px; margin-top: 35px;">
                                <a href="/add_vegan_to_checkout/{{$vegan_id}}" class="back-btn"> <i class="fa fa-arrow-left"></i></a>
                                <input type="hidden" value= {{ Session::get('prod_cat_id')}} name="cat_name" >
                                <button type="submit" class="btn btn-primary" style="width: 19%;background-color: #febe14;border-color: #febe14;" >
                                    Proceed To Checkout
                                </button>
                            </div>
                        </div>    
                    </div>
                </div>
                {{ Form:: close() }}
            </div>
        </div>
    </div>
</div>
