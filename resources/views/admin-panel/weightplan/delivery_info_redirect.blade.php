@extends('layouts.app')

@section('content')
    <div class="container-fluid progressive-natti-body">
        <div class="row">
        
            <?php  use Illuminate\Support\Facades\Auth;
            $userInfo=Auth::User();

            if($userInfo)
            {
                if($userInfo->role_id==1){ ?>
           <div class="col-sm-12">

                    @include('admin-panel.weightplan.delivery_info')
                </div>
                <?php }
                else
                {
                ?>
                <div class="col-sm-12">

                    @include('admin-panel.weightplan.delivery_info')
                </div>
               

                <?php } } else{ ?>
                <div class="col-sm-12">

                    @include('admin-panel.weightplan.delivery_info')
                </div>
               
                <?php } ?>
        </div>
    </div>

@endsection


