<!-- <section class="delivery-form">
    <div class="container">
      <h3>Delivery Info</h3>
      <div class="row">
       <form method="post" action="/paypal-checkout">
         {!! csrf_field() !!}
        <label>First Name :</label>
        <input type="text" name="fname" required placeholder="First Name" />
        <label>Last Name :</label>
        <input type="text" name="lname" required placeholder="Last Name" />
        <label>Shipping Address :</label>
        <input type="text" name="sadd" placeholder="Shipping Address" required />
        <label>Delivery Address :</label>
        <input type="text" name="ddate" placeholder="Delivery Address" required />
        <input type="hidden" name="amount" value="{{$vegan->price+$tol_extra_snacks_price+$tol_price}}">
        <input type="hidden" name="vegan_title" value="{{$vegan->title}}">
        <a href="#" class="btn btn-left" id="back-btn">Back</a>
        <button class="btn-right" type="submit">Checkout</button>
       </form>
      </div>
    </div>
  </section>
 -->
  <section class="delivery-form">
				<div class="container">
						<div class="row">
						<div class="col-sm-3">
						</div>
							<form id="delivery" class="col-sm-6" method="post" action="/paypal-checkout">
							{!! csrf_field() !!}
							<input type="hidden" name="amount" value="{{$vegan->price+$tol_extra_snacks_price+$tol_price}}">
        					<input type="hidden" name="vegan_title" value="{{$vegan->title}}">
							<h3><span>Delivery Info</span></h3>
							<table>
							<tr>
							<td>
								<label class="label-ctn">First Name :</label>
								</td>
								<td>
								<input type="text" name="fname" required placeholder="First Name" />
								</td>
							<tr>
							<td>
								<label class="label-ctn">Last Name :</label>
								</td>
								<td>
								<input type="text" name="lname" required placeholder="Last Name" />
								</td>
							<tr>
							<td>
								<label class="label-ctn">Shipping Address :</label>
								</td>
								<td>
								<input type="text" name="sadd" placeholder="Shipping Address" required />
								</td>
							<tr>
							<td>
								<label class="label-ctn">Delivery Address :</label>
								</td>
								<td>
								<input type="date" name="ddate" placeholder="Delivery Date" required />
								</td>
							<tr>
							<td>
								<!-- <button class="btn-left" id="back-btn">Back</button> -->
								</td>
								<td>
								<button class="btn-right" type="submit">Checkout</button>
								</table>
							</form>
						</div>
				</div>
		</section>