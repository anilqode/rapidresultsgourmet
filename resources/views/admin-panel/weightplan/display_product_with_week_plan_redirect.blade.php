@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <?php  use Illuminate\Support\Facades\Auth;
            $userInfo=Auth::User();

            if($userInfo)
            {
                if($userInfo->role_id==1){ ?>
           <div class="col-sm-12">
                    @include('admin-panel.weightplan.display_product_with_week_plan')
                </div>
                <?php }
                else
                {
                ?>
                <div class="col-sm-12">
                    @include('admin-panel.weightplan.display_product_with_week_plan')
                </div>
           

    <?php } } else{ ?>
                <div class="col-sm-12">
                @include('admin-panel.weightplan.display_product_with_week_plan')
                </div>
               

    <?php            }?>
        </div>
    </div>

@endsection


