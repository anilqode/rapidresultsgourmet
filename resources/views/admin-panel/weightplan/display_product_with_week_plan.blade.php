<script type="text/javascript">
function showModal(name, id, img, complimentary) {
    var imgPath = '/' + img;
    var proceedId = '#proceed' + id;
    var imgId = '#imgPop' + id;
    var popupClass = '.popupModal' + id;
    var modalItem = 'modal_item' + id;
    var complimentarySnacks = parseInt(complimentary);
    const demoClasses = document.querySelectorAll(popupClass);
    demoClasses[0].style.display = 'block';

    // document.getElementById('popupModal').style.display='block';
    document.getElementById(modalItem).value = name;

    var category_id = document.getElementById("cat_id").value;

    if (!complimentarySnacks) {
        var proceedAction = '/add-snacks-to-checkout/' + id;
    } else {
        var proceedAction = '/add_vegan_to_checkout/' + id;
    }


    $(imgId).attr("src", imgPath);

    $(proceedId).attr("href", proceedAction);
}

function cancelModal(id) {
    var popupClass = '.popupModal' + id;

    const demoClasses = document.querySelectorAll(popupClass);
    demoClasses[0].style.display = 'none';
}
</script>

<div class="row">
    <div class="col-md-12 ">
        <div class="">
            <div class="set-meal-parallax">
                <div class="container">
                    <div class="row">
                        <div class="text-center stage-tracker">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/set-meal-plan">Your Plan</a></li>
                                <li class="breadcrumb-item active">{{ $mealPlan }}</li>

                                <li class="breadcrumb-item"><a href="#">Add a Snack</a></li>
                                <li class="breadcrumb-item "><a href="#">Shopping Cart</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-offset-4 col-sm-4 parallax-inside-set-meal">
                            <h2>
                                <input type="text" id="mealPlan" name="mealPlan" value="{{ $mealPlan }}" readonly>
                                <input type="hidden" name="cat_id" id="cat_id" value="{{Session::get('prod_cat_id')}}">
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body weekplan">
                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="col-sm-12 product-vegan-page">
                            @foreach($product as $prod)
                            <div class="col-sm-4 product-vegan-radio ">
                                <div id="ck-button" style="background-image:url('/{{$prod->imagePath}}')">
                                    <label>
                                        <input id='testName' name="vegan" type="radio" style="display:none;"
                                            value="{{$prod->id}}">
                                        @if(Session::get('first_comp')==0)
                                        <a href="/add-snacks-to-checkout/{{$prod->id}}">
                                            <span>
                                                <img src="/{{$prod->imagePath}}">
                                            </span>
                                        </a>
                                        @else
                                        <a href="/add_vegan_to_checkout/{{$prod->id}}">
                                            <span>
                                                <img src="/{{$prod->imagePath}}">
                                            </span>
                                        </a>
                                        @endif
                                    </label>
                                </div>
                                <div class="product-vegan-item-desc">
                                    <label for="prod-title"
                                        class="col-md-12 control-label product-title-checkout">{{$prod->title}}</label>
                                    <div id="variantPrice">
                                        <h5 class="fromprice"><span>Monthly Price: &pound;{{ number_format((float)$prod->price, 2, '.', '')  }}</h5>
                                        <h5 class="fromprice">
                                            <span>Weekly Price:</span> &pound;
                                            <?php if ($prod->weekly_price)
                                                echo number_format((float)$prod->weekly_price, 2, '.', '');
                                            else
                                                echo "--";
                                            ?>
                                        </h5>
                                        <h5 class="fromprice">
                                            <span>Price per Meal:</span> &pound;
                                            <?php if ($prod->price_per_meal)
                                                echo number_format((float)$prod->price_per_meal, 2, '.', '');
                                            else
                                                echo "--";
                                            ?>
                                        </h5>
                                    </div>
                                    <br />
                                    @if(Session::get('first_comp')==0)
                                    <a href="/add-snacks-to-checkout/{{$prod->id}}" class="btn btn-light dblock">
                                        Choose Plan
                                    </a>
                                    @else
                                    <a href="/add_vegan_to_checkout/{{$prod->id}}" class="btn btn-light  dblock">
                                        Choose Plan
                                    </a>
                                    @endif
                                    <p>
                                        {{ str_limit(strip_tags($prod->description), 50) }}
                                        <a id="view-product" class="btn btn-light"
                                            onclick="showModal('{{$prod->title}}', '{{$prod->id}}', '{{$prod->imagePath}}', '{{ session()->get('first_comp') }}')">Read
                                            More</a>
                                        @if (strlen(strip_tags($prod->description)) > 50)

                                        @endif

                                    </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="text-center" style="font-size: 20px;">
    <a href="/set-meal-plan" class="back-btn"><i class="fa fa-arrow-left"></i> </a>
</div>
@foreach($product as $prod)
<article class="modal popupModal{{$prod->id}}" id="popupModal">
    <section class="modelcontent_wrap">
        <div class="header_modal">
            <h5 class="title_vegCarb">
                <input type="text" class="count_edit" style="width: 75%;" name="food_item" id="modal_item{{$prod->id}}"
                    readonly>
                <span onclick="cancelModal({{$prod->id}});" class="close_popup">&times;</span>
            </h5>
        </div>
        <div class="col-sm-12 text-center">
            <img src="/{{$prod->imagePath}}" width="300px">
        </div>
        <div class="col-sm-12">
            <?php echo ($prod->description) ?>
        </div>
        <hr>
        <div class="footer_modal">
            <a class="btn btn-primary close_popup" onclick="cancelModal({{$prod->id}});">
                Cancel
            </a>
            <a id="proceed{{$prod->id}}" class="btn btn-primary close_popup pull-right">
                Proceed
            </a>
        </div>
    </section>
</article>
@endforeach