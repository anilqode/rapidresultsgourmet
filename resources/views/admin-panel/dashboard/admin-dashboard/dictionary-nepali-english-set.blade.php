

<?php
if($category!=null)
{ ?>


<section class="practical-session-questionsets">
    <div class="practical-session-container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Dictionary Test {{$cat}} </h4>
                @if(!$category->isEmpty())
                    @foreach($category as $cat)
                        <div class="col-sm-2 practical-questionsets">
                            <a href="/user-question-set/{{$cat->dictionary_cat_id}}/{{$cat->id}}"><p>{{$cat->name}}</p></a>
                        </div>

                    @endforeach
                @else
                    <div class="not-active-service">
                        "Sorry",You Have not yet Paid for this Service.Pay for this Course and Visit back for access.
                    </div>
                @endif

            </div>
        </div>
    </div>
</section>

<?php


}
else{ ?>
<section class="practical-session-questionsets">
    <div class="practical-session-container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Dictionary Test {{$cat}}</h4>
                <div class="not-active-service">

                    "Sorry",You Have not yet Paid for this Service.Pay for this Course and Visit back for access.

                </div>
            </div>
        </div>
    </div>
</section>
<?php }
?>
