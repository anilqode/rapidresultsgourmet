<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
@extends('layouts.app')

@section('content')


@endsection
@section('nav')
    <section class="dashboard-page">
        <div class="container-fluid">
            <div class="row">
                <?php
                use Illuminate\Support\Facades\Auth;
                $userInfo=Auth::User();
                if($userInfo)
                {
                if($userInfo->role_id==1){
                ?>

                <div class="col-sm-3 col-md-3 dashboard-nav">
                    @include('admin-panel/left-nav')
                </div>
                <div class="col-sm-9 col-md-9 dashboard-body">

                    @include('admin-panel/dashboard/admin-dashboard/DashboardBody')
                    @include('admin-panel/dashboard/admin-dashboard/DashboardHeader')
                </div>
                <?php } else { ?>
                 <div class="col-sm-3 col-md-3 dashboard-nav">
                    @include('admin-panel/user-nav')
                </div>
                <div class="col-sm-9">
                    @include('admin-panel/dashboard/admin-dashboard/DashboardBody')
                    @include('admin-panel/dashboard/admin-dashboard/DashboardHeader')
                </div>
            </div>
            <?php     }
            } else {
                return redirect('/login');
            }
            ?>
        </div>
        </div>
    </section>
@endsection
