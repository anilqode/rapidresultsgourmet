<?php
if(!isset($_SESSION))
{
    session_start();
}
?>

<section class="practical-session-category">
    <div class="practical-session-container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="practical-session-category">Dictionary Category</h4>
                @foreach($practical_category as $cat)
                    <div class="col-sm-3 category-box">
                        <a href="/dictionary-test-session/{{$cat->id}}"><p>{{$cat->name}}</p></a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>

</section>

</section>