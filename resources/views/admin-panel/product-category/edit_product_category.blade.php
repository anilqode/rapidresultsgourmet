@inject('getChecked', 'App\Services\DessertCheckbox')

<div class="page-edit-section">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="create-category-form">

        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>UPDATE PRODUCT CATEGORIES</h4>
                    </div>

                    <div class="panel-body">
                        {!! Form::model($CategoryInfos,['method'=>'PATCH','files' =>
                        true,'url'=>'/product-category/'.$CategoryInfos->id.'/update']) !!}
                        {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="panel panel-default ">
                                    <div class="panel-body">
                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="name" class="col-md-4 control-label">Name</label>
                                            <div class="col-md-6">
                                                <input id="name" type="text" class="form-control" name="name"
                                                    value="{{ $CategoryInfos->name }}" required autofocus>

                                                @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                            <label for="slug" class="col-md-4 control-label">Slug</label>

                                            <div class="col-md-6">
                                                <input id="slug" type="text" class="form-control" name="slug"
                                                    value="{{ $CategoryInfos->slug }}" required autofocus>

                                                @if ($errors->has('slug'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('slug') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                            <label for="slug" class="col-md-4 control-label">Meal Plan Type</label>

                                            <div class="col-md-6">
                                                <select class="form-control" name="meal_plan_type"
                                                    onchange="checkVisibility(this)" id="mealPlanTye" required>
                                                    @if($CategoryInfos->meal_plan_type == 's')
                                                    <option value="s">Set Meal Plan</option>
                                                    <option value="n">Fitreats and More</option>
                                                    @else
                                                    <option value="n">Fitreats and More</option>
                                                    <option value="s">Set Meal Plan</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                            <label for="slug" class="col-md-4 control-label">Price ( For Display only
                                                )</label>

                                            <div class="col-md-6">
                                                <input id="price" type="text" class="form-control" name="price"
                                                    value="{{ $CategoryInfos->price }}" autofocus>

                                                @if ($errors->has('slug'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('price') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                            <label for="body" class="col-md-4 control-label">Description</label>
                                            <div class="col-md-6">
                                                <textarea id="body" class="form-control" name="body"
                                                    value="{{ $CategoryInfos->body }}" autofocus></textarea>
                                                @if ($errors->has('body'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('body') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div style="display:none;" id="fitBase"
                                            class="form-group{{ $errors->has('total_delivery_cost_fit') ? ' has-error' : '' }}">
                                            <label for="total_delivery_cost_fit" class="col-md-4 control-label">Total
                                                Delivery Price</label>
                                            <div class="col-md-6">
                                                <input class="form-control" name="total_delivery_cost_fit"
                                                    value="{{ $CategoryInfos->total_delivery_cost_fit }}">
                                                @if ($errors->has('total_delivery_cost_fit'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('total_delivery_cost_fit') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div id="visibility">
                                            <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                                <label for="slug" class="col-md-4 control-label">Featured Image</label>
                                                <div class="col-md-2">
                                                    <img src="/{{ $CategoryInfos->image_path }}" alt=""
                                                        style="width: 60%">
                                                </div>
                                                <div class="col-md-4">
                                                    {!!
                                                    Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile'))
                                                    !!}
                                                </div>

                                            </div>
                                            <div class="form-group {{ $errors->has('slug') ? ' has-error' : '' }}">
                                                <label for="slug" class="col-md-4 control-label"> Short Intro</label>

                                                <div class="col-md-6">
                                                    <input id="short_intro" type="text" class="form-control"
                                                        name="short_intro" value="{{ $CategoryInfos->short_intro }}"
                                                        autofocus>

                                                    @if ($errors->has('slug'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('short_intro') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group{{ $errors->has('deliveries') ? ' has-error' : '' }}">
                                                <label for="deliveries" class="col-md-4 control-label">Total
                                                    Deliveries</label>
                                                <div class="col-md-6">
                                                    <input type="number" id="deliveries" class="form-control"
                                                        name="deliveries" value="{{ $CategoryInfos->deliveries }}"
                                                        autofocus>
                                                    @if ($errors->has('deliveries'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('deliveries') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div
                                                class="form-group{{ $errors->has('delivery_rate') ? ' has-error' : '' }}">
                                                <label for="delivery_rate" class="col-md-4 control-label">Delivery
                                                    Rate</label>
                                                <div class="col-md-6">
                                                    <input type="text" id="delivery_rate" class="form-control"
                                                        name="delivery_rate" value="{{ $CategoryInfos->delivery_rate }}"
                                                        autofocus>
                                                    @if ($errors->has('delivery_rate'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('delivery_rate') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                            $(document).ready(function() {
                                                $("#delivery_rate").change(function() {

                                                    a = document.getElementById("deliveries").value;

                                                    b = document.getElementById("delivery_rate").value;

                                                    document.getElementById("total_delivery_cost")
                                                        .value = (a * b).toFixed(2);
                                                });
                                                $("#deliveries").change(function() {

                                                    a = document.getElementById("deliveries").value;

                                                    b = document.getElementById("delivery_rate").value;

                                                    document.getElementById("total_delivery_cost")
                                                        .value = (a * b).toFixed(2);
                                                });
                                                a = document.getElementById("deliveries").value;

                                                b = document.getElementById("delivery_rate").value;

                                                document.getElementById("total_delivery_cost")
                                                    .value = (a * b).toFixed(2);
                                            });
                                            </script>
                                            <div
                                                class="form-group{{ $errors->has('total_delivery_cost') ? ' has-error' : '' }}">
                                                <label for="total_delivery_cost" class="col-md-4 control-label">Total
                                                    Delivery Price</label>
                                                <div class="col-md-6">
                                                    <input id="total_delivery_cost" class="form-control" readonly
                                                        name="total_delivery_cost"
                                                        value="{{ $CategoryInfos->total_delivery_cost }}" autofocus>
                                                    @if ($errors->has('total_delivery_cost'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('total_delivery_cost') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>


                                            <div class="form-group{{ $errors->has('dess') ? ' has-error' : '' }}">
                                                <label for="dessert" class="col-md-4 control-label">Dessert</label>
                                                <div class="col-md-8 col-sm-8">
                                                    {{-- {{ Form::checkbox('comp_dess',$CategoryInfos->comp_dess, array("class" => "form-control margin-bottom-12", 'id' => 'comp_dess')) }}--}}
                                                    <input type="checkbox" name="comp_dess" id="comp_dess"
                                                        onchange="displayComp(this, '#comp', '')"
                                                        {{$getChecked->checkNotificationSettingsForComp($CategoryInfos->comp_dess)}}>
                                                    <label for="comp_dess">Complimentary Dessert</label>
                                                    <input type="checkbox" name="paid_dess"
                                                        onchange="displayComp(this, '#paid_comp', '#paid_comp')"
                                                        {{$getChecked->checkNotificationSettingsForComp1($CategoryInfos->paid_dess)}}
                                                        id="paid_dess">
                                                    <label for="paid_dess">Paid Dessert</label>
                                                </div>
                                            </div>

                                            <div id="comp"
                                                class="form-group{{ $errors->has('fit') ? ' has-error' : '' }} {{ $CategoryInfos->comp_dess == '1' ? 'show': 'hide'}}">
                                                <label for="fit" class="col-md-4 control-label">No of Complimentary
                                                    Fitreats</label>
                                                <div class="col-md-6">
                                                    <input type="number" id="fit" class="form-control" name="fit"
                                                        value="{{ $CategoryInfos->fit }}" autofocus>
                                                    @if ($errors->has('fit'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('fit') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div id="paid_comp" class="form-group{{ $errors->has('paidDes') ? ' has-error' : '' }}
                                                {{ $CategoryInfos->paid_dess == '1' ? 'show': 'hide'}}">
                                                <label for="paidDes" class="col-md-4 control-label">No of Paid
                                                    Fitreats</label>
                                                <div class="col-md-6">
                                                    <input type="number" id="paidDes" class="form-control"
                                                        name="paidDes" autofocus value="{{ $CategoryInfos->paidDes }}">
                                                    @if ($errors->has('fit'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('paidDes') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label">Best Seller / Most Popular</label>
                                                <div class="col-md-6">
                                                    <input type="checkbox" id="best_seller" name="best_seller"
                                                        value="1">
                                                    <label for="best_seller">Best Seller</label>
                                                    <input type="checkbox" id="most_popular" name="most_popular"
                                                        value="1">
                                                    <label for="most_popular">Most Popular</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" class="btn btn-primary">
                                                    Update
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{ Form:: close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(() => {
    if ($('#mealPlanTye').val() == 'n') {
        document.getElementById('visibility').style.display = "none";
        document.getElementById('fitBase').style.display = "block";
    } else {
        document.getElementById('visibility').style.display = "block";
        document.getElementById('fitBase').style.display = "none";
    }
});

function checkVisibility(el) {
    let mealType = el.value;
    let dontDisplay = document.getElementById('visibility');
    let doDisplay = document.getElementById('fitBase');

    let short_intro = document.getElementById('short_intro');
    let fit = document.getElementById('fit');
    let comp_dess = document.getElementById('comp_dess');

    if (mealType == "n") {
        short_intro.value = null;
        fit.value = null;
        comp_dess.value = null;
        dontDisplay.style.display = "none";
        doDisplay.style.display = "block";
    } else {
        dontDisplay.style.display = "block";
        doDisplay.style.display = "none";
    }
}

function displayComp(el, id) {
    if ($(id).hasClass('show')) {
        $(id).removeClass('show');
        $(id).addClass('hide');
        temp = "0";
    } else {
        $(id).removeClass('hide');
        $(id).addClass('show');
        temp = "1";
    }
}

// function displayComp(el, id) {
//     if (!el.classList.toggle('active')) {
//         $(id).hide();
//     } else {
//         $(id).show();
//     }
// }
</script>