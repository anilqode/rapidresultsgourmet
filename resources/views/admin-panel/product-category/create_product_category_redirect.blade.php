<?php
if(!isset($_SESSION))
{
    session_start();
}
?>

@extends('layouts.app')


@section('content')
    <div class="container-fluid progressive-natti-body">
        <div class="row">
            <div class="col-sm-3">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-9">
                @include('admin-panel.product-category.create_product_category')
            </div>
            <div class="col-sm-12">
                <div class="container">
                @include('admin-panel.product-category.display_product_category')
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection