<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="create-category-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>CREATE PRODUCT CATEGORIES</h4>
                </div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/store-product-categories','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">
                                <div class="panel-body">
                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-4 control-label">Name</label>
                                        <div class="col-md-6">
                                            <input id="name" type="text" class="form-control" name="name"
                                                value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                        <label for="slug" class="col-md-4 control-label">Slug</label>

                                        <div class="col-md-6">
                                            <input id="slug" type="text" class="form-control" name="slug"
                                                value="{{ old('slug') }}" required autofocus>

                                            @if ($errors->has('slug'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('slug') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                        <label for="slug" class="col-md-4 control-label">Meal Plan Type</label>

                                        <div class="col-md-6">
                                            <select class="form-control" name="meal_plan_type"
                                                onchange="checkVisibility(this)" required>
                                                <option value="0">Select Plan</option>
                                                <option value="s">Set Meal Plan</option>
                                                <option value="n">Fitreats and More</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}" id="dispPr">
                                        <label for="slug" class="col-md-4 control-label">Price ( For Display only
                                            )</label>

                                        <div class="col-md-6">
                                            <input id="price" type="text" class="form-control" name="price"
                                                value="{{ old('price') }}" autofocus>

                                            @if ($errors->has('slug'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                        <label for="body" class="col-md-4 control-label">Description</label>
                                        <div class="col-md-6">
                                            <textarea id="body" class="form-control" name="body"
                                                value="{{ old('body') }}" autofocus></textarea>
                                            @if ($errors->has('body'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('body') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div style="display:none;" id="fitBase"
                                        class="form-group{{ $errors->has('total_delivery_cost') ? ' has-error' : '' }}">
                                        <label for="total_delivery_cost_fit" class="col-md-4 control-label">Total
                                            Delivery Price<br>
                                            <span>( Price will be same for all Fitreats. )</span>
                                        </label>
                                        <div class="col-md-6">
                                            <input class="form-control" name="total_delivery_cost_fit">
                                            @if ($errors->has('total_delivery_cost_fit'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('total_delivery_cost_fit') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div id="visibility">
                                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                            <label for="slug" class="col-md-4 control-label">Featured Image</label>

                                            <div class="col-md-6">

                                                {!! Form::label('featured_image','Upload Profile
                                                Image',array('id'=>'','class'=>'margin-top-20')) !!}
                                                {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile'))
                                                !!}
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('slug') ? ' has-error' : '' }}">
                                            <label for="slug" class="col-md-4 control-label"> Short Intro</label>

                                            <div class="col-md-6">
                                                <input id="short_intro" type="text" class="form-control"
                                                    name="short_intro" value="{{ old('short_intro') }}" autofocus>

                                                @if ($errors->has('slug'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('short_intro') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('deliveries') ? ' has-error' : '' }}">
                                            <label for="deliveries" class="col-md-4 control-label">Total
                                                Deliveries</label>
                                            <div class="col-md-6">
                                                <input type="text" id="deliveries" class="form-control"
                                                    name="deliveries" autofocus>
                                                @if ($errors->has('deliveries'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('deliveries') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('delivery_rate') ? ' has-error' : '' }}">
                                            <label for="delivery_rate" class="col-md-4 control-label">Delivery
                                                Rate</label>
                                            <div class="col-md-6">
                                                <input type="text" id="delivery_rate" class="form-control"
                                                    name="delivery_rate" autofocus>
                                                @if ($errors->has('delivery_rate'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('delivery_rate') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                        $(document).ready(function() {
                                            $("#delivery_rate").change(function() {

                                                a = document.getElementById("deliveries").value;

                                                b = document.getElementById("delivery_rate").value;

                                                document.getElementById("total_delivery_cost")
                                                    .value = (a * b).toFixed(2);
                                            });
                                            $("#deliveries").change(function() {

                                                a = document.getElementById("deliveries").value;

                                                b = document.getElementById("delivery_rate").value;

                                                document.getElementById("total_delivery_cost")
                                                    .value = (a * b).toFixed(2);
                                            });
                                            a = document.getElementById("deliveries").value;

                                            b = document.getElementById("delivery_rate").value;

                                            document.getElementById("total_delivery_cost")
                                                .value = (a * b).toFixed(2);
                                        });
                                        </script>
                                        <div
                                            class="form-group{{ $errors->has('total_delivery_cost') ? ' has-error' : '' }}">
                                            <label for="total_delivery_cost" class="col-md-4 control-label">Total
                                                Delivery Price</label>
                                            <div class="col-md-6">
                                                <input id="total_delivery_cost" class="form-control" readonly
                                                    name="total_delivery_cost" autofocus>
                                                @if ($errors->has('total_delivery_cost'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('total_delivery_cost') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>


                                        <div class="form-group{{ $errors->has('dess') ? ' has-error' : '' }}">
                                            <label for="dess" class="col-md-4 control-label">Dessert</label>
                                            <div class="col-md-6">
                                                <input type="checkbox" id="comp_dess" name="comp_dess" value="1"
                                                    onchange="displayComp(this, '#comp')">
                                                <label for="comp_dess">Complimentary Dessert</label>
                                                <input type="checkbox" id="paid_dess" name="paid_dess" value="1"
                                                    onchange="displayComp(this, '#paid_comp')">
                                                <label for="paid_dess">Paid Dessert</label>
                                                @if ($errors->has('dess'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('dess') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div id="comp" class="form-group{{ $errors->has('fit') ? ' has-error' : '' }}"
                                            style="display:none;">
                                            <label for="fit" class="col-md-4 control-label">No of Complimentary
                                                Fitreats</label>
                                            <div class="col-md-6">
                                                <input type="number" id="fit" class="form-control" name="fit" autofocus>
                                                @if ($errors->has('fit'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('fit') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div id="paid_comp"
                                            class="form-group{{ $errors->has('paidDes') ? ' has-error' : '' }}"
                                            style="display:none;">
                                            <label for="paidDes" class="col-md-4 control-label">No of Paid
                                                Fitreats</label>
                                            <div class="col-md-6">
                                                <input type="number" id="paidDes" class="form-control" name="paidDes"
                                                    autofocus>
                                                @if ($errors->has('fit'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('paidDes') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Best Seller / Most Popular</label>
                                            <div class="col-md-6">
                                                <input type="checkbox" id="best_seller" name="best_seller" value="1">
                                                <label for="best_seller">Best Seller</label>
                                                <input type="checkbox" id="most_popular" name="most_popular" value="1">
                                                <label for="most_popular">Most Popular</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Create
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>

<script>
function checkVisibility(el) {
    let mealType = el.value;
    let dontDisplay = document.getElementById('visibility');
    let doDisplay = document.getElementById('fitBase');
    let priceDisplay = document.getElementById('dispPr');


    let short_intro = document.getElementById('short_intro');
    let deliveries = document.getElementById('deliveries');
    let delivery_rate = document.getElementById('delivery_rate');
    let fit = document.getElementById('fit');
    let comp_dess = document.getElementById('comp_dess');

    if (mealType == "n") {
        short_intro.value = null;
        deliveries.value = null;
        delivery_rate.value = null;
        fit.value = null;
        comp_dess.value = null;
        dontDisplay.style.display = "none";
        priceDisplay.style.display = "none";
        doDisplay.style.display = "block";
    } else {
        dontDisplay.style.display = "block";
        priceDisplay.style.display = "block";
        doDisplay.style.display = "none";
    }
}

function displayComp(el, id) {
    if (!el.classList.toggle('active')) {
        $(id).hide();
    } else {
        $(id).show();
    }
}
</script>