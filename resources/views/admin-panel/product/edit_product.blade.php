<div class="post-edit-section">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-heading ">UPDATE PRODUCT</h4>
                </div>


                {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                {{ csrf_field() }}

                <div class="panel-body">
                    {!! Form::model($product,['method'=>'PATCH','files' => true,'url'=>'/update-product/'.$product->id])
                    !!}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="title" class="col-md-4 control-label">Product Name</label>
                                        <div class="col-md-12 col-sm-12 from-group-spacing">

                                            {{ Form:: text('title', $product->title, array("class" => "form-control margin-bottom-12", 'id' => 'title')) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="name" class="col-md-4 control-label">PRODUCT CATEGORY</label>

                                        <div class="col-md-12 col-sm-12 from-group-spacing">

                                            <select class="form-control margin-bottom-12"
                                                placeholder='Select Any Category' value="{{$product->cat_id}}"
                                                id='disc_id' name="dict_cat">



                                                <option id="{{ $product->name}}" value="{{$product->name}}">
                                                    {{ $product->name }}</option>
                                                <input type="hidden" id="{{ $product->cat_id}}" name="cat_id"
                                                    value="{{$product->cat_id}}">


                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 pricing-product">
                                        <div
                                            class="col-sm-3 mr-1 sale-price {{ $product->food_id == 1 ? 'show': 'hide'}}">
                                            <div class="form-group">
                                                <label for="price_per_meal" class="col-md-12 control-label">Enter
                                                    Price per Meal</label>
                                                <input type="text" name="price_per_meal" class="form-control input-lg"
                                                    value="{{ $product->price_per_meal }}" />

                                            </div>
                                        </div>
                                        <div
                                            class="col-sm-3 mr-1 sale-price {{ $product->food_id == 1 ? 'show': 'hide'}}">
                                            <div class="form-group">
                                                <label for="weekly_price" class="control-label">Enter Weekly
                                                    Price</label>
                                                <input type="text" name="weekly_price" class="form-control input-lg"
                                                    value="{{ $product->weekly_price }}" />
                                            </div>
                                        </div>
                                        <div
                                            class="col-sm-3 mr-1 sale-price {{ $product->food_id == '4' ? 'hide': 'show'}}">
                                            <div class="form-group">
                                                <label for="price" class="control-label">
                                                    @if($product->food_id == '1')
                                                    Monthly PRICE
                                                    @else
                                                    Price
                                                    @endif
                                                </label>
                                                {{ Form:: text('price', $product->price, array("class" => "form-control input-lg", 'id' => 'price')) }}
                                            </div>

                                        </div>
                                        <div id="PriceQtyD" class="{{ $product->food_id == '4' ? 'hide': 'show'}}">
                                            <div class="col-sm-6 sale-price">
                                                <div
                                                    class="form-group{{ $errors->has('price_qty_display') ? ' has-error' : '' }}">
                                                    <label for="price_qty_display" class="col-md-12 control-label">Price
                                                        Quantity Display (Only for Fitreats)</label>
                                                    <div class="col-md-12">
                                                        <input id="price_qty_display" type="text"
                                                            class="form-control input-lg" name="price_qty_display"
                                                            value="{{ $product->price_qty_display }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-12">

                                        <div class="col-sm-4 margin-left-20">
                                            {!! Form::label('featured_image','Select Featured
                                            Image',array('id'=>'','class'=>'margin-top-20')) !!}
                                            {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}

                                            <div class="form-group userprofile-bg" id="imagePreview"
                                                style=" background-size: contain; height: 377px; background-repeat: no-repeat;background-position: center;">
                                                <img src="/{{ $product->imagePath }}" width="100%" id="editimage" />
                                            </div>
                                        </div>

                                        <div class="col-sm-8">
                                            <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                                                <label for="body" class="col-md-12 control-label">body</label>

                                                <div class="col-md-12">

                                                <script src="https://cdn.tiny.cloud/1/vjfxriquojk0k7dia3w9hf4rb82k4340nhfi3xr47rlswbon/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

                                                    {{form::textarea ('body',$product->description, array("class" => "tinymce form-control my-editor margin-bottom-12", 'id' => 'description'))}}


                                                    <script>
                                                    var editor_config = {
                                                        path_absolute: "/",
                                                        selector: "textarea.my-editor",
                                                        plugins: [
                                                            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                                            "searchreplace wordcount visualblocks visualchars code fullscreen",
                                                            "insertdatetime media nonbreaking save table contextmenu directionality",
                                                            "emoticons template paste textcolor colorpicker textpattern"
                                                        ],
                                                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                                                        relative_urls: false,
                                                        file_browser_callback: function(field_name, url, type,
                                                            win) {
                                                            var x = window.innerWidth || document
                                                                .documentElement.clientWidth || document
                                                                .getElementsByTagName('body')[0].clientWidth;
                                                            var y = window.innerHeight || document
                                                                .documentElement.clientHeight || document
                                                                .getElementsByTagName('body')[0].clientHeight;

                                                            var cmsURL = editor_config.path_absolute +
                                                                'laravel-filemanager?field_name=' + field_name;
                                                            if (type == 'image') {
                                                                cmsURL = cmsURL + "&type=Images";
                                                            } else {
                                                                cmsURL = cmsURL + "&type=Files";
                                                            }

                                                            tinyMCE.activeEditor.windowManager.open({
                                                                file: cmsURL,
                                                                title: 'Filemanager',
                                                                width: x * 0.8,
                                                                height: y * 0.8,
                                                                resizable: "yes",
                                                                close_previous: "no"
                                                            });
                                                        }
                                                    };

                                                    tinymce.init(editor_config);
                                                    </script>


                                                    @if ($errors->has('body'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('body') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-sm-12">
                                        <label for="meta_desc" class="col-md-12 control-label">META DESCRIPTION</label>
                                        <div class="col-md-12 col-sm-12 from-group-spacing">
                                            {{ Form:: text('meta_desc', $product->meta_desc, array("class" => "form-control margin-bottom-12", 'id' => 'meta_desc')) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <label for="meta_keyword" class="col-md-12 control-label">META KEYWORDS</label>
                                        <div class="col-md-12 col-sm-12 from-group-spacing">
                                            {{ Form:: text('meta_keyword', $product->meta_keyword, array("class" => "form-control margin-bottom-12", 'id' => 'meta_keyword')) }}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">

                                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        {{ Form:: close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>