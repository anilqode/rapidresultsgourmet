<?php
if (!isset($_SESSION)) {
    session_start();
}
?>

@extends('layouts.app')

@section('styles')
<style>
.col-sm-12.pricing-product {
    margin-left: 15px;
}

.col-sm-3.sale-price.show {
    margin-right: 30px;
}
</style>
@endsection

@section('content')
<div class="container-fluid progressive-natti-body">
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            @include('admin-panel.product.edit_product')
        </div>
    </div>
</div>
<script type="text/javascript" src="js/imagePreview.js"></script>
@endsection