<?php
if(!isset($_SESSION))
{
    session_start();
}
?>

@extends('layouts.app')

@section('content')
    <div class="container-fluid progressive-natti-body">
        <div class="row">
            <div class="col-sm-3">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-9">
                @include('admin-panel.product.display_product')
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection
