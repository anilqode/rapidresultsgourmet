<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         true,
                } );
            } );

        </script>

        {{--  <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th>PRODUCT NAME</th>
                <th>PRODUCT CATEGORY</th>
                <th>WEEK</th>
                <th>MEAL/DAY</th>
                <th>Edit/Delete </th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>PRODUCT NAME</th>
                <th>PRODUCT CATEGORY</th>
                <th>WEEK</th>
                <th>MEAL/DAY</th>
                <th>Edit/Delete </th>

            </tr>
            </tfoot>
            <tbody>


            @foreach($product as $dqs)

                <tr>
                    <td>{{$dqs->title}}</td>
                    <td>{{$dqs->name}}</td>
                    <td>{{$dqs->week}}</td>
                    <td>{{$dqs->mealname}}</td>
                    <td><a href="/edit/{{$dqs->id}}/product/{{$dqs->cat_id}}">Edit</a>/<a href="/delete-product/{{$dqs->id}}"  onclick="return confirm('Are you sure?')" class="user-delete">Delete</a></td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>