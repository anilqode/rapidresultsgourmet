@extends('layouts.app')

@section('content')
	<div class="container policy">
		<h3>PRIVACY POLICY</h3>
		<ol class="tnc">
			<li>Introduction
				<ol class="tnc-second">
					<li>We are committed to safeguarding the privacy of our website visitors and service users.</li>
					<li>This policy applies where we are acting as a data controller with respect to the personaldata of our website visitors and service users; in other words, where we determine the purposesand means of the processing of that personal data.</li>
					<li>We will ask you to consent to our use of cookies in accordance with the terms of this policywhen you first visit our website.</li>
					<li>Our website incorporates privacy controls which affect how we will process your personaldata. By using the privacy controls, you can specify whether you would like to receive directmarketing communications and limit the publication of your information.</li>
					<li>In this policy, “we”, “us” and “our” refer to Rapid Results Gourmet Ltd.</li>
				</ol>
				</li>
			<li>Credit
				<ol class="tnc-second">
					<li>This document was created in part by using a template from SEQ Legal(https://seqlegal.com). However, the document has been heavily modified from the originalversion.</li>
				</ol>
				</li>
			<li >How we use your personal data
				<ol class="tnc-second">
					<li>In this Section 3 we have set out:
						<ol class="tnc-third">
							<li>the general categories of personal data that we may process;</li>
							<li>in the case of personal data that we did not obtain directly from you, the source andspecific categories of that data;</li>
							<li>the purposes for which we may process personal data; and</li>
							<li>the legal bases of the processing.</li>
						</ol>
						</li>
					<li>We may process data about your use of our website and services (“usage data“). Theusage data may include your IP address, geographical location, browser type and version,operating system, referral source, length of visit, page views and website navigation paths, aswell as information about the timing, frequency and pattern of your service use. The source ofthe usage data is Google Analytics, Google Adwords, Facebook and HotJar. The usage datamay also include certain ecommerce data, including, products purchased, coupons used,
					checkout behavior and attribution data. No purchase information shall be gathered, e.g creditcard or debit card information. Purchase information will be captured by our third party paymentgateways Stripe and Paypal. This usage data may be processed for the purposes of analysingthe use of the website and services. The legal basis for this processing is our legitimateinterests, namely monitoring and improving our website and services.</li>
					<li>We may process your account data (“account data“). The account data may include yourname, email address. The source of the account data is you or your employer. The accountdata may be processed for the purposes of operating our website, providing our services,ensuring the security of our website and services, maintaining back-ups of our databases andcommunicating with you. The legal basis for this processing is consent.</li>
					<li>We may process your information included in your personal profile on our website (“profiledata“). The profile data may include your name, address, telephone number, email address,gender, date of birth, interests and hobbies. The profile data may be processed for the purposesof enabling and monitoring your use of our website and services. The legal basis for thisprocessing is consent.</li>
					<li>We may process your personal data that are provided in the course of the use of ourservices (“service data“). The service data may include start and end date of service, deliveryinformation (such as, when a preferred delivery time is captured), dietary requirements,macronutrient requirements. The source of the service data is you or your employer. Theservice data may be processed for the purposes of operating our website, providing ourservices, ensuring the security of our website and services, maintaining back-ups of ourdatabases and communicating with you. The legal basis for this processing is consent.</li>
					<li>We may process information contained in any enquiry you submit to us regarding goodsand/or services (“enquiry data“). The enquiry data may be processed for the purposes ofoffering, marketing and selling relevant goods and/or services to you. The legal basis for thisprocessing is consent.</li>
					<li>We may process information relating to transactions, including purchases of goods andservices, that you enter into with us and/or through our website (“transaction data“).Thetransaction data may include your contact details and the transaction details. Credit or debitcard details are processed via third party payment gateways Stripe and PayPal. The transactiondata may be processed for the purpose of supplying the purchased goods and services andkeeping proper records of those transactions. The legal basis for this processing is theperformance of a contract between you and us and/or taking steps, at your request, to enter intosuch a contract and our legitimate interests, namely our interest in the proper administration ofour website and business.</li>
					<li>We may process information that you provide to us for the purpose of subscribing to ouremail notifications and/or newsletters (“notification data“). The notification data may be
					processed for the purposes of sending you the relevant notifications and/or newsletters. Thelegal basis for this processing is consent.</li>
					<li>We may process information contained in or relating to any communication that you sendto us (“correspondence data“). The correspondence data may include the communicationcontent and metadata associated with the communication. Our website will generate themetadata associated with communications made using the website contact forms. Thecorrespondence data may be processed for the purposes of communicating with you andrecord-keeping. The legal basis for this processing is our legitimate interests, namely the properadministration of our website and business and communications with users.</li>
					<li>We may process any of your personal data identified in this policy where necessary for theestablishment, exercise or defence of legal claims, whether in court proceedings or in anadministrative or out-of-court procedure. The legal basis for this processing is our legitimateinterests, namely the protection and assertion of our legal rights, your legal rights and the legalrights of others.</li>
					<li>We may process any of your personal data identified in this policy where necessary for thepurposes of obtaining or maintaining insurance coverage, managing risks, or obtainingprofessional advice. The legal basis for this processing is our legitimate interests, namely theproper protection of our business against risks.</li>
					<li>In addition to the specific purposes for which we may process your personal data set outin this Section 3, we may also process any of your personal data where such processing isnecessary for compliance with a legal obligation to which we are subject, in order to protect yourvital interests or the vital interests of another natural person.</li>
					<li>Please do not supply any other person’s personal data to us, unless we prompt you to doso.</li>
				</ol>
				</li>
			<li >Providing your personal data to others
				<ol class="tnc-second">
					<li>We may disclose your personal data to any member of our group of companies (thismeans our subsidiaries, our ultimate holding company and all its subsidiaries) insofar asreasonably necessary for the purposes, and on the legal bases, set out in this policy. Our groupof companies currently includes:– Rapid Results Gourmet Ltd – Company Reg  www.rapidresultsgourmet.co.uk</li>
					<li>We may disclose your personal data to our insurers and/or professional advisers insofaras reasonably necessary for the purposes of obtaining or maintaining insurance coverage, managing risks, obtaining professional advice, or the establishment, exercise or defence of legalclaims, whether in court proceedings or in an administrative or out-of-court procedure.</li>
					<li>Financial transactions relating to our website and services are handled by our paymentservices providers, Stripe and PayPal. We will share transaction data with our payment servicesproviders only to the extent necessary for the purposes of processing your payments, refundingsuch payments and dealing with complaints and queries relating to such payments and refunds.You can find information about the payment services providers’ privacy policies and practices athttps://stripe.com/gb https://www.paypal.com/uk/webapps/mpp/merchant.</li>
					<li>In addition to the specific disclosures of personal data set out in this Section 4, we maydisclose your personal data where such disclosure is necessary for compliance with a legalobligation to which we are subject, or in order to protect your vital interests or the vital interestsof another natural person. We may also disclose your personal data where such disclosure isnecessary for the establishment, exercise or defence of legal claims, whether in courtproceedings or in an administrative or out-of-court procedure.</li>
				</ol>
				</li>
			<li >Retaining and deleting personal data
				<ol class="tnc-second">
					<li>This Section 6 sets out our data retention policies and procedure, which are designed tohelp ensure that we comply with our legal obligations in relation to the retention and deletion ofpersonal data.</li>
					<li>Personal data that we process for any purpose or purposes shall not be kept for longerthan is necessary for that purpose or those purposes.</li>
					<li>We will retain your personal data as follows:
					<ol class="tnc-third">
						<li>usage data will be retained for a maximum period of 2 years following the date it was obtained unless otherwise specified by the user.</li>
						<li>account data will be retained for a maximum period of 2 years following the date it wasobtained unless otherwise specified by the user.</li>
						<li>profile data will be retained for a maximum period of 2 years following the date it was obtained unless otherwise specified by the user.</li>
						<li>service data will be retained for a maximum period of 2 years following the date it was obtained unless otherwise specified by the user.</li>
						<li>enquiry data will be retained for a maximum period of 2 years following the date it wasobtained unless otherwise specified by the user.</li>
						<li>transaction data will be retained for a maximum period of 2 years following the date it was obtained unless otherwise specified by the user.</li>
						<li>notification data will be retained for a maximum period of 2 years following the date it was obtained unless otherwise specified by the user.</li>
						<li>correspondence data will be retained for a maximum period of 2 years following the date it was obtained unless otherwise specified by the user.</li>
					</ol></li>
					<li>Notwithstanding the other provisions of this Section 6, we may retain your personal datawhere such retention is necessary for compliance with a legal obligation to which we aresubject, or in order to protect your vital interests or the vital interests of another natural person.</li>
				</ol>
				</li>
			<li>Amendments
				<ol class="tnc-second">
					<li>We may update this policy from time to time by publishing a new version on our website.</li>
					<li>You should check this page occasionally to ensure you are happy with any changes to thispolicy.</li>
					<li>We may notify you of changes to this policy by email or through the private messagingsystem on our website.</li>
				</ol>
				</li>
			<li>Your rights
				<ol class="tnc-second">
					<li>In this Section 8, we have summarised the rights that you have under data protection law.Some of the rights are complex, and not all of the details have been included in our summaries.Accordingly, you should read the relevant laws and guidance from the regulatory authorities fora full explanation of these rights.</li>
					<li>Your principal rights under data protection law are:
						<ol class="tnc-third">
							<li>the right to access;</li>
							<li>the right to rectification;</li>
							<li>the right to erasure;</li>
							<li>the right to restrict processing;</li>
							<li>the right to object to processing;</li>
							<li>the right to data portability;</li>
							<li>the right to complain to a supervisory authority; and</li>
							<li>the right to withdraw consent.</li>
						</ol></li>
					<li>You have the right to confirmation as to whether or not we process your personal dataand, where we do, access to the personal data, together with certain additional information.That additional information includes details of the purposes of the processing, the categories ofpersonal data concerned and the recipients of the personal data. Providing the rights andfreedoms of others are not affected, we will supply to you a copy of your personal data. The firstcopy will be provided free of charge, but additional copies may be subject to a reasonable fee.You can access parts of your personal data by visiting ​www.rapidresultsgourmet.co.uk​ whenlogged into our website.</li>
					<li>You have the right to have any inaccurate personal data about you rectified and, takinginto account the purposes of the processing, to have any incomplete personal data about youcompleted.</li>
					<li>In some circumstances you have the right to the erasure of your personal data withoutundue delay. Those circumstances include: the personal data are no longer necessary inrelation to the purposes for which they were collected or otherwise processed; you withdrawconsent to consent-based processing; you object to the processing under certain rules ofapplicable data protection law; the processing is for direct marketing purposes; and the personaldata have been unlawfully processed. However, there are exclusions of the right to erasure. Thegeneral exclusions include where processing is necessary: for exercising the right of freedom ofexpression and information; for compliance with a legal obligation; or for the establishment,exercise or defence of legal claims.</li>
					<li>In some circumstances you have the right to restrict the processing of your personal data.Those circumstances are: you contest the accuracy of the personal data; processing is unlawfulbut you oppose erasure; we no longer need the personal data for the purposes of ourprocessing, but you require personal data for the establishment, exercise or defence of legalclaims; and you have objected to processing, pending the verification of that objection. Whereprocessing has been restricted on this basis, we may continue to store your personal data.However, we will only otherwise process it: with your consent; for the establishment, exercise ordefence of legal claims; for the protection of the rights of another natural or legal person; or forreasons of important public interest.</li>
					<li>You have the right to object to our processing of your personal data on grounds relating toyour particular situation, but only to the extent that the legal basis for the processing is that theprocessing is necessary for: the performance of a task carried out in the public interest or in theexercise of any official authority vested in us; or the purposes of the legitimate interests pursuedby us or by a third party. If you make such an objection, we will cease to process the personal information unless we can demonstrate compelling legitimate grounds for the processing whichoverride your interests, rights and freedoms, or the processing is for the establishment, exerciseor defence of legal claims.</li>
					<li>You have the right to object to our processing of your personal data for direct marketingpurposes (including profiling for direct marketing purposes). If you make such an objection, wewill cease to process your personal data for this purpose.</li>
					<li>You have the right to object to our processing of your personal data for scientific orhistorical research purposes or statistical purposes on grounds relating to your particularsituation, unless the processing is necessary for the performance of a task carried out forreasons of public interest.</li>
					<li>To the extent that the legal basis for our processing of your personal data is:
					<ol class="tnc-third">
						<li>consent; or</li>
						<li>that the processing is necessary for the performance of a contract to which you are partyor in order to take steps at your request prior to entering into a contract,and such processing is carried out by automated means, you have the right to receive yourpersonal data from us in a structured, commonly used and machine-readable format. However,this right does not apply where it would adversely affect the rights and freedoms of others.</li>
					</ol></li>
					<li>If you consider that our processing of your personal information infringes data protectionlaws, you have a legal right to lodge a complaint with a supervisory authority responsible fordata protection. You may do so in the EU member state of your habitual residence, your placeof work or the place of the alleged infringement.</li>
					<li>To the extent that the legal basis for our processing of your personal information isconsent, you have the right to withdraw that consent at any time. Withdrawal will not affect thelawfulness of processing before the withdrawal.</li>
					<li>You may exercise any of your rights in relation to your personal data by written notice tous in addition to the other methods specified in this Section 8.</li>
				</ol></li>
			<li >Our details
				<ol class="tnc-second">
					<li>This website is owned and operated by Rapid Results Gourmet Ltd.</li>
					<li>We are registered in England and Wales under registration number ​11079838​​, and our
					registered office is at 7 Railway Street, Gravesend DA11 9DU.</li>
					<li>Our principal place of business is at 7 Railway Street, Gravesend DA11 9DU.</li>
					<li>You can contact us:
					<ol class="tnc-third">
						<li>by post, to the postal address given above;</li>
						<li>using our website contact form;</li>
						<li>by telephone, on the contact number published on our website from time to time; or</li>
						<li>by email, using the email address published on our website from time to time.</li>
					</ol>
					</li>
				</ol>
				</li>
			<li>Data protection officer
				<ol class="tnc-second">
					<li>Our data protection officer’s contact details are: info@rapidresultsgourmet.co.uk.</li>
				</ol>
				</li>
		</ol>
	</div>
@endsection