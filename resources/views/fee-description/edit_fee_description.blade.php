

<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="edit-questionset-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>EDIT FEE DESCRIPTION </h4></div>

                <div class="panel-body">
                    {!! Form::model($descInfo,['method'=>'post','files' => true,'url'=>['/update-fee-description/'.$descInfo->fee_id]]) !!}

                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">
                        {{--  <div class="col-sm-12">
                              <label for="name">Select Dictionary Category</label>

                              {{ Form:: select('dict_cat',$dictCatInfo,$dictionaryCatInfo->id,array("class" => "form-control margin-bottom-12", 'placeholder'=>'Select Any Category', 'id' => 'disc_id')) }}

                          </div>--}}

                        <div class="col-sm-12">
                            <label for="name">Select Fee</label>



                            <select class = "form-control margin-bottom-12" placeholder='Select Any Category' value="{{$descInfo->fee_id}}" id = 'disc_id' name="dict_cat" >



                                <option id="" value="{{$descInfo->fee_id}}">{{ $descInfo->description }}({{ $descInfo->course }})</option>


                            </select>

                        </div>

                        <div class="form-group">
                            <?php $counter=1;?>
                            @foreach($description as $dict)

                                <div class="col-sm-12 description-margin">
                                    <div class="col-md-4 col-sm-4">
                                        <label>Question #<?php echo $counter?> : </label>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <input type='textbox' class="form-control" value="{{$dict->desc}}" name="description[]" id='description'>

                                        <input type='hidden' class="form-control" value="{{$dict->id}}" name="id[]" id='id'>
                                    </div>

                                </div>

                                <?php $counter++; ?>
                                @endforeach
                        </div>





                        <div class="col-sm-12 submit-question-set ">
                            <button type="submit" class=" btn btn-primary">
                                Update Question Set
                            </button>

                        </div>


                    </div>
                </div>
                {{ Form:: close() }}
            </div>
        </div>
    </div>
</div>

</div>