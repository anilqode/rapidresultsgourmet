
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-9">
                @include('fee-description.add_fee_description')
            </div>
        </div>
    </div>
@endsection