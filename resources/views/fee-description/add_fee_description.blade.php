
<script type="text/javascript" src="/js/feetextboxcreationdynamic.js"></script>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>


<!-- Template. This whole data will be added directly to working form above -->

<div class="student-form">

    <div class="row">
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Add Fee Description</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/post-fee-description','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-12 ">
                            <div class="panel panel-default ">

                                <div class="panel-body">

                                    <div class="form-group ">
                                        <div class="col-sm-4">
                                            <label for="name">Select Dictionary Category</label>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <select class = "form-control margin-bottom-12" placeholder='Select Any Category' id = 'fee_id' name="fee_id" >
                                                <option selected="selected" value="">Select FEE</option>
                                                @foreach($discatid as $dict)

                                                    <option id="{{ $dict->id }}" value="{{ $dict->id }}">{{ $dict->course }}({{ $dict->amount}})</option>

                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="form-top">
                                            <div id='TextBoxesGroup'>
                                                <div id="TextBoxDiv1">
                                                    <div class="form-group">
                                                        <div class="col-sm-12">
                                                            <div class="col-md-4 col-sm-4">
                                                                <label>Description #1 : </label>
                                                            </div>
                                                            <div class="col-md-6 col-sm-6">

                                                                <input type='textbox' class="form-control"  name="description[]" id='description'>
                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type='button' value='Add Button' id='addButton'>
                                    <input type='button' value='Remove Button' id='removeButton'>


                                    <div class="clear" style="clear:both"></div>



                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>    </div>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}


                </div>
            </div>
        </div>
    </div>

</div>