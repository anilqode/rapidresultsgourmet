@extends('layouts.app')

@section('metaTitle')
<title>Gourmet Meals Delivered to your door - UK | Rapid Result Gourmet</title>
@endsection

@section('metaDescription')
<meta name="description"
    content="Stay Healthy and reach your Fitness Goals with Rapid Results Gourmet's Fresh, Delicious, Balanced Meals delivered straight to your door.">
@endsection

@section('keywords')
<meta name="keywords"
    content="Healthy meals delivered to your door uk, Cheap meal prep, Prepared meals, Cheapest, Prep meals, Gym, ready, service, diet, Ready cooked">
@endsection

@section('canonical')
<link rel="canonical" href="https://rapidresultsgourmet.co.uk/">
@endsection

@section('styles')
<style>
.about-us p {
    font-size: 18px;
    text-align: justify;
}

.about-us {
    padding: 60px;
}

.about-us.row h1 {
    font-size: 28px;
    text-align: center;
}

.imgWrapper p {
    padding: 0px 20px;
    text-align: center;
}

.imgWrapper h4 {
    letter-spacing: 5px;
    font-size: 25px;
    text-align: center;
}

.imgWrapper {
    border: 1px solid #febe14;
    padding: 40px 15px;
    box-shadow: 3px 4px 8px 3px #d1d1d14a;
    height: 290px;
}

.text-center.googleReviews.starsImage.slideanim.slide img {
    padding: 1px 5px;
    cursor: default;
}

.text-center.googleReviews.starsImage.slideanim.slide {
    padding: 0px 0px 25px;
}

@media only screen and (max-width: 1025px) {
    .about-us.row h1 {
        font-size: 19px;
    }

    .about-us p {
        font-size: 16px;
    }
}

@media only screen and (max-width: 769px) {
    .about-us {
        padding: 40px;
    }

    .imgWrapper {
        padding: 9px 20px;
        height: 240px;
    }

    .about-us p {
        font-size: 16px;
    }
}

@media only screen and (max-width: 426px) {
    .outerLayer {
        background-attachment: inherit !important;
    }

    .imgWrapper {
        height: 175px;
        margin-bottom: 20px;
    }

    .about-us h1.text-center {
        font-size: 22px;
        margin: 0px 0 13px;
    }

    .about-us p {
        font-size: 17px !important;
    }

    .text-center.googleReviews.starsImage.slideanim.slide {
        padding: 0px 0px 0px;
    }
}

@media only screen and (max-width: 376px) {
    .imgWrapper {
        height: 200px;
        margin-bottom: 15px;
    }
}

@media only screen and (max-width: 322px) {
    .imgWrapper {
        height: 250px;
        margin-bottom: 15px;
    }
}


.modal.left .modal-dialog,
.modal.right .modal-dialog {
    position: fixed;
    margin: auto;
    width: 320px;
    height: 100%;
    -webkit-transform: translate3d(0%, 0, 0);
    -ms-transform: translate3d(0%, 0, 0);
    -o-transform: translate3d(0%, 0, 0);
    transform: translate3d(0%, 0, 0);
}

.modal.left .modal-content,
.modal.right .modal-content {
    height: 100%;
    overflow-y: auto;
}

.modal.left .modal-body,
.modal.right .modal-body {
    padding: 15px 15px 80px;
}

/*Left*/
.modal.left.fade .modal-dialog {
    left: -320px;
    -webkit-transition: opacity 0.3s linear, left 0.3s ease-out;
    -moz-transition: opacity 0.3s linear, left 0.3s ease-out;
    -o-transition: opacity 0.3s linear, left 0.3s ease-out;
    transition: opacity 0.3s linear, left 0.3s ease-out;
}

.modal.left.fade.in .modal-dialog {
    left: 0;
}

/*Right*/
.modal.right.fade .modal-dialog {
    right: -320px;
    -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
    -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
    -o-transition: opacity 0.3s linear, right 0.3s ease-out;
    transition: opacity 0.3s linear, right 0.3s ease-out;
}

.modal.right.fade.in .modal-dialog {
    right: 0;
}

/* ----- MODAL STYLE ----- */
.modal-content {
    border-radius: 0;
    border: none;
}

.modal-header {
    border-bottom-color: #EEEEEE;
    background-color: #FAFAFA;
}

/* ----- v CAN BE DELETED v ----- */


.demo {
    padding-top: 60px;
    padding-bottom: 110px;
}

.btn-demo {
    margin: 15px;
    padding: 10px 15px;
    border-radius: 0;
    font-size: 16px;
    background-color: #FFFFFF;
}

.btn-demo:focus {
    outline: 0;
}

.demo-footer {
    position: fixed;
    bottom: 0;
    width: 100%;
    padding: 15px;
    background-color: #212121;
    text-align: center;
}

.demo-footer>a {
    text-decoration: none;
    font-weight: bold;
    font-size: 16px;
    color: #fff;
}

.googleReviews {
    padding: 60px 60px 0px;
    cursor: pointer;
}

.googleReviews img {
    box-shadow: 2px 2px 9px #d5d5d5d9;
    margin: 0px ;
    width: 215px;
    border: 1px solid #febe14;
    padding: 10px 35px;
    transition: .35s all ease-out;
}

.googleReviews img:hover,
.textWrapper a:hover {
    box-shadow: 2px 3px 11px #b0b0b0d9;
    transform: scale(1.2);
}

#myModal2 {
    background-color: transparent !important;
}

section.more-about-us {
    border-top: 1px solid #d3d3d3;
    border-bottom: 1px solid #d3d3d3;
}

.our-mission {
    background-color: white;
}

.textWrapper a {
    color: white;
    padding: 6px 20px;
    box-shadow: 2px 2px 7px #d2d2d2;
    transition: .35s all ease-out;
}

.outerLayer {
    background: url(images/rrgimages/parallax_home.png);
    background-attachment: fixed;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center center;
}

section.intro {
    background: rgba(0, 0, 0, .7);
    padding: 60px 0px;
    color: white;
}

.why-us h2 {
    margin-bottom: 0px;
}

p.shortDesc {
    text-align: justify;
    padding: 0px 20px;
    margin: 0px 15px;
    box-sizing: border-box;
}

.hometext p {
    font-size: 20px;
    text-align: center;
    padding: 30px 0px;
    letter-spacing: 1px;
    line-height: 30px;
    color: #e3e3e3;
}

.mb-35 {
    margin-bottom: 35px;
}

.slideanim {
    visibility: hidden;
}

.slide {
    animation-name: slide;
    -webkit-animation-name: slide;
    animation-duration: 1s;
    -webkit-animation-duration: 1s;
    visibility: visible;
}

.sliderButton {
    z-index: 1;
    top: 70%;
    left: 6%;
    position: absolute;
    width: 201px;
    height: 44px;
    border-radius: 10px !important;
    opacity: 0;
}

@keyframes slide {
    0% {
        opacity: 0;
        transform: translateY(70%);
    }

    100% {
        opacity: 1;
        transform: translateY(0%);
    }
}

@-webkit-keyframes slide {
    0% {
        opacity: 0;
        -webkit-transform: translateY(70%);
    }

    100% {
        opacity: 1;
        -webkit-transform: translateY(0%);
    }
}

@media only screen and (max-width: 1250px) {
    .imgWrapper {
        padding: 30px 15px;
        height: 340px;
    }
}

@media only screen and (max-width: 1099px) {
    .imgWrapper {
        height: 390px;
    }
}

@media only screen and (max-width: 1050px) {
    .imgWrapper {
        height: 400px;
    }
}

@media screen and (max-width: 1025px) {
    .hometext p {
        font-size: 16px;
        padding: 0px 0px;
        line-height: 27px;
    }

    .imgWrapper h4 {
        letter-spacing: 3px;
        font-size: 21px;
    }

    .imgWrapper {
        padding: 25px 4px;
    }

    p.shortDesc {
        padding: 0px 6px;
        margin: 0px 10px;
    }
}

@media screen and (max-width: 1024px) {
    .imgWrapper {
        height: 265px;
    }
}

@media screen and (max-width: 969px) {
    .imgWrapper {
        height: 305px;
    }
}

@media screen and (max-width: 915px) {
    .imgWrapper {
        height: 360px;
    }
}

@media screen and (max-width: 769px) {
    .fixed .menu-header {
        margin-top: 9px;
    }

    .googleReviews .text-center {
        margin-bottom: 30px;
    }

    .imgWrapper {
        padding: 15px 4px;
        height: 370px;
    }

    .imgWrapper h4 {
        font-size: 18px;
    }

    p.shortDesc {
        padding: 0px 4px;
        margin: 0px 0px;
        text-align: center;
    }

    .googleReviews img {
        width: 160px;
        margin-top:20px;
    }
}

@media screen and (max-width: 426px) {
    div#jssor_1 {
        margin-top: 70px !important;
    }

    .mobileMenu {
        margin-top: -70px !important;
    }

    .googleReviews {
        padding: 50px 0px 0px;
    }

    section.intro {
        padding: 20px 0px;
    }

    .intro h1 {
        font-size: 20px;
    }

    .mb-35 {
        margin-bottom: 15px;
    }

    .hometext p {
        padding: 0 15px;
        line-height: 22px;
    }

    .imgWrapper {
        padding: 15px 15px;
        height: 220px;
    }

    .how-it-work-heading {
        padding: 10px 5px 11px 5px;
        margin: 0 auto 30px;
    }

    .how-it-work-heading h4 {
        font-size: 20px;
    }

    .pick-goal,
    .meal-plan,
    .fresh-plan,
    .deliver {
        padding-left: 30px !important;
        padding-right: 30px !important;
        margin-bottom: 20px;
    }

    .start-new-plan {
        width: 94%;
        padding: 10px 0px 10px 0px;
        margin: 40px auto !important;
    }

    .why-us h2 {
        font-size: 23px;
    }

    .about-us p {
        font-size: 14px !important;
    }

    .start-new-plan:hover h4 {
        transform: scale(1.2);
    }

}

@media screen and (max-width: 376px) {
    .start-new-plan h4 {
        font-size: 15px;
    }

    .dv-box {
        width: 66px;
        padding: 15px;
    }

    .fa-icn {
        font-size: 29px;
    }
}

@media screen and (max-width: 321px) {
    .start-new-plan h4 {
        font-size: 13px;
    }

    .imgWrapper {
        padding: 15px 5px;
        height: 300px;
    }
}
</style>
@endsection

@section('content')
@include('layouts.homeSlider')
@endsection

@section('homeContent')
<div class="container">
    <div class="row">
       
<div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div id="rplg"></div>
                <script type="text/javascript">
                rplg_embed = window.rplg_embed || [];
                rplg_embed.push({
                    id: 289
                });
                (function() {
                    var mc = document.createElement('script');
                    mc.type = 'text/javascript';
                    mc.async = true;
                    mc.src = 'https://embed.richplugins.com/static/js/embed.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(mc, s.nextSibling);
                })();
                </script>

            </div>

        </div><!-- modal-content -->
    </div><!-- modal-dialog -->
</div>
<div class="col-sm-4 align-right">
<div class="googleReviews slideanim slide" onclick="openModal(event)">
    <div class="text-center ">
        <div class="Image">
            <img src="images/rrgimages/review.jpg" alt="Reviews" />
        </div>
    </div>
</div>


</div>
<div class="col-sm-4 align-center">
<div class="text-center googleReviews starsImage slideanim slide">
    <div class="Image">
        <img src="images/rrgimages/stars.jpeg" alt="Ratings" style="
    width: 184px;"/>
    </div>
</div>
</div>

<div class="col-sm-4 align-left">
        <div class="textWrapper text-center slideanim slide">
        <a href="https://www.google.com/maps/place/Rapid+Results+Gourmet/@51.4721407,-0.2060837,17z/data=!3m1!4b1!4m5!3m4!1s0x48760fc76c0bb9df:0xb9fbd8e94d543d5f!8m2!3d51.4721407!4d-0.203895"
        target="_blank" rel="noopener noreferrer" class="btn leave-review"> Leave us a Review</a>
</div>
</div>
</div>
</div>
@include('layouts.mainHomeContent2')
@endsection

@section('scripts')
<script src="/js/jssor.slider-26.3.0.min.js"></script>
<script src="js/homeSlider.js"></script>
<script>
function openModal(evt) {
    $('#myModal2').modal('show');
}
</script>

<script>
$(document).ready(function() {
    $(window).scroll(function() {
        $(".slideanim").each(function() {
            var pos = $(this).offset().top;

            var winTop = $(window).scrollTop();
            if (pos < winTop + 600) {
                $(this).addClass("slide");
            }
        });
    });
});
</script>
@endsection