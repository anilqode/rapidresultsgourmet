@extends('layouts.app')

@section('content')
    <style>
        section.faq-content {
            width: 85% !important;
            margin: 0px auto;
        }
        .accordian {
            color: #444;
            cursor: pointer;
            padding: 18px;
            width: 100%;
            text-align: left;
            outline: none;
            font-size: 15px;
            transition: 0.4s;
            background: none;
            border: 1px solid black;
            margin-bottom: 20px;
        }

        .active, .accordian:hover {
            background-color: #000;
            color: white;
        }
        section.faq-content h3 span {
            font-size: 40px;
        }
        .panel {
            padding: 10px 18px 0px;
            display: none;
            background-color: white;
            overflow: hidden;
            margin-top: -2px;
            margin-bottom: 20px;
            box-shadow: 0px 5px 10px 0px #00000059;
        }

        div#faqNav ul li {
            padding: 20px 0;
        }
        div#faqNav ul li a {
            color: black !important;
            background: #80808024;
            margin-bottom: 10px;
            border-radius: 10px;
            padding: 20px 30px;
            text-decoration: none;
        }
        div#faqNav ul li a:hover {
            background: #cacaca !important;
        }
        h2.top-header {
            font-size: 45px;
            padding: 40px 0px 15px;
        }
        article.faqs-content-total h3 {
            padding: 30px 0px;
        }

        .faq-page .delivery{
            padding: 20px 116px !important;
        }
        .faq-page .nutrition {
            padding: 20px 112px !important;
        }
        .faq-page .meals {
            padding: 20px 127px !important;
        }
    </style>
    <div class="container faq-page text-center">
        <h2 class="top-header">FAQ's</h2>
        <div id="faqNav">
            <ul class="list-unstyled">
                <li><a class="placing_orders" href="#placing_orders">PLACING ORDERS AND PAYMENTS</a></li>
                <li><a class="delivery" href="#delivery">DELIVERY</a></li>
                <li><a class="meals" href="#meals">MEALS</a></li>
                <li><a class="nutrition" href="#nutrition">NUTRITION</a></li>
            </ul>
        </div>
    </div>

    <article class="faqs-content-total">
        <section class="faq-content" id="placing_orders">
            <div class="container">
                <h3>
                    <span>PLACING ORDERS AND PAYMENTS</span>
                    <hr>
                </h3>
                <div class="accordian-part slideanim">
                    <button class="accordian">
                        Q. 1: How do I place an order?
                    </button>
                    <div class="panel">
                        <p>
                            On our Plans page, select which plan is suited best for your goals, lifestyle and meal preference. For example, you can choose a 2 Meals a Day plan and select a Mixed Meat menu. Follow each step and complete the form providing your delivery details and meal preferences. Once you've made payment, you'll receive an email from us with your actual order form to select your meals. Easy as that! :)
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 2: How soon can I start?
                    </button>
                    <div class="panel">
                        <p>
                            After you've placed your order, you'll receive an email confirming when your first delivery date will be.  If you place your order by Tuesday, you will receive your order the following Monday or Tuesday. You can always let us know if you'd like to start at a later date.

                        </p>
                    </div>
                    <button class="accordian">
                        Q. 3: What is the price per meal?
                    </button>
                    <div class="panel">
                        <p>
                            Prices per meal vary from £7.00 to £10.00  depending on your plan, plan type and duration of your meal plan contract.
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 4: How do I pay?
                    </button>
                    <div class="panel">
                        <p>
                            You can pay with PayPal, bank transfer or you also have the option to set-up a weekly or monthly direct debit. We accept Visa, Mastercard and most major credit cards.
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 5: Can I change my order?
                    </button>
                    <div class="panel">
                        <p>
                            Yes, you can change your order for the following week by letting us know your new meals and preferences on your order form by Tuesday. You will receive a link to this order form when you first sign up.
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 6: What if I want to pause my pack?
                    </button>
                    <div class="panel">
                        <p>
                            Not a problem! We understand you have holidays and time away.
                            If you can give us 5 days notice, your pack will be frozen until you return. :)
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 7: Do you do corporate orders/catering?
                    </button>
                    <div class="panel">
                        <p>
                            Yes we do! Please get in touch with us and we’ll take you through the ordering process step-by-step.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="faq-content" id="delivery">
            <div class="container">
                <h3>
                    <span>DELIVERY</span>
                </h3>
                <div class="accordian-part">
                    <button class="accordian">
                        Q. 1: Where do you deliver?
                    </button>
                    <div class="panel">
                        <p>
                            Nationwide! As long as you have a post code. :)
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 2: How does delivery work?
                    </button>
                    <div class="panel">

                            <p><b><i>South London and London deliveries:</i></b></p>
                        <ul style="margin-left: 16px;line-height: 30px;">
                            <li>
                                 Your delivery will be on MONDAYS
                            </li>
                            <li>
                                You will receive a 1-hour delivery slot on Sunday evening.
                            </li>
                        </ul>




                            <p><b><i> Anywhere outside of London:</i></b></p>

                        <ul style="margin-left: 16px;line-height: 30px;">
                            <li> Your delivery will be on TUESDAYS </li>
                            <li> For deliveries well outside of London, you will receive a 1-hour delivery slot on Tuesday morning. </li>
                            <li> When you receive your email/text, please click the link to assign a safe place for the courier to leave your meals in case you're not in. </li>

                            <li> Kindly note that it is your responsibility to coordinate this with the courier (DPD outside of London/our RRG delivery driver inside of london) if you will be unavailable to receive your meals or need a change of delivery instructions. </li>

                            <li> We recommend on delivery, you freeze the meals you plan to have at the end of the week to preserve its freshness, as all the meals are cooked fresh with no preservatives. </li>
                        </ul>

                    </div>
                    <button class="accordian">
                        Q. 3: How is my food transported for delivery?
                    </button>
                    <div class="panel">
                        <p>
                            We use well respected courier services such as DPD, who have years of experience delivering chilled fresh food.
                            Your food is stored in chilled cooler boxes where the food is guaranteed to stay chilled for 72 hours!
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 4: What time do you deliver?
                    </button>
                    <div class="panel">
                        <p>
                            Deliveries are made any time between 8:00 a.m. to 6:00 p.m. on Mondays and Tuesdays. You will receive a message either from our delivery driver or dedicated courier informing you of your allocated delivery time slot.
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 5: Can I change the delivery address?
                    </button>
                    <div class="panel">
                        <p>
                            Yes you can :) Delivery address changes can be made 3 days before your next scheduled delivery date.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="faq-content" id="meals">
            <div class="container">
                <h3>
                    <span>MEALS</span>
                </h3>
                <div class="accordian-part">
                    <button class="accordian">
                        Q. 1: What food can I expect?
                    </button>
                    <div class="panel">
                        <p>
                            Our menu changes every month, so you'll never get bored of the same meals! You have the option to let us mix up your meals for you based on your meal plan type, preferences/requirements/allergies and goals. You can also choose your own meals every week by submitting your order form by Tuesday for the following week's delivery.

                        </p>
                    </div>
                    <button class="accordian">
                        Q. 2: How long do I heat my food for?
                    </button>
                    <div class="panel">
                        <p>
                            2 minutes in a microwave. If using a conventional oven, heat your  meal at 180°C for 8 to 10 minutes if it is chilled from the fridge. If you have frozen your meal, defrost for a few hours before heating. Vegan meals, salads and wraps can also be eaten cold! We know you're on the go :)
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 3: How long do the meals last?
                    </button>
                    <div class="panel">
                        <p>
                            3-4 days since all your meals are freshly made.
                            Please freeze the meals if you plan to eat past the 4th day.

                        </p>
                    </div>
                    <button class="accordian">
                        Q. 4: What if I have a food intolerance or allergy?
                    </button>
                    <div class="panel">
                        <p>
                            Please just let us know when you place your order.
                            There will be a section in the online order form where you can let us know all your dietary preferences,
                            allergies and likes/dislikes. We will be sure that these are noted for all your meals.
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 5: Is your meat halal?
                    </button>
                    <div class="panel">
                        <p>
                            Yes, all our meat is halal.
                        </p>
                    </div>


                </div>
            </div>
        </section>
        <section class="faq-content" id="nutrition">
            <div class="container">
                <h3>
                    <span>NUTRITION</span>
                </h3>
                <div class="accordian-part">
                    <button class="accordian">
                        Q. 1: How will I know the calories of each meal?
                    </button>
                    <div class="panel">
                        <p>
                            Based on your goals, each meal will be perfectly portioned to meet your nutrional and caloric needs. If you need additional information or specific macronutrient breakdown, please just let us know!
                        </p>
                    </div>
                    <button class="accordian">
                        Q. 2: Can you meet my exact nutritional needs?
                    </button>
                    <div class="panel">
                        <p>
                            Of course! All of our plans can be made bespoke according to your needs. Please  select your goal from the Order Form, and if you have any specific requirements such as exact weight goal and timeframe you'd like to reach your goal by, please just let us know in the comments section or by contacting us via email or call.

                        </p>
                    </div>
                    <button class="accordian">
                        Q. 3: Can I request different nutritional needs on different days?
                    </button>
                    <div class="panel">
                        <p>
                            Yes! On any of our plans, we can help you organise your calories on training days, rest days, carb cycling, intermittent fasting, etc. Just let us know in your order form and we'll be in touch:)
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <script>
            var acc = document.getElementsByClassName("accordian");
            var i;

            for (i = 0; i < acc.length; i++) {
                acc[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var panel = this.nextElementSibling;
                    // alert(panel);
                    if (panel.style.display === "block") {
                        panel.style.display = "none";
                    } else {
                        panel.style.display = "block";
                    }
                });
            }
        </script>
    </article>

@endsection