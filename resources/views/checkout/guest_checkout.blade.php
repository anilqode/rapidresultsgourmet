@extends('layouts.app')


@section('content')
{!! Form::open(array('url'=>'/paypal-checkout','method'=>'POST','files' => true )) !!}
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7">


            <!-- <form class="form-horizontal" role="form" method="POST" action="/user-register-checkout" id="FormCreate"> -->

            {!! csrf_field() !!}

            <div class="row">

                <div class="col-sm-12 register-form-user">
                <p class="billing-details"> Existing Customers <a href="/login">Click here for Login</a></p>
                    <div class="panel panel-info">

                    
                        <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">

                            <div class="col-sm-6">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span>  DELIVERY DETAILS</h5>
                            </div>
                      

                        </div>
                    </div>
                </div>
                        <div class="panel-body checkoutguest">

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                                <label for="name" class="col-md-12 control-label">Name</label>

                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-input" name="name" value="{{ old('name') }}" placeholder="Enter your Name" required autofocus>

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email"  style="padding-top: 10px;" class="col-md-12 control-label">E-Mail Address</label>

                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" placeholder="Enter Valid Email Address" required>

                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone"  style="padding-top: 10px;" class="col-md-12 control-label">Phone Number</label>

                                <div class="col-md-12">
                                    <input id="phone" type="phone" class="form-input" name="phone" value="{{ old('phone') }}" placeholder="Enter Valid Phone Number" required>

                                    @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" style="padding-top: 10px;" class="col-md-12 control-label">Delivery Address</label>

                                <div class="col-md-12">
                                    <input id="address" type="text" class="form-input " name="address" placeholder="Address" required>
                                    <input id="town" type="text" class="form-input address" name="town" placeholder="Town/City" required>
                                    <input id="county" type="text" class="form-input address" name="county" placeholder="County" required>
                                    <input id="postcode" type="text" class="form-input address" name="postcode" placeholder="Post Code" required>
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="email" class="col-md-12 control-label" style="margin-top:15px;">Order notes (optional)</label>

                                <div class="col-md-12">

                                    <textarea id="order_notes" type="textarea" name="review" placeholder="Notes about your order, eg special notes for delivery"></textarea>
                                </div>
                            </div>

                            <div style="margin-top:10px;">
                                <a onclick="openPasswordSection();" class="registerAccount">
                                    If you would like to keep track of your order and make your future orders easier, please sign up here</a>
                            </div>

                            <div id="passwordSection" style="display:none;" class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-12 control-label">Password</label>

                                <div class="col-md-12">
                                    <input id="password" type="password" class="form-input" name="password" placeholder="Enter Password">

                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>

                                    <div class="col-md-12">
                                        <input id="confirm_password" type="password" class="form-input" name="password_confirmation" placeholder="Confirm Password">
                                        <span id='message'></span>
                                    </div>


                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn" name="paymentMode" value="registerGuest">Create my Account</button>
                                    </div>

                                </div>






                            </div>



                        </div>
                    </div>
                </div>


            </div>

            <!-- </form> -->
        </div>
        <div class="col-sm-5">
        <div class="panel panel-info" style="margin-top:60px;">
        <div class="panel-heading">
                    <div class="panel-title">
                        <div class="row">
                            <div class="col-sm-12">
                                <h5><span class="glyphicon glyphicon-shopping-cart"></span> YOUR PAYMENT DETAILS</h5>
                            </div>
        
                        </div>
                    </div>
                </div>
            <div class="panel-body guestcheckoutbox" >
                <div class="">
                    <div class="row">

                        <div class="col-sm-6 col-xs-5"><strong>Product</strong></div>
                        <div class="col-sm-2 col-xs-3 text-right"><strong>Qty</strong></div>
                        <div class="col-sm-4 col-xs-4 text-right"><strong>Price</strong></div>

                    </div>
                    <hr>

                    @if(session()->get('first_comp') == 0)

                    @else

                    <div class="row giveborder">

                        <div class="col-sm-6 col-xs-5">
                            <strong> {{$freesnacks->title}}({{$fitreatss}})</strong>
                            <h4><small>{!! $freesnacks->description !!}</small></h4>
                        </div>
                        <div class="col-sm-4 price-vegan col-xs-4">
                            <div class="col-sm-8 text-right">
                                <h6><strong>Complementary </strong></h6>
                            </div>
                        </div>
                    </div>
                    <hr>
                    @endif
                    <div class="extrasnacks-checkout-box">
                        @if($orderDetail)

                        @foreach($orderDetail as $key=>$order)
                        <div class="row giveborder">
                            <div class="col-sm-6 col-xs-5">
                                {{$order->plan_name}}
                                @if($order->plan_name!='Fitreats')
                                ({{$order->vegan}})

                                @endif
                            </div>

                            <div class="col-sm-2 col-xs-3 clean text-center">
                                @if($order->plan_name!='Fitreats') <span class="shop-quantity">1</span> @endif
                            </div>


                            <div class="col-sm-4 col-xs-4 del text-right mb10">
                                @if($order->plan_name!='Fitreats')
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">Monthly</div>
                                        <div class="col-sm-6 col-xs-6"> £{{ number_format((float)$order->vegan_price, 2, '.', '')}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">Weekly</div>
                                        <div class="col-sm-6 col-xs-6"> £{{number_format((float)$order->weekly_price, 2, '.', '')}}</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">Per Meal </div>
                                        <div class="col-sm-6 col-xs-6"> £{{number_format((float)$order->price_per_meal, 2,'.', '')}}</div>
                                    </div>


                                @endif

                            </div>
                        </div>



                            @if($order->plan_name!='Fitreats')
                            @if(@isset($order->extra_snacks))



                                @foreach($extra_snacks_detail as $key=>$extra_order)
                                <div class="row giveborder">
                                    <div class="col-sm-6 col-xs-5" style="font-size:12px;">


                                             {{$extra_order->extra_snack}}



                                    </div>
                                    <div class="col-sm-2 col-xs-3 align-center">
                                        @if($order->plan_name!='Fitreats')
                                        {{$extra_order->quantity}}
                                        @endif
                                    </div>
                                    <div class="col-sm-4 price-vegan col-xs-4 text-right">

                                        £{{ number_format((float)$extra_order->rate, 2, '.', '')}}


                                    </div>
                                </div>
                                @endforeach


                            @endif
                            @else
                                <div class="row giveborder">
                            <div class="col-sm-6 col-xs-5" style="font-size:12px;">
                               {{$order->vegan}}

                            </div>
                            <div class="col-sm-2 col-xs-3 align-center">
                                {{$order->quantity}}
                            </div>
                            <div class="col-sm-4 price-vegan col-xs-4 text-right">
                              £ {{ number_format((float)$order->extra_price, 2, '.', '')  }}
                            </div>
                                </div>
                            @endif

                        </div>

                        @endforeach
                        @endif
                    </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <table width="100%">
                                <tr>
                                    <td>
                                        Sub-total:
                                    </td>
                                    <td class="text-right">
                                        £ {{$sub_total_in_decimal}}
                                    </td>
                                </tr>
                            </table>

                            <hr>
                        </div>

                        <div class="col-sm-12">

                            <table width="100%">
                                <tr>
                                    @if(isset($delivery_info))
                                        <td> Delivery Charge : <br>{{$delivery_info[0]->deliveries}} deliveries x £ {{$delivery_info[0]->delivery_rate}}</td>
                                        <td>
                                        </td>
                                        <td class="text-right"> £{{number_format((float)$delivery_info[0]->total_delivery_cost, 2, '.', '')}}</td> @endif

                                </tr>
                            </table>

                            <hr>
                        </div>

                        @if(session()->get('coupon'))
                            <div class="col-sm-12">
                                <table width="100%">
                                    <tr>
                                        <td> <span style="color:green">Discount:</span> </td>
                                        <td class="text-right"> <span id="discountAmt">
                                            <?php
                                                $totAmt = $sub_total_in_decimal;
                                                if (session()->get('coupon')) {
                                                    if (session()->get('coupon_type') == 1) {

                                                        $disc = session()->get('coupon') * 0.01 * $totAmt;
                                                        $disc = number_format((float)$disc, 2, '.', '');
                                                    } else {
                                                        $disc = session()->get('coupon');
                                                    }
                                                }
                                                ?>
                                        </span>
                                            <?php echo '£ ' . $disc; ?></td>
                                </table>
                                <hr>

                            </div>

                        @endif

                        <div class="col-sm-12">
                            <table width="100%">
                                <tr>
                                    <td> <b>TOTAL PRICE:</b> </td>
                                    <td class="text-right">

                                        @if(isset($delivery_info))
                                            <span id="totalAmt">
                                            <?php
                                                $totalAmount = $sub_total_in_decimal;
                                                if (session()->get('coupon')) {
                                                    if (session()->get('coupon_type') == 1) {

                                                        $totalAmount = $totalAmount - session()->get('coupon') * 0.01 * $totalAmount;
                                                        $totalAmount = number_format((float)$totalAmount, 2, '.', '');
                                                        $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                                    } else {
                                                        $totalAmount = $totalAmount - session()->get('coupon');
                                                        $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                                    }
                                                } else {

                                                    $totAmt = $totalAmount + $delivery_info[0]->total_delivery_cost;
                                                }
                                                ?>
                                        </span>
                                            £{{ number_format((float)$totAmt, 2, '.', '')  }}

                                            @endif
                                            </strong>

                                    </td>
                                </tr>

                            </table>
                            <hr>
                        </div>
                        @if($setMealCount==1)
                            <div class="col-sm-12">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            Monthly Total Price:
                                        </td>
                                        <td class="text-right">£ {{ number_format((float)$totAmt, 2, '.', '')  }}</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Weekly Total Price:
                                        </td>
                                        <td class="text-right">£ {{ number_format((float)$totAmt/4, 2, '.', '')  }}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr>
                        @endif



                        <div class="cols-sm-12">
                            <div class="col-sm-12 couponDiv">
                                <p class="extra-added"><b>How would you like to pay?</b>
                                    You can pay weekly, monthly or your total amount. Please select below: </p>
                            </div>
                        </div>
                    </div>


                    <div class="cols-sm-12">
                        <div class="col-sm-12 couponDiv">
                            <div class="row">
                                <div class="col-sm-12">

                                    <input type="hidden" name="order_id" value="{{$plan_no}}">
                                    <input type="hidden" name="amount" value="{{ $totAmt }}">
                                    <input type="hidden" name="vegan_title" value="RRG Order">


                                </div>
                                <div class="col-sm-12 text-right">

                                    <div id="paypal">
                                        <button type="submit" class="btn btn-primary btn-o" name="paymentMode" value="gocardlessWeekly">Pay Weekly</button>
                                        <!-- <a href="/redirect_to_register">Place My Order</a> -->
                                    </div>

                                </div>
                                <div class="col-sm-12 text-right">

                                    <div id="paypal">
                                        <button type="submit" class="btn btn-primary btn-o" name="paymentMode" value="gocardlessMonthly">Pay Monthly</button>
                                        <!-- <a href="/redirect_to_register">Place My Order</a> -->
                                    </div>

                                </div>
                                <div class="col-sm-12 text-right">

                                    <div id="paypal">
                                        <button type="submit" class="btn btn-primary btn-o" name="paymentMode" value="paypal">
                                            Pay  Total £{{ number_format((float)$totAmt, 2, '.', '')  }}

                                        </button>
                                        <img src="images/rrgimages/pay-pal.png" width="100%" style="margin-top:10px;">
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>




        </div>
        </div>
    </div>
</div>
</div>
</div>

{{ Form:: close() }}
<script>
    function openPasswordSection() {
        var x = document.getElementById("passwordSection");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    }

    $('#password, #confirm_password').on('keyup', function() {
        if ($('#password').val() == $('#confirm_password').val()) {
            $('#message').html('Matching').css('color', 'green');
        } else
            $('#message').html('Not Matching').css('color', 'red');
    });
</script>
@endsection