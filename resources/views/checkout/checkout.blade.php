<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://paymentgateway.commbank.com.au/checkout/version/47/checkout.js"
            data-error="errorCallback"
            data-cancel="cancelCallback",
            data-complete="completeCallback",
            data-beforeRedirect="Checkout.saveFormFields"
            data-afterRedirect="Checkout.restoreFormFields">
    </script>
    <script type="text/javascript">

        var sessionId= '';
        var sessionVersion = '';
        var status='';
        var successIndicator = '';

        function getSession(amt,id,desc,email,phone,enroll_id,address){

            console.log(id, email);

            fetch('/fetch').then(function(response) {
                return response.text();
            }).then(function(data) {
                console.log(data);
                $('body').append(data);
                split = data.split('+');
                status = split[0];
                successIndicator= split[1];
                sessionId = split[2];
                sessionVersion= split[3];

                console.log('status => '+status+" successIndicator => "+successIndicator);


                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/checkoutstore',
                    type: 'get',
                    data: {amount: amt,id: id,description: desc,email:email,phone:phone,enroll_id:enroll_id,address:address, successIndicator: successIndicator},
                    dataType: 'JSON',
                    success: function (data) {
                        alert(data['message']);
                    }
                });

            }).catch(function(error) {
                $('body').append('Error: ' + error);
            });
        }
    </script>


    <script type="text/javascript">
        var amt ='';
        var id='';
        var desc ='';

        function paynow(amt, id,desc) {
            console.log("SessionId => "+sessionId)
            console.log("SessionVersion => "+sessionVersion);

            Checkout.configure({
                merchant: 'TESTPROSTUCOM201',
                order: {
                    amount: function () {
                        //Dynamic calculation of amount
                        return amt;
                    },
                    currency: 'AUD',
                    description: function () {
                        //Dynamic calculation of amount
                        return desc;
                    },
                    id: function () {
                        //Dynamic calculation of amount
                        return id;
                    },
                },
                interaction: {
                    merchant: {
                        name: 'ProgressiveNaati',
                        address: {
                            line1: 'Level 2,552 Princes Highway Rockdale,',
                            line2: 'New South Wales 2216, Australia'
                        }
                    }
                },
                session: {
                    id: sessionId,
                    version: sessionVersion
                }
            });
        }
    </script>

    <script type="text/javascript">
        var amt;
        var desc, id,email,phone,enroll_id,address;

        $(document).ready(function(){
            $('#first').click(function () {
                $(this).prop('disabled', true);
                amt = $('#amount').val();
                desc = $('#description').val();
                id = $('#id').val();
                email=$('#email').val();
                enroll_id=$('#enroll_id').val();
                phone=$('#phone').val();
                address=$('#address').val();
                console.log(email, id);
                getSession(amt,id,desc,email,phone,enroll_id,address);
                // Delay is set to give some time for the resposne data to set to our variable
                setTimeout(function() {
                    paynow(amt,id,desc);//your code to be executed after 2 second
                }, 3000);
                // Delay is given to pause the asynchronous execution of the JS.
                setTimeout(function() {
                    // Checkout.showLightbox();
                    Checkout.showPaymentPage();
                }, 3000);
            });
        });

        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }

        function cancelCallback() {
            alert('Payment cancelled');
        }

        function completeCallback(resultIndicator, sessionVersion){
            console.log('Success with session data =>  resultIndicator: '+resultIndicator+ "<br> sessionVersion: "+sessionVersion);
            // alert('Success Payment')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/verify',
                type: 'get',
                data: {resultIndicator: resultIndicator},
                dataType: 'JSON',
                success: function (data) {
                    console.log(data['message']);
                }
            });
        }
    </script>
</head>
<body>
<div class="panel panel-default">
    <div class="panel-heading"><h4>Payment</h4></div>

    <div class="panel-body">
<div class="input-details">
  <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12">
              <div class="col-sm-12 ">
              <input type="hidden"  name="enroll_id" value="{{$express->id}}" id="enroll_id" >
          </div>
          <div class="col-sm-12 checkout-btn-area">
              <label>Email:</label>   <input type="text" class="form-control" name="email"  id="email" placeholder="Enter Valid Email Address" required>
              <input type="hidden" class="form-control" name="emailing"  id="emailing" value="sangroula2@gmail.com" >
          </div>
              <div class="col-sm-12 checkout-btn-area">
              <label>Phone:</label>   <input type="text" class="form-control" name="phone"  placeholder="Enter Valid Phone Number" id="phone" required>
          </div>
              <div class="col-sm-12 checkout-btn-area">
              <label>Address:</label>   <input type="text" class="form-control"  name="address" id="address" placeholder="Enter Address" required>
          </div>
          </div>
      </div>
  </div>
</div>
        <input type="hidden" name="amount" value="<?php echo $express->amount; ?>" id="amount">
        <input type="hidden" name="id" value="{{$express->id}}" id="id">
        <input type="hidden" name="description" value="{{$express->description}}" id="description">
<div class="payment-button">
        <input type="button" class="payment-button" id="first" value="Pay with Payment Page" onclick="Checkout.showPaymentPage();" />

</div> </div>

</div>



</body>
</html>