@extends('layouts.app')


@section('content')
<div class="container">

    <div class="row">

        <div class="col-sm-12 text-center">
            <div class="panel-body guestcheckoutbox thank-you">
                @include('layouts.error_message')
                <div class="alert alert-success">
                    <strong>Congratulations! </strong>Your payment has been successfully processed.
                </div>
                <label class="yourOrder">Thank You</label>

                <p>We have received your order. You'll receive a confirmation email from us in your inbox soon.</p>
                <p>Enjoy your meals!</p>


            </div>



        </div>




    </div>
</div>
</div>

@endsection