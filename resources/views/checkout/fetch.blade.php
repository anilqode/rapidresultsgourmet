
<?php
$user="merchant.TESTPROSTUCOM201";
$pass="ffc54a91fe0d145803e07561420adf75";
$orderId = mt_rand();
$redirectURL = 'https://www.progressivenaati.com.au/invoice';
/*
    if set, will be redirected to this URL after payment success with parameters.
    if not set, functionCallback() of the javascript will be used
*/

$url = 'https://paymentgateway.commbank.com.au/api/rest/version/47/merchant/TESTPROSTUCOM201/session';

$array = [
    "apiOperation" => "CREATE_CHECKOUT_SESSION",
    "order" => [
        "currency" => "AUD",
        "id" => $orderId,
    ],
    // uncomment this out if you want the user to redirect to another page
    "interaction" => [
   	"returnUrl" => $redirectURL
     ],
];

$curl_post_data = json_encode($array);

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($curl, CURLOPT_USERPWD, "$user:$pass"); //Your credentials goes here
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //IMP if the url has https and you don't want to verify source certificate

$result = curl_exec($curl);
curl_close($curl);


// json_encode($result);
$res = (json_decode($result));

$session = $res->session;

echo $res->result."+".$res->successIndicator."+".$session->id."+".$session->version;
?>