@extends('layouts.app')

@section('content')
<div class="payment-select text-center">
	<h3>Choose the mode</h3>
	<a href="/subscriptionMonthly" >
	<input type="text">
		<div class="payment-option">
			<div class="header">
				<h4>Monthly Subscription</h4>
			</div>
			<div class="content">
				<input type="text" name="gocardless[]" value="{{json_encode($gocardRequest,TRUE)}}" >
			
				<p>
					Choosing this mode sets up an automatic recurring payment once in a month and amount will be collected partially. 
				</p>
			</div>
		</div>
	</a>
	<a href="/subscription" >
		<div class="payment-option">
			<div class="header">
				<h4>Weekly Subscription</h4>
			</div>
			<div class="content">
				<p>
					Choosing this mode sets up an automatic recurring payment once in a week and amount will be collected partially.
				</p>
			</div>
		</div>
	</a>
</div>
@endsection