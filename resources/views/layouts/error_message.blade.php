@if(count($errors)> 0)
    <div class="alert alert-danger">
        <strong>Woops! </strong>There were some problems with input.<br><br>
        <ul>
            @foreach($errors->all() as $error)
                <li>
                    {{ $error }}
                </li>
            @endforeach
        </ul>
    </div>
@endif