<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('metaTitle')
    @yield('metaDescription')
    @yield('keywords')
    @yield('canonical')
    <meta property="og:url" content="https://rapidresultsgourmet.co.uk/" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content=" Healthy Meals Delivered to Your Door All Over The UK" />
    <meta property="og:description" content="Stay Healthy and reach your Fitness Goals with Rapid Results Gourmet's Fresh, Delicious, Balanced Meals delivered straight to your door all over the UK. You’ll find a plan that fits your specific goals, needs, and lifestyle!
" />
    <meta property="og:image" content="" />
    <meta property="og:locale" content="en-gb" />
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="rapidresultsgourmet">
    <meta name="twitter:title" content="Gourmet Meals delivered to your Door all over UK">
    <meta name="twitter:description"
        content="Rapid Result Gourmet provides the best healthy food delivery service all over the UK at a cheap price. Ready cooked fresh meals delivered to your door.">

    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <link rel="manifest" href="/img/site.webmanifest">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom_plan.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">
    $.noConflict();
    </script>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&family=Quicksand:wght@600&display=swap"
        rel="stylesheet">
    @yield('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
        integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"
        integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>
    <script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "Organization",
        "name": "Rapid Results Gourmet",
        "url": "https://rapidresultsgourmet.co.uk/",
        "address": "106 New King's Rd, Fulham London SW6 4LY, United Kingdom",
        "sameAs": [
            "https://www.facebook.com/rapidresultsgourmet/",
            "https://www.instagram.com/rapidresultsgourmet/",
            "https://www.pinterest.co.uk/rapidresultsgourmet/"
        ]
    }
    </script>

    <div id="app">

        <?php

   use Harimayco\Menu\Facades\Menu;
    use Illuminate\Support\Facades\DB;
    $menuList = Menu::getByName('top_menu');

    ?>
        @include ("layouts.header")


        @yield('content')

        @yield('homeContent')
        @yield('posts')

        @yield('nav')
        @yield('contents')

    </div>
    @include ("layouts.footer")

    @yield('scripts')
    <script>
    $(document).ready(function() {
        $(document).scroll(function() {
            $('#scrollTop').css('display', 'block');
            $('#header').addClass(' fixed');
            if ($(window).scrollTop() == 0) {
                $('#scrollTop').css('display', 'none');
                $('#header').removeClass(' fixed');
            }
        });
    });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-159854674-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-159854674-1');
    </script>

</body>

</html>