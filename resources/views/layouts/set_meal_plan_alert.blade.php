@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>Congratulation! </strong>{{Session::get('success')}}
    </div>
@endif
@if(Session::has('alert'))
    <div class="alert plan-alert">
        <strong>Sorry ! </strong> Because we custom make your order, you may only select one meal plan each time you check out.<!-- {{Session::get('alert')}} -->
        <Strong><a href="/shopping-basket" class="alertmesssgepopup">Proceed to Checkout</a></strong>
    </div>

@endif