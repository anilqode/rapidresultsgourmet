<section class="header homepage" id="header">
    <div class="container-header">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-2 text-right">
                    <a href="/"><img src="/images/rrgimages/logo.jpg" id="logo" alt="logo" /></a>
                </div>
                <div class=" col-sm-8 menu-header">
                    @foreach($menuList as $pageinfo=>$value)

                    <ul class="nav navbar-nav">
                        @if($value['id'] != 10)
                        @if(count($value['child'])>0)
                        <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">{{ $value['label']}} <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @foreach($value['child'] as $childkey=>$childValue)
                                <li class="menu-item">
                                    <a href="{{$childValue['link']}}">{{$childValue['label']}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                        @else
                        <li class="active"><a href="{{ $value['link']}}">{{ $value['label']}}</a>

                        </li>
                        @endif
                        @endif
                    </ul>
                    @endforeach
                </div>
                <div class="col-sm-2 header-login " style="float: right;">
                    <?php if (Auth::check()) { ?><ul>

                            <a href="/logout">
                                <li class="menu-item"> <i class="fa fa-power-off"></i></li>
                            </a>
                            <a href="/shopping-basket" style="padding-right: 12px;">
                                <li class="menu-item p-10">
                                    <span class="glyphicon glyphicon-shopping-cart"></span>
                                </li>
                            </a>


                            <a href="/home">
                                <li class="menu-item"> <i class="fa fa-user-circle-o"></i></li>
                            </a>

                        </ul>
                    <?php } else { ?>
                        <ul>

                            <a href="/shopping-basket">
                                <li class="menu-item p-10">
                                    <span class="glyphicon glyphicon-shopping-cart"></span>
                                </li>
                            </a>

                            <a href="/login">
                                <li class="menu-item p-10">
                                    <i class="fa fa-sign-in">
                                    </i>Login
                                </li>
                            </a>
                        </ul>
                        {{-- <script>
                         function showForm(){
                             document.getElementById('loginForm').style.display = "block";
                         }

                         function hideForm(){
                             document.getElementById('loginForm').style.display = "none";
                         }</script>--}}


                    <?php  } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    @media only screen and (max-width: 767px) {

        .topnav {
            overflow: hidden;
            background-color: #333;
        }

        .topnav a {
            float: left;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
        }

        .topnav .logo-mob{
            float: left;
        }

        .active {
            color: white;
        }

        .topnav .icon {
            display: block;
        }

        .dropdown {
            float: left;
            overflow: hidden;
        }

        .dropdown .dropbtn {
            font-size: 17px;
            border: none;
            outline: none;
            color: white;
            padding: 14px 16px;
            background-color: inherit;
            font-family: inherit;
            margin: 0;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            float: none;
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            text-align: left;
        }

        .topnav a:hover,
        .dropdown:hover .dropbtn {
            background-color: #555;
            color: white;
        }

        .dropdown-content a:hover {
            background-color: #ddd;
            color: black;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .logo-mob {
        float: left !important;
        display: block!important;
        padding: 0px 13px !important;
        margin-left:100px;
        border: none;
        text-decoration: none !important;
        border-bottom: 0px solid #efefef !important;
    }
   span.fa.fa-shopping-bag {
    font-size: 30px;
    margin-left: 0px;
}

    }

    @media screen and (max-width: 600px) {

        .topnav a:not(:first-child),
        .dropdown .dropbtn {
            display: none;
        }

        .topnav a.icon {
            float: right;
            display: block;
        }
    }

    @media screen and (max-width: 600px) {
        .topnav.responsive {
            position: relative;
        }

        .topnav.responsive .icon {
            position: absolute;
            right: 20px;
            top: 12px;
        }

        .topnav.responsive a {
            float: none;
            display: block;
            text-align: left;
        }

        .topnav.responsive .dropdown {
            float: none;
        }

        .topnav.responsive .dropdown-content {
            position: relative;
        }

        .topnav.responsive .dropdown .dropbtn {
            display: block;
            width: 100%;
            text-align: left;
        }
    }
    @media only screen and (max-width:375px){
        .logo-mob {
    margin-left: 70px !important;
}
    }
     @media only screen and (max-width:320px){
        .logo-mob {
    margin-left: 49px !important;
}
    }
</style>

<div class="mobileMenu fixed">
 
    <div class="topnav" id="myTopnav" style="padding-top: 13px;">
    <a href="/shopping-basket" style="float: left;"><span class="glyphicon glyphicon-shopping-cart"></span></a>
    <a href="/" class="logo-mob">
        <img src="/images/rrgimages/logo.jpg" alt="Rapid Results Gourmet Logo">
    </a>
        <a href="/about_us" style="clear:both;">About</a>
    
        <a href="/contact">Contact Us</a>
        <a href="/set-meal-plan">Plans</a>
        <a href="/fitreats-and-supplements">Fitreats</a>
       

        <a href="/blog">Blog</a>
        <a href="/faqs">Faqs</a>

        <?php
        $userInfo = Auth::User();

        if (strlen($userInfo) == 0) { ?>
            <a href="/login">Login</a>
            <!-- <a href=" /user-register">Sign Up</a> -->
        <?php } else { ?>
            <a href=" /dashboard">My Account</a>
            <a href=" /logout">Logout</a>
        <?php  }  ?>
      

        <a href="javascript:void(0);" style="font-size:25px; padding-top: 2px;" class="icon" onclick="myFunction()">&#9776;</a>
                           
                            
    </div>
</div>

<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }
</script>