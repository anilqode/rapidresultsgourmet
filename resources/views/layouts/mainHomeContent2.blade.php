<div class="outerLayer">
    <section class="intro text-center">
        <div class="container">
            <h1 class="mb-35">Delicious, healthy, freshly made meals delivered to your door!</h1>
            <div class="hometext  slideanim">
                <p>
                    Say goodbye to grocery shopping, prepping and cleaning! Let our chefs prepare you healthy,
                    well-balanced ready to eat meals delivered fresh to your door.
                </p>
                <p>
                    A personalised nutrition plan for YOU! Whether you want to lose, maintain or gain weight, we have
                    got you covered. This is not a diet, this is a lifestyle; and it's never tasted better! We work
                    around your lifestyle and dietary requirements with 5-star chefs, nutritionists and trainers. We
                    combine decades of expertise to make the perfect bespoke service for you!</p>
                <p>
                    No more worrying about eating processed food filled with preservatives. With Rapid Results Gourmet,
                    you'll eat clean, flavourful, portion controlled nutritious food made only with organic produce and
                    quality ingredients.
                </p>
                <p>
                    <strong>Feel good inside and out with each nourishing dish you eat!</strong>
                </p>
            </div>
        </div>
    </section>
</div>
<section class="why-us  slideanim">
    <div class="container-fluid">
        <div class="row">
            <h2 class="text-center">Why choose Rapid Result Gourmet?</h2>
            <div class="col-sm-12">
                <div class="about-us row">
                    <div class="col-sm-4">
                        <div class="imgWrapper first-img">
                            <h4>UK Nationwide Delivery</h4>
                            <p>Get your meals delivered fresh to your front door, in a hygienic cooler box or bag. We'll
                                confirm your delivery slot with you and a safe place to leave your meals if you're not
                                home! </p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="imgWrapper second-img">
                            <h4>100% <br>Fresh never Frozen</h4>
                            <p>Your meals are prepared by our chefs on the same day of delivery. All meals are made
                                ready-to-eat and are served in plastic-free, biodegradable tupperware!</p>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="imgWrapper third-img">
                            <h4>5* Hygiene Rating</h4>
                            <p>Your health is our #1 priority! We use the safest cooking process in the industry and
                                maintain a 5-star hygiene and food service rating throughout the whole process - from
                                the field to your fork!
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class=" slideanim">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-offset-3 col-sm-offset-3 how-it-work-heading">
                <h4>HOW IT WORKS</h4>
            </div>
            
                <div class="col-md-2 col-sm-3 pick-goal">
                <div class="dv-box"><i class="fas fa-weight fa-icn"></i></div>
                <h4 class="services-head"> Choose your Plan</h4>
                <p class="shortDesc">Choose your plan based on your goals, taste, and appetite.</p>
            </div>

            <div class="col-md-2 col-sm-3 deliver">
                <div class="dv-box"><i class="fas fa-clipboard-check fa-icn"></i></div>
                <h4 class="services-head">Add your Preferences</h4>
                <p class="shortDesc">Let us know what you like and don't like, and customize your portion size.</p>
            </div>

            <div class="col-md-2 col-sm-3 deliver">
                <div class="dv-box"><i class="fas fa-credit-card fa-icn"></i></div>
                <h4 class="services-head">Add Payment Method</h4>
                <p class="shortDesc">Use our flexible weekly or monthly payment options by debit/credit card or PayPal.
                </p>
            </div>
            <div class="col-md-2 col-sm-3 deliver">
                <div class="dv-box"><i class="fas fa-truck fa-icn"></i></div>
                <h4 class="services-head">Delivered to you</h4>
                <p class="shortDesc">Your fresh meals are delivered to your front door in our hygienic, insulated cooler
                    bag!</p>
            </div>

        </div>
        <div class="col-sm-12">
            <a href="/set-meal-plan" class="start-meal">
                <div class="col-md-offset-3 col-sm-offset-3 start-new-plan">
                    <h4>START YOUR MEAL PLAN NOW</h4>
                </div>
            </a>
        </div>
    </div>
</section>