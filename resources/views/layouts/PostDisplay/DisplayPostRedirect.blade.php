@extends('layouts.app')

@section('content')
    <div class="container">
    <div class="row">
        <div class="col-sm-8">
            @include('layouts.PostDisplay.DisplayPost')
        </div>

        <div class="col-sm-4">
            @include('admin-panel.SideBar')
        </div>
    </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection
