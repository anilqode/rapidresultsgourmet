<section class="school-Principle">


</section>
<div class="container">
    <div class="row">

        <div class="col-sm-4 left-widget">

        </div>
        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-258" class="post-258 page type-page status-publish hentry">

                    <header class="entry-header">
                        <h1 class="entry-title">Principle Desk’s</h1>	</header><!-- .entry-header -->

                    <div class="entry-content-wrapper">
                        <div class="entry-content">
                            <div class="row">
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4 principle">
                                    <img src="/wp-content/uploads/2017/05/principal.jpg" alt="" width="100%" height="427" class="aligncenter size-full wp-image-314" srcset="http://myschool.nepgeeks.com/wp-content/uploads/2017/05/principal.jpg 443w, http://myschool.nepgeeks.com/wp-content/uploads/2017/05/principal-300x289.jpg 300w, http://myschool.nepgeeks.com/wp-content/uploads/2017/05/principal-311x300.jpg 311w" sizes="(max-width: 443px) 100vw, 443px"><p></p>
                                    <h4 class="name">Prabash Luitel</h4>
                                    <h6 class="position">Principle</h6>
                                </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                            <p>As athletes, the thump of dancer feet and the sound of melodious voices harmonizing. The perpetual energy, movement and enthusiasm permeate the atmosphere at St. George’s. We are a school with a difference! We value individualism, creativity and innovation and strive to nurture them in our students.Our motto “Spread the Light” is at the heart of everything we do at the school.</p>
                            <p> We aim to not just impart knowledge to the students, but also to inculcate in them – wisdom, compassion and a humanitarian spirit. We have a multi-cultural student population; hence we teach children the importance of tolerance and respecting each other’s culture. Discipline, values and integrity are the very foundation of this school.</p>
                            <p>Besides rigorous scholastic programmes, we seek to develop and nurture the different facets of a child. The school encourages all students to participate in a variety of co-curricular activities from dance, art and drama to a variety of sports to social work to environmental conservation activities, (the list goes on). It is important for a child to explore and find their strengths in order to reach their true potential. Whether a child is an introvert or extrovert personality, the aim is to nurture the child into a self-confident individual.</p>
                            <p>As a school we think about how to engage students and hold their attention. If a lesson is to be productive, how do we make sure the students actually absorb the topic? With this constant question in our mind we continuously upgrade our teaching tools and techniques as well as our academic programmes. In a technology prevalent world today, we appeal to young minds by incorporating smart boards, tablet computers and mobile devices into the teaching-learning process.</p>
                            <p>Our mission is to continue to do what we have always done: develop this school and the students with integrity and values; also, to give our students the best opportunities and the best all-round education. Our vision is to produce conscientious, smart and confident citizens of India who will go out into the world and make us proud!</p>
                        </div><!-- .entry-content -->
                    </div><!-- .entry-content-wrapper -->

                    <footer class="entry-footer">
                    </footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
    </div>
</div>


