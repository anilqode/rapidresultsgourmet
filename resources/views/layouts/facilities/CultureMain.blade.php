<section class="school-culture">


</section>
<div class="container">
    <div class="row">

        <div class="col-sm-4 left-widget">

        </div>
        <div class="col-sm-8 right-information">
            <section class="school-building-details">
                <article id="post-252" class="post-252 page type-page status-publish hentry">

                	<header class="entry-header">
                		<h1 class="entry-title">Cultural Activity</h1>	</header><!-- .entry-header -->

                    	<div class="entry-content-wrapper">
                		<div class="entry-content">
                			<p>large number of Co-curricular activities are organized throughout the academic year. Important activities are:</p>
                <p>Dramatics ( English/Hindi/Nepali )<br>
                Public Speaking ( Declamation, Debate, Elocution, Extempore Speech, Story Telling &amp; Recitation )<br>
                Creative Writing ( Essay, Story &amp; Poetry )<br>
                Creative Work ( Magazine Designing, Wall Magazine, Bulletin Board, Flower Arrangement, Rangoli &amp; Salad Decoration )<br>
                Creative Art ( Painting, Drawing &amp; Sketching )<br>
                Quiz ( General Awareness, Current Affairs &amp; Situational Awareness )<br>
                Dance ( Classical &amp; Folk )<br>
                Music ( Classical &amp; Folk )</p>
                					</div><!-- .entry-content -->
                	</div><!-- .entry-content-wrapper -->

                	<footer class="entry-footer">
                			</footer><!-- .entry-footer -->
                </article>
            </section>


        </div>
    </div>
</div>


