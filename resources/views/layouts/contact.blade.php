@extends('layouts.app')

@section('content')
    <div class="error-message-contact">
        <div class="container">
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    </div>
    </div>

<div class="jumbotron jumbotron-sm contact-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1 text-center">
                    Have Questions?<br> <small>Ask us anything!</small></h1>
            </div>
        </div>
    </div>
</div>
<section class="contact-us">
<div class="container">
    <div class="row">
        <div class="col-sm-8 contact-left">
            <div class="well well-sm back-ground-none">
                <form action="/contact-store" method="POST">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Name</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="phone">
                                    Contact Number</label>

                                    <input type="tel" class="form-control" id="phone" name="phone_number" placeholder="Enter contact number" required="required" />
                            </div>
                            <div class="form-group">
                                <label>Captcha</label>
                                <input type="integer" name="firstNum" class="simple_Captcha form-control" style="display:inline-block; min-width: 17%; text-align: center; max-width: 17%;" readonly value="{{ rand(0,10) }}"> <label> &nbsp; + &nbsp; </label>
                                <input type="integer" name="secondNum" class="simple_Captcha form-control" style="display:inline-block; min-width: 17%; text-align: center; max-width: 17%;" readonly value="{{ rand(0,10) }}">
                                <label> &nbsp; = &nbsp; </label>
                                <input type="text" name="resultNum" style="display:inline-block; min-width: 17%; max-width: 17%; text-align: center;" class="simple_Captcha form-control" >
                            </div>

                        </div>
                        <div class="col-md-6">

                            <div class="form-group">
                                <label for="name">
                                    Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                      placeholder="Message"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                Send Message</button>
                        </div>
                        <div class="col-md-12">

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-sm-4 contact-address">
            <form>
                <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
                <address>
                    <strong>Rapid Results Gourmet</strong><br>
                    106 New King's Rd, Fulham <br>London SW6 4LY <br> United Kingdom<br>     
                </address>
                <address>
                    <strong>Email Us</strong><br>
                    <a href="mailto:info@rapidresultsgourmet.co.uk">info@rapidresultsgourmet.co.uk</a>
                </address>
                <address>
                    <strong>Call Us</strong><br>
                    <a class="contact_no" href="tel:+447927393481">+44 7927 393481</a>
                </address>
                <div class="col-sm-12 social-media">
                    <a class="facebook" href="https://www.facebook.com/rapidresultsgourmet" target="_blank"><i class="fa fa-facebook" style="font-size: 20px"></i></a>
                    <a href="https://twitter.com/search?q=rapidresultsgourmet" target="_blank"><i class="fa fa-twitter" style="font-size: 20px"></i></a>
                    <a href="https://www.instagram.com/rapidresultsgourmet/" target="_blank"><i class="fa fa-instagram" style="font-size: 20px"></i></a>
                </div>
            </form>
        </div>
    </div>
</div>
</section>
@endsection