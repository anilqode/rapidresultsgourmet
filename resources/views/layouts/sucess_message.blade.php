@if(Session::has('success'))
    <div class="alert alert-success">
        <strong>Congratulation! </strong>{{Session::get('success')}}
    </div>
@endif
@if(Session::has('alert'))
    <div class="alert alert-danger">
        <strong>Sorry! </strong>{{Session::get('alert')}}
    </div>
@endif