<section class="bottom-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-2">
                    <a href="/"><img src="/images/rrgimages/logo.jpg" alt="Rapid Results Gourmet logo"></a>
                </div>
                <div class="col-sm-7 developer">
                    <p><a href="/terms-and-condition" target="_blank">Terms &amp; Conditions</a> | <a
                            href="/privacy-policy" target="_blank">Privacy Statement</a></p>
                    <p>© Rapid Results Gourmet. All Rights Reserved.</p>

                    <p>Designed &amp; Developed by <a href="https://www.lucentbusiness.com/" target="_blank">Lucent
                            Business</a> in partnership with <a href="http://www.nepgeeks.com" target="_blank">Nepgeeks
                            Technology</a> </p>
                </div>
                <div class="col-sm-3 contact-details">
                    <ul>
                        <li><i class="fa fa-map-marker"></i>106 New King's Rd, Fulham <br>London SW6 4LY, United Kingdom
                        </li>
                        <li><i class="fa fa-envelope"></i><a class="envolepmail"
                                href="mailto:info@rapidresultsgourmet.co.uk">info@rapidresultsgourmet.co.uk</a></li>
                        <li><i class="fa fa-phone"></i><a href="tel:+447927393481">+44 7927 393481</a></li>
                    </ul>
                    <div class="social-media">
                        <a href="https://facebook.com/rapidresultsgourmet" target="_blank"><i class="fa fa-facebook"
                                style="font-size: 20px"></i></a>
                        <a href="https://twitter.com/search?q=rapidresultsgourmet" target="_blank"><i
                                class="fa fa-twitter" style="font-size: 20px"></i></a>
                        <a href="https://instagram.com/rapidresultsgourmet" target="_blank"><i class="fa fa-instagram"
                                style="font-size: 20px"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<a id="scrollTop" class="scroll-top-right" href="#">
    <span class="fa fa-long-arrow-up" aria-label="Scroll to the top of the page">

    </span>
</a>