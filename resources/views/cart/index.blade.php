@extends('layouts.app')
@section('content')
<div class="container cart-part">
	<div class="message-box">
            @include('layouts.error_message')
            @include('layouts.sucess_message_edit_testimonial')
        </div>
	<div class="table-respon">
		<table class="table table-hover cart-table">

		<thead>
			<td>S.N.</td>
			<td>Item(s)</td>
			<td>Quantity</td>
			<td>Price</td>
			<td>Total Price</td>
			<td>Actions</td>
		</thead>
		<?php $serial = 0 ;?>
		@foreach($cartItems as $cartItem)
			<tbody>
				<form action="/cart/update/{{ $cartItem->rowId}}/{{$cartItem->id}}" name="cartForm{{$serial}}" method="post">
				{{ csrf_field() }}
				<td>{{++$serial }}</td>
				<td>{{$cartItem->name}}</td>				
				<td><input type="number" name="quantity" min="0" id="cartquantity{{$serial}}" value="{{$cartItem->qty}}" onchange="changePrice(<?php  echo $serial; ?>)"/></td>
				<td>&pound; <span id="{{$serial}}" >{{$cartItem->price}}</span> </td>
				<td >&pound; <span id="totalPrice{{$serial}}">{{$cartItem->price * $cartItem->qty}}</span> </td>
				<td><!-- <button type="submit" class="btn btn-info"> Update</button> |  --><a href="/cart/delete/{{ $cartItem->rowId }}/{{$cartItem->id}}" class="btn btn-danger">Delete</a></td>
				</form>
			</tbody>
		@endforeach
		<tbody>
			<td></td>
			<td colspan="2">Sub total</td>
			<td colspan="2">&pound; <span id="netTotal">{{ Cart::subtotal()}}</span></td>
			<td colspan="1"></td>
		</tbody>
	</table>
	</div>
	<a href="/fitreats-and-supplements" class="btn btn-info left">Continue Shopping</a>
	<a href="/dashboard" class="btn btn-primary right">Proceed to Check Out</a>
</div>
<script>
	let unitPrice , qty, totalPrice, netTotal=0, counter = <?php echo $serial;?>, i;
	function changePrice(numb){
		unitPrice = document.getElementById(numb).innerHTML;
		unitPrice = parseInt(unitPrice);
		totalPrice = document.getElementById("totalPrice"+numb);
		qty = document.getElementById("cartquantity"+numb).value;
		totalPrice.innerHTML = unitPrice * qty;
		cartTotal(numb);
	}

	function cartTotal(numb){
		let temp = [];
		for(i = 1; i <= counter; i++){
			temp[i] = document.getElementById("totalPrice"+i).innerHTML;
			temp[i] = parseInt(temp[i]);
		}
		netTotal = temp.reduce(add, 0);
		document.getElementById('netTotal').innerHTML = parseFloat(netTotal);
		numb -= 1;
		numb = "cartForm"+numb;
		document.forms[numb].submit();
	}
	function add(a,b){
		return a+b;
	}
</script>
@endsection
