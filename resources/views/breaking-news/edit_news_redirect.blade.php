@extends('layouts.app')

@section('content')
    <div class="container-fluid progressive-natti-body">
        <div class="row">
            <div class="col-sm-3">
                @include('admin-panel.left-nav')
            </div>
            <div class="col-sm-9">

                @include('breaking-news.edit_news')
            </div>
        </div>
    </div>

@endsection


