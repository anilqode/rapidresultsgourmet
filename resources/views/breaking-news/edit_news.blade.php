<div class="news-edit-section">
    <div class="table-header"><h4 class="panel-heading ">UPADATE BREAKING NEWS</h4></div>
    <div class="row">
        <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
    {{ csrf_field() }}

    <div class="row">
        {!! Form::model($newsInfo,['method'=>'PATCH','files' => true,'action'=>['BreakingNewsController@update',$newsInfo->id]]) !!}

        <div class="col-sm-12 ">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="form-group{{ $errors->has('title') ? 'has-error' : '' }}">
                        <label for="title" class="col-md-4 control-label">Title</label>
                        <div class="col-md-10 col-sm-12">

                            {{ Form:: text('title', $newsInfo->title,  array("class" => "form-control margin-bottom-12", 'id' => 'title')) }}
                        </div>
                    </div>








                    <div class="form-group">
                        <div class="col-sm-12">

                            {{ Form::submit('Update', array('class' => 'btn btn-primary', 'id' => 'submit_id')) }}

                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{ Form:: close() }}
    </div>


</div>