
<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         true,
                } );
            } );

        </script>

        {{--  <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>TITLE</th>
                <th>EDIT</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>TITLE</th>
                <th>EDIT</th>
            </tr>
            </tfoot>
            <tbody>


            @foreach($newsInfo  as $news)

                <tr>

                    <td>{{$news->id}}</td>
                    <td>{{$news->title }}</td>
                    <td><a href="news/<?php echo $news->id; ?>/edit">Edit</a>/<a href="/news/<?php echo $news->id; ?>/delete/ "  onclick="return confirm('Are you sure?')" class="user-delete">Delete</a></td>
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>