<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         true,
                } );
            } );

        </script>

        {{--  <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th>Order ID</th>
                <th>Meal Name</th>
                <th>Quant</th>
                <th>Price</th>
               
            </tr>
            </thead>
            
            <tbody>


            @foreach($orderDetails as $orderInfo)

                <tr>
                    <td>{{$orderInfo->user_id}}</td>
                    <td>{{$orderInfo->meal_name}}</td>
                    <td>{{$orderInfo->meal_quantity}}</td>
                    @php
                        $product = \App\Product::where([
                            ['week_id', $orderInfo->weekplans_id],
                            ['meal_id', $orderInfo->meal_id]])->first();
                    
                        $price = $orderInfo->meal_quantity * $product->price;
                    @endphp
                    <td>{{$price}}</td>
                    
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>