<div id="content" class="orderDetail">
    <table style="margin: 0 auto;text-align: left;border: 1px solid black;">
        <tr>
            <td><b>Order Number</b></td>
            <td style="padding-left: 20px;"> {{ $orderInfo->plan_no}} </td>
        </tr>
        <tr>
            <td><b>Plan Name</b></td>
            <td style="padding-left: 20px;"> {{ $orderInfo->plan_name }}</td>
        </tr>
        <tr>
            <td><b>Meals</b></td>
            <td style="padding-left: 20px;
						">{{ $orderInfo->vegan }}</td>
        </tr>
        <tr>
            <td><b>Complimentary Snacks</b></td>
            <td style="
							padding-left: 20px;">{{$complimentary_snacks}} </td>
        </tr>
        <tr>
            <td><b>Extra Snacks</b></td>
            <td style="
								padding-left: 20px;">{{$extra_snacks_set_meal }} </td>
        </tr>
        <tr>
            <td><b>Fitreats</b></td>
            <td style="
								padding-left: 20px;">{{$fitreats}}</td>
        </tr>
        <tr>
            <td><b>Total Amount Paid</b></td>
            <td style="
									padding-left: 20px;">£{{$orderInfo->total_price}}</td>
        </tr>
        <tr>
            <td><b>Customer Name</b></td>
            <td style="
										padding-left: 20px;">{{$orderInfo->name}}</td>
        </tr>
        <tr>
            <td><b>Customer Phone Number</b></td>
            <td style="
											padding-left: 20px;">{{ $orderInfo->phone_number }}</td>
        </tr>
        <tr>
            <td><b>Customer Address</b></td>
            <td style="
												padding-left: 20px;">{{$orderInfo->addressline1}} {{$orderInfo->addressline2 }}  {{$orderInfo->addressline3}} {{$orderInfo->postcode}}</td>
        </tr>
        <tr>
            <td><b>Customer Email</b></td>
            <td style="
													padding-left: 20px;">{{$orderInfo->email}}</td>
        </tr>
        <tr>
            <td><b>Customer Request</b></td>
            <td style="
													padding-left: 20px;">{{$orderInfo->review}}</td>
        </tr>
    </table>
</div><!-- #content -->
