<?php
if(!isset($_SESSION))
{
    session_start();
}
?>

@extends('layouts.app')

@section('content')
<div class="container-fluid progressive-natti-body">
    <div class="row">
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            <div class="tab-content">
                <div id="class1" class="tab-pane fade in active">
                    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
                    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

                    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
                    <script>
                        $(document).ready(function() {
                            $('#example').DataTable( {
                                "scrollY":        "200px",
                                "scrollCollapse": true,
                                "paging":         true,
                            } );
                        } );

                    </script>

                    {{--  <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
                        <table id="example" class="display" cellspacing="0" width="100%">
                            <thead>
                                <tr>

                                    <th>Order ID</th>
                                    <th>FiTreat and Supplement Items</th>
                                    <th>Amt Paid</th>
                                    <th>Cust Name</th>
                                    <th>Cust Phone</th>
                                    <th>Cust Email</th>
                                    <th>Cust Address</th>
                                </tr>
                            </thead>

                            <tbody>


                                @foreach($fitreats_order_detail as $fitreats_order_details)

                                <tr>
                                    <td>{{ $fitreats_order_details->order_id }}</td>
                                    <td>{{ $fitreats_order_details->orders }}</td>
                                    <td>{{ $fitreats_order_details->total_price }}</td>
                                    <td>{{ $fitreats_order_details->name }}</td>
                                    <td>{{ $fitreats_order_details->phone_number }}</td>
                                    <td>{{ $fitreats_order_details->email }}</td>
                                    <td>{{$fitreats_order_details->address}} {{$fitreats_order_details->addressline1}} {{$fitreats_order_details->addressline2}}- {{$fitreats_order_details->postcode}}</td>
                                </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
    @endsection
