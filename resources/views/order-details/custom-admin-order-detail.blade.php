<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable( {
                    "scrollY":        "200px",
                    "scrollCollapse": true,
                    "paging":         true,
                } );
            } );

        </script>

        {{--  <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>

                <th>Order ID</th>
                <th>Meal Name</th>
                <th>Veg and Carb</th>
                <th>Quant</th>
                <th>Price</th>

                <th>Cust Name</th>
                <th>Cust Phone</th>
                <th>Cust Email</th>
                <th>Cust Address</th>
                <th>Edit</th>
            </tr>
            </thead>
            
            <tbody>


            @foreach($orderDetails as $orderInfo)
                <tr>
                    <td>{{$orderInfo->user_id}}/{{$orderInfo->plan_no}}</td>
                    <td>{{$orderInfo->meal_name}}</td>
                    <td>
                        @foreach (explode('+', $orderInfo->vegcarb) as $vegcarb)
                            @if ($loop->first) @continue @endif
                                ({{ $vegcarb }})
                        @endforeach
                    </td>
                    <td>{{$orderInfo->meal_quantity}}</td>
                    @php
                        $product = \App\Product::where([
                            ['week_id', $orderInfo->weekplans_id],
                            ['meal_id', $orderInfo->meal_id]])->first();
                    
                        $price = $orderInfo->meal_quantity * $product->price;
                    @endphp
                    <td>{{$price}}</td>
                    @php
                    $user = \App\User::where('id', $orderInfo->user_id
                                    )->first();
                    @endphp
                    <td>{{$user->name}}</td>
                    <td>{{$user->phone_number}}</td>
                      <td>{{$user->email}}</td>
                        <td>{{$user->address}} {{$user->addressline1}} {{$user->addressline2}} {{$user->addressline3}} - {{$user->postcode}}</td>
                    <td>
                        <a href="/custom-edit/{{$orderInfo->user_id}}/{{$orderInfo->plan_no}}"><i class="fa fa-edit"></i></a>
                    </td>
                          
                </tr>

            @endforeach
            </tbody>
        </table>
    </div>
</div>