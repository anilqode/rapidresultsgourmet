<div class="panel-heading page-title">
    <h4>Order Summary</h4>
</div>

<div class="tab-content">
    <div id="class1" class="tab-pane fade in active">

        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example').DataTable({

                    "scrollCollapse": true,
                    "paging": true,
                });
            });
        </script>

        {{-- <table id="question-answer-report" class="display-question-answer-report" cellspacing="0" width="100%">--}}
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
                <tr>

                    <th>Order ID</th>

                  
                    <th>Order Detail</th>
                </tr>
            </thead>

            <tbody>


                @foreach($orderDetails as $orderInfo)
                <tr>
                    <td>{{$orderInfo->id}}</td>
                    
                    <td>
                    <button type="button" class="btn   viewDetail menuCheckButton " onclick="loadOrderDetail('1', '{{$orderInfo->id}}')" data-toggle="modal">
                            View Detail
                        </button>
                    </td>
                </tr>
                @endforeach
              
            </tbody>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">


                <h5 class="modal-title text-center" id="exampleModalLabel"><h3>Order Details</h3></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="bindHere">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
			    body{
					font-family: \'Roboto\', sans-serif;
					padding: 0;
					margin: 0;
				}

             #wrapper{
				width: 100%;
				max-width: 600px;
				margin: 0 auto;
				box-shadow: 0px 3px 6px #000;
				border: 1px solid #ccc;
			}

        #header{
			width: 100%;
			height: 155px;
			padding: 20px 0 7px;
		}

#header h4 {
		color: #000000;
		font-size: 30px;
		font-weight: 500;
		text-transform: uppercase;
	}


#content h5 {
	color: #666666;
	font-size: 24px;
	font-weight: 700;
}

#content h6 {
		color: #04a7e0;
		font-size: 30px;
		font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
	text-align: center;
}

.top_30{
	margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}

.orderDetail td{
	padding: 8px;
    width:50%;
   
}

</style>

<script>
    function loadOrderDetail(userType, plan_no) {



        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
        jQuery.ajax({
            url: "/loadOrderDetail",
            type: 'post', // replaced from put
            dataType: "JSON",
            data: {
                "userType": userType,
                "plan_no": plan_no // method and token not needed in data
            },
            success: function(response) {
               

                console.log('response is', response);

                jQuery("#bindHere").empty();

                jQuery("#bindHere").append(response.html);
                jQuery('#exampleModalLong').modal('show');

                //console.log(response); // see the reponse sent

                //jQuery.noConflict();
               
            },
            error: function(xhr) {
              
                console.log(xhr.responseText); // this line will save you tons of hours while debugging
                // do something here because of error
            }
        });
    }
</script>