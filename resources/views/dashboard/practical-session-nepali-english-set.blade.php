<?php
if(!isset($_SESSION))
{
    session_start();
}
?>
<section class="practical-session-questionsets">
<div class="container">
    <div class="col-sm-12">
        <h4>Practical Session Sub-Category</h4>
        @foreach($category as $cat)
<div class="col-sm-2 practical-questionsets">
    <a href="/user-recording-question-set/{{$cat->dictionary_cat_id}}/{{$cat->id}}"><p>{{$cat->name}}</p></a>

</div>

            @endforeach
    </div>
</div>
</section>