<div class="container " style="padding: 90px 0px; text-align: center">
<img src="/images/rrgimages/logo.png" alt="Rapid Results Gourmet">
<div class="down-message" style="font-size: 32px">
<p>Sorry, We are making a few changes and upgrades to our website. <br><br>If you have any enquiries please DM us through instagram or email us at <strong>info@rapidresultsgourmet.co.uk</strong> or even you can call or whatsapp to <strong>07927393481</strong>.<br><br><br>Thank you and sorry for any inconvenience.</p>
</div>
</div>