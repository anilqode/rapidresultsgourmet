<?php
if(isset($_GET['resultIndicator'])){
    echo "Response from server. <hr>";
    print_r($_GET);
    echo "<hr>";
}
?>


    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://paymentgateway.commbank.com.au/checkout/version/47/checkout.js"
            data-error="errorCallback"
            data-cancel="cancelCallback",
            data-complete="completeCallback",
            data-beforeRedirect="Checkout.saveFormFields"
            data-afterRedirect="Checkout.restoreFormFields">
    </script>
    <script type="text/javascript">


        var sessionId= '';
        var sessionVersion = '';
        var status='';
        var successIndicator = '';

        function getSession(){

            fetch('/fetch').then(function(response) {
                return response.text();
            }).then(function(data) {
                console.log(data);
                $('body').append(data);
                split = data.split('+');
                status = split[0];
                successIndicator= split[1];
                sessionId = split[2];
                sessionVersion= split[3];

                console.log('status => '+status+" successIndicator => "+successIndicator);


                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    url: '/enrol-now-store',
                    type: 'get',
                    data: {successIndicator: successIndicator},
                    dataType: 'JSON',
                    success: function (data) {
                        console.log(data);
                    }
                });

            }).catch(function(error) {
                $('body').append('Error: ' + error);
            });
        }
    </script>


    <script type="text/javascript">
        var amt ='';
        var id='';
        var desc ='';

        function paynow(amt, id,desc) {
            console.log("SessionId => "+sessionId)
            console.log("SessionVersion => "+sessionVersion);

            Checkout.configure({
                merchant: 'TESTPROSTUCOM201',
                order: {
                    amount: function () {
                        //Dynamic calculation of amount
                        return amt;
                    },
                    currency: 'AUD',
                    description: function () {
                        //Dynamic calculation of amount
                        return desc;
                    },
                    id: function () {
                        //Dynamic calculation of amount
                        return id;
                    },
                },
                interaction: {
                    merchant: {
                        name: 'ProgressiveNaati',
                        address: {
                            line1: 'Level 2,552 Princes Highway Rockdale,',
                            line2: 'New South Wales 2216, Australia'
                        }
                    }
                },
                session: {
                    id: sessionId,
                    version: sessionVersion
                }
            });
        }
    </script>

    <script type="text/javascript">
        var amt;
        var desc, id;

        $(document).ready(function(){
            $('#first').click(function () {
                amt = $('#amount').val();
                desc = $('#description').val();
                id = $('#id').val();
                getSession();
                // Delay is set to give some time for the resposne data to set to our variable
                setTimeout(function() {
                    paynow(amt, id,desc);//your code to be executed after 2 second
                }, 2000);
                // Delay is given to pause the asynchronous execution of the JS.
                setTimeout(function() {
                    // Checkout.showLightbox();
                    Checkout.showPaymentPage();
                }, 2000);
            });
        });

        function errorCallback(error) {
            console.log(JSON.stringify(error));
        }

        function cancelCallback() {
            alert('Payment cancelled');
        }

        function completeCallback(resultIndicator, sessionVersion){
            console.log('Success with session data =>  resultIndicator: '+resultIndicator+ "<br> sessionVersion: "+sessionVersion);
            // alert('Success Payment')
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/verify',
                type: 'get',
                data: {resultIndicator: resultIndicator},
                dataType: 'JSON',
                success: function (data) {
                    alert(data['message']);
                }
            });
        }
    </script>
</head>
<body>


<input type="hidden" name="amount" value="<?php echo $express->amount; ?>" id="amount">
<input type="hidden" name="id" value="{{$express->id}}" id="id">
{{-- <input type="hidden" name="course" value="{{$express->course}}" id="course">--}}
<input type="hidden" name="description" value="{{$express->description}}" id="description">

<input type="button" id="first" value="Pay with Payment Page" onclick="Checkout.showPaymentPage();" />