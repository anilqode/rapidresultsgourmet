<div class="tab-content">
    <div id="class" class="tab-pane fade in active">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

        <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
        <script>
            $(document).ready(function() {
                $('#example1').DataTable();

                $('#example').DataTable( {

                    searching: false, paging: false, info: false, fixedHeader: false
                } );
            } );

        </script>

        @if($userInfo->role_id ==1)
        <a href="/exportCSV" class=" btn exportCSV"><i class="fas fa-file-export"></i>Export as CSV</a>
        <table id="example1" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Image</th>
                <th>Role</th>
                <th>Name </th>
                <th>E-mail </th>
                <th>Edit/Delete</th>

            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>Image</th>
                <th>Role</th>
                <th>Name </th>
                <th>E-mail </th>
                <th>Edit/Delete</th>

            </tr>
            </tfoot>
            <tbody>


            @foreach($user_infos as $userinfo)

                <tr>

                    <td> <img src=" /{{ $userinfo->image_path}}" class="user-pic"/></td>
                    <td><span class="user-role"><b> <?php $h= $userinfo->role_id ;
                                switch($h)

                                {
                                    case 1: echo 'Administrator';
                                        break;
                                    case 2: echo 'User';
                                        break;

                                    default: echo 'child';

                                }
                                ?></b></span></td>
                    <td><span class="user-red"><b>{{ $userinfo->name}}</b></span></td>
                    <td>{{ $userinfo->email}}</td>
                    <td><a href="/userinfo/{{$userinfo->id}}/edit" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                        <a href="/userinfo/{{$userinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                    </td>

                </tr>

            @endforeach
            </tbody>
        </table>
        @else
        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Image</th>
                <th>Role</th>
                <th>Name </th>
                <th>E-mail </th>
                <th>Edit/Delete</th>

            </tr>
            </thead>
            <tbody>


            @foreach($user_infos as $userinfo)

                <tr>

                    <td> <img src=" /{{ $userinfo->image_path}}" class="user-pic"/></td>
                    <td><span class="user-role"><b> <?php $h= $userinfo->role_id ;
                                switch($h)

                                {
                                    case 1: echo 'Administrator';
                                        break;
                                    case 2: echo 'User';
                                        break;

                                    default: echo 'child';

                                }
                                ?></b></span></td>
                    <td><span class="user-red"><b>{{ $userinfo->name}}</b></span></td>
                    <td>{{ $userinfo->email}}</td>
                    <td><a href="/userinfo/{{$userinfo->id}}/edit" class="user-edit"><i class="fa fa-pencil" aria-hidden="true"></i>  Edit</a>
                        <a href="/userinfo/{{$userinfo->id}}/delete" onclick="return confirm('Are you sure?')" class="user-delete"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                    </td>

                </tr>

            @endforeach
            </tbody>
        </table>
        @endif
    </div>
</div>




