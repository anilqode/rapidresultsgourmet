
@extends('layouts.app')

@section('content')
    <div class="row">
        <?php
        if($userInfos->role_id==1)
{?>
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            @include('user_information.user_info_edit_form')
        </div>
        <?php
}else{ ?>
  <div class="col-sm-3">
            @include('admin-panel.user-nav')
        </div>
        <div class="col-sm-9">
            @include('user_information.user_info_edit_form')
        </div>
        <?php } ?>

    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection
