
@extends('layouts.app')

@section('content')
<?php

if($user_info->role_id==1)
{?>
 <div class="col-sm-3">
    @include('admin-panel.left-nav')
    {{--@include('vendor.voyager.deviceinfo.deviceinfo')--}}
    </div>
    <div class="col-sm-9">
    @include('user_information.change-password-form')
</div>
    <div class="clear-both" style="clear:both;"></div>
<?php
}else{ ?>
     <div class="col-sm-3">
    @include('admin-panel.user-nav')
    {{--@include('vendor.voyager.deviceinfo.deviceinfo')--}}
    </div>
    <div class="col-sm-9">
    @include('user_information.change-password-form')
</div>
    <div class="clear-both" style="clear:both;"></div>
<?php }
?>

   
@endsection