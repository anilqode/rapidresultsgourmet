

@extends('layouts.app')

@section('content')
    <div class="container-fluid progressive-natti-body">
    <div class="row">
        <?php if($userInfo->role_id ==1){ ?>


        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            @include('user_information.user_info_display')
        </div>


            <?php }
            else if($userInfo->role_id ==2)
            { ?>
 <div class="col-sm-3">
            @include('admin-panel.user-nav')
        </div>
            <div class=" col-sm-9">
                @include('user_information.user_info_display')
            </div>
            <?php  } ?>
    </div>
</div>
@endsection
