@extends('layouts.app')

@section('content')

 
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 ">
            @include('layouts.error_message')
            @include('layouts.sucess_message')
        </div>
    </div>
 </div>  
 <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 naati-login">
            <div class="panel-info panel-default" style="margin-top:10px;">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6 col-sm-12 login-email">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6 col-sm-12 login-password">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        @isset($Normalid)
                            <input type="hidden" name="Normalid" value="{{$Normalid}}">
                        @endisset

                        @isset($week_id)
                            <input type="hidden" name="week_id" value="{{$week_id}}">
                        @endisset

                        @if(Session::has('mealTitle'))

                                    <input type="hidden" name="mealTitle" value="{{ Session::get('mealTitle') }}">

                                    <input type="hidden" name="total_price" value="{{ Session::get('total_price') }}">

                                    <input type="hidden" name="vegan" value="{{ Session::get('vegan') }}">

                                    <input type="hidden" name="vegan_price" value="{{ Session::get('vegan_price') }}">

                                    <input type="hidden" name="complementary_snacks" value="{{ Session::get('complementary_snacks') }}">

                                    <input type="hidden" name="extra_snacks" value="{{ Session::get('extra_snacks') }}">

                                    <input type="hidden" name="extra_price" value="{{ Session::get('extra_price') }}">

                                    <input type="hidden" name="gourment_snacks" value="{{ Session::get('gourment_snacks') }}">

                                    <input type="hidden" name="gourment_price" value="{{ Session::get('gourment_price') }}">

                                @else

                                    <input type="hidden" name="mealTitle" value="NAN" readonly>

                                    <input type="hidden" name="totalAmount" value="NAN" readonly>

                                @endif

                        <div class="form-group">
                            <div class="col-md-8  col-md-offset-4 submit">
                                <button type="submit" class="btn btn-primary login-btn">
                                    Login
                                </button>
                            </div>
                            <div class="col-md-12 forgot-password text-center" >
                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                    <div class="text-center" style="display:none;">
                        <form method="get" action="/redirect-from-custom-to-register">
                            If you are not registered, Please register

                            @isset($Normalid)
                                <input type="hidden" name="Normalid" value="{{$Normalid}}">
                            @endisset

                            @isset($week_id)
                                <input type="hidden" name="week_id" value="{{$week_id}}">
                            @endisset


                            @if(Session::has('mealTitle'))

                                    <input type="hidden" name="mealTitle" value="{{ Session::get('mealTitle') }}">

                                    <input type="hidden" name="total_price" value="{{ Session::get('total_price') }}">

                                    <input type="hidden" name="vegan" value="{{ Session::get('vegan') }}">

                                    <input type="hidden" name="vegan_price" value="{{ Session::get('vegan_price') }}">

                                    <input type="hidden" name="complementary_snacks" value="{{ Session::get('complementary_snacks') }}">

                                    <input type="hidden" name="extra_snacks" value="{{ Session::get('extra_snacks') }}">

                                    <input type="hidden" name="extra_price" value="{{ Session::get('extra_price') }}">

                                    <input type="hidden" name="gourment_snacks" value="{{ Session::get('gourment_snacks') }}">

                                    <input type="hidden" name="gourment_price" value="{{ Session::get('gourment_price') }}">

                                @else

                                    <input type="hidden" name="mealTitle" value="NAN" readonly>

                                    <input type="hidden" name="totalAmount" value="NAN" readonly>

                                @endif

                            <input type="submit" class="btn" value="Register">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
