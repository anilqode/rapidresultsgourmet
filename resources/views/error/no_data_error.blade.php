@extends('layouts.app')

@section('content')
    <div class="row">

        <div class="col-sm-12 no-error-data">
            <h4>The page You are Searching is Currently Unavailable. Please Check it later.</h4>
            <h5>Thank You.</h5>
        </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection
