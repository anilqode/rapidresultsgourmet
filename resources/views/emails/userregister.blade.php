<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2> New Account Register</h2><br/>

        <div>
           <p><strong>{{ $email}}</strong> has created an account with the https://www.progressivenaati.com.au.</p> <br/>
           
<h4>Thank you!</h4>
<p><strong>Progressive Educational Training Services Pty Ltd (Trading as: Progressive Study Centre)<strong></p>

<p>Address: Level 2, 552 Princes Highway, Rockdale, NSW 2216, Australia</p>

<p>Email: admin@progressivenaati.com.au</p>

<p>Phone: 02 8542 3501</p>

<p>ABN: 38 622 120 361</p>
        </div>

    </body>
</html>