@extends('layouts.app')

@section('content')
<section class="Payement-invoice">
   <div class="container">
       <div class="row">
           <div class="col-sm-12">
               <h4 class="pay-invoice-title">Payment Details</h4>
               
               @foreach($enroll as $en)
               <div class="invoce-box">
<p><span>Dear </span><strong>{{$en->email}}</strong>,</p>
<p>You have recently made the payment of <strong>$AUD{{$en->amount}}</strong> for the Course:<strong>{{$en->description}}</strong>  </p>
<strong>Note: </strong><span>Please check your mail for further Information.</span>
<h5>Thank you!</h5>
<p><strong>Progressive Educational Training Services Pty Ltd (Trading as: Progressive Study Centre)<strong></p>

<p>Address: Level 2, 552 Princes Highway, Rockdale, NSW 2216, Australia</p>

<p>Email: admin@progressivenaati.com.au</p>

<p>Phone: 02 8542 3501</p>

<p>ABN: 38 622 120 361</p>
  </div>
@endforeach
               
           </div>
       </div>
   </div> 
    
</section>
@endsection
 
 
