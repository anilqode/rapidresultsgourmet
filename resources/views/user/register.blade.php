
@extends('layouts.app')

@section('content')
    <div class="container-fluid progressive-natti-body">

    <div class="row">
       <?php  use Illuminate\Support\Facades\Auth;
        $userInfo=Auth::User();

            if($userInfo)
            {
        if($userInfo->role_id==1){ ?>
        <div class="col-sm-3">
            @include('admin-panel.left-nav')
        </div>
        <div class="col-sm-9">
            @include('user.register-form')
        </div>
        <?php }
           else
           {
           ?>
           <div class="col-sm-12">
               @include('user.register-form')
           </div>

        <?php } }
           else{?>
           <div class="col-sm-12">
               @include('user.register-form')
           </div>
        <?php } ?>
    </div>
    </div>
    <script type="text/javascript" src="js/imagePreview.js"></script>
@endsection