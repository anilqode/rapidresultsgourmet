<?php

?>
<div class="row">
    <div class="col-sm-12">
        @include('layouts.error_message')
        @include('layouts.sucess_message')
    </div>
</div>
<div class="user-registration">
    <div class="row">

        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading sign-up-heading"><h4>SIGN UP</h4></div>

                <div class="panel-body">
                    {!! Form::open(array('url'=>'/user-register','method'=>'POST','files' => true )) !!}
                    {{--<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">--}}
                    {{ csrf_field() }}
                    <div class="row">

                        <div class="col-sm-8 register-form-user">
                            <div class="panel panel-default ">

                                <div class="panel-body">

                                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                        <label for="name" class="col-md-12 control-label">Name</label>

                                        <div class="col-md-12">
                                            <input id="name" type="text" class="form-input" name="name" value="{{ old('name') }}" placeholder="Enter your Name" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label for="email" class="col-md-12 control-label">E-Mail Address</label>

                                        <div class="col-md-12">
                                            <input id="email" type="email" class="form-input" name="email" value="{{ old('email') }}" placeholder="Enter Valid Email Address" required>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        <label for="phone" class="col-md-12 control-label">Phone Number</label>

                                        <div class="col-md-12">
                                            <input id="phone" type="phone" class="form-input" name="phone" value="{{ old('phone') }}" placeholder="Enter Valid Phone Number" required>

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-12 control-label">Password</label>

                                        <div class="col-md-12">
                                            <input id="password" type="password" class="form-input" name="password" placeholder="Enter Password" required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm" class="col-md-12 control-label">Confirm Password</label>

                                        <div class="col-md-12">
                                            <input id="password-confirm" type="password" class="form-input" name="password_confirmation" placeholder="Confirm Password" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="dob" class="col-md-6 control-label">Date of Birth</label>
                                        <div class="col-md-6">
                                            <input id="date" type="date" name="dob" value="2017-06-01">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="gender" id="gender" class="col-md-6 control-label">Gender</label>
                                        <div class="col-sm-6">
                                            <div class="radio-container">
                                                <input checked class="hidden-input" type="radio" id="Male" name="gender" value="male">
                                                <label class="" for="Male">Male</label>
                                            </div>
                                            <div class="radio-container">
                                                <input  class="hidden-input" type="radio" id="Female" name="gender" value="female">
                                                <label class="" for="Female">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-md-12 control-label">Delivery Address</label>

                                        <div class="col-md-12">
                                            <input id="address" type="text" class="form-input " name="address" value="{{ old('address') }}" placeholder="Address" required>
                                            <input id="town" type="text" class="form-input address" name="town" value="{{ old('town') }}" placeholder="Town/City" required >
                                            <input id="county" type="text" class="form-input address" name="county" value="{{ old('county') }}" placeholder="County" required>
                                            <input id="postcode" type="text" class="form-input address" name="postcode" value="{{ old('postcode') }}" placeholder="Post Code" required>
                                        </div>
                                    </div>

                                    </div>
                            </div>
                        </div>
                        <div class="col-sm-4 margin-left-20 user-form-signup-image">
                            {!! Form::label('featured_image','Upload Profile Image',array('id'=>'','class'=>'margin-top-20')) !!}
                            {!! Form::file('image',array('class'=>'imageUpload','id'=>'uploadFile')) !!}
                            <div class="form-group userprofile-bg" id="imagePreview">

                            </div>    </div>
                        <div class="form-group register-me">
                            <div class="col-sm-12">
                                <div class="radio-container terms-condition">
                                    <input class="hidden-input" name="terms" type="checkbox" id="Terms" value="1" data-parsley-required="" data-parsley-error-message="Please read and agree to our terms and conditions" data-parsley-multiple="terms" value="null" required>
                                    <label class="" for="Terms">By signing up you agree RRG <a href="/T&C/" target="_blank">terms and conditions</a></label>
                                </div>

                                @if(Session::has('id'))
                                    <input type="text" name="id" value="{{Session::get('id')}}">
                                @endif

                                @if(Session::has('idFromBasket'))
                                    <input type="hidden" name="idFromBasket" value="{{ Session::get('idFromBasket') }}">
                                @else

                                    <input type="hidden" name="idFromBasket" value="NAN" readonly>

                                @endif
                                @if(Session::has('mealTitle'))

                                    <input type="hidden" name="mealTitle" value="{{ Session::get('mealTitle') }}">

                                    <input type="hidden" name="total_price" value="{{ Session::get('total_price') }}">

                                    <input type="hidden" name="vegan" value="{{ Session::get('vegan') }}">

                                    <input type="hidden" name="vegan_price" value="{{ Session::get('vegan_price') }}">

                                    <input type="hidden" name="complementary_snacks" value="{{ Session::get('complementary_snacks') }}">

                                    <input type="hidden" name="extra_snacks" value="{{ Session::get('extra_snacks') }}">

                                    <input type="hidden" name="extra_price" value="{{ Session::get('extra_price') }}">

                                    <input type="hidden" name="gourment_snacks" value="{{ Session::get('gourment_snacks') }}">

                                    <input type="hidden" name="gourment_price" value="{{ Session::get('gourment_price') }}">
<input type="hidden" name="total_price" value="{{ Session::get('total_price') }}">

                                        <input type="hidden" name="review" value="{{ Session::get('review') }}">
                                        <input type="hidden" name="delivery_charge" value="{{ Session::get('total_delivery_cost') }}">

                                @else

                                    <input type="hidden" name="mealTitle" value="NAN" readonly>

                                    <input type="hidden" name="totalAmount" value="NAN" readonly>

                                @endif

                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </div>
                    {{ Form:: close() }}
                </div>
            </div>
        </div>
    </div>

</div>
<script>

    var dateControl = document.querySelector('input[type="date"]');
    dateControl.value = '2017-06-01';
</script>