@extends('layouts.app')

@section('content')

	<section class="contact-us">
    <div class="container">
        <div class="row">
            <div class="col-md-8 contact-left">
                <div class="well well-sm back-ground-none">
                    <form action="/contact-store" method="POST">
                        <input type="hidden" name="_token" value="y9Eo8bdJ5HHMBjLWnr4nMmIPlItPex0p1RkcFRu6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">
                                        Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter name" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="email">
                                        Email Address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="phone">
                                        Contact Number</label>

                                    <input type="tel" class="form-control" id="phone" name="phone_number" placeholder="Enter contact number" required="required">
                                </div>
                                <div class="form-group">
                                    <label for="subject">
                                        What is your Meal Choose?</label>
                                    <select id="subject" name="subject" class="form-control" required="required">
                                        <option value="na" selected="">Choose One:</option>
                                        <option value="Nursing">2 Weeks</option>
                                        <option value="Engineering">4 Weeks</option>
                                        <option value="Accounting">6 Weeks</option>
                                        <option value="Others">Others</option>


                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="subject">
                                        What is your goal ?</label>
                                    <select id="course" name="course" class="form-control" required="required">
                                        <option value="na" selected="">Choose One:</option>
                                        <option value="Yes">Weight Loss</option>
                                        <option value="No">Weight Maintain</option>
                                        <option value="No">Weight Gain</option>


                                    </select>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="name">
                                        Message</label>
                                    <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required" placeholder="Message"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                    Send Message</button>
                            </div>
                            <div class="col-md-12">

                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4 contact-address">
                <form>
                    <legend><span class="glyphicon glyphicon-globe"></span>&nbsp;Our office</legend>
                    <address>
                        <strong>Rapid Results Gourmet</strong><br>
                        Level 2,552 Princes Highway Rockdale,<br>
                        New South Wales 2216, Australia<br>

                    </address>
                    <address>
                        <strong>Email Us</strong><br>
                        <a href="mailto:info@progressivenaati.com.au">info@aapid.com.uk</a>
                    </address>
                    <address>
                        <strong>Call Us</strong><br>
                        <a href="tel:0285423501">02 8542 3501 (Ext 2)</a>
                    </address>
                    <div class="col-sm-12 social-media">
                        <a class="facebook" href=""><i class="fa fa-facebook"></i></a>
                        <a href=""><i class="fa fa-twitter"></i></a>
                        <a href=""><i class="fa fa-instagram"></i></a>
                        <a href=""><i class="fa fa-youtube"></i></a>
                        <a href=""><i class="fa fa-youtube"></i></a>
                    </div>
                    <!--<address>
                        <strong>ABN</strong><br>
                        <a>38 622 120 361</a>
                    </address>-->
                </form>
            </div>
        </div>
    </div>
</section>
@endsection