<section class="meal-section">
    <video id="video" preload src="images/rrgimages/Videorrg.MOV" autoplay loop controls muted></video>
    <div class="retreat-img">
        <img class="retreat-logo" src="images/rrgimages/retreats.jpeg"><br>
        <img class="sec-logo" src="images/rrgimages/sec-logo.jpeg">
    </div>
    <div class="moz-logo">
        <img src="images/rrgimages/logomoz.jpeg">
    </div>
    <div class="container">
        <div class="text-meal">
            <p>
            MORE Travel. MORE Fitness. MORE Yoga. MORE Energy. MORE Friends. MORE Adventure.</p>
            <img src="\images\rrgimages\retreats.jpg" alt="retreats">
            <p>
            Drop a message at info@rapidresultsgourmet.co.uk to let us know you’re interested and we’ll send you info on our exclusive early bird deals, payment plans and packages!</p>
        </div>
        <div class="parent1">
            <div class="parent"> 
                <div class="img">
                    <img src="images/rrgimages/well-being1.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being2.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being8.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being10.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being3.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being4.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being7.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being9.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being5.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/img1.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img2.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img3.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img4.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img5.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img6.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img7.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img8.jpeg"/>
                </div>
                <div class="img">
                    <img src="images/rrgimages/img9.jpeg"/>
                </div>

                <div class="clear"></div>
                <div class="heading-trans">
                    <h4>Transformations</h4>
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being14.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being16.jpeg" />
                </div>
                <div class="img">
                    <img src="images/rrgimages/well-being13.jpeg" />
                </div>        
                <div class="img">
                    <img src="images/rrgimages/well-being15.jpeg" />
                </div>               
            </div>
        </div>
    </div>
</section>