<section class="fees-comparision">
    <div class="container">
        <h3>Fees &amp; Full Course Comparision</h3>
        <div class="row">
            <table>
                <tr>
                    <th class="col-sm-3">
                    </th>
                    <th class="col-sm-3 express">
                        <div class="express-price">
                            <h4>IELTS</h4>
                            <span>express</span>
                        </div>
                        <h5><span>IELTS</span>Express</h5>
                        <p>
                            Last minute IELTS preparation. Ideal for those who need to get familiar with test format and strategies.
                        </p>
                        <p class="fill1">

                        </p>

                    </th>
                    <th class="col-sm-3 express master-bg">
                        <div class="express-price">
                            <h4>IELTS</h4>
                            <span>master</span>
                        </div>
                        <h5><span>IELTS</span>Master</h5>
                        <p>
                            Comprehensive IELTS preparation. Ideal for those who want to improve their English skills and get familiar with the test format and strategies.
                        </p>
                        <p class="fill2">

                        </p>
                    </th>
                    <th class="col-sm-3 express master-plus-bg">
                        <div class="express-price">
                            <h4>IELTS</h4>
                            <span>master<i class="fa fa-plus"></i></span>
                        </div>
                        <h5><span>IELTS</span>Master Plus</h5>
                        <p>
                            Complete IELTS preparation. Ideal for those who want to improve their English skills, get familiar with the test and receive personalised assessment and feedback.
                        </p>
                        <p class="fill3">
                        </p>
                    </th>
                </tr>


                <tr class="course-price">
                    <td class="col-sm-3 left-label">Price
                    </td>
                    <td class="col-sm-3 express"><span>AUD $49</span>
                    </td>
                    <td class="col-sm-3 express master-bg"><span>AUD $99</span>
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><span>AUD $299</span>
                    </td>
                </tr>
                <tr class="days-access">
                    <td class="col-sm-3 left-label">Days access
                    </td>
                    <td class="col-sm-3 express">45 days
                    </td>
                    <td class="col-sm-3 express master-bg">90 days
                    </td>
                    <td class="col-sm-3 express master-plus-bg">90 days
                    </td>
                </tr>
                <tr class="study-time">
                    <td class="col-sm-3 left-label">Study time
                    </td>
                    <td class="col-sm-3 express">25 hours
                    </td>
                    <td class="col-sm-3 express master-bg">100 hours
                    </td>
                    <td class="col-sm-3 express master-plus-bg">100 hours
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Study pages
                    </td>
                    <td class="col-sm-3 express">Over 190
                    </td>
                    <td class="col-sm-3 express master-bg">Over 1000
                    </td>
                    <td class="col-sm-3 express master-plus-bg">Over 1000
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Interactive activities
                    </td>
                    <td class="col-sm-3 express">100 activities
                    </td>
                    <td class="col-sm-3 express master-bg">Over 260 activities
                    </td>
                    <td class="col-sm-3 express master-plus-bg">Over 260 activities
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Practise test in all 4 modules (Reading, Writing, Listening and Speaking)
                    </td>
                    <td class="col-sm-3 express"><i class="fa fa-check"></i>
                    </td>
                    <td class="col-sm-3 express master-bg"><i class="fa fa-check"></i>
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Detailed advice and model answers for all activities
                    </td>
                    <td class="col-sm-3 express">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-bg"><i class="fa fa-check"></i>
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Voice recording tools
                    </td>
                    <td class="col-sm-3 express">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-bg"><i class="fa fa-check"></i>
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">5 hours of audio recordings
                    </td>
                    <td class="col-sm-3 express">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-bg"><i class="fa fa-check"></i>
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Individual feedback on 2 writing tasks
                    </td>
                    <td class="col-sm-3 express">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-bg">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">Individual feedback on 3 speaking parts
                    </td>
                    <td class="col-sm-3 express">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-bg">&nbsp;
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><i class="fa fa-check"></i>
                    </td>
                </tr>
                <tr>
                    <td class="col-sm-3 left-label">
                    </td>
                    <td class="col-sm-3 express"><a href="#"><button class="btn btn-primary btn-md">Enroll Now</button>
                    </td>
                    <td class="col-sm-3 express master-bg"><button class="btn btn-primary btn-md">Enroll Now</button>
                    </td>
                    <td class="col-sm-3 express master-plus-bg"><button class="btn btn-primary btn-md">Enroll Now</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>

<section class="tesimonial-carousel  progresstive-page-container">
    <div class="container">
        <div class="row">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    @foreach($record as $testinomial)
                        @if($testinomial->id==$first)

                            <div class="item active">
                                <div class="test-slider">
                                    <div class="col-sm-4">
                                        <img src="<?php echo $testinomial->image_path ?>" alt=""/>
                                    </div>
                                    <div class="col-sm-8">
                                        <p> <?php echo $testinomial->description ?>

                                        <h6>
                                            <?php echo $testinomial->name ?>
                                        </h6>
                                        <strong> <p> <?php echo $testinomial->desg ?></strong>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="item">
                                <div class="test-slider">
                                    <div class="col-sm-4">
                                        <img src="<?php echo $testinomial->image_path ?>" alt=""/>
                                    </div>
                                    <div class="col-sm-8">
                                        <p> <?php echo $testinomial->description ?>

                                        <h6>
                                            <?php echo $testinomial->name ?>
                                        </h6>
                                        <strong> <p> <?php echo $testinomial->desg ?></strong>
                                    </div>
                                </div>
                            </div>
                            @endif

                            @endforeach





                                    <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                </div>
            </div>
        </div>
</section>
