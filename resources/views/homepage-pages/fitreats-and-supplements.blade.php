@extends('layouts.app')

@section('content')
<section class="first_part ">
    <div class="container">
        <div class="message-box">
            @include('layouts.error_message')
            @include('layouts.sucess_message_edit_testimonial')
        </div>
        <h2 class="top-header">Fitreats and More</h2>
        <p class="extra">Explore our ...</p>
        <div class="row">
            @foreach($fitreats as $fitreat)
            <div class="col-sm-6 pro-img">
                <div class="img-f">
                    <a href="#{{$fitreat->id}}" class="text_part">{{ $fitreat->name}}</a>
                </div>
            </div>
            @endforeach
        </div>
        @foreach($fitreats as $fitreat)
        <div id="{{$fitreat->id}}" class="fit-and-sup">
            <div class="fit_part {{ $loop->iteration == 1 ? 'first' : '' }}">
                <h2>{{ $fitreat->name }}</h2>
                <p>{{ $fitreat->body }}</p>
                <div class="row center">
                    @foreach($products as $product)
                    @if($product->cat_id === $fitreat->id)
                    @if(!($fitreat->id == 22))
                    <div class="col-sm-4">
                        @else
                        <div class="col-sm-4">
                            @endif
                            <div class="ft-prod1">
                            <form method="get" action="/fitreats/<?php $fitreats_title = str_replace(" ", "-", $product->title); echo strtolower($fitreats_title);?>">
                                 
                                <img src="{{$product->imagePath}}" name="submit"  class="imgg"><br><br>
                                <h4>{{$product->title}}</h4>
                                <p id="demo" class="center">{{ $product->price_qty_display }}</p>
                                <button type="button" class="btn read-more btn-primary" data-toggle="modal"
                                    data-target="#modal{{$product->id}}" style="display:none;">Read More</button><br><br>
                                
                                    <button type="submit" class="btn read-more btn-primary">Show More</button>
                                    <input type="hidden" value="{{$product->id}}" name="fitreat_id">
                                  
        
                                    </form>
                                    <!-- <button class="btn btn-info">Cart</button> -->
                            </div>
                            <div class="modal_part">
                                <!-- Modal -->
                                <div class="modal fade" id="modal{{$product->id}}" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <form method="post" action="/cart/{{ $product->id }}">
                                                {{ csrf_field() }}
                                                <div class="modal-header">
                                                    <h4 class="modal-title">{{$product->title}}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <img src="{{$product->imagePath}}" class="imgg"><br><br>
                                                    @if(($fitreat->id == 22))
                                                    <h5>Price: &pound; {{ $product->price }}</h5>
                                                    @endif
                                                    <p><?php echo ($product->description) ?></p><br><br>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-close"
                                                        data-dismiss="modal">Close</button>
                                                    <label for="quantity">Quantity:</label>
                                                    <input type="number" id="quantity" name="quantity" min="1" max="5">
                                                    <button type="submit" class="btn btn-default  model_cart">Add To
                                                        Cart</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach

                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <br><br>
            @endforeach
        </div>
</section>
@endsection