<script src="https://paymentgateway.commbank.com.au/checkout/version/47/checkout.js"
		data-error="errorCallback"
		data-cancel="cancelCallback">
</script>



<script type="text/javascript">
	var amt ='';
	var id='';
	var desc ='';

	function paynow(amt, id,desc) {


		Checkout.configure({
			merchant: 'TESTPROSTUCOM201',
			order: {
				amount: function () {
					//Dynamic calculation of amount
					return amt;
				},
				currency: 'AUD',
				description: function () {
					//Dynamic calculation of amount
					return desc;
				},


				id: function () {
					//Dynamic calculation of amount
					return id;
				},
			},
			interaction: {
				merchant: {
					name: 'ericshrestha',
					address: {
						line1: '200 Sample St',
						line2: '1234 Example Town'
					}
				}
			}
		});
	}
</script>

<script type="text/javascript">
	var amt;
	var desc, id;

	$(document).ready(function(){




		$('#first').click(function () {

			amt = $('#amount').val();
			desc = $('#description').val();
			id = $('#id').val();
			paynow(amt, id,desc);
			Checkout.showPaymentPage();


		});
		$('#second').click(function () {

			// alert('2');
			amt = $('#amount2').val();
			desc = $('#description2').val();
			id = $('#id2').val();
			paynow(amt, id,desc);
			Checkout.showPaymentPage();

		});
		$('#third').click(function () {

			// alert('2');
			amt = $('#amount3').val();
			desc = $('#description3').val();
			id = $('#id3').val();
			paynow(amt, id,desc);
			Checkout.showPaymentPage();

		});
		$('#fourth').click(function () {

			// alert('2');
			amt = $('#amount4').val();
			desc = $('#description4').val();
			id = $('#id4').val();
			paynow(amt, id,desc);
			Checkout.showPaymentPage();

		});
	});

	function errorCallback(error) {
		console.log(JSON.stringify(error));
	}

	function cancelCallback() {
		console.log('Payment cancelled');
	}
</script>




<section class="class-room-based-course">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 classroom-tutorials">
				<h4 class="title">{{$classroom->description}}</h4>
				<div class="col-sm-4 image-wrap">

					<img class="thumb" src="/images/3/classroom2.jpg" alt="go publish"/>
					<p class="price"><span>AUD ${{$classroom->amount}}</span><br/>{{$classroom->course}}</p>
					<div class="classroom-payment">
						<h5 class="classroom-description"></h5>

					</div>
				</div>
				 <div class="col-sm-8 text-wrap">
                    <?php $count=1 ?>
					@foreach($description_class as $class)
                        <?php
                        if ($count%3 == 1)
                        { ?>
						<div class="col-sm-6 text-wrap-left">
							<li data-toggle="tooltip" data-placement="right" title="{{$class->description}}"><i class="fa fa-check"></i>{{$class->description}}</li><hr>

						</div>
                        <?php    }
                        else
                        { ?>
						<div class="col-sm-6 text-wrap-right">

							<li data-toggle="tooltip" data-placement="right" title="{{$class->description}}"><i class="fa fa-check"></i>{{$class->description}}</li><hr>


						</div>
                        <?php  }
                        ?>
					@endforeach

					<div class="col-sm-12 col-md-6 col-md-offset-6 payment-classroom-course">
						<a href="/checkout/5">Purchase Classroom Course</a>

					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<section class="pricing course-pricing course-details">
	<div class="container">
		<h3>Online Courses</h3>
		<div class="row">
			<div class="col-sm-4 express-sec">
				<h4 class="title">GO EXPRESS</h4>
				<img class="thumb" src="/images/3/gopublish.png" alt="go publish"/>
				<p class="price"><span>AUD ${{$express->amount}}</span><br/>{{$express->course}}</p>
				<div class="pricing-details">
					<p class="price-topic">{{$express->description}}</p>



					<a href="/checkout/1">Enroll Now</a>

					<ul class="express-services"><hr>
						@foreach($description_express as $exp)
							<li data-toggle="tooltip" data-placement="right" title="{{$exp->description}}"><i class="fa fa-check"></i> {{$exp->description}}</li><hr>

						@endforeach
					</ul>
				</div>
			</div>

			<div class="col-sm-4 master">
				<h4 class="title">GO ADVANCED</h4>
				<img class="thumb" src="/images/3/gopremium1.jpg" alt="go publish"/>
				<p class="price"><span>AUD ${{$advance->amount}}</span><br/>{{$advance->course}}</p>
				<div class="pricing-details">
					<p class="price-topic">{{$advance->description}}</p>
					<a href="/checkout/3">Enroll Now</a>
					<ul class="express-services"><hr>
						@foreach($description_advance as $adv)
							<li data-toggle="tooltip" data-placement="right" title="{{$adv->description}}"><i class="fa fa-check"></i> {{$adv->description}}</li><hr>

						@endforeach</ul>
				</div>
			</div>

			<div class="col-sm-4 master-plus">
				<h4 class="title">GO MASTER</h4>
				<img class="thumb" src="/images/3/rocket21.gif" alt="go publish"/>
				<p class="price"><span>AUD ${{$master->amount}}</span><br/>{{$master->course}}</p>
				<div class="pricing-details">
					<p class="price-topic">{{$master->description}}</p>
					<a href="/checkout/4">Enroll Now</a>
					<ul class="express-services"><hr>
						@foreach($description_master as $mas)
							<li data-toggle="tooltip" data-placement="right" title="{{$mas->description}}"><i class="fa fa-check"></i> {{$mas->description}}</li><hr>

						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="tesimonial-carousel tesimonial-carousel  progresstive-page-container course-page">
    <div class="container">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                @foreach($record as $testinomial)
                    @if($testinomial->id==$first)

                        <div class="item active">
                            <div class="test-slider">
                                <div class="row">
                                    <div class="check-test">
                                        <div class="col-sm-3">
                                            <img src="<?php echo $testinomial->image_path ?>" alt=""/>
                                        </div>
                                        <div class="col-sm-9">
                                            <p> <?php echo $testinomial->description ?>

                                            <h6>
                                                <?php echo $testinomial->name ?>
                                            </h6>
                                            <strong> <p> <?php echo $testinomial->desg ?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="item">
                            <div class="test-slider">
                                <div class="row">
                                    <div class="check-test">
                                        <div class="col-sm-3">
                                            <img src="<?php echo $testinomial->image_path ?>" alt=""/>
                                        </div>
                                        <div class="col-sm-9">
                                            <p> <?php echo $testinomial->description ?>

                                            <h6>
                                                <?php echo $testinomial->name ?>
                                            </h6>
                                            <strong> <p> <?php echo $testinomial->desg ?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endif

            @endforeach





            <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
</section>
