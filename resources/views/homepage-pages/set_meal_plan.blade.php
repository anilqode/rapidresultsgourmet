<!--<div class="contact-par" style="background-image: url(/images/rrgimages/second-slider.jpeg);height: 580px;background-size: cover;background-position: center center;">
   
</div>-->
<section class="welcome-to-set-meal-plan">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
            @include('layouts.error_message')
            @include('layouts.set_meal_plan_alert')
         </div>
        </div>
        <div class="row">
            <div class="col-sm-offset-4 col-sm-4 parallax-inside-contact text-center">
                <h2 class="top-header">
                    PLANS
                </h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-1 set-meal-detail">
                <p>
                    Select from our bespoke plans, whether your goal is weight loss, building lean muscle, maintaining
                    your weight or just eating healthy, you’ll find a plan that fits your specific goals, needs and
                    lifestyle!
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6  plan-slogan">
            <strong>Enjoy a fresh new menu every month.</strong> 
            </div>
            <div class="col-sm-6 text-center plan-slogan">
            <strong>Order your favourites or change your meals every week so you'll never get bored! </strong>
            </div>
        </div>
            
               <div class="row">

                <div class="col-sm-10 offset-sm-1 set-meal-detail">
            
                <div class="text-center ourMenu">
                    @if(is_object($menu))
                    <a href="/menu/{{$menu->id}}" class="btn btn-primary menuCheckButton center" target="_blank">
                        <div>Menu</div>
                    </a>
                    <p>{{ $menu->description }}</p>
                    @else
                    <button disabled="true" class="btn menuCheckButton">{{ $menu }}</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<div class="foodFeature">
    <div class="container">
        <div class="row text-center">
            <p>All your dishes are prepared only with nutritious, quality ingredients and are cooked on the day of your
                delivery!</p>
            <div class="col-sm-1"></div>
            <div class="col-sm-2">
                <div class="imageTitle">
                    <img src="images/rrgimages/halal.png" alt="Halal">
                    <p>Halal</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="imageTitle">
                    <img src="images/rrgimages/nutrition.png" alt="Balanced Fiber">
                    <p>Balanced Fiber</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="imageTitle">
                    <img src="images/rrgimages/diet-food.png" alt="Complex Carbohydrates">
                    <p>Complex Carbohydrates</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="imageTitle">
                    <img src="images/rrgimages/vegetables.png" alt="Fresh never Frozen">
                    <p>Fresh never Frozen</p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="imageTitle">
                    <img src="images/rrgimages/hydroponic.png" alt="Non-GMO">
                    <p>Non-GMO</p>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="food0-type set-meal-plan ">
    <div class="container">
        <div class="row">
            @foreach($setMealPlanInfo as $mealInfo)
            <div class="col-sm-4 mb-50 relative ">
                @if($mealInfo->best_seller)
                <div class="bestseller">
                   <img src="images/rrgimages/bestsellers.png" alt="bestseller">
                </div>
                @endif
                <div class="content-part-hover content-part-hover1">
                    @if($mealInfo->most_popular)
                    <div class="featured">
                        <p>Most Popular</p>
                    </div>
                    @endif
                    <!-- 					<div class="wrapper-check"> -->
                    <a href="weekplan/{{$mealInfo->id}}/1"><img src="{{$mealInfo->image_path}}" /></a>
                    <h4>
                        {{$mealInfo->name}}
                    </h4>
                    <br>
                    <p>
                        <span class="price"> {{$mealInfo->price}}</span>
                    </p>
                    <p>
                        {{$mealInfo->short_intro}}<br>
                    </p>
                    <a href="weekplan/{{$mealInfo->id}}/1" class="btn bbtn-choose-plan">CHOOSE PLAN</a>
                    <button class="btn read-more">Read More</button>
                </div>
                <div class="content-part" style="display: none;">
                    <a href="#"><img src="{{$mealInfo->image_path}}" /></a>
                    <h4>
                        {{$mealInfo->name}}
                    </h4>
                    <br>
                    <p>
                        {{$mealInfo->short_intro}}
                    </p>
                    <div class="set-meal-detail">
                        {{$mealInfo->body}}
                    </div>

                    <a href="weekplan/{{$mealInfo->id}}/1" class="btn btn-choose-plan">CHOOSE PLAN</a>
                </div>
            </div>
            @endforeach

        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('.read-more').on("click", function() {
            $('.content-part-hover1, .content-part-hover2, .content-part-hover3').siblings().hide();
            $('.content-part-hover1, .content-part-hover2, .content-part-hover3').show();
            $(this).parent().hide();
            $(this).parent().next().show();
        });
    });
    </script>
</section>