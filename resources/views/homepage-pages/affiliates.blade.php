@extends('layouts.app')

@section('content')
   <!--  <div class="container-fluid">
<div class="row">

        <div class="col-sm-12" style="margin-left: 0px;
    		padding-left: 0px;
    		padding-right: 0px;">

    		<section>
    <div class="text">
        {{--<h4><span>ALWAYS FRESH,</span> NEVER FROZEN</h4>--}}
    </div> -->
<!-- </section> -->

<section class="fifth-part">
    <div class="container">
        <h5>WHO WE WORK WITH</h5>
        <div class="row">
            <div class="col-sm-6">
                <img src="/images/rrgimages/affiliates.jpeg"/>
            </div>
            <div class="col-sm-6 ">
                <div class="txt-part">
                    Our passion is to help athletes and active individuals like yourself, recover from strenuous training sessions,
                    whilst staying on weight, cutting for competition or event/holiday, or gaining for strength. For our athletes,
                    this goes for on and off season, whilst keeping them at peak performance.<br>

                    The athletes we work with are professional UFC fighters, MMA (Mixed <div class="tablet left">Martial Arts) athletes, professional boxers
                    and footballers from local football teams to the premier league. Some of our affiliates include several
                    specialist studios, gyms, dojos and nutritionists.<br>

                    We also cater to businesses and offices - helping staff make healthy choices and habits by delivering you and
                    your team breakfasts and lunches, ready to be eaten! Save your business time and money, whilst keeping your
                    staffs’ best interest at heart <i class="fa fa-heart"></i>.<br><br>

                    <strong>Let us treat you like the athlete that you are!</strong></div></p>
                </div>
            </div>
            <hr class="only-xs">
        </div>
        <div class="row">  
            <hr class="only-xs">  
 <div class="col-sm-6 xs-top">
                <img src="/images/rrgimages/affiliates1.jpeg" alt="affiliates"/>
            </div>
            <div class="col-sm-6 xs-bottom">
                <div class="txt-part">
                    <p class="name">Yiannis Fleming BA (Hons), MSc</p>
                    <p>“My name is Yiannis Fleming AKA Mr. Sport and I'm here to clear up the common food misconceptions you hear on a daily basis. I'm head nutrition coach to a number of UFC fighters, pro footballers &amp; public figures, and I've decided to team up with Rapid Results Gourmet to help make eating for your goal a lot easier! No more restrictive, low-calorie diets, and whack weight loss products. After trying some of the meals from RRG and hearing the feedback from a number of my athletes &amp; online clients that purchase meals<div class="tablet right"> from them, I knew it was the food prep company to work with. Not only do they create amazing tasting meals, but they portion them specifically to YOUR body &amp; requirements. Something we very rarely see. Asheef and the team are so friendly and always on hand to make sure that your experience is the best.<br><br>
                    I spend a lot of my time clearing up diet myths and swaying people away from fads &amp; BS products, and RRG are one of the few companies that I will endorse. Their meals are not only great tasting &amp; well-portioned, but their vision &amp; ethics are in line with mine. No longer should we keep people are arm's length just to make a quick buck. Let's put the health back into health &amp; fitness.<br><br>

                    Mr. Sport | Performance Coach | <a href="https://mrsport.org">www.mrsport.org</a></div></p>
                </div>
            </div>
           
        </div>
            
        <div class="row">
                <div class="col-sm-6 top-space">
                    <img src="/images/rrgimages/affiliates2.jpeg" alt="affiliates"/>
                </div>
                <div class="col-sm-6">
                    <div class="txt-part">
                        <div class="little-info">
                            <p class="name">Lawrence okolie</p>
                                <p>
                            "Consistently high quality great tasting food, made to order. Making weight is made so much easier now!, as I don't dread the food, I always look forward to it!"</p>
                        </div>
                   </div>
            </div>
            <hr class="only-xs">
        </div>
                
                <div class="row">
 <div class="col-sm-6 xs-topsec">
                        <img src="/images/rrgimages/affiliates3.jpeg" alt="affiliates3"/>
                    </div>
                    <div class="col-sm-6 xs-bottomsec">
                        <div class="txt-part">
                            <div class="some-info">
                            <p class="name">Nathaniel Wood</p>
                            <p >“RRG tastes like I’m eating naughty when it’s nothing but goodness! Weight cut made easy; conditioning up and energy through the roof!”</p>
                        </div>
                        </div>
                    </div>
                   
                </div>
    </div>
</section>

        <!-- </div>
        </div>
</div> -->
@endsection