
@extends('layouts.app')

@section('styles')
<style>
.featured p {
	font-size: 20px;
    text-shadow: 3px 3px 8px black;
    transition: .3s all cubic-bezier(0.18, 0.89, 0.32, 2.28);
    cursor: pointer;
}

.featured p:hover {
    transform: scale(1.2);
}
.featured {
    background: orange;
    padding: 25px 0px 1px;
    text-transform: uppercase;
    transform: rotate(46deg);
    position: absolute;
    left: 49%;
    top: 17px;
    overflow: hidden;
}
.bestseller {
    position: absolute;
    top: -50px;
    text-align: right;
    z-index: 1;
}
.bestseller img:hover {
    transform: rotate(396deg) scale(2);
}
.bestseller img {
    margin-top: 0px;
    width: 35%;
    transform: rotate(28deg);
    margin-right: -34px;
    transition: .7s all cubic-bezier(0.18, 0.89, 0.32, 1.28);
}
.foodFeature {
    background: #efefef;
    padding: 50px 0px 20px;
}
.imageTitle, .imageTitle p {
    padding: 15px 0px;
}
</style>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12 progresstive-page-padding" style="padding:0px;">
                @include('homepage-pages.set_meal_plan')
            </div>
        </div>
    </div>
@endsection