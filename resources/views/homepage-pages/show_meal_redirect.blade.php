
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-12">
                @include('homepage-pages.show_meal')
            </div>
        </div>
    </div>
@endsection