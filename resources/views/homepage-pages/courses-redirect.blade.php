
@extends('layouts.app')

@section('content')
    <div class="container-fluid">
    <div class="row">

            <div class="col-sm-12 progresstive-page-padding">
                @include('homepage-pages.courses')
            </div>
        </div>
    </div>
@endsection