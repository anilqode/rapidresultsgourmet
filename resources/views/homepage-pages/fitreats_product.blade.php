<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
@extends('layouts.app')
@section('content')
<div class="container">

    <form method="post" action="/cart/{{ $product->id }}">
        {{ csrf_field() }}
        <div class="row">

            <div class="col-sm-4">
                <img src="/{{$product->imagePath}}" width="100%" class="fitreat-img">
            </div>
            <div class="col-sm-8">
                <div class="fitreat-content-box"
                    <h4 class="fitreat-title">{{$product->title}}</h4>
                    <p><?php echo ($product->description) ?></p><br>
                    <h5>Price: {{ $product->price_qty_display }} </h5>
                    <label for="quantity">Quantity:</label>
                    <input type="number" id="quantity" name="quantity" value="1" class="fitreat-quantity" required><br>
                    <button type="submit" class="btn fitreat-add-cart">Add To
                        Cart</button>

                </div>

            </div>

        </div>

    </form>


</div>
<script type="text/javascript" src="js/imagePreview.js"></script>
@endsection