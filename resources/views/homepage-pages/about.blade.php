<section class="about-to">
    <div class="text">
        {{--<h4><span>ALWAYS FRESH,</span> NEVER FROZEN</h4>--}}
    </div>
</section>
<section class="second-part ">
    <div class="container">
        <h4>OUR VISION</h4>
        <p> “Rapid Results Gourmet is leading the way in nutrition for the motivated and health conscious professional. We are here to rid the U.K of unnecessary illness, gimmicks and fads through our wholesome fusion of recipes and dishes. We simplify the nation's access to clean, honest and healthy food; Efficiently, one delivery at a time.”</p>
    </div>
</section>
<section class="third-part ">
    <div class="container">
        <div class="row">
            {{--<div class="col-sm-6">--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/uploads/2017/01/macros.jpg" />--}}
            {{--</div>--}}
            <div class="col-sm-12 txt-part">
                <h5>WHO WE ARE <span>and</span> HOW WE STARTED</h5>
                <p>We combine decades of professional chefs, trainers and athletes’ expertise to create innovative,
                    flavoursome, and nutritional food for you to enjoy in the most efficient, hassle-free way possible.
                    We put our heads together to create “<a href="/">Rapid Results Gourmet</a>.” An effective,
                    unique meal prep solution to save you time and money and enable you to get the most out of your hard work.
                    Whether you are in the gym, the office or constantly on the go, our mission is to help you stay focused on your goals and keep you progressing within your busy, active lifestyle.</p>
            </div>
        </div>
    </div>
</section>
<section class="fourth-part ">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 txt-part">
                <h5>WHAT SETS US APART</h5>
                <p>Your meals are prepared with ultimate care and expertise from the field to the kitchen to your door.
                    From the creation of unique, innovative recipes to the selection of fresh,
                    hand-picked ingredients all cooked by our expert team of chefs. We pride ourselves in our entire process.
                    We ONLY deal with fresh produce. We prep the day before delivery and cook on the SAME day of delivery. Our motto is
                    <b>“ALWAYS FRESH, NEVER FROZEN.”</b><br/><br>

                    We play a huge part in professional athletes weight cuts  and maintenance through their vigorous training routines.
                    We are able to get your calories down to the “T” to ensure your goals and weights are met through the
                    combination of our nutritionists, chefs and delivery systems. Our meat is all halal and we have an extensive
                    plant-based menu which is soya, Quorn and dairy-free. We stay clear of ALL processed foods.
                    We work around your allergies, dietary preferences and all your specific goals. Our menu is constantly innovating,
                    with new meals and snacks added every week.<br><br>

                    <b>“The Difference”</b> - We have created our very own, freshly made vegan, high-protein <strong>“FiTreats”</strong>
                    to keep you going in between meals for dessert or for a top pre or post workout snack. We ensure they will keep
                    your sweet tooth at bay. <i class="fa fa-smile"></i><br><br>

                    We stand by our promise that your meals are always FRESH and NEVER frozen, which is why we deliver your meals
                    twice a week straight to your home, work, gym or wherever is most convenient for you to keep you going through
                    your busy lifestyle. </p>
            </div>
            {{--<div class="col-sm-2 images-more">--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/themes/soulmatelife/_assets/svg/icon-grey-burn.svg"/>--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/themes/soulmatelife/_assets/svg/icon-grey-balance.svg"/>--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/themes/soulmatelife/_assets/svg/icon-grey-build.svg"/>--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/themes/soulmatelife/_assets/svg/icon-grey-sirt.svg"/>--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/themes/soulmatelife/_assets/svg/icon-grey-reset.svg"/>--}}
                {{--<img src="https://www.soulmatefood.com/wp-content/themes/soulmatelife/_assets/svg/icon-grey-custom.svg"/>--}}
            {{--</div>--}}

        </div>
    </div>
</section>
<section class="sixth-part " style="background-color: white; color: black;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 txt-part">
                <h5>OUR <span>"WHY?"</span></h5>
            <p style="color: black;">
                You and your goals are our “Why?”

                Being in the fitness and food industry for years, we understand how much of a challenge it is to get healthy food and snacks. We see processed food, high-sugar snacks and bogus supplements everywhere; when all we need is fresh, nutritious, healthy food! With our process and meals, we simplify what nutrition really is, make it accessible and efficient so you can focus on YOU. Let us save you time and money whilst improving your health and well-being.

                Making England healthier and more knowledgeable one delivery at a time <i class="far fa-smile-wink"></i>

                Remember, <strong>“You are what you eat.”</strong>

                We look forward to having you on board. Onwards and upwards!</p>
            </div>
            {{--<div class="col-sm-6">--}}
                {{--<div class="img-desc">--}}
                    {{--<img src="https://www.soulmatefood.com/wp-content/uploads/2017/01/allchange.jpg"/>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>
