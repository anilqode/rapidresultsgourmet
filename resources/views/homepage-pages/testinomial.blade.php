<section class="testimonial-details ">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-10 col-md-offset-2 col-sm-offset-1 testinomial-header">
                <h4>Your Feedback is Important to Us!</h4>
            </div>
        </div>
        <div class=" testinomial-details ">
        <div class="row text-center">
            <a class="btn btn-primary menuCheckButton" href="/add-testinomial">Create Your Testimonial</a>
        </div>
        <br>
        @foreach($testinomial as $record)
            <div class="col-sm- 12 testinomial-wrap ">
                <div class="testinomial-image-wrap">
                    <div class="col-sm-3">
                        <div class="image-testinomial">
                            <img src="<?php echo $record->image_path; ?>" alt=""/>
                        </div>
                    </div>
                </div>
                <div class="testinomial-text-wrap">
                    <div class="col-sm-9">
                        <div class="text-testinomial">
                            <p>
                                <?php echo $record->description; ?>
                            </p>
                            <p><span><?php echo $record->name; ?></span></p>
                            <p><span><?php echo $record->desg; ?></span></p>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
        
    </div>
</div>
</section>
<section class="paginate-testinomial">
    <div class="container">
    <div class="row">
        <div class="col-sm-12 testinomial-pagination">
            <?php echo $testinomial->render(); ?>
        </div>
    </div>
    </div>

</section>
