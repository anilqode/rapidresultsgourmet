@extends('layouts.app')

@section('content')
	<div class="container policy">
		<h3>Terms and Conditions of RAPID RESULTS GOURMET LTD</h3>
		<p>This page (together with the documents referred to on it) tells you the terms and conditions on
		which we will supply to you the products (Products) listed on our website
		www.rapidresultsgourmet.co.uk (our site) via one of our subscription services (Services). Please
		read these terms and conditions carefully before ordering any Products from our site or
		subscribing to one of our Services. You should understand that by ordering any of our Products
		or subscribing to one of our Services, you agree to be bound by these terms and conditions.
		You should print a copy of these terms and conditions for future reference.
		Please tick the checkbox in the “start now” to accept these terms and conditions. Please
		understand that if you refuse to accept these terms and conditions, you will not be able to order
		any Products from our site.</p>
		<ol class="tnc">
			<li>INFORMATION ABOUT US
				<ol class="tnc-second">
					<li>We operate the website www.rapidresultsgourmet.co.uk. We
					are RAPID RESULTS GOURMET LTD, a company registered in the UK.</li>
				</ol>
				</li>
			<li>SERVICE AVAILABILITY
				<ol class="tnc-second">
					<li>Our site is only intended for use by people residing in the
					United Kingdom of Great Britain and Northern Ireland (Serviced Countries). We do not accept
					orders from individuals outside those countries.</li>
				</ol>
				</li>
			<li >YOUR STATUS By placing an order through our site, you warrant that:
				<ol class="tnc-second">
					<li>you are legally capable of entering into binding contracts; and</li>
					<li>you are at least 18 years old;</li>
					<li>you are resident in one of the Serviced Countries; and</li>
					<li>you are accessing our site from that country.</li>					
				</ol>
				</li>
			<li >HOW THE CONTRACT IS FORMED BETWEEN YOU AND US
				<ol class="tnc-second">
					<li>After placing an order on our standard order form, you will receive an e-mail from us acknowledging that we have received your order. Please note that this does not mean that your order has been accepted. Your order constitutes an offer to us to buy Products by subscribing to a Service. All orders are subject to acceptance by us, and we will confirm such acceptance to you by sending you an e-mail that confirms that the Product has been dispatched (Dispatch Confirmation). The contract between us (Contract) will only be formed when we send you the Dispatch Confirmation.</li>
					<li>The Contract will relate only to those Products whose dispatch we have confirmed in the
					Dispatch Confirmation. We will not be obliged to supply any other Products which may have been part of your order until the dispatch of such Products has been confirmed in a separate Dispatch Confirmation.</li>
					<li>The subscription plan to our Services consist of an initial charge and then followed by
					recurring period charges as agreed to by you. By entering into this Agreement, you acknowledge that your subscription has an initial and recurring payment feature and you accept responsibility for all recurring charges prior to deactivation. www.rapidresultsgourmet.co.uk may submit periodic charges (e.g., monthly) without further authorisation from you, until you provide prior notice that you have terminated this authorisation or wish to change your payment method. Such notice will not affect charges submitted before www.rapidresultsgourmet.co.uk reasonably could act. To terminate your authorisation or change your payment method email info@rapidresultsgourmet.co.uk.</li>
					<li>By subscribing to www.rapidresultsgourmet.co.uk you are agreeing to pay recurring periodic subscriptions for an indefinite time until deactivated by you or us, on the subscription terms set out in the application form you have completed, subject to variation in accordance with 4.5 below. You can deactivate your subscription at any time within the deactivation deadline. You will not be charged for any deactivation. You can re-subscribe at any time following your deactivation, but we reserve the right not to permit re-subscription where we have previously elected to terminate a subscription by you.</li>
					<li>Cancelling your subscription is easy. You just need to go to email info@rapidresultsgourmet.co.uk. (This must be arranged 14 full days before the end of your subscription period):</li>
					<li>We reserve the right at our absolute discretion not to renew your subscription at any time without giving any reasons for our decision.</li>
				</ol>
				</li>
			<li >VOUCHERS AND GIFT CARDS
				<ol class="tnc-second">
					<li>We may offer gift cards, discount promotions and other types of voucher (Voucher) which require to be activated by email application in order for the holder to commence delivery of Products through a Service. If paid for, the Voucher is deemed to have been sold at the time of payment for it. All of these terms and conditions shall become applicable as between us and the holder of the Voucher (Holder) when the Holder redeems the Voucher by applying for a Service to commence.</li>
					<li>A Voucher may only be used once by its Holder and may not be copied, reproduced, distributed or published either directly or indirectly in any form or stored in a data trieval system without our prior written approval.</li>
					<li>We reserve the right to withdraw or deactivate any Voucher (other than a paid-up gift card) for any reason at any time.</li>
					<li>Vouchers may only be redeemed through the website www.rapidresultsgourmet.co.uk and
					not through any other website or method of communication. To use your Voucher you will be required to enter its unique code at the online checkout and use of such code will be deemed to confirm your agreement to these terms  nd conditions and any special conditions attached to the Voucher.</li>
					<li>Any discounts attached to Vouchers apply to the price of the Products ordered only and not to delivery charges, which will be chargeable at normal rates.</li>
					<li>We reserve the right to exclude the use of voucher codes on specific products. For example vouchers linked to the normal Rapid Results Gourmet subscription cannot be used on the Rapid Results Gourmet Christmas Box or Gift Box. Vouchers will not be applied in conjunction with any existing account credit.</li>
				</ol>
				</li>
			<li>AVAILABILITY AND DELIVERY
				<ol class="tnc-second">
					<li>Your order will be fulfilled by the delivery date set out in the Dispatch Confirmation or, if no delivery date is specified, then within 30 days of the date of the Dispatch Confirmation, unless there are exceptional circumstances.</li>
				</ol>
				</li>
			<li>RISK AND TITLE
				<ol class="tnc-second">
					<li>The Products will be at your risk from the time of delivery.</li>
					<li>Ownership of the Products will only pass to you when we receive full payment of all sums due in respect of the Products, including delivery charges.</li>
				</ol></li>
			<li >PRICE AND PAYMENT
				<ol class="tnc-second">
					<li>The price of the Products and our delivery charges will be as quoted on our site from time to time, except in cases of obvious error.</li>
					<li>Product prices include VAT.</li>
					<li>Product prices and delivery charges are liable to change at any time, but changes will not affect orders in respect of which we have already sent you a Dispatch Confirmation.</li>
					<li>Payment for all Products and Services must be by credit, debit card or paypal. We accept payment with Visa and Mastercard.</li>
				</ol>
				</li>
			<li>OUR REFUNDS POLICY
				<ol class="tnc-second">
					<li>If you are unhappy with your box for a legitimate reason such as: the box was missing ingredients, the box was damaged, the box did not arrive. We will offer an appropriate refund as long as it can be shown that the box you were charged for was not provided as it should have been.</li>
				</ol>
				</li>
			<li >WARRANTY
				<ol class="tnc-second">
					<li>We warrant to you that any Product purchased from us through our site will, on delivery, conform with its description, be of satisfactory quality, and be reasonably fit for all the purposes for which products of that kind are commonly supplied.</li>
				</ol>
				</li>
			<li >OUR LIABILITY
				<ol class="tnc-second">
					<li>Subject to clause</li>
					<li>if we fail to comply with these terms and conditions we shall only be liable to you for the purchase price of the Products.</li>
					<li>Nothing in this agreement excludes or limits our liability for:</li>
					<li>Death or personal injury caused by our negligence;</li>
					<li>Fraud or fraudulent misrepresentation;</li>
					<li>Any breach of the obligations implied by section 12 of the Sale of Goods Act 1979;</li>
					<li>Defective products under the Consumer Protection Act 1987; or</li>
					<li>Any other matter for which it would be illegal for us to exclude or attempt to exclude our liability.</li>
				</ol>
				</li>
			<li >WRITTEN COMMUNICATIONS 
				<ol class="tnc-second">
					<li>Applicable laws require that some of the information or communications we send to you should be in writing. When using our site, you accept that communication with us will be mainly electronic. We will contact you by e-mail or provide you with information by posting notices on our website. For contractual purposes, you agree to this electronic means of communication and you acknowledge that all contracts, notices, information and other communications that we provide to you electronically comply with any legal requirement that such communications be in writing. This condition does not affect your statutory rights.</li>
				</ol>
				</li>
			<li >NOTICES
				<ol class="tnc-second">
					<li>All notices given by you to us must be given to RAPID RESULTS GOURMET LTD at www.rapidresultsgourmet.co.uk. We may give notice to you at either the e-mail or postal address you provide to us when placing an order, or in any of the ways specified in clause 12 above. Notice will be deemed received and properly served immediately when posted on our website, 24 hours after an e-mail is sent, or three days after the date of posting of any letter. In proving the service of any notice, it will be sufficient to prove, in the case of a letter, that such letter was properly addressed, stamped and placed in the post and, in the case of an e-mail, that such e-mail was sent to the specified e-mail address of the addressee.</li>
				</ol>
				</li>
			<li >TRANSFER OF RIGHTS AND OBLIGATIONS
				<ol class="tnc-second">
					<li>The contract between you and us is binding on you and us and on our respective successors and assignees.</li>
					<li>You may not transfer, assign, charge or otherwise dispose of a Contract, or any of your rights or obligations arising under it, without our prior written consent.</li>
					<li>We reserve the right to withdraw or deactivate any Voucher (other than a paid-up gift card) for any reason at any time.</li>
					<li>We may transfer, assign, charge, sub-contract or otherwise dispose of a Contract, or any of our rights or obligations arising under it, at any time during the term of the Contract.</li>
				</ol>
				</li>
			<li >INTELLECTUAL PROPERTY RIGHTS
				<ol class="tnc-second">
					<li>We are the owner or the licensee of all intellectual property rights in our site, whether registered or unregistered, and in the material published on it. These works are protected by copyright laws and all such rights are reserved.</li>
					<li>You may print off one copy, and may download extracts, of any pages from our site for your personal reference. You must not use any part of our copyright materials for commercial purposes without first obtaining a licence to do so from us and our licensors.</li>
					<li>If you post comments on the Products or Services to any website, blog or social media network (Commentary) you must ensure that such Commentary represents your fairly-held opinions. By subscribing to the Services you irrevocably authorise us to quote from your Commentary on our site and in any advertising or social media outlets which we may create or contribute to.</li>
				</ol>
				</li>
			<li >EVENTS OUTSIDE OUR CONTROL
				<ol class="tnc-second">
					<li>We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations under a Contract that is caused by events outside our reasonable control (Force Majeure Event).</li>
					<li>A Force Majeure Event includes any act, event, non-happening, omission or accident beyond our reasonable control and includes in particular (without limitation) the following: 
					<ol class="tnc-third">
						<li>Strikes, lock-outs or other industrial action;</li>
						<li>Civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for war;</li>
						<li>Fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster;</li>
						<li>Impossibility of the use of railways, shipping, aircraft, motor transport or other means of public or private transport;</li>
						<li>Impossibility of the use of public or private telecommunications networks; and</li>
						<li>The acts, decrees, legislation, regulations or restrictions of any government.</li>
					</ol>
					</li>
					<li>Our performance under any Contract is deemed to be suspended for the period that the Force Majeure Event continues, and we will have an extension of time for performance for the duration of that period. We will use our reasonable endeavours to bring the Force Majeure Event to a close or to find a solution by which our obligations under the Contract may be performed despite the Force Majeure Event.</li>
				</ol>
				</li>
			<li>WAIVER
				<ol class="tnc-second">
					<li>If we fail, at any time during the term of a Contract, to insist upon strict performance of any of your obligations under the Contract or any of these terms and conditions, or if we fail to exercise any of the rights or remedies to which we are entitled under the Contract, this will not constitute a waiver of such rights or remedies and will not relieve you from compliance with such obligations.</li>
					<li>A waiver by us of any default will not constitute a waiver of any subsequent default.</li>
					<li>No waiver by us of any of these terms and conditions will be effective unless it is expressly stated to be a waiver and is communicated to you in writing in accordance with clause 14 above.</li>
				</ol>
			</li>
			<li>SEVERABILITY
				<ol class="tnc-second">
					<li>If any of these terms and Conditions or any provisions of a Contract are determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such term, condition or provision will to that extent be severed from the remaining terms, conditions and provisions which will continue to be valid to the fullest extent permitted by law.</li>
				</ol>
			</li>
			<li>ENTIRE AGREEMENT
				<ol class="tnc-second">
					<li>These terms and conditions and any document expressly referred to in them constitute the whole agreement between us and supersede all previous discussions, correspondence, negotiations, previous arrangement, understanding or agreement between us relating to the subject matter of any Contract.</li>
					<li>We each acknowledge that, in entering into a Contract, neither of us relies on any representation or warranty (whether made innocently or negligently) that is not set out in these terms and conditions or the documents referred to in them.</li>
					<li>Each of us agrees that our only liability in respect of those representations and warranties that are set out in this agreement (whether made innocently or negligently) will be for breach of contract.</li>
					<li>Nothing in this clause limits or excludes any liability for fraud</li>
				</ol>
			</li>
			<li>OUR RIGHT TO VARY THESE TERMS AND CONDITIONS
				<ol class="tnc-second">
					<li>We have the right to revise and amend these terms and conditions from time to time to reflect changes in market conditions affecting our business, changes in technology, changes in payment methods, changes in relevant laws and regulatory requirements and changes in our system's capabilities.</li>
					<li>You will be subject to the policies and terms and conditions in force at the time that you order Products from us, unless any change to those policies or these terms and conditions is required to be made by law or governmental authority (in which case it will apply to orders previously placed by you), or if we notify you of the change to those policies or these terms and conditions before we send you the Dispatch Confirmation (in which case we have the right to assume that you have accepted the change to the terms and conditions, unless you notify us to the contrary within seven working days of receipt by you of the Products).</li>
				</ol>
			</li>
			<li>LAW AND JURISDICTION
				<ol class="tnc-second">
					<li>Contracts for the purchase of Products through our site and any dispute or claim arising out of or in connection with them or their subject matter or formation (including non-contractual disputes or claims) will be governed by English law. Any dispute or claim arising out of or in connection with such Contracts or their formation (including non-contractual disputes or claims) will be subject to the non-exclusive jurisdiction of the courts of England and Wales. Try Rapid Results Gourmet now</li>
				</ol>
			</li>
		</ol>
		<p>Order Rapid Results Gourmet and we'll deliver fresh meals straight to your doorstep each week.
		• Delicious meals • Fresh ingredients • Flexible subscription</p>
		<p>Browse Our Website:www.rapidresultsgourmet.co.uk</p>
		<p>© Rapid Results Gourmet 2018</p>
		<p>This website uses cookies. By browsing this website you consent to the use of cookies. For further information regarding the use of cookies please access our data protection declaration.</p>
	</div>
@endsection