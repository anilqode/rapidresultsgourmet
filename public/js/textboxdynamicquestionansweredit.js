

$(document).ready(function () {

        //clicking button to add dlr acc description

    var counter = document.getElementById("counter").value;

    $("#addButton").click(function () {
            //  checkCounter(counter);
            addTextbox(counter, 'question', 'TextBoxesGroup', 'TextBoxDiv');
            addTextbox2(counter, 'option1', 'TextBoxesGroup', 'TextBoxDiv');

            addTextbox3(counter, 'option2', 'TextBoxesGroup', 'TextBoxDiv');
            addTextbox4(counter, 'option3', 'TextBoxesGroup', 'TextBoxDiv');
            addTextbox5(counter, 'option4', 'TextBoxesGroup', 'TextBoxDiv');
            addTextbox6(counter, 'answer', 'TextBoxesGroup', 'TextBoxDiv');

            counter++;
        });


        //clicking button to add Fact acc Code

        /* var faccounter = 2;
         $("#createButton").click(function () {
         checkCounter(faccounter);
         addTextbox(faccounter, 'factacc', 'facaccGroup', 'factBoxDiv');
         faccounter++;
         });*/

        /*

         //clicking button to add Fact acc description

         var facDesCounter = 2;
         $("#createFactDescButton").click(function () {
         checkCounter(facDesCounter);
         addTextbox(facDesCounter, 'factdesc', 'facAccDescriptionGroup', 'factDescriptionBoxDiv');  //addTextbox(counter,name,textboxgroupname,divabovetextboxname)
         facDesCounter++;
         });


         //clicking button to add memo

         var memoCounter = 2;
         $("#createMemoButton").click(function () {
         checkCounter(memoCounter);
         addTextbox(memoCounter, 'memo', 'memoGroup', 'memoDiv');  //addTextbox(counter,name,textboxgroupname,divabovetextboxname)
         memoCounter++;
         });

         //clicking button to add memo

         var optionCounter = 2;
         $("#createOptionButton").click(function () {
         checkCounter(optionCounter);
         addTextbox(optionCounter, 'option', 'optionGroup', 'optionDiv');  //addTextbox(counter,name,textboxgroupname,divabovetextboxname)
         optionCounter++;
         });


         //clicking button to add memo

         var packageCounter = 2;
         $("#createPackageButton").click(function () {
         checkCounter(packageCounter);
         addTextbox(packageCounter, 'package', 'packageGroup', 'packageDiv');  //addTextbox(counter,name,textboxgroupname,divabovetextboxname)
         packageCounter++;
         });


         //clicking button to add memo

         var commentCounter = 2;
         $("#createCommentButton").click(function () {
         checkCounter(commentCounter);
         addTextbox(commentCounter, 'comment', 'commentGroup', 'commentDiv');  //addTextbox(counter,name,textboxgroupname,divabovetextboxname)
         commentCounter++;
         });
         */


        //Click  remove button to remove dlr acc added textbox
        $("#removeButton").click(function () {
            counter--
            checkRemoveCounter(counter);
            removeTextbox(counter, 'TextBoxDiv');
            removeTextbox2(counter, 'TextBoxDiv');
            removeTextbox3(counter, 'TextBoxDiv');
            removeTextbox4(counter, 'TextBoxDiv');
            removeTextbox5(counter, 'TextBoxDiv');
            removeTextbox6(counter, 'TextBoxDiv');

        });


        /* //Click  remove button to remove fact acc added textbox
         $("#factremoveButton").click(function () {
         faccounter--
         checkRemoveCounter(faccounter);
         removeTextbox(faccounter, 'factBoxDiv');
         });


         //Click  remove button to remove fact acc added textbox
         $("#factDescRemoveButton").click(function () {
         facDesCounter--
         checkRemoveCounter(facDesCounter);
         removeTextbox(facDesCounter, 'factDescriptionBoxDiv');
         });

         //Click  remove button to remove fact acc added textbox
         $("#memoRemoveButton").click(function () {
         memoCounter--
         checkRemoveCounter(memoCounter);
         removeTextbox(memoCounter, 'memoDiv');
         });

         //Click  remove button to remove fact acc added textbox
         $("#optionRemoveButton").click(function () {
         optionCounter--
         checkRemoveCounter(optionCounter);
         removeTextbox(optionCounter, 'optionDiv');
         });

         //Click  remove button to remove fact acc added textbox
         $("#packageRemoveButton").click(function () {
         packageCounter--
         checkRemoveCounter(packageCounter);
         removeTextbox(packageCounter, 'packageDiv');
         });


         //Click  remove button to remove fact acc added textbox
         $("#commentRemoveButton").click(function () {
         commentCounter--
         checkRemoveCounter(commentCounter);
         removeTextbox(commentCounter, 'commentDiv');
         });


         /// to find the values entered in the textbox
         */
        $("#getButtonValue").click(function () {
            var msg = '';

            for (i = 1; i < counter; i++) {
                msg += "\n Textbox #" + i + " : " + $('#textbox' + i).val();

            }
            alert(msg);

        });
    }
);

//functions for checking counter

function checkCounter(counter) {

    if (counter > 10) {
        alert("Only 10 textboxes allow");
        return false;
    }

}

//functions for checking remove counter

function checkRemoveCounter(counter) {
    if (counter == 1) {
        alert("No more textbox to remove");
        return false;
    }

}

//functions for adding textbox  based on the counter, name, textgroup and div passed above

function addTextbox(counter, textbox , textgroup, TextBoxDiv) {

    var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", TextBoxDiv + counter);
    newTextBoxDiv.after().html('    <hr class="recording-border" style="font-weight: 600; border-top:2px solid;margin-top: 33px;\n' +
        '    padding-bottom: 14px;"><div class=form-group> <div class="col-md-4 col-sm-4"><label>Question #' + counter + ' : </label></div>' +
        '' + ' <div class="col-md-6 col-sm-6"><input  class=" form-control" type=text name=' + textbox + '[]' + ' id=' + textbox + counter + ' value="" ></div>'

        + '</div>');

    newTextBoxDiv.appendTo("#" + textgroup);


}


function addTextbox2(counter, textbox , textgroup, TextBoxDiv) {


    var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", TextBoxDiv + counter);
    newTextBoxDiv.after().html('<div class=form-group><div class="col-sm-2"></div> <div class="col-md-2 col-sm-2"><label>Option 1</label></div>'
        + ' <div class="col-md-6 col-sm-6"><input  class=" form-control" type=text name=' + textbox + '[]' + ' id=' + textbox + counter + ' value="" ></div>'


    );

    newTextBoxDiv.appendTo("#" + textgroup);


}
function addTextbox3(counter, textbox , textgroup, TextBoxDiv) {

    var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", TextBoxDiv + counter);
    newTextBoxDiv.after().html('<div class=form-group><div class="col-sm-2"></div> <div class="col-md-2 col-sm-2"><label>Option 2</label></div>'
        + ' <div class="col-md-6 col-sm-6"><input  class=" form-control" type=text name=' + textbox + '[]' + ' id=' + textbox + counter + ' value="" ></div>'

        + '</div>');



    newTextBoxDiv.appendTo("#" + textgroup);


}
function addTextbox4(counter, textbox , textgroup, TextBoxDiv) {

    var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", TextBoxDiv + counter);
    newTextBoxDiv.after().html('<div class=form-group><div class="col-sm-2"></div> <div class="col-md-2 col-sm-2"><label>Option 3 </label></div>'
        + ' <div class="col-md-6 col-sm-6"><input  class=" form-control" type=text name=' + textbox + '[]' + ' id=' + textbox + counter + ' value="" ></div>'

        + '</div>');

    newTextBoxDiv.appendTo("#" + textgroup);


}
function addTextbox5(counter, textbox , textgroup, TextBoxDiv) {

    var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", TextBoxDiv + counter);
    newTextBoxDiv.after().html('<div class=form-group><div class="col-sm-2"></div> <div class="col-md-2 col-sm-2"><label>Option 4</label></div>'
        + ' <div class="col-md-6 col-sm-6"><input  class=" form-control" type=text name=' + textbox + '[]' + ' id=' + textbox + counter + ' value="" ></div>'

        + '</div>');

    newTextBoxDiv.appendTo("#" + textgroup);


}
function addTextbox6(counter, textbox , textgroup, TextBoxDiv) {

    var newTextBoxDiv = $(document.createElement('div'))
        .attr("id", TextBoxDiv + counter);
    newTextBoxDiv.after().html('<div class=form-group><div class="col-sm-2"></div> <div class="col-md-2 col-sm-2"><label>Answer</label></div>'
        + ' <div class="col-md-6 col-sm-6"><select required class=" form-control" type=text name=' + textbox + '[]' + ' id=' + textbox + counter + ' >    <option selected="selected" value=""  >Select Answer</option>'+
        '<option value="1">A</option>'+
        ' <option value="2">B</option>'+
        ' <option value="3">C</option>'+
        '<option value="4">D</option>'+'</select></div>'

        + '</div>');

    newTextBoxDiv.appendTo("#" + textgroup);


}

//function to remove textbox based on the counter and textbox div passed above

function removeTextbox(counter, textboxdiv) {

    $("#" + textboxdiv + counter).remove();
}
function removeTextbox2(counter, textboxdiv) {

    $("#" + textboxdiv + counter).remove();
}
function removeTextbox3(counter, textboxdiv) {

    $("#" + textboxdiv + counter).remove();
}
function removeTextbox4(counter, textboxdiv) {

    $("#" + textboxdiv + counter).remove();
}
function removeTextbox5(counter, textboxdiv) {

    $("#" + textboxdiv + counter).remove();
}
function removeTextbox6(counter, textboxdiv) {

    $("#" + textboxdiv + counter).remove();
}

