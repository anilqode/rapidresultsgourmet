<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuUploader extends Model
{
    protected $fillable = [ 'menu', 'description'];
}
