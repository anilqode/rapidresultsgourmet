<?php

namespace App\Services;
use App\User;
use Illuminate\Support\Facades\Auth;

class GoCardless
{

	protected $client, $sessionId;

	public function __construct(){
		// $this->client = new \GoCardlessPro\Client([
		// 	'access_token' => 'sandbox_ZOZdgc0WGlpGRUcjhSLbUDfhAVzuegP9dN_2RXGH',
		// 	'environment' => \GoCardlessPro\Environment::SANDBOX
		// ]);

		$this->client = new \GoCardlessPro\Client([
			'access_token' => 'live_kNSwRn7_W4XY2rrXfNxRk1uhxafP0mY4eYx29z6f',
			'environment' => \GoCardlessPro\Environment::LIVE
		]);

		$customers = $this->client->customers()->list()->records;
       //dd($customers);

		$this->sessionId = session()->getId();
	}

	public function addCustomer($request){
		//$user = auth()->user();
		if(!Auth::check())
		{
		$redirectFlow = $this->client->redirectFlows()->create([
		    "params" => [
		        // This will be shown on the payment pages
		        "description" => " ",
		        // Not the access token
		        "session_token" => $this->sessionId,
			   // "success_redirect_url" => "http://127.0.0.1:8000/success",
			   //"success_redirect_url" =>"http://rapidresult.nepgeeks.com/success",
			   "success_redirect_url" =>"https://rapidresultsgourmet.co.uk/success",
			   
		        // Optionally, prefill customer details on the payment page
		        "prefilled_customer" => [
		          "given_name" => $request->name,
		          "family_name" => " ",
		          "email" => $request->email,
		          "address_line1" =>  $request->address,
		          "city" => $request->town,
		          "postal_code" =>$request->postcode
		        ]
		    ]
		]);
		}
		else{

			$user=Auth::user();

			$redirectFlow = $this->client->redirectFlows()->create([
				"params" => [
					// This will be shown on the payment pages
					"description" => " ",
					// Not the access token
					"session_token" => $this->sessionId,
				   // "success_redirect_url" => "http://127.0.0.1:8000/success",
				   "success_redirect_url" =>"https://rapidresultsgourmet.co.uk/success",
					// Optionally, prefill customer details on the payment page
					"prefilled_customer" => [
					  "given_name" => 'Dear',
					  "family_name" =>$user->name,
					  "email" => $user->email,
					  "address_line1" =>  $user->addressline1.''. $user->addressline2,
					  "city" => $user->addressline3,
					  "postal_code" =>$user->postcode
					]
				]
			]);
		}
		// session()->put('redirectId', $redirectFlow->id);

		return $redirectFlow;
	}

	public function redirect(){
		// dd(session()->getId());
		$redirectFlow = $this->client->redirectFlows()->complete(
		    session()->get('redirectId'), //The redirect flow ID from above.
		    ["params" => ["session_token" => session()->get('sessionToken') ]]
		);

		return $redirectFlow;
	}

	// public function oneOff($amount, $mandate, $invoiceNo, $uniqueKey){
	// 	$payment = $this->client->payments()->create([
	// 	  "params" => [
	// 	      "amount" => $amount, // 10 GBP in pence
	// 	      "currency" => "GBP",
	// 	      // "charge_date" => date("d.m.Y"),
	// 	      "links" => [
	// 	          "mandate" => $mandate
	// 	                       // The mandate ID from last section
	// 	      ],
	// 	      // Almost all resources in the API let you store custom metadata,
	// 	      // which you can retrieve later
	// 	      "metadata" => [
	// 	          "invoice_number" => $invoiceNo
	// 	      ]
	// 	  ],
	// 	  "headers" => [
	// 	      "Idempotency-Key" => $uniqueKey
	// 	  ]
	// 	]);

	// 	return $payment;
	// }

	public function subscription($amount, $mandate, $invoiceNo, $uniqueKey){
		$subscription = $this->client->subscriptions()->create([
		  "params" => [
		      "amount" => $amount, // 15 GBP in pence
		      "currency" => "GBP",
		      "interval_unit" => "weekly",
		      "links" => [
		          "mandate" => $mandate
		                       // Mandate ID from the last section
		      ],
		      "metadata" => [
		          "subscription_number" => $invoiceNo
		      ]
		  ],
		  "headers" => [
		      "Idempotency-Key" => $uniqueKey
		  ]
		]);

		return $subscription;
	}

	public function subscriptionMonthly($amount, $mandate, $invoiceNo, $uniqueKey){
		$subscription = $this->client->subscriptions()->create([
		  "params" => [
		      "amount" => $amount, // 15 GBP in pence
		      "currency" => "GBP",
		      "interval_unit" => "monthly",
		      "links" => [
		          "mandate" => $mandate
		                       // Mandate ID from the last section
		      ],
		      "metadata" => [
		          "subscription_number" => $invoiceNo
		      ]
		  ],
		  "headers" => [
		      "Idempotency-Key" => $uniqueKey
		  ]
		]);

		return $subscription;
	}

	public function cancelSubscription($id){
		return $this->client->subscriptions()->cancel($id);
	}

	public function cancelPayment($id){
		return $this->client->payments()->cancel($id);
	}
}