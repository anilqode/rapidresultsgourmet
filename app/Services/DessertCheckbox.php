<?php
/**
 * Created by PhpStorm.
 * User: apexa
 * Date: 2/17/2019
 * Time: 12:35 PM
 */

namespace App\Services;


use Illuminate\Support\Facades\DB;
class DessertCheckbox
{
    public function checkNotificationSettingsForComp($comp_dess)
    {
        $data='';

        if ($comp_dess == '0') {
            return false;
        }

        if ($comp_dess == 1) {
            $data = DB::select("select * from productcategories where comp_dess = $comp_dess");
        }
        if (count($data) > 0) {
            return "checked";
        } else {
            return false;
        }
    }
    public function checkNotificationSettingsForComp1($paid_dess)
    {   $data='';

        if ($paid_dess == '0') {
            return false;
        }

        if ($paid_dess == 1) {
            $data = DB::select("select * from productcategories where paid_dess = $paid_dess");
        }

        if (count($data) > 0) {
            return "checked";
        } else {
            return false;
        }
    }
}