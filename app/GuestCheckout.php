<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestCheckout extends Model
{
    protected $fillable = [
        'name','email','phone', 'addressline1','addressline2','addressline3','postcode',
    ];
}
