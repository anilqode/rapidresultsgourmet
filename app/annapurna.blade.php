<section class="contact-us">
<h3>Annapurna Region</h3>
	<p>Home / Adventure/ <span>Annapurna Region</span></p>
</section>
<section class="parallax abc">
	<div class="wrap-p">
		
	</div>
</section>
<p class="not-er"><b> <strong>NOTE:</strong>To experience Nepal's Himalayas you should be physically and mentally strong, having a sound health.</b></p>
<section class="mount-kilash-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 description-detail">
				<div class="full-detail">
					<div class="tab">
						<button class="tablinks active" onclick="openDetail(event, 'Itinerary');">15 Days:  Incredible ABC/MBC</button>
<button class="tablinks" onclick="openDetail(event, 'trip');">15 Days:Magnificent Round Annapurna</button>
<button class="tablinks" onclick="openDetail(event, 'punhill');">8 Days: Splendid Trek(Pun Hill)</button>
<button class="tablinks" onclick="openDetail(event, 'jomsom');">12 Days: Jomsom Trek(Buddhist Pilgrimage Journey)</button>
<button class="tablinks" onclick="openDetail(event, 'mustang');">17 Days:Forbidden Ancient Valley(Upper Mustang Trek)</button>
<button class="tablinks" onclick="openDetail(event, 'dhaulagiri');">15 Days:Annapurna Dhaulagiri(In Style Holiday)</button>
</div>

					<div id="Itinerary" class="tabcontent" style="display:block;">
<p>
					This trek begins from Pokhara, the city of lakes. Then the trek adds beautiful mountain scenery with incredible cultural diversity. The trail leads between peaks – Himchuli and Machhapuchhre and through the bamboo forest of Modi River Valley. Then the trail goes to the gates of Sanctuary. Once into the Sanctuary you will be lost into the amphitheater snow peaks including Annapurna (8090m) and Annapurna south (7219m).  From here, the trekkers can also experience other major peaks including Annapurna (7455m) and Annapurna 3rd (7555). This space with tiny thatched roofed villages, set among the terraced fields, makes a majestic picture with the backdrop of the magnificent snow range of Manaslu, Dhaulagiri and Annapurna including other Himalayan ranges and Machhapurchhre.
				</p>
<ul class="itinerary-list">
<li><span class="days">Day 1:</span> Arrival Kathmandu – transfer to hotel <span style="float:right">1820m</span></li>
<li><span class="days">Day 2:</span> Kathmandu - Pokhara <span style="float:right">6hrs drive</span></li>
<li><span class="days">Day 3:</span> Pokhara - Ghandruk <span style="float:right">1646m</span></li>
<li><span class="days">Day 4:</span> Ghandruk - Chomorong <span style="float:right">1952m</span></li>
<li><span class="days">Day 5:</span> Chomorong - Doban <span style="float:right">2505m</span></li>
<li><span class="days">Day 6:</span> Doban - Deurali <span style="float:right">3220m</span></li>
<li><span class="days">Day 7:</span> Deurali - Machhapurchhre Base Camp (MBC) <span style="float:right">3700m</span></li>
<li><span class="days">Day 8:</span> MBC - Annapurna Base Camp  ( ABC)  <span style="float:right">4130m</span></li>
<li><span class="days">Day 9:</span> ABC - Doban <span style="float:right">2505m</span></li>
<li><span class="days">Day 10:</span> Doban - Chomorong <span style="float:right">1952m</span></li>
<li><span class="days">Day 11:</span> Chomorong - Tadapani <span style="float:right">2590m </span></li>
<li><span class="days">Day 12:</span> Tadapani - Tirkhedhunga <span style="float:right">1577m </span></li>
<li><span class="days">Day 13:</span> TirkheDhunga - Pokhara <span style="float:right">820m</span></li>
<li><span class="days">Day 14:</span> Fly back kathmandu or 7 hrs drive back. <span style="float:right"> </span></li>
<li><span class="days">Day 15:</span> Departure <span style="float:right"> </span></li>
</ul>
					</div>
					<div id="trip" class="tabcontent" style="display:none;">
<p>The trek around the Annapurna circuit is famous for its spectacular views of mountains and incredible cultural diversity. As you walk through the rural villages, you may have witnessed the Nepalese cultural diversity. After passing the Thorang La Pass you head towards the Muktinath, Hindu and Buddhist shrine. Then, passing through mountain ridges you may get yourself at Pokhara, the city of lakes.</p>
						<ul class="itinerary-list">
<li>
	<span class="days">Day 1:</span> Arrival Kathmandu airport – Transfer to hotel <span style="float:right"></span></li>
<li>
	<span class="days">Day 2:</span> Kathmandu -- Beshishar <span style="float:right">823m</span>
	</li>
<li>
	<span class="days">Day 3:</span> Beshishar – Bahundand<span style="float:right">1311m</span></li>
<li>
	<span class="days">Day 4:</span> Bahundand – Chamje <span style="float:right">1430m</span></li>
<li>
	<span class="days">Day 5:</span> Chamje - Dharapani <span style="float:right">1934m</span></li>
<li>
	<span class="days">Day 6:</span> Dharapani –- Chame<span style="float:right">2713m</span></li>
<li>
	<span class="days">Day 7:</span> Chame ---- Pisang<span style="float:right">3185m</span></li>
<li>
	<span class="days">Day 8:</span> Pisang -- Manang <span style="float:right">3351m</span></li>
<li>
	<span class="days">Day 9:</span> Acclimatization Day</li>
<li>
	<span class="days">Day 10:</span>  Manang -- Chaurileter <span style="float:right">4200m</span>
</li>

<li>
	<span class="days">Day 11:</span> Chaurileter -- Thurung-La Pass Phedi <span style="float:right">4450m</span></li>
<li>
	<span class="days">Day 12:</span> Thurung La Phedi -- Muktinath <span style="float:right">3800m</span></li>
<li>
	<span class="days">Day 13:</span> Muktinath - Jomsom <span style="float:right">2713m</span>
</li>
<li>
	<span class="days">Day 14:</span>Jomson To Pokhara 8hrs drive or by flight (15min) </li>
<li>
	<span class="days">Day 15:</span> Departure or <br> Rafting 2day 1 night and Jungle Safari Chitwan  (3day 2night) .135 km.  5hrs  (15min flight ) 0r Bardiya (3day 2night ) 631km. 12 -14 hrs, 55min flight to Nepalgaung  and 3hrs drive, You will be suggested for it . Depends on the client interest. 
	</li>

</ul>
					</div>
<div id="punhill" class="tabcontent" style="display:none;">
<p>Pun Hill mountain trek is very popular tourist destination of Nepal among tourist all over the world. Tourist can view the wide ranges of Annapurna, Dhaulagiri and Pun Hill mountain range. After reaching Ghandruk, you will head towards the Poon Hill next day.Pun Hill is one of the most fascinating places from where one can get the best view of sunrise  over the mountains in the early morning. This amazing views may forger your pass time.</p>
						<ul class="itinerary-list">
<li><span class="days">Day 1:</span> Arrival at Kathmandu air port - - transfer to hotel <span style="float:right"></span></li>
<li><span class="days">Day 2:</span> DRive to Pokhara --- Ulleri</li>
<li><span class="days">Day 3:</span> Ulleri --– Ghorepani</li>
<li><span class="days">Day 4:</span> Ghorepani --– Pun hill(morning)--Tadapani</li>
<li><span class="days">Day 5:</span> Tadapani --– Ghandruk</li>
<li><span class="days">Day 6:</span> Ghandruk --– Naya pul</li>
<li><span class="days">Day 7:</span> Naya pul --- Pokhara  <span style="float:right"></span></li>
<li><span class="days">Day 8:</span> Departure or <br> Rafting 2day 1 night and Jungle Safari Chitwan  (3day 2night) .135 km.  5hrs  (15min flight ) 0r Bardiya (3day 2night ) 631km. 12 -14 hrs, 55min flight to Nepalgaung  and 3hrs drive, You will be suggested for it . Depends on the client interest. <span style="float:right"> </span></li>
</ul>
					</div>
<div id="jomsom" class="tabcontent" style="display:none;">
<p>Generally trekking in Annapurna region begins from low altitude, gently winding through tropical forest gorges, waterfall and numerous villages up to the incredible Annapurna and Dhaulagiri ranges. Trekking in this area follows culture with tea house experience. In the early morning the sun reflects over the famous mountain ranges: Fishtail, Machhapurchhre, and Dhaulagiri may make your heart stop. Viewing at Fishtail or the sacred mountain, Dhaulagiri mountain ranges forgets your coffee sip and stop breathing. Amazing………. Amazing ……..the life time experience.
</p>
<ul class="itinerary-list">
<li><span class="days">Day 1:</span> Arrival Kathmandu airport – Transfer to hotel <span style="float:right"></span></li>
<li><span class="days">Day 2:</span> Kathmandu – Pokhara – Tikhedhunga 7 hrs drive  <span style="float:right">1577m</span></li>
<li><span class="days">Day 3:</span> Tikhedhung – Ghorepani – Pun hill <span style="float:right">2830m</span></li>
<li><span class="days">Day 4:</span> Ghorepani - Punhill –Tatopani <span style="float:right">1190m</span></li>
<li><span class="days">Day 5:</span> Tatopani – Ghasa <span style="float:right">2013m</span></li>
<li><span class="days">Day 6:</span> Ghasa – Tukche <span style="float:right">2590m</span></li>
<li><span class="days">Day 7:</span> Tukche – Jomsom  <span style="float:right">2710m</span></li>
<li><span class="days">Day 8:</span> Jomsom -- Muktinath <span style="float:right">2710m</span></li>
<li><span class="days">Day 9:</span> Muktinath - Jomsom <span style="float:right">2505m</span></li>
<li><span class="days">Day 10:</span> Jomsom – Pokhara 15min flight and half day Pokhara sight seeing.  <span style="float:right"></span></li>
<li><span class="days">Day 11:</span> Pokhara-Kathmandu 7 hrs by drive <span style="float:right"> </span></li>
<li><span class="days">Day 12:</span> Departure <span style="float:right"> </span></li>
</ul>
					</div>
<div id="mustang" class="tabcontent" style="display:none;">
<p>Upper Mustang, the hidden paradise of Mustang, is one of the ancient kingdoms of Nepal. This great Himalayan region remains untraveled.  Visitor will experience ancient Tibetan Buddhist Stupas and the forbidden kingdom of Nepal. Only a few visitors have crossed the high passes to the valley of Thulung is the place of Lo Mangthang which is the seat of the king of Mustang, who is still in residence. After arriving Pokhara, the next day will be Jomsom from where the trek begins. You ascend the ridge to the north with a magnificent view of the Annapurna ranges rising behind you. The trail continues to climb on to this southern edge of Tibetan Plateau towards Mustang valley. There are several pass crossings which offer breathtaking views south to the Annapurna ranges and north to Tibet. After crossing a ridge with height 3700m, you will be at the city of forbidden kingdom Mustang. This is the remarkable city with its monasteries and palace of Mustang king. Here you can ride pony towards the Tibetan border. If you are lucky, you may handshake with the Mustang king. You then back to Jomsom and fly back to Pokhara or Kathmandu. To do trekking /mountain biking in Upper Mustang area one need to have special trekking permits to visit this region.</p>
<ul class="itinerary-list">
<li><span class="days">Day 1:</span> Arrival Kathmandu airport – Transfer to hotel <span style="float:right"></span></li>
<li><span class="days">Day 2:</span> 2 Kathmandu - Pokhara 6 hrs drive/ flight 35 min. <span style="float:right">820m</span></li>
<li><span class="days">Day 3:</span>Pokhara –Jomsom (15 min.flight) - Kagbeni                                         <span style="float:right">2810m</span></li>
<li><span class="days">Day 4:</span> Kagbeni –Chhomnang <span style="float:right">2920m</span></li>
<li><span class="days">Day 5:</span> Chhomnang  -- Giling <span style="float:right">3600m</span></li>
<li><span class="days">Day 6:</span> Giling –Tsarang <span style="float:right">3500m</span></li>
<li><span class="days">Day 7:</span> Tsarang ---- Lo Manthang   <span style="float:right">3700m</span></li>
<li><span class="days">Day 8:</span> Lo Manthang-------Explore Ancient and forbidden Valley
 Valley of Mustang also known as Forbidden Kingdom <span style="float:right">
</span></li>
<li><span class="days">Day 9:</span> Lo Manthang --- Dhile <span style="float:right">3360m</span></li>
<li><span class="days">Day 10:</span> Dhile--- Yara   <span style="float:right">3760m</span></li>
<li><span class="days">Day 11:</span>Yara --Tange <span style="float:right">3320</span></li>
<li><span class="days">Day 12:</span> Tange—Tentang   <span style="float:right">2900m</span></li>
<li><span class="days">Day 13:</span> Tentang -- Muktinath <span style="float:right">3760m</span></li>
<li><span class="days">Day 14:</span> Muktinath – Jomsom  <span style="float:right">2710m</span></li>
<li><span class="days">Day 15:</span> Jomsom—Pokhara 15 min flight <span style="float:right">820m</span></li>
<li><span class="days">Day 16:</span> Pokhara  -Kathmandu 35min.flight/ 6hrs drive by private vechicle <span style="float:right"></span></li>
<li><span class="days">Day 17:</span> Departure <span style="float:right"> </span></li>

</ul>
</div>
<div id="dhaulagiri" class="tabcontent" style="display:none;">
<p></p>
<ul class="itinerary-list">
<li>
	<span class="days">Day 1:</span> Arrival Kathmandu airport – Transfer to hotel <span style="float:right"></span></li>
<li>
	<span class="days">Day 2:</span> kathmandu city tour 
	</li>
<li>
	<span class="days">Day 3:</span> Kathamndu – Pokhara by drive</li>
<li>
	<span class="days">Day 4:</span> Pokhara – Dhampus Phedi by drive and trek to Bichhuk 
	 <span style="float:right">1799m and 1500m</span></li>
<li style="display:none;">
	<span class="days">Day 5:</span>Bichhuk - Ghandrung  <span style="float:right">1951m</span></li>
<li>
	<span class="days">Day 5:</span> Bichhuk - Ghandrung <span style="float:right">1951m</span></li>
<li>
	<span class="days">Day 6:</span> Ghandrung –Tadapani <span style="float:right">2700m</span></li>
<li>
	<span class="days">Day 7:</span> Tadapani ---- Dobato<span style="float:right">3420m</span></li>
<li>
	<span class="days">Day 8:</span> Dobato -- Chistibung <span style="float:right">3000m</span></li>
<li>
	<span class="days">Day 9:</span> Chistibung--- Kopra Ridge   <span style="float:right">3810m</span></li>
<li>
	<span class="days">Day 10:</span> Kopra Ridge --- excursion to Baraha Lake  and trek back to Kopra ridge   <span style="float:right">3810m</span></li>
<li>
	<span class="days">Day 11:</span> Kopra --- Chitre    <span style="float:right">2391m</span></li>

<li>
	<span class="days">Day 12:</span> Chitre --- Sunrise excursion to Pun Hill and Trek to Tirkhedhunga
	 <span style="float:right">3232m and 1577m</span></li>
<li>
	<span class="days">Day 13:</span> Tirkhedhunga — Nayapul and Bus to pokhara
	<span style="float:right">1040m and 915m </span></li>
<li>
	<span class="days">Day 14:</span> Fly to kathmandu </li>
<li>
	<span class="days">Day 15:</span> Departure or <br> Rafting 2day 1 night and Jungle Safari Chitwan  (3day 2night) .135 km.  5hrs  (15min flight ) 0r Bardiya (3day 2night ) 631km. 12 -14 hrs, 55min flight to Nepalgaung  and 3hrs drive, You will be suggested for it . Depends on the client interest. 
	</li>

</ul>
</div>
<div class="get_itenary_form">
<h3>Contact Us</h3>
[contact-form-7 id="1261" title="Get Itenary"]
</div>
			</div>
			</div>
			<div class="col-sm-5 img-group less-content-abc">
				<ul>
					<li><img src="https://www.actual-adventure.com/pagegallery/annapurna-base-camp-trek-7-days80.jpg" alt="abc"/></li>
					<li><img src="https://cdn.tourradar.com/s3/tour/original/88051_26113c76.jpg" alt="abc"/></li>
					<li><img src="https://www.mountainmarttreks.com/pagegallery/8-days-poon-hill-annapurna-base-camp-trek22.jpg" alt="abc"/></li>
					<li><img src="/wp-content/uploads/2018/12/IMG_1648.jpg" alt="upper mustang"/></li>
				</ul>
			</div>
		
	</div>
</section>
