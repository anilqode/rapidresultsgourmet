<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
    	'type', 'code', 'value', 'applied_amt', 'time_of_use', 'used_times', 'status', 'email'
    ];
}