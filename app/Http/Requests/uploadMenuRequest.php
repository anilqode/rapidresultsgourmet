<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class uploadMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'menu' => 'required|mimes:pdf|max:10000',
            'description' => 'required | max: 500'
        ];
    }
}
