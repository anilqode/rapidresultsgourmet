<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ( $request->user()->role_id != '2' && $request->user()->role_id != '1')
        {
            return new Response(view('unauthorized')->with('role', '2'));
}
        return $next($request);
    }
}
