<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    protected function tokensMatch($request)
    {
        $token = $request->input('_token');

        if(empty($token) && $request->ajax())
            $token = $request->header('X-CSRF-TOKEN');

        return $request->session()->token() == $token;
    }
}
