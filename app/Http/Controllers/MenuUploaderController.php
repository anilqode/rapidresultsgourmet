<?php

namespace App\Http\Controllers;

use App\MenuUploader;
use Illuminate\Http\Request;
use App\Http\Requests\uploadMenuRequest;
use App\Http\Requests\updateMenuUploadRequest;
use Response;

class MenuUploaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menuploads = MenuUploader::all();
        return view('admin-panel.menuUploader.index', compact('menuploads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel.menuUploader.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(uploadMenuRequest $request)
    {
        $input = $request->all();

        if(file_exists($request->menu)){
            $filename = 'menu'.time().'.'.$request->menu->getClientOriginalExtension();
            $location = public_path('/menuUploads');
            $request->menu->move($location, $filename);
            $input['menu'] = $filename;
        }

        $result = MenuUploader::create($input);
        return redirect()->back()->with('success', 'A new menu is uploaded successfully!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MenuUploader  $menuUploader
     * @return \Illuminate\Http\Response
     */
    public function show(MenuUploader $menuUploader)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MenuUploader  $menuUploader
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        $menuupload = MenuUploader::findOrFail($id);
        return view('admin-panel.menuUploader.edit', compact('menuupload'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MenuUploader  $menuUploader
     * @return \Illuminate\Http\Response
     */
    public function update(updateMenuUploadRequest $request, $id)
    {
        $existingMenu = MenuUploader::findOrFail($id);
        $input = $request->all();
        if(file_exists($request->menu)){
            $filename = 'menu'.time().'.'.$request->menu->getClientOriginalExtension();
            $location = public_path('/menuUploads');
            $request->menu->move($location, $filename);
            $input['menu'] = $filename;
        }
        else{
            $input['menu'] = $existingMenu->menu;
        }
        $existingMenu->update($input);
        return redirect()->back()->with('success', 'An existing menu is edited successfully!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MenuUploader  $menuUploader
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = MenuUploader::findOrFail($id);
        $menu->delete();
        return redirect()->back()->with('success', 'Deleted successfully!!');
    }

    public function pdf($id){
        $filename = MenuUploader::findOrFail($id);
        $filename = $filename->menu;
        $path = public_path('menuUploads/' . $filename);
        return Response()->file($path);
    }
}
