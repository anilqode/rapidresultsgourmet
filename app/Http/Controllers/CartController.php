<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = Cart::content();
        return view('cart.index', compact('cartItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $product = Product::findorFail($id);

        //added from here

        $product_name = $product->title;
        $product_price = $product->price;
        $quantity = $request->quantity;
        $plan_no = session()->get('plan_no');
        $total_price = $quantity * $product_price;

        if (Auth::user()) {
            $userId = $userId = Auth::user()->id;
            
            DB::table('setmealdata')->insert([
                'user_id' => $userId, 'plan_name' => 'Fitreats',
                'vegan'=>$product_name,
                'extra_snacks' => $product_name,
                'vegan_price' => $product_price, 'complementary_snacks' => '',
                'extra_snacks' => '', 'extra_price' => $total_price,
                'gourment_snacks' => '', 'gourment_price' => '',
                'total_price' =>  $total_price, 'plan_no' => $plan_no,
                'review' => '', 'total_delivery_cost' => '',
                'quantity' => $quantity
            ]);
        } else {
            if (empty($plan_no)) {
                $last_plan_no = DB::table('setmealdata')->orderBy('id', 'desc')->first();
                if (empty($last_plan_no)) {
                    $plan_no = 1;
                } else {
                    $plan_no = $last_plan_no->plan_no + 1;
                }
            }


            DB::table('setmealdata')->insert([
                'user_id' => '', 'plan_name' => 'Fitreats', 'vegan' => $product_name,
                'vegan_price' => $product_price, 'complementary_snacks' => '',
                'extra_snacks' => '', 'extra_price' => $total_price,
                'gourment_snacks' => '', 'gourment_price' => '',
                'total_price' =>  $total_price, 'plan_no' => $plan_no,
                'review' => '', 'total_delivery_cost' => '',
                'quantity' => $quantity
            ]);

            $plan_no = session()->put('plan_no', $plan_no);
        }




        // $duplicates = Cart::search(function($items, $rowId) use ($product){
        //     return $items->id === $product->id;
        // });

        // if($duplicates->isNotEmpty()){
        //     return redirect('/cart')->with('success', $product->title.' exists in your Cart!');
        // }

        // Cart::add($product->id, $product->title, 1, $product->price)->associate('App\Product');


        return redirect('/shopping-basket');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rowId, $id)
    {
        $product = Product::findorFail($id);
        Cart::update($rowId, request('quantity'));
        return redirect()->back()->with("success", $product->title . " is updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($rowId, $id)
    {
        Cart::remove($rowId);

        return redirect()->back()->with("success", "Fitreat is deleted successfully.");
    }

    public function clearCart(Request $request)
    {
        $result = $request->hiddenField;
        if ($result) {
            Cart::destroy();
            return redirect('/dashboard')->with('success', 'Fitreats cart is cleared successfully');
        }
        return redirect('/dashboard');
    }
}
