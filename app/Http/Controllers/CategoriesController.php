<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $category_infos = DB::table('categories')->select('id','name', 'slug', 'body','created_at')->get();

        return view('admin-panel.create_category_redirect',compact('category_infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $slug_exists=DB::table('categories')
            ->select('slug')
            ->where('slug','=',$request->slug)->first();
        if(count($slug_exists)<=0)

        {
            Category::create([
                'name' => $request->name,
                'body' => $request->body,
                'slug' => $request->slug,

            ]);


            return redirect('/create-categories')->with("success", "Category Created Successfully");

        }
            else{
                redirect('/create-categories');


            }

    }





    /**
     * Display the specified resource.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(Categories $categories)
    {
        //
        $category_infos = DB::table('categories')->select('id','name', 'slug', 'body','created_at')->get();

        return view('admin-panel.create_category_redirect.',compact('category_infos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $CategoryInfos = Category::where('id', $id)->first();


        return view('admin-panel.CategoryEditRedirect', compact('CategoryInfos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       Category::where('id', $id)
            ->update([
                'name' => $request->name,
                'slug' => $request->slug,
                'body' => $request->body,

            ]);

        return redirect('category/' . $id . '/edit')->with("success", 'Your Category Has Been Edited .');
    }


    public function delete($id)
    {
        Category::where('id', $id)->delete();
        return redirect('create-categories/');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categories $categories)
    {
        //
    }
}
