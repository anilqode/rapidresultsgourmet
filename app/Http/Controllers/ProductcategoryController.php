<?php

namespace App\Http\Controllers;

use App\Product;
use App\Productcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ProductcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $category_infos = Productcategory::select('id', 'name', 'slug', 'body', 'meal_plan_type', 'deliveries', 'delivery_rate', 'total_delivery_cost', 'fit', 'comp_dess', 'paid_dess', 'created_at')->get();

        return view('admin-panel.product-category.create_product_category_redirect', compact('category_infos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->image)) {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/ProductCategoryImages';
            $featuredImg = $featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            $image_path = $destinationPathFeatured . '/' . $featuredImg;
            //save into the database
        } else {
            $image_path = '/images/3/blank-profile-picture-973460_960_720.png';
        }


        $slug_exists = Productcategory::select('slug')
            ->where('slug', '=', $request->slug)->count();

        if ($slug_exists <= 0) {
            if (isset($request->comp_dess))
                $value = $request->comp_dess;
            else
                $value = 0;
            if (isset($request->paid_dess))
                $value1 = $request->paid_dess;
            else
                $value1 = 0;
            Productcategory::create([
                'name' => $request->name,
                'body' => $request->body,
                'slug' => $request->slug,
                'meal_plan_type' => $request->meal_plan_type,
                'price' => $request->price,
                'short_intro' => $request->short_intro,
                'deliveries' => $request->deliveries,
                'delivery_rate' => $request->delivery_rate,
                'total_delivery_cost' => $request->total_delivery_cost,
                'total_delivery_cost_fit' => $request->total_delivery_cost_fit,
                'fit' => $request->fit,
                'paidDes' => $request->paidDes,
                'comp_dess' => $value,
                'paid_dess' => $value1,
                'image_path' => $image_path,
                'best_seller' => $request->best_seller,
                'most_popular' => $request->most_popular
            ]);

            return redirect('/create-product-categories')->with("success", "Category Created Successfully");
        } else {
            redirect('/create-product-categories');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function show(Productcategory $productcategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function edit(Productcategory $productcategory, $id)
    {
        $CategoryInfos = Productcategory::where('id', $id)->first();

        $meal_plan_type = ["0" => "Select Meal Plan", "s" => "set meal plan", "c" => "custom meal plan", "n" => "No plan"];


        // return $CategoryInfos;


        return view('admin-panel.product-category.edit_product_category_redirect', compact('CategoryInfos', 'meal_plan_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Productcategory  $productcategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->image)) {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/ProductCategoryImages';
            $featuredImg = $featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            $image_path = $destinationPathFeatured . '/' . $featuredImg;
            //save into the database
        } else {
            $featuredImage = Productcategory::select('image_path')->where('id', $id)->first();
            $image_path = $featuredImage->image_path;
        }

        if (isset($request->comp_dess))
            $value = 1;
        else
            $value = 0;

        if (isset($request->paid_dess))
            $value1 = 1;
        else
            $value1 = 0;
        /*dd($value);
dd($value1);*/
        //    dd($value);
        Productcategory::where('id', $id)
            ->update([
                'name' => $request->name,
                'slug' => $request->slug,
                'body' => $request->body,
                'meal_plan_type' => $request->meal_plan_type,
                'price' => $request->price,
                'short_intro' => $request->short_intro,
                'deliveries' => $request->deliveries,
                'delivery_rate' => $request->delivery_rate,
                'total_delivery_cost' => $request->total_delivery_cost,
                'total_delivery_cost_fit' => $request->total_delivery_cost_fit,
                'fit' => $request->fit,
                'paidDes' => $request->paidDes,
                'comp_dess' => $value,
                'paid_dess' => $value1,
                'image_path' => $image_path,
                'best_seller' => $request->best_seller,
                'most_popular' => $request->most_popular

            ]);

        return redirect('/product-category/' . $id . '/edit')->with("success", 'Your Category Has Been Updated .');
    }

    public function delete($id)
    {
        Productcategory::where('id', $id)->delete();
        return redirect('/create-product-categories');
    }
}