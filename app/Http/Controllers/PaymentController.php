<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

/** All Paypal Details class **/

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\DB;
use Redirect;
use Session;
use URL;
use App\Coupon;
use App\GuestUser;
use App\Services\GoCardless;
use App\User;

use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{

	public function __construct()
	{
		/** PayPal api context **/
		$paypal_conf = \Config::get('paypal');
		$this->_api_context = new ApiContext(new OAuthTokenCredential(
			$paypal_conf['client_id'],
			$paypal_conf['secret']
		));
		$this->_api_context->setConfig($paypal_conf['settings']);
		$this->client = new GoCardless();
	}



	public function goCardless($request)
	{

		session()->put('buttonClicked', $request->paymentMode);
		session()->put('orderId', $request->order_id);
		session()->put('totalPrice', $request->amount);
		session()->put('veganTitle', $request->vegan_title);
		$redirect = $this->client->addCustomer($request);

		$success_url = $redirect->redirect_url;

		session()->put('redirectId', $redirect->id);

		session()->put('sessionToken', $redirect->session_token);
		//dd(success_url);

		echo redirect($success_url);
		//dd($redirect->session_token);
	}

	public function goCardlessSuccess()
	{
		// dd(session()->get('redirectId'));
		$redirectToPayment = $this->client->redirect();
		session()->put('mandateId', $redirectToPayment->links->mandate);
		session()->put('successURL', $redirectToPayment->confirmation_url);
		// return Redirect::route('add_to_checkout');

		$buttonClicked=session()->get('buttonClicked');

		

		if($buttonClicked=='gocardlessMonthly'){
			$this->subscriptionMonthly();

		}
		elseif($buttonClicked=='gocardlessWeekly'){
			$this->goCardlessSubscription();
		}

		return redirect('thank-you');
	}

	public function subscriptionMonthly()
	{
		$totalMonthlyPrice=session()->get('totalPrice');
		$payment = $this->client->subscriptionMonthly(
		session()->get('totalPrice')*100,
			session()->get('mandateId'),
			strval(session()->get('orderId')),
			session()->get('sessionToken')
		);
		session()->put('paymentId', $payment->id);
		if (session()->get('coupon')) {
			$coupon = Coupon::where('code', session()->get('coupon_code'))->first();
			$coupon->update([
				'used_times' => $coupon->used_times + 1
			]);
			$this->sendMailtoPromoter($coupon);
		}
		session()->forget('coupon');
		session()->forget('coupon_type');
		$this->insertIntoGoCardlessPayment('Monthly');
		$this->sendMailGoCardless('Gocardless Monthly',$totalMonthlyPrice);
		return redirect(session()->get('successURL'));
	}

	public function goCardlessSubscription()
	{

		$totalWeeklyPrice=session()->get('totalPrice')/4;
		$payment = $this->client->subscription(
			number_format((float) ((session()->get('totalPrice'))/4), 2, '.', '')*100,
			session()->get('mandateId'),
			strval(session()->get('orderId')), 
			session()->get('sessionToken')
		);
		session()->put('paymentId', $payment->id);
		if (session()->get('coupon')) {
			$coupon = Coupon::where('code', session()->get('coupon_code'))->first();
			$coupon->update([
				'used_times' => $coupon->used_times + 1
			]);
			$this->sendMailtoPromoter($coupon);
		}
		session()->forget('coupon');
		session()->forget('coupon_type');
		$this->insertIntoGoCardlessPayment('Weekly');
		$this->sendMailGoCardless('Gocardless Weekly',$totalWeeklyPrice);
		return redirect(session()->get('successURL'));
	}


	public function insertIntoGoCardlessPayment($str)
	{

		$value = DB::table('gocardless_payment')->insert([
			'order_id' => session()->get('orderId'),
			'mode' => $str,
			'paymentId' => session()->get('paymentId')
		]);
		$this->updateSetMealOrder(session()->get('orderId'));
		return $value;
	}

	public function sendMailGoCardless($paymentMode,$paidAmount)
	{

		if(Auth::check()){
			$orderInfo = DB::table('setmealdata')
			->leftjoin('users', 'users.id', 'setmealdata.user_id')
			->where('setmealdata.plan_no', session()->get('orderId'))
			->select('setmealdata.id','plan_no', 'plan_name', 'vegan', 'extra_snacks', 'total_price', 'complementary_snacks', 'users.name', 'users.addressline3', 'users.addressline1', 'users.addressline2', 'users.postcode', 'users.phone_number', 'users.email', 'review')
			->first();
		}
		else{
			$orderInfo = DB::table('setmealdata')
			->leftjoin('guest_users', 'setmealdata.plan_no', 'guest_users.order_id')
			->where('setmealdata.plan_no', session()->get('orderId'))
			->select('setmealdata.id','plan_no', 'plan_name', 'vegan', 'extra_snacks', 'total_price', 'complementary_snacks', 'guest_users.name', 'guest_users.addressline3', 'guest_users.addressline1', 'guest_users.addressline2', 'guest_users.postcode', 'guest_users.phone_number', 'guest_users.email', 'guest_users.review')
			->first();	
		}

		//dd($orderInfo);

		$extraSnacksInfo = DB::table('extra_snack_plans')
				->where('plan_no', session()->get('orderId'))
				->select('extra_snack', 'quantity')->get();

			$extra_snacks_set_meal = '';
			foreach ($extraSnacksInfo as $extrainfo) {
				$extra_snacks_set_meal .= $extrainfo->extra_snack . ' Quantity : ' . $extrainfo->quantity . '<br>';
			}

			//dd($extra_snacks_set_meal);


			$fitreatInfo = DB::table('setmealdata')
				->where('setmealdata.plan_no', session()->get('orderId'))->where('set_meal_selected', '0')->where('plan_name', 'Fitreats')
				->select('vegan', 'quantity')->get();


			$fitreats = '';
			foreach ($fitreatInfo as $fitinfo) {
				$fitreats .= $fitinfo->vegan . ' Quantity : ' . $fitinfo->quantity . '<br>';
			}

		$complimentary_snacks = $orderInfo->complementary_snacks;
		if(!isset($complimentary_snacks)){
			$complimentary_snacks=[];
		}
		$extra_snacks = $orderInfo->extra_snacks;

		if (!isset($complimentary_snacks)) {
			$complimentary_snacks = 'not available';
		}

		if (!isset($extra_snacks)) {
			$extra_snacks = 'not available';
		}

		$this->sendEmail($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks,$paymentMode,$paidAmount);
		$this->sendEmailToCustomer($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks,$paymentMode,$paidAmount);
		return true;
	}

	public function cancelCardPayment()
	{
		return view('gocardless.cancelOrder');
	}

	public function cancel()
	{
		$orderId = (int)Input::get('orderId');
		$result = DB::table('gocardless_payment')->where('order_id', $orderId)->first();
		$status = $this->client->cancelSubscription($result->paymentId);
		return redirect('dashboard');
	}


	public function payWithPaypal(Request $request)
	{

		if(isset($request->review)){
		DB::table('setmealdata')->where('plan_no','=',$request->order_id)
		->update(['review'=>$request->review]);
	}
		//return $request->all();
		if ($request->paymentMode == 'registerGuest') {

			$user=User::create([

				'role_id' => '2',
				'name' => $request->name,
				'email' => $request->email,
				'phone_number' => $request->phone,
				'addressline1' => $request->address,
				'addressline2' => $request->town,
				'addressline3' => $request->county,
				'postcode' => $request->postcode,
				'password' => bcrypt($request->password)

			]);

			Session::put('order_review', $request->review);
			Auth::login($user);
			return redirect('dashboard');
		} else if ($request->paymentMode == 'paypal') {
			
			session()->put('order_id', $request->order_id);
			$payer = new Payer();
			$payer->setPaymentMethod('paypal');
			$item_1 = new Item();
			$item_1->setName($request->vegan_title)
				/** item name **/
				->setCurrency('GBP')
				->setQuantity(1)
				->setPrice($request->amount);
			/** unit price **/
			$item_list = new ItemList();
			$item_list->setItems(array($item_1));
			$amount = new Amount();
			$amount->setCurrency('GBP')
				->setTotal($request->amount);
			$transaction = new Transaction();
			$transaction->setAmount($amount)
				->setItemList($item_list)
				->setDescription('Your transaction description');
			$redirect_urls = new RedirectUrls();
			$redirect_urls->setReturnUrl(URL::route('status'))
				/** Specify return URL **/
				->setCancelUrl(URL::route('status'));
			$payment = new Payment();
			$payment->setIntent('Sale')
				->setPayer($payer)
				->setRedirectUrls($redirect_urls)
				->setTransactions(array($transaction));
			/** dd($payment->create($this->_api_context));exit; **/
			try {
				$payment->create($this->_api_context);

				if (!Auth::check()) {
					$this->saveGuestUser($request);
				}

			} catch (\PayPal\Exception\PayPalConnectionException $ex) {
				if (\Config::get('app.debug')) {
					\Session::put('error', 'Connection timeout');
					//return Redirect::route('dashboard');// Route to redirect on cancel
				} else {
					\Session::put('error', 'Some error occur, sorry for inconvenient');
					//return Redirect::route('dashboard');// Route to redirect on cancel
				}
			}
			foreach ($payment->getLinks() as $link) {
				if ($link->getRel() == 'approval_url') {
					$redirect_url = $link->getHref();
					break;
				}
			}
			/** add payment ID to session **/
			Session::put('paypal_payment_id', $payment->getId());
			if (isset($redirect_url)) {
				/** redirect to paypal **/
				return Redirect::away($redirect_url);
			}
			\Session::put('error', 'Unknown error occurred');
			return Redirect::route('dashboard'); // Route to redirect on cancel
		} elseif ($request->paymentMode == 'gocardlessMonthly') {
			if (!Auth::check()) {
				$this->saveGuestUser($request);
			}
			$this->goCardless($request);
		}elseif ($request->paymentMode == 'gocardlessWeekly') {
			if (!Auth::check()) {
				$this->saveGuestUser($request);
			}
			$this->goCardless($request);
		}
	}


	public function thankYou()
	{

		session()->forget('plan_no');
        session()->flush();
        session()->invalidate();
		return view('checkout.thank-you');
	}

	public function getPaymentStatus()
	{
		/** Get the payment ID before session clear **/
		$payment_id = Session::get('paypal_payment_id');
		/** clear the session payment ID **/
		Session::forget('paypal_payment_id');
		if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
			\Session::put('error', 'Payment failed');
			return Redirect::route('dashboard'); // Route to redirect on cancel
		}
		$payment = Payment::get($payment_id, $this->_api_context);
		$execution = new PaymentExecution();
		$execution->setPayerId(Input::get('PayerID'));
		/**Execute the payment **/
		$result = $payment->execute($execution, $this->_api_context);
		if ($result->getState() == 'approved') {
			//Session::put('success', 'Payment success');
			$order_id = session::get('order_id');
			// return Redirect::route('add_to_checkout/'.);

			$this->updateSetMealOrder($order_id);
		

			if(Auth::check()){
				$orderInfo = DB::table('setmealdata')
				->leftjoin('users', 'users.id', 'setmealdata.user_id')->where('setmealdata.plan_no', $order_id)
				->select('setmealdata.id','setmealdata.plan_no', 'plan_name', 'vegan', 'extra_snacks', 'total_price', 'users.name',  'users.addressline1', 'users.addressline2', 'users.addressline3', 'users.postcode', 'users.phone_number', 'users.email', 'setmealdata.review')
				->first();
			}
			else{
				$this->updateGuestUser($order_id);
				$orderInfo = DB::table('setmealdata')
				->leftjoin('guest_users', 'guest_users.order_id', 'setmealdata.plan_no')->where('setmealdata.plan_no', $order_id)
				->select('setmealdata.id', 'setmealdata.plan_no','plan_name', 'vegan', 'extra_snacks', 'total_price', 'guest_users.name',  'guest_users.addressline1', 'guest_users.addressline2', 'guest_users.addressline3', 'guest_users.postcode', 'guest_users.phone_number', 'guest_users.email', 'guest_users.review')
				->first();
			}

			

			$extraSnacksInfo = DB::table('extra_snack_plans')
				->where('plan_no', $order_id)
				->select('extra_snack', 'quantity')->get();

			$extra_snacks_set_meal = '';
			foreach ($extraSnacksInfo as $extrainfo) {
				$extra_snacks_set_meal .= $extrainfo->extra_snack . ' Quantity : ' . $extrainfo->quantity . '<br>';
			}

			//dd($extra_snacks_set_meal);


			$fitreatInfo = DB::table('setmealdata')
				->where('setmealdata.plan_no', $order_id)->where('set_meal_selected', '0')->where('plan_name', 'Fitreats')
				->select('vegan', 'quantity')->get();


			$fitreats = '';
			foreach ($fitreatInfo as $fitinfo) {
				$fitreats .= $fitinfo->vegan . ' Quantity : ' . $fitinfo->quantity . '<br>';
			}


			//dd($fitreats);


			// $complimentary_snacks = $orderInfo->complementary_snacks;
			$extra_snacks = $orderInfo->extra_snacks;

			if (!isset($complimentary_snacks)) {
				$complimentary_snacks = 'not available';
			}

			if (!isset($extra_snacks)) {
				$extra_snacks = 'not available';
			}

			if (session()->get('coupon')) {
				$coupon = Coupon::where('code', session()->get('coupon_code'))->first();
				$coupon->update([
					'used_times' => $coupon->used_times + 1
				]);
				$this->sendMailtoPromoter($coupon);
			}
			session()->forget('coupon');
			session()->forget('coupon_type');

			$paymentMode='Paypal';
			$paidAmount=$orderInfo->total_price;

			$result = $this->sendEmail($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks,$paymentMode,$paidAmount);
			//dd($result);
			$this->sendEmailToCustomer($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks,$paymentMode,$paidAmount);
			return redirect('thank-you')->with('order_id', $order_id);
		}
		\Session::put('error', 'Payment failed');
		return Redirect::route('thank-you'); // Route to redirect on cancel
	}

	public function updateSetMealOrder($order_id)
	{
		$updated = DB::table('setmealdata')->where('plan_no', $order_id)
			->update(['is_paid' => 1]);
		return $updated;
	}

	public function sendEmail($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks,$paymentMode, $paidAmount)
	{
		$discountAmtInfo=DB::table('order_discounts')->where('plan_no',$orderInfo->plan_no)->first();

		if(!isset($discountAmtInfo)){
			$discountAmt=0;
			$netAmount=$orderInfo->total_price;
		}
		else{
			$discountAmt=$discountAmtInfo->amount;
			$netAmount=$orderInfo->total_price - $discountAmt;
		}
        if(!isset($fitreats)){
            $fitreats='None';
        }

        if (empty($fitreats)) {
            $fitreats='None';
        }


        if(!isset($extra_snacks_set_meal)) {
            $extra_snacks_set_meal = 'None';
        }

        if (empty($extra_snacks_set_meal)) {
            $extra_snacks_set_meal='None';
        }



		$review = isset($orderInfo->review) ? $orderInfo->review : 'Not Available';

		$txt = '
		<html>
		 <head>
			<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
			<style type="text/css">
			    body{
					font-family: \'Roboto\', sans-serif;
					padding: 0;
					margin: 0;
				}

             #wrapper{
				width: 100%;
				max-width: 600px;
				margin: 0 auto;
				box-shadow: 0px 3px 6px #000;
				border: 1px solid #ccc;
			}

        #header{
			width: 100%;
			height: 155px;
			padding: 20px 0 7px;
		}

#header h4 {
		color: #000000;
		font-size: 30px;
		font-weight: 500;
		text-transform: uppercase;
	}


#content h5 {
	color: #666666;
	font-size: 24px;
	font-weight: 700;
}

#content h6 {
		color: #04a7e0;
		font-size: 30px;
		font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
	text-align: center;
}

.top_30{
	margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}

td{
	padding: 8px;
}

</style>
</head>
<body>

	<div id="wrapper"  class="text-center">
		<div id="header" style="background: #363636;">

			<img src="https://rapidresultsgourmet.co.uk/images/rrgimages/logo.jpg" style="outline: none;text-decoration: none;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;padding-top: 4px; width:142px;">
		</div><!-- #header -->

		<div id="content">
			<h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received</h2>
			<h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Attention <b>Rapid Results Gourmet</b></h2>
			<p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
				The Following order has been made </p>
				<table style="margin: 0 auto;text-align: left;border: 1px solid black;">
					<tr><td><b>Order Number</b></td><td style="padding-left: 20px;">' . $orderInfo->plan_no . '</td></tr>
					<tr><td><b>Plan Name</b></td><td style="padding-left: 20px;">' . $orderInfo->plan_name . '</td></tr>
					<tr><td><b>Meals</b></td><td style="padding-left: 20px;
						">' . $orderInfo->vegan . '</td></tr>
							<tr><td><b>Extra Snacks</b></td><td style="
								padding-left: 20px;">' . $extra_snacks_set_meal . '</td></tr>
								<tr><td><b>Fitreats</b></td><td style="
								padding-left: 20px;">' . $fitreats . '</td></tr>
							
								<tr><td><b>Gross Amount</b></td><td style="
									padding-left: 20px;">£' .  number_format((float) $orderInfo->total_price , 2, '.', ''). '</td></tr>



									<tr><td><b>Discount </b></td><td style="
									padding-left: 20px;">£' .  number_format((float) $discountAmt , 2, '.', '')  . '</td></tr>

									<tr><td><b>Total Amount </b></td><td style="
									padding-left: 20px;">£' . number_format((float) $netAmount , 2, '.', '')  . '</td></tr>
									<tr><td><b>Payment Mode</b></td><td style="
									padding-left: 20px;">' . $paymentMode. '</td></tr>   
									<tr><td><b>Paid Amount</b></td><td style="
									padding-left: 20px;">£' . number_format((float) $paidAmount , 2, '.', '') . '</td></tr>   

									<tr><td><b>Customer Name</b></td><td style="
										padding-left: 20px;">' . $orderInfo->name . '</td></tr>
										<tr><td><b>Customer Phone Number</b></td><td style="
											padding-left: 20px;">' . $orderInfo->phone_number . '</td></tr>
											<tr><td><b>Customer Address</b></td><td style="
												padding-left: 20px;">' . $orderInfo->addressline1 . ' ' . $orderInfo->addressline2 . ' ' . $orderInfo->addressline3 . ' ' . $orderInfo->postcode . '</td></tr>
												<tr><td><b>Customer Email</b></td><td style="
													padding-left: 20px;">' . $orderInfo->email . '</td></tr>
													<tr><td><b>Customer Request</b></td><td style="
													padding-left: 20px;">' . $review . '</td></tr>
												</table>
											</div><!-- #content -->
											<div id="footer">

												<img src="https://rapidresultsgourmet.co.uk/images/rrgimages/logo.png" alt="" />

												<p>7 RAILWAY STREET GRAVESEND, DA11 9DU <br />
													<span id="footeremail" style="color:#fff;">info@rapidresultsgourmet.co.uk</span> | +44 7927 393481</p>


													<ul id="footer_social_icons" style="padding-left: 0px;">
														<li><a></a></li>
														<li><a></a></li>
														<li><a></a></li>
														<li><a></a></li>
													</ul>
												</div><!-- #footer -->


											</div><!-- #wrapper -->
										</body>
										</html>';

		$to = "rapidresultsorder@gmail.com";
		$subject = "Customer RRG Order Summary";
		$headers = "From:rapidresultsorder@gmail.com\r\n";
		$headers .= "CC: nepgeeks@gmail.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";

		$result = mail($to, $subject, $txt, $headers);

		return $result;
	}


	public function sendEmailToCustomer($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks,$paymentMode,$paidAmount)
	{

		$discountAmtInfo=DB::table('order_discounts')->where('plan_no',$orderInfo->plan_no)->first();

		if(!isset($discountAmtInfo)){
			$discountAmt=0;
			$netAmount=$orderInfo->total_price;
		}
		else{
			$discountAmt=$discountAmtInfo->amount;
			$netAmount=$orderInfo->total_price - $discountAmt;
		}

        if(!isset($fitreats)){
		    $fitreats='None';
        }

        if (empty($fitreats)) {
            $fitreats='None';
        }

        if(!isset($extra_snacks_set_meal)) {
            $extra_snacks_set_meal = 'None';
        }

        if (empty($extra_snacks_set_meal)) {
            $extra_snacks_set_meal='None';
        }


        $txt = '<html>
		<head>
			<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
			<style type="text/css">
				body{
					font-family: \'Roboto\', sans-serif;
					padding: 0;
					margin: 0;
				}
        #wrapper{
				width: 100%;
				max-width: 600px;
				margin: 0 auto;
				box-shadow: 0px 3px 6px #000;
				border: 1px solid #ccc;
			}

        #header{

			width: 100%;
			height: 155px;
			padding: 20px 0 7px;
		}

        #header h4 {
		color: #000000;
		font-size: 30px;
		font-weight: 500;
		text-transform: uppercase;
	}


        #content h5 {
	color: #666666;
	font-size: 24px;
	font-weight: 700;
}

        #content h6 {
color: #04a7e0;
font-size: 30px;
font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
	text-align: center;
}

.top_30{
	margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}
td{
padding:8px;
}
</style>
</head>
<body>

	<div id="wrapper"  class="text-center">
		<div id="header" style="background: #363636;">

			<img src="https://rapidresultsgourmet.co.uk/images/rrgimages/logo.jpg" style="outline: none;text-decoration: none;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;padding-top: 4px; width:142px;">
		</div><!-- #header -->

		<div id="content">
			<h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received</h2>
			<h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Hello <b>' . $orderInfo->name . '</b></h2>
			<p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
				Thank you for placing your order.<br> Your Order Summary</p>
				<table style="margin: 0 auto;text-align: left; border: 1px solid black;">
					<tr><td><b>Order Number</b></td><td style="padding-left: 20px;">' . $orderInfo->plan_no . '</td></tr>
					<tr><td><b>Plan Name</b></td><td style="padding-left: 20px;">' . $orderInfo->plan_name . '</td></tr>
					<tr><td><b>Meals</b></td><td style="padding-left: 20px;
						">' . $orderInfo->vegan . '</td></tr>
						
							<tr><td><b>Extra Snacks</b></td><td style="
								padding-left: 20px;">' . $extra_snacks_set_meal . '</td></tr>
								<tr><td><b>Fitreats</b></td><td style="
								padding-left: 20px;">' . $fitreats . '</td></tr>

								<tr><td><b>Gross Amount</b></td><td style="
								padding-left: 20px;">£' .  number_format((float) $orderInfo->total_price , 2, '.', ''). '</td></tr>

								<tr><td><b>Discount</b></td><td style="
								padding-left: 20px;">£' .  number_format((float) $discountAmt , 2, '.', '')  . '</td></tr>

								<tr><td><b>Total Amount </b></td><td style="
								padding-left: 20px;">£' . number_format((float) $netAmount , 2, '.', '')  . '</td></tr>
								<tr><td><b>Payment Mode</b></td><td style="
								padding-left: 20px;">' . $paymentMode. '</td></tr>   
								<tr><td><b>Paid Amount</b></td><td style="
								padding-left: 20px;">£' . number_format((float) $paidAmount , 2, '.', '') . '</td></tr>   
									
										<tr><td><b>Customer Phone Number</b></td><td style="
											padding-left: 20px;">' . $orderInfo->phone_number . '</td></tr>
											<tr><td><b>Customer Address</b></td><td style="
												padding-left: 20px;">' . $orderInfo->addressline1 . ' ' . $orderInfo->addressline2 . ' ' . $orderInfo->addressline3 . ' ' . $orderInfo->postcode . '</td></tr>
												<tr><td><b>Customer Email</b></td><td style="
													padding-left: 20px;">' . $orderInfo->email . '</td></tr>
													<tr><td><b>Customer Request</b></td><td style="
													padding-left: 20px;">' . $orderInfo->review . '</td></tr>

												</table>
											</div><!-- #content -->
											<div id="footer">

												<img src="https://rapidresultsgourmet.co.uk/images/rrgimages/logo.png" alt="" />

												<p>7 RAILWAY STREET GRAVESEND, DA11 9DU <br />
													<span id="footeremail" style="color:#fff;">info@rapidresultsgourmet.co.uk</span> | +44 7927 393481</p>


													<ul id="footer_social_icons" style="padding-left: 0px;">
														<li><a><img src="http://qodebox.com/images/social-facebook.jpg" alt="" /></a></li>
														<li><a><img src="http://qodebox.com/images/social-gplus.jpg" alt="" /></a></li>
														<li><a><img src="http://qodebox.com/images/social-twitter.jpg" alt="" /></a></li>
														<li><a><img src="http://qodebox.com/images/social-linkedin.jpg" alt="" /></a></li>
													</ul>
												</div><!-- #footer -->


											</div><!-- #wrapper -->
										</body>
										</html>
										';

		$to = $orderInfo->email;
		$subject = "Your RRG Order Summary";
		$headers = "From:rapidresultsorder@gmail.com\r\n";
		$headers .= "CC:" . $orderInfo->email . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


		// $headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";$headers .= "CC: susan@example.com\r\n";
		// $headers .= "MIME-Version: 1.0\r\n";
		// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


		$sent = mail($to, $subject, $txt, $headers);
		//dd($sent);
	}

	public function sendMailtoPromoter($coupon)
	{
		$txt = "The coupon with code " . $coupon->code . " has been used for " . $coupon->used_times . " times.";
		$to = $coupon->email;
		$subject = "Well done! You have just earned some more commision from RRG!";
		$headers = "From:rapidresultsorder@gmail.com\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
		mail($to, $subject, $txt, $headers);
	}

	public function saveGuestUser($request)
	{

		//dd($request);

		$user = GuestUser::create([
			'name' => $request->name,
			'email' => $request->email,
			'role_id' => 2,
			'phone_number' => $request->phone,
			'addressline1' => $request->address,
			'addressline2' => $request->town,
			'addressline3' => $request->county,
			'postcode' => $request->postcode,
			'order_id' => $request->order_id,
			'review' => $request->review
		]);
	}

	public function updateGuestUser($order_id)
	{

		$update = GuestUser::where('order_id', $order_id)
			->update(['is_paid' => 1]);
	}
}
