<?php

namespace App\Http\Controllers;

use App\Testinomial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TestinomialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin-panel.testinomial.add_testinomial_redirect');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       /* return $request->all();*/
        $record=DB::table('testinomials')->get();
        if($record)

        {
            Testinomial::create([
                'name' => $request->name,
                'desg' => $request->desg,
                'image_path' => $request->image,
                'description' => $request->description,

            ]);


            return redirect('/add-testinomial')->with("success", "Testinomial Created Successfully");

        }
        else{
            redirect('/add-testinomial')->with("Sorry", "Testinomial Could not be Created Successfully");


        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
       $record=DB::table('testinomials')->get();
return view('admin-panel.testinomial.display_testinomial_redirect',compact('record'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $record=DB::table('testinomials')->where('id',$id)
            ->find($id);
            

        return view('admin-panel.testinomial.edit_testinomial_redirect',compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     /*   return $request->all();*/
        Testinomial::where('id', $id)
            ->update([
                'name' => $request->name,
                'desg' => $request->desg,
                'image_path' => $request->image_path,
                'description' => $request->description,
            ]);

        return redirect('/edit/' . $id . '/testinomial')->with("success", 'Your Testinomial Has Been Edited .');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testinomial  $testinomial
     * @return \Illuminate\Http\Response
     */
    public function delete(Testinomial $testinomial,$id)
    {

        Testinomial::where('id', $id)->delete();
        return redirect('/display-testinomial');
    }
}
