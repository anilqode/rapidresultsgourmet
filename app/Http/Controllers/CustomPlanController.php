<?php

namespace App\Http\Controllers;

use App\Productcategory;
use App\Product;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

class CustomPlanController extends Controller
{

	//custom home function
	public function customIndex()
	{

		$mealPlan = [];
		$weekplan = DB::table('weeks')->select('week', 'id', 'days')->get();
		$mealplan = DB::table('meals')->select('meal', 'id')->get();
		//return $mealplan;
		foreach ($weekplan as $plan) {
			$mealPlanInfo = [];
			$mealPlanInfo['week'] = $plan->week;
			$mealPlanInfo['week_id'] = $plan->id;
			$mealPlanInfo['days'] = $plan->days;
			foreach ($mealplan as $mplan) {
				$mealPlanInfo[$mplan->id] = $mplan->meal;
			}
			$mealPlan[] = $mealPlanInfo;
		}


		//return $mealPlan;

		// foreach($mealPlan as $plan){
		// 	return $plan['week'];
		// }

		//print_r($mealPlanInfo) ;

		return view('admin-panel.customplan.custom_index', compact('mealPlan'));
	}

	public function storeCustomPlan($weekplanid, $id)
	{

		$temp_user_id = Auth::user()->id;

		return redirect('/main-meal-page/' . $weekplanid . '/' . $id . '/' . $temp_user_id);
	}

	public function getMainMealPage($weekplanid, $meal_id, $temp_user_id)
	{
		$tempIdExist = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->exists();
		if ($tempIdExist) {
			$foodItem = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->get();

			$total_count_quantity = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->sum('meal_quantity');

			$basket_item_count = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->count();

			session()->put('food_item', $foodItem);
			session()->put('count_quantity', $total_count_quantity);
			session()->put('basket_item_count', $basket_item_count);
		}

		$categoryData = DB::table('productcategories')->get();

		$vegCarbId = DB::table('productcategories')->where('name', 'VegCarb')->first();

		// return($vegCarbId);

		$vegCarbProducts = DB::table('products')->where(
			'cat_id',
			$vegCarbId->id
		)->get();


		return view('admin-panel.customplan.main_meal_page', compact('weekplanid', 'temp_user_id', 'categoryData', 'vegCarbProducts', 'meal_id'));
	}

	public function addToBasket(Request $request)
	{

		$food_item = $request->food_item;
		$count_quantity = $request->count_quantity;
		$weekplanid = $request->weekplanid;
		$meal_id = $request->meal_id;
		$vegcarbs = $request->select_veg_and_carbs;
		$temp_user_id = $request->temp_user_id;
		$all_vegcarb = ' ';
		if ($vegcarbs) {
			foreach ($vegcarbs as $vegcarb) {
				$all_vegcarb = $all_vegcarb . '+' . $vegcarb;
			}
		}


		if ($request->count_quantity) {

			$compareFoodItem = DB::table('customtracktemp')->where([
				['temp_user_id', $temp_user_id],
				['meal_name', $food_item],
				['vegcarb', $all_vegcarb]
			])->exists();


			if ($compareFoodItem) {
				DB::table('customtracktemp')->where([
					['temp_user_id', $temp_user_id],
					['meal_name', $food_item],
				])
					->update(['meal_quantity' => $count_quantity, 'vegcarb' => $all_vegcarb]);
			} else {
				DB::table('customtracktemp')->insert(
					['temp_user_id' => $temp_user_id, 'weekplans_id' => $weekplanid, 'meal_name' => $food_item, 'meal_quantity' => $count_quantity, 'vegcarb' => $all_vegcarb, 'meal_id' => $meal_id,]
				);
			}
		}

		$foodItem = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->get();

		$total_count_quantity = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->sum('meal_quantity');

		$basket_item_count = DB::table('customtracktemp')->where('temp_user_id', $temp_user_id)->count();

		return redirect()->back()->with('food_item', $foodItem)
			->with('count_quantity', $total_count_quantity)
			->with('basket_item_count', $basket_item_count);
	}

	public function updateBasketData(Request $request, $temp_id, $meal, $vegcarb, $sn)
	{

		$inputId = 'count_edit' . $sn;
		$checkInput = $request->$inputId;
		if (!$checkInput) {
			$inputId = 'count_edit_m' . $sn;
			$checkInput = $request->$inputId;
		}
		if ($checkInput == 0) {
			DB::table('customtracktemp')->where([
				['temp_user_id', $temp_id],
				['meal_name', $meal],
				['vegcarb', $vegcarb]
			])
				->delete();
		} else {
			DB::table('customtracktemp')->where([
				['temp_user_id', $temp_id],
				['meal_name', $meal],
				['vegcarb', $vegcarb]
			])
				->update(['meal_quantity' => $request->$inputId]);
		}
	}

	public function readData($temp)
	{
		$total_count_quantity = DB::table('customtracktemp')->where('temp_user_id', $temp)->sum('meal_quantity');
		return response($total_count_quantity);
	}

	public function redirectFromCustom($week_id, $Normalid)
	{
		if (auth()->user()) {
			$url = '/custom-meal-plans/' . $week_id . '/' . $Normalid . '/' . auth()->user()->id;
			return redirect($url);
		}
		return view('auth.login', compact('week_id', 'Normalid'));
	}

	public function redirectFromCustomToRegister(Request $request)
	{

		$mealTitle = null;
		$vegan = null;
		$vegan_price = null;
		$complementary_snacks = null;
		$extra_snacks = null;
		$gourment_snacks = null;
		$gourment_price = null;
		$total_price = null;
		if ($request->mealTitle != "NAN") {
			$mealTitle = $request->mealTitle;
			$vegan = $request->vegan;
			$vegan_price = $request->vegan_price;
			$complementary_snacks = $request->complementary_snacks;
			$extra_snacks = $request->extra_snacks;
			$gourment_snacks = $request->gourment_snacks;
			$gourment_price = $request->gourment_price;
			$total_price = $request->total_price;
		}

		return redirect('/user-register')->with('mealTitle', $mealTitle)
			->with('vegan', $request->vegan)
			->with('vegan_price', $request->vegan_price)
			->with('complementary_snacks', $request->complementary_snacks)
			->with('extra_snacks', $request->extra_snacks)
			->with('extra_price', $request->extra_price)
			->with('gourment_snacks', $request->gourment_snacks)
			->with('gourment_price', $request->gourment_price)
			->with('total_price', $request->total_price)
			->with('Normalid', $request->Normalid)
			->with('week_id', $request->week_id);
	}

	public function redirectToRegister($id)
	{
		if (Auth::user()) {
			$basket_item_counts = DB::table('customtracktemp')->where('temp_user_id', $id)->count();

			if ($basket_item_counts) {
				$itemFromBaskets = DB::table('customtracktemp')->where('temp_user_id', $id)->get();

				$userId = Auth::user()->id;

				$planNoExist = DB::table('customplanusers')->where('user_id', $userId)->exists();

				$plan_no = 1;
				if ($planNoExist) {
					$last_plan_no = DB::table('customplanusers')->where('user_id', $userId)->orderBy('plan_no', 'desc')->first();

					$plan_no += $last_plan_no->plan_no;
				}



				foreach ($itemFromBaskets as $itemFromBasket) {

					if ($itemFromBasket->weekplans_id == 1) {
						$noOfDelivery = 8;
					} elseif ($itemFromBasket->weekplans_id == 2) {
						$noOfDelivery = 12;
					} elseif ($itemFromBasket->weekplans_id == 3) {
						$noOfDelivery = 16;
					}
					DB::table('customplanusers')->insert(
						['user_id' => $userId, 'weekplans_id' => $itemFromBasket->weekplans_id, 'meal_name' => $itemFromBasket->meal_name, 'meal_quantity' => $itemFromBasket->meal_quantity, 'vegcarb' => $itemFromBasket->vegcarb, 'plan_no' => $plan_no, 'noOfDelivery' => $noOfDelivery, 'meal_id' => $itemFromBasket->meal_id]
					);
				}

				DB::table('customtracktemp')->where('temp_user_id', $userId)->delete();
			}

			return redirect('/dashboard');
		} else {
			return redirect('/user-register')->with('idFromBasket', $id);
		}
	}




	public function dataToDashboard()
	{

		//dd(Auth::user()->role_id);

		if(Auth::user()->role_id!=1){

	

		$plan_no = session()->get('plan_no');
		//return $plan_no;
		if (Auth::check()) {
			$user_id=Auth::user()->id;

			if(isset($plan_no)){
				DB::table('setmealdata')->where('plan_no', '=', $plan_no)->update([
					'user_id'=>$user_id
				]);
			}else{
				$plan_no_info=DB::table('setmealdata')->where('user_id', '=', $user_id)->where('is_paid','=',0)->orderBy('id','desc')->first();
				if(isset($plan_no_info)){
					$plan_no=$plan_no_info->plan_no;
				}
				
			}

			//dd($plan_no);
		}
		if(isset($plan_no)){
		$setMealCount = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->get()->count();
		$setMealName = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->first();
		$orderDetail = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->get();
		$extra_snacks_detail = DB::table('extra_snack_plans')->where('plan_no', '=', $plan_no)->get();
		if ($setMealCount == 1) {
			if (isset($plan_no)) {
				$mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->where('set_meal_selected', '=', '1')->first()->vegan_price;
				$extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('extra_price');
				$sub_total = $mealPlanPrice + $extra_meal_price;
				$sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
			} else {
				$sub_total_in_decimal = '0.00';
			}
			$delivery_info = DB::table('productcategories')->where('name', $setMealName->plan_name)->get();
		}
		 else if($setMealCount == 0) {
			if (isset($plan_no)) {
				// $mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->first()->vegan_price;
				$extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('total_price');
				$sub_total = $extra_meal_price;
				$sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
				if($extra_meal_price<>0){
					$delivery_info = DB::table('productcategories')->where('name', 'Fitreats')->get();
				}
				else{
					$delivery_info='';
				}
				
			} else {
				$sub_total_in_decimal = '0.00';
			}

		
			//$delivery_info ='';
	
		}

		//dd($orderDetail);

		return view('admin-panel/Dashboard', compact('orderDetail', 'extra_snacks_detail', 'sub_total_in_decimal', 'delivery_info', 'setMealCount', 'plan_no'));
	}
	else if(!isset($plan_no)){
		//dd('hello');
		return view('admin-panel/Dashboard');
	}

}
else{
	return view('admin-panel/Dashboard');
}

		//return view('admin-panel/Dashboard', compact('orderDetail', 'extra_snacks_detail', 'sub_total_in_decimal', 'delivery_info', 'setMealCount','plan_no'));
	}


	public function customEdit($userId, $planNo)
	{

		$foodItems = DB::table('customplanusers')->where([
			['user_id', $userId],
			['plan_no', $planNo],
		])->get();

		$count_quantity = DB::table('customplanusers')->where([
			['user_id', $userId],
			['plan_no', $planNo],
		])->sum('meal_quantity');

		$basket_item_count = DB::table('customplanusers')->where([
			['user_id', $userId],
			['plan_no', $planNo]
		])->count();

		session()->put('food_item', $foodItems);
		session()->put('count_quantity', $count_quantity);
		session()->put('basket_item_count', $basket_item_count);

		$categoryData = DB::table('productcategories')->get();

		$vegCarbId = DB::table('productcategories')->where('name', 'VegCarb')->first();

		// return($vegCarbId);

		$vegCarbProducts = DB::table('products')->where(
			'cat_id',
			$vegCarbId->id
		)->get();
		return view('admin-panel.customplan.custom_edit', compact('categoryData', 'vegCarbProducts', 'planNo', 'userId'));
	}


	public function updateEditBasketData(Request $request, $user_id, $planNo, $meal, $vegcarb, $sn)
	{
		$inputId = 'count_edit' . $sn;
		$checkInput = $request->$inputId;
		if ($checkInput == 0) {
			DB::table('customplanusers')->where([
				['user_id', $user_id],
				['plan_no', $planNo],
				['meal_name', $meal],
				['vegcarb', $vegcarb],
			])
				->delete();
		} else {
			DB::table('customplanusers')->where([
				['user_id', $user_id],
				['plan_no', $planNo],
				['meal_name', $meal],
				['vegcarb', $vegcarb],
			])
				->update(['meal_quantity' => $request->$inputId]);
		}
	}
	public function readEditData($userId, $planNo)
	{
		$total_count_quantity = DB::table('customplanusers')->where([
			['user_id', $userId],
			['plan_no', $planNo],
		])->sum('meal_quantity');
		return response($total_count_quantity);
	}
	public function addToEditBasket(Request $request)
	{

		$food_item = $request->food_item;
		$count_quantity = $request->count_quantity;
		$weekplanid = $request->weekplanid;
		$meal_id = $request->meal_id;
		$vegcarbs = $request->select_veg_and_carbs;
		$user_id = Auth::user()->id;
		$plan_no = $request->planNo;

		$is_paid = DB::table('customplanusers')->where([
			['user_id', $user_id],
			['plan_no', $plan_no],
		])->first();

		$all_vegcarb = ' ';
		if ($vegcarbs) {
			foreach ($vegcarbs as $vegcarb) {
				$all_vegcarb = $all_vegcarb . '+' . $vegcarb;
			}
		}
		if ($request->count_quantity) {
			// DB::table('customtracktemp')->truncate();
			$compareFoodItem = DB::table('customplanusers')->where([
				['user_id', $user_id],
				['plan_no', $plan_no],
				['meal_name', $food_item],
				['vegcarb', $all_vegcarb]
			])->exists();
			if ($weekplanid == 1) {
				$noOfDelivery = 8;
			} elseif ($weekplanid == 2) {
				$noOfDelivery = 12;
			} elseif ($weekplanid == 3) {
				$noOfDelivery = 16;
			}
			if ($compareFoodItem) {
				DB::table('customplanusers')->where([
					['user_id', $user_id],
					['plan_no', $plan_no],
					['meal_name', $food_item],
				])
					->update(['meal_quantity' => $count_quantity, 'vegcarb' => $all_vegcarb, 'is_paid' => $is_paid->is_paid]);
			} else {
				DB::table('customplanusers')->insert(
					['user_id' => $user_id, 'weekplans_id' => $weekplanid, 'meal_name' => $food_item, 'meal_quantity' => $count_quantity, 'vegcarb' => $all_vegcarb, 'plan_no' => $plan_no, 'noOfDelivery' => $noOfDelivery, 'meal_id' => $meal_id, 'is_paid' => $is_paid->is_paid]
				);
			}
		}
		return redirect()->back();
	}


	public function customDelete($userId, $planNo)
	{
		DB::table('customplanusers')->where([
			['user_id', $userId],
			['plan_no', $planNo],
		])->delete();

		DB::table('customplanusers')->where([
			['user_id', $userId],
			['plan_no', '>', $planNo],
		])->decrement('plan_no');

		return redirect('/dashboard');
	}

	public function setDelete($id)
	{
		session()->forget('coupon');
		session()->forget('coupon_type');
		DB::table('setmealdata')->where('id', $id)->delete();
		return redirect('/dashboard');
	}

	public function customEditEmail($order_id, $plan_no)
	{

		$customActive = DB::table('customplanusers')->where([
			['user_id', $order_id],
			['plan_no', $plan_no],
			['is_paid', 1]
		])->exists();

		if ($customActive) {
			$customInformation = DB::table('customplanusers')->where([
				['user_id', $order_id],
				['plan_no', $plan_no]
			])->get();

			$customInformationCount = DB::table('customplanusers')->where([
				['user_id', $order_id],
				['plan_no', $plan_no]
			])->count();


			$userInfo = DB::table('users')->where('id', $order_id)->first();

			$txt1 = '<html>
		<head>
			<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
			<style type="text/css">
				body{
					font-family: \'Roboto\', sans-serif;
					padding: 0;
					margin: 0;
				}
        #wrapper{
				width: 100%;
				max-width: 600px;
				margin: 0 auto;
				box-shadow: 0px 3px 6px #000;
				border: 1px solid #ccc;
			}

        #header{

			width: 100%;
			height: 155px;
			padding: 20px 0 7px;
		}

        #header h4 {
		color: #000000;
		font-size: 30px;
		font-weight: 500;
		text-transform: uppercase;
	}


        #content h5 {
	color: #666666;
	font-size: 24px;
	font-weight: 700;
}

        #content h6 {
color: #04a7e0;
font-size: 30px;
font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
	text-align: center;
}

.top_30{
	margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}
</style>
</head>
<body>

	<div id="wrapper"  class="text-center">
		<div id="header" style="background: #363636;">

			<img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" style="outline: none;text-decoration: none;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;padding-top: 4px; width:142px;">
		</div><!-- #header -->

		<div id="content">
			<h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received</h2>
			<h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Attention <b>Rapid Results Gourmet</b></h2>
			<p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
				The Following order has been made </p>
				<table style="margin: 0 auto;text-align: left;">
				<tr>
						<td><b>Custom Order Number</b></td>
						<td style="padding-left: 20px;">
							' . $customInformation[0]->user_id . '/' . $customInformation[0]->plan_no . '
						</td>
						</tr>
				';

			$customInfo = '';
			for ($i = 0; $i < $customInformationCount; $i++) {
				$veg = '';

				foreach (explode('+', $customInformation[$i]->vegcarb) as $key => $vegcarb) {
					if ($key == 0) {
						continue;
					} elseif ($key == 1) {
						$veg = $vegcarb;
						continue;
					}
					$veg = $veg . ' , ' . $vegcarb;
				}
				$customInfo .= '
						<tr>
						<td><b>Custom Orders</b></td>
						<td style="padding-left: 20px;">
							' . $customInformation[$i]->meal_name . '
						</td>
						<td style="padding-left: 20px;">
							' . $customInformation[$i]->meal_quantity . '
						</td>
						<td style="padding-left: 20px;">
							' . $veg . '
						</td>
					</tr>';
			}

			$txt2 =	'<tr><td><b>Customer Name</b></td><td style="
									padding-left: 20px;">' . $userInfo->name . '</td></tr>
				<tr><td><b>Customer Phone Number</b></td><td style="
					padding-left: 20px;">' . $userInfo->phone_number . '</td></tr>
					<tr><td><b>Customer Address</b></td><td style="
						padding-left: 20px;">' . $userInfo->address . ' ' . $userInfo->addressline1 . ' ' . $userInfo->addressline2 . ' ' . $userInfo->postcode . '</td></tr>
						<tr><td><b>Customer Email</b></td><td style="
							padding-left: 20px;">' . $userInfo->email . '</td></tr>

						</table>

					</div><!-- #content -->
					<div id="footer">

						<img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" alt="" />

						<p>7 RAILWAY STREET GRAVESEND, DA11 9DU <br />
							<span id="footeremail" style="color:#fff;">info@rapidresultsgourmet.co.uk</span> | +44 7927 393481</p>


							<ul id="footer_social_icons" style="padding-left: 0px;">
								<li><a><img src="http://qodebox.com/images/social-facebook.jpg" alt="" /></a></li>
								<li><a><img src="http://qodebox.com/images/social-gplus.jpg" alt="" /></a></li>
								<li><a><img src="http://qodebox.com/images/social-twitter.jpg" alt="" /></a></li>
								<li><a><img src="http://qodebox.com/images/social-linkedin.jpg" alt="" /></a></li>
							</ul>
						</div><!-- #footer -->


					</div><!-- #wrapper -->
				</body>
				</html>';

			$txt = $txt1 . $customInfo . $txt2;

			$to = 'rajatbhadra.1@gmail.com';
			$subject = "Your RRG Order Summary";

			$headers = "From:" . strip_tags('Rapid Results Gourmet') . "\r\n";
			$headers .= "CC: rajatbhadra.1@gmail.com\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


			// $headers = "From: " . strip_tags($_POST['req-email']) . "\r\n";$headers .= "CC: susan@example.com\r\n";
			// $headers .= "MIME-Version: 1.0\r\n";
			// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


			mail($to, $subject, $txt, $headers);

			$txt1 = '<html>
		<head>
			<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
			<style type="text/css">
				body{
					font-family: \'Roboto\', sans-serif;
					padding: 0;
					margin: 0;
				}
        #wrapper{
				width: 100%;
				max-width: 600px;
				margin: 0 auto;
				box-shadow: 0px 3px 6px #000;
				border: 1px solid #ccc;
			}

        #header{

			width: 100%;
			height: 155px;
			padding: 20px 0 7px;
		}

        #header h4 {
		color: #000000;
		font-size: 30px;
		font-weight: 500;
		text-transform: uppercase;
	}


        #content h5 {
	color: #666666;
	font-size: 24px;
	font-weight: 700;
}

        #content h6 {
color: #04a7e0;
font-size: 30px;
font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
	text-align: center;
}

.top_30{
	margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}
</style>
</head>
<body>

	<div id="wrapper"  class="text-center">
		<div id="header" style="background: #363636;">

			<img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" style="outline: none;text-decoration: none;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;padding-top: 4px; width:142px;">
		</div><!-- #header -->

		<div id="content">
			<h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received</h2>
			<h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Hello <b>' . $userInfo->name . '</b></h2>
			<p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
				The Following order has been made </p>
				<table style="margin: 0 auto;text-align: left;">
				<tr>
						<td><b>Custom Order Number</b></td>
						<td style="padding-left: 20px;">
							' . $customInformation[0]->user_id . '/' . $customInformation[0]->plan_no . '
						</td>
						</tr>
				';
			$customInfo = '';
			for ($i = 0; $i < $customInformationCount; $i++) {
				$veg = '';

				foreach (explode('+', $customInformation[$i]->vegcarb) as $key => $vegcarb) {
					if ($key == 0) {
						continue;
					} elseif ($key == 1) {
						$veg = $vegcarb;
						continue;
					}
					$veg = $veg . ' , ' . $vegcarb;
				}
				$customInfo .= '
						<tr>
						<td><b>Custom Orders</b></td>
						<td style="padding-left: 20px;">
							' . $customInformation[$i]->meal_name . '
						</td>
						<td style="padding-left: 20px;">
							' . $customInformation[$i]->meal_quantity . '
						</td>
						<td style="padding-left: 20px;">
							' . $veg . '
						</td>
					</tr>';
			}

			$txt2 =	'<tr><td><b>Customer Name</b></td><td style="
										padding-left: 20px;">' . $userInfo->name . '</td></tr>
										<tr><td><b>Customer Phone Number</b></td><td style="
											padding-left: 20px;">' . $userInfo->phone_number . '</td></tr>
											<tr><td><b>Customer Address</b></td><td style="
												padding-left: 20px;">' . $userInfo->address . ' ' . $userInfo->addressline1 . ' ' . $userInfo->addressline2 . ' ' . $userInfo->postcode . '</td></tr>
												<tr><td><b>Customer Email</b></td><td style="
													padding-left: 20px;">' . $userInfo->email . '</td></tr>

												</table>

											</div><!-- #content -->
											<div id="footer">

												<img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" alt="" />

												<p>7 RAILWAY STREET GRAVESEND, DA11 9DU <br />
													<span id="footeremail" style="color:#fff;">info@rapidresultsgourmet.co.uk</span> | +44 7927 393481</p>


													<ul id="footer_social_icons" style="padding-left: 0px;">
														<li><a><img src="http://qodebox.com/images/social-facebook.jpg" alt="" /></a></li>
														<li><a><img src="http://qodebox.com/images/social-gplus.jpg" alt="" /></a></li>
														<li><a><img src="http://qodebox.com/images/social-twitter.jpg" alt="" /></a></li>
														<li><a><img src="http://qodebox.com/images/social-linkedin.jpg" alt="" /></a></li>
													</ul>
												</div><!-- #footer -->


											</div><!-- #wrapper -->
										</body>
										</html>
										';
			$txt = $txt1 . $customInfo . $txt2;

			$userInfo = DB::table('users')->where('id', $order_id)->first();
			$toUser = $userInfo->email;

			mail($toUser, $subject, $txt, $headers);
		}

		return redirect('/home');
	}
}
