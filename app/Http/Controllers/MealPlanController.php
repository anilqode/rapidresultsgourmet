<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MealPlanController extends Controller
{
    public function index() {
    	$meals = DB::table('meals')->get();
    	return view('admin-panel.admin-meal.index', compact('meals'));
    }

    public function create() {

    	return view('admin-panel.admin-meal.create');
    }

    public function store(Request $request) {
    	
    	$this->validate($request, [
            'meal_plan' => 'required',
      ]);


    	DB::table('meals')
            ->insert([
                'meal' => $request->meal_plan,
            ]);

    	return redirect()->back()->with('store', 'Your data created successfully');
    }

    public function edit($id) {
    	$meal = DB::table('meals')->where('id', $id)->first();

    	return view('admin-panel.admin-meal.edit', compact('meal'));
    }

    public function update(Request $request, $id) {
    	$this->validate($request, [
            'meal_plan' => 'required',
      ]);


	DB::table('meals')->where('id', $id)
        ->update([
            'meal' => $request->meal_plan,
        ]);

    	return redirect()->back()->with('update', 'Your data updated successfully');
    }

    public function delete($id) {
    	
    	DB::table('meals')->where('id', $id)->delete();
    	return redirect()->back()->with('delete', 'Your data deleted successfully!');
    }
}
