<?php

namespace App\Http\Controllers;
use App\User;
use App\Main;
use App\Post;
use App\Slider;
use App\Testinomial;
use Harimayco\Menu\Facades\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use GuzzleHttp\Client;

class MainController extends Controller
{
    public function index()
    {
        $menuList = Menu::getByName('top_menu');

        /*return $menuList;*/

        $post=Post::latest()->get();
        $postt=Post::get();
        $record=Testinomial::get();
        $slider=Slider::get();
        $first=DB::table('testinomials')->select('id')->first()->id;
 /*       return $description_express;*/

        return view('/welcome2',compact('post','postt','menuList','record','first','slider'  ));
    }

    public function enrolnow($id)
    {
/*        return $_SERVER['REQUEST_URI'];*/
        $express=Fee::where('id','1')->first();
        return view('payment.client_details_redirect',compact('id','express'));

    }
    public function enrolnowstore(Request $request)
    {
 /* return $_SERVER['REQUEST_URI'];*/

    /* return $request->all();*/
        $email=$request->email;
        $phone=$request->phone;
        $address=$request->address;
        $enroll_id=$request->enroll_id;
        $rand_id='progressive'.uniqid();

      $record=DB::table('enrolls')->where(['email'=>$email,'phone_number'=>$phone,'enroll_id'=>$enroll_id])->get();
      if(count($record)<=0)
      {
         DB::table('enrolls')->insert([
             'order_id'=>$rand_id,
             'enroll_id'=>$enroll_id,
             'email'=>$email,
             'address'=>$address,
             'phone_number'=>$phone,
             'status'=>0,
         ]);
      }
      else{
          return redirect('/enrol-now/'.$enroll_id);

      }


        $express=Fee::where('id',$enroll_id)->first();

      return view('payment.forward_payment_redirect',compact('enroll_id','express'));
    }

    public function blog()
    {
        $first=DB::table('testinomials')->select('id')->first()->id;
        $record=Testinomial::get();
        $post = DB::table('posts')->orderBy('id', 'DSC')->paginate(7);

        /*$url = "https://api.instagram.com/v1/users/self/media/recent/?access_token=5810652274.7d8cd37.9b2f73656c2c47378ac704653e78cdd8";

        $client = new Client();

        $response = $client->request(
            'GET',
            $url
        );

        $insta_data = json_decode($response->getBody());

        $insta_info=$insta_data->data; */
        // changes made by anil 
   
        $insta_info=""; 
        
        return view('layouts.blog.BlogRedirect', ['post' => $post],compact('first','record','insta_info'));
    }

    public function showmenu()
    {
//  $menuList = Menu::get(1);
        $menuList = Menu::getByName('top_menu');

/*return $menuList;*/
        return view('header',compact('menuList'));

    }

    public function menudisplay()
    {
        $menuList = Menu::getByName('top_menu');
        return view('admin-panel.ourmenu',compact('menuList'));
    }


    public function checkoutindex() {
        $id = $_GET['id'];
        $enroll_id = $_GET['enroll_id'];
        $amount = $_GET['amount'];
        $description = $_GET['description'];
        $email=$_GET['email'];
        $phone=$_GET['phone'];
        $address=$_GET['address'];
        $successIndicator = $_GET['successIndicator'];
        $record_exists=User::where('email',$email)->count();

        if($record_exists==0)
        {
            User::create([
                'role_id'=>2,
                'name'=>'null',
                'email'=>  $email,
                'phone_number'=>$phone,
                'address'=>$address,
               'api_token' => 1,
                'image_path'=>'/images/3/blank-profile-picture-973460_960_720.png',
                'password'=>bcrypt($phone),
            ]);
        }



        Enroll::create([
           'user_id'=>$id,
            'phone_number'=>$phone,
            'email'=>$email,
            'address'=>$address,
           'description'=> $description,
            'enroll_id'=>$enroll_id,
            'amount'=>$amount,
            'session_indicator'=>$successIndicator

        ]);



        return response()->json(['message' => 'click to proceed your payment']);
    }
    public function checkout($id)

    {

        $express=Fee::where('id',$id)->first();

            return view('checkout.checkout_main',compact('express'));

    }

    public function sendmail()
    {
        $resultIndicator='9a5e030f75294be1';


        $record=DB::table('enrolls')->select('email','enrolls.phone_number','address','enrolls.amount','enrolls.description','enrolls.status','course')->where('session_indicator', $resultIndicator)->leftjoin('fees','fees.id','enrolls.enroll_id')->first();
        $data= array(
            'email' => $record->email,
            'emailing' => $record->emailing,
            'subject' =>'Fee Payment',
            'courses'=>$record->course,
            'status'=>$record->status,
            'address' => $record->address,
            'phone' => $record->phone_number,
            'amount'=>$record->amount,
            'bodyMsg' => $record->description
        );


        Mail::send('emails.payments',$data, function($message) use ($data){
            $message->from('mail@progressivenaati.com.au');
            $message->to([$data['email'],'emailing'])->cc('sangroula2@gmail.com');
           
            $message->subject($data['subject']);
        });
return 'sucess';
        $to = "sumanmali1167@gmail.com";

        $subject = "My subject";

        $txt = "Hello world!";
        $headers = "From: mail@progressivenaati.com.au" . "\r\n" .
            "CC: sangroula2@gmail.com";

        mail($to,$subject,$txt,$headers);
        return 'true';

    }




    public function checkoutupdate(){
        $resultIndicator = $_GET['resultIndicator'];
            $update = DB::table('enrolls')->where('session_indicator', $resultIndicator)->update(['status' => 1]);
        $record=DB::table('enrolls')->select('email','enrolls.phone_number','address','enrolls.amount','enrolls.description','enrolls.status','course')->where('session_indicator', $resultIndicator)->leftjoin('fees','fees.id','enrolls.enroll_id')->first();
        $data= array(
            'email' => $record->email,
           'emailing' => $record->emailing,
            'subject' =>'Fee Payment',
            'courses'=>$record->course,
            'status'=>$record->status,
            'address' => $record->address,
            'phone' => $record->phone_number,
            'amount'=>$record->amount,
            'bodyMsg' => $record->description
        );



   
        Mail::send('emails.payments',$data, function($message) use ($data){
            $message->from('info@progressivenaati.com.au');
            $message->to($data['email'])->cc(['sangroula2@gmail.com']);
            $message->subject($data['subject']);
        });
       
    

            return response()->json(['message' => $update]);

    }


}
