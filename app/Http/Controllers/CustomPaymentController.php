<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
/** All Paypal Details class **/
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Illuminate\Support\Facades\DB;
use Redirect;
use Session;
use URL;
use Carbon\Carbon;

class CustomPaymentController extends Controller
{

    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithPaypal(Request $request){

        session()->put('order_id',$request->order_id);
        session()->put('plan_no',$request->plan_no);
        session()->put('custom_price',$request->amount);
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName($request->vegan_title) /** item name **/
        ->setCurrency('GBP')
        ->setQuantity(1)
        ->setPrice($request->amount); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('GBP')
        ->setTotal($request->amount);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setItemList($item_list)
        ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('customstatus')) /** Specify return URL **/
        ->setCancelUrl(URL::route('customstatus'));
        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                \Session::put('error', 'Connection timeout');
                return Redirect::route('dashboard');
            }
            else {
                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::route('dashboard');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
        return Redirect::route('dashboard');
    }



    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            \Session::put('error', 'Payment failed');
            return Redirect::route('dashboard');
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        if ($result->getState() == 'approved') {
            \Session::put('success', 'Payment success');
            $order_id=session::get('order_id');
            $plan_no=session::get('plan_no');
            
            $custom_price=session::get('custom_price');
                   // return Redirect::route('add_to_checkout/'.);

            $this->updateCustomMealOrder($order_id, $plan_no);

            $this->sendEmail($order_id, $plan_no, $custom_price);

            $this->sendEmailToCustomer($order_id, $plan_no, $custom_price);

            return redirect('dashboard')->with('order_id',$order_id);
        }
        \Session::put('error', 'Payment failed');
        return Redirect::route('dashboard');
    }

    public function updateCustomMealOrder($order_id, $plan_no){
        $customInfo = DB::table('customplanusers')->where([
                        ['user_id',$order_id],
                        ['plan_no',$plan_no]
                        ])->first();

        if($customInfo->weekplans_id == 1) {
            $weeks = 4;
        } elseif($customInfo->weekplans_id == 2) {
            $weeks = 8;
        } else {
            $weeks = 12;
        }

        $updated=DB::table('customplanusers')->where([
                        ['user_id',$order_id],
                        ['plan_no',$plan_no]
                        ])->update(['is_paid'=>1, 'created_at'=>Carbon::now()->addWeeks($weeks)]);
        return $updated;
    }

    public function sendEmail($order_id, $plan_no, $custom_price){

        $customInformation = DB::table('customplanusers')->where([
                        ['user_id',$order_id],
                        ['plan_no',$plan_no]
                        ])->get();

        $customInformationCount = DB::table('customplanusers')->where([
                        ['user_id',$order_id],
                        ['plan_no',$plan_no]
                        ])->count();


        $userInfo = DB::table('users')->where('id', $order_id)->first();


        $txt1 = '<html>
        <head>
            <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
            <style type="text/css">
                body{
                    font-family: \'Roboto\', sans-serif;
                    padding: 0;
                    margin: 0;
                }
        #wrapper{
                width: 100%;
                max-width: 600px;
                margin: 0 auto;
                box-shadow: 0px 3px 6px #000;
                border: 1px solid #ccc;
            }

        #header{

            width: 100%;
            height: 155px;
            padding: 20px 0 7px;
        }

        #header h4 {
        color: #000000;
        font-size: 30px;
        font-weight: 500;
        text-transform: uppercase;
    }


        #content h5 {
    color: #666666;
    font-size: 24px;
    font-weight: 700;
}

        #content h6 {
color: #04a7e0;
font-size: 30px;
font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
    text-align: center;
}

.top_30{
    margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}
</style>
</head>
<body>

    <div id="wrapper"  class="text-center">
        <div id="header" style="background: #363636;">

            <img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" style="outline: none;text-decoration: none;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;padding-top: 4px; width:142px;">
        </div><!-- #header -->

        <div id="content">
            <h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received</h2>
            <h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Attention <b>Rapid Results Gourmet</b></h2>
            <p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
                The Following order has been made </p>
                <table style="margin: 0 auto;text-align: left;">
                <tr>
                        <td><b>Custom Order Number</b></td>
                        <td style="padding-left: 20px;">
                            '.$customInformation[0]->user_id.'/'.$customInformation[0]->plan_no.'
                        </td>
                        </tr>
                ';
                $customInfo = '';
                for($i=0; $i < $customInformationCount; $i++){
                    $veg = '';

                    foreach (explode('+', $customInformation[$i]->vegcarb) as $key=>$vegcarb) {
                        if ($key == 0){
                            continue;
                        } elseif($key == 1){
                            $veg = $vegcarb;
                            continue;
                        }
                        $veg = $veg.' , '.$vegcarb;
                    }
                $customInfo .= '
                        <tr>
                        <td><b>Custom Orders</b></td>
                        <td style="padding-left: 20px;">
                            '.$customInformation[$i]->meal_name.'
                        </td>
                        <td style="padding-left: 20px;">
                            '.$customInformation[$i]->meal_quantity.'
                        </td>
                        <td style="padding-left: 20px;">
                            '.$veg.'
                        </td>
                    </tr>';
                    
                }       

                        $txt2 = '<tr><td><b>Price Detail</b></td><td style="
                                        padding-left: 20px;">£'.$custom_price.'</td></tr>
                                <tr><td><b>Customer Name</b></td><td style="
                                        padding-left: 20px;">'.$userInfo->name.'</td></tr>
                                        <tr><td><b>Customer Phone Number</b></td><td style="
                                            padding-left: 20px;">'.$userInfo->phone_number.'</td></tr>
                                            <tr><td><b>Customer Address</b></td><td style="
                                                padding-left: 20px;">'.$userInfo->address.' '. $userInfo->addressline1. ' '. $userInfo->addressline2. ' '. $userInfo->postcode.'</td></tr>
                                                <tr><td><b>Customer Email</b></td><td style="
                                                    padding-left: 20px;">'.$userInfo->email.'</td></tr>

                                                </table>

                                            </div><!-- #content -->
                                            <div id="footer">

                                                <img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" alt="" />

                                                <p>7 RAILWAY STREET GRAVESEND, DA11 9DU <br />
                                                    <span id="footeremail" style="color:#fff;">info@rapidresultsgourmet.co.uk</span> | +44 7927 393481</p>


                                                    <ul id="footer_social_icons" style="padding-left: 0px;">
                                                        <li><a><img src="http://qodebox.com/images/social-facebook.jpg" alt="" /></a></li>
                                                        <li><a><img src="http://qodebox.com/images/social-gplus.jpg" alt="" /></a></li>
                                                        <li><a><img src="http://qodebox.com/images/social-twitter.jpg" alt="" /></a></li>
                                                        <li><a><img src="http://qodebox.com/images/social-linkedin.jpg" alt="" /></a></li>
                                                    </ul>
                                                </div><!-- #footer -->


                                            </div><!-- #wrapper -->
                                        </body>
                                        </html>
                                        ';
        $txt = $txt1.$customInfo.$txt2;

        $to = "rapidresultsorder@gmail.com";
        $subject = "New Order for RRG";

        $headers = "From:rapidresultsorder@gmail.com\r\n";
	$headers .= "CC: rapidresultsorder@gmail.com\r\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";



                        mail($to,$subject,$txt,$headers);
                    }


public function sendEmailToCustomer($order_id, $plan_no, $custom_price){

        $customInformation = DB::table('customplanusers')->where([
                        ['user_id',$order_id],
                        ['plan_no',$plan_no]
                        ])->get();

        $customInformationCount = DB::table('customplanusers')->where([
                        ['user_id',$order_id],
                        ['plan_no',$plan_no]
                        ])->count();
        
        $userInfo = DB::table('users')->where('id', $order_id)->first();


        $txt1 = '<html>
        <head>
            <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
            <style type="text/css">
                body{
                    font-family: \'Roboto\', sans-serif;
                    padding: 0;
                    margin: 0;
                }
        #wrapper{
                width: 100%;
                max-width: 600px;
                margin: 0 auto;
                box-shadow: 0px 3px 6px #000;
                border: 1px solid #ccc;
            }

        #header{

            width: 100%;
            height: 155px;
            padding: 20px 0 7px;
        }

        #header h4 {
        color: #000000;
        font-size: 30px;
        font-weight: 500;
        text-transform: uppercase;
    }


        #content h5 {
    color: #666666;
    font-size: 24px;
    font-weight: 700;
}

        #content h6 {
color: #04a7e0;
font-size: 30px;
font-weight: 700;
}

        #content p {
color: #666666;
font-family: Roboto;
font-size: 16px;
font-weight: 400;
margin-bottom: 70px;
}

#footeremail a{
color:#fff;
}

        #footer{
background: #363636;
height: 150px;
width: 100%;
padding: 20px 0 22px;
}

        #footer p{
color: #fff;
font-size: 16px;
font-weight: 400;
line-height: 24px;
}

.text-center{
    text-align: center;
}

.top_30{
    margin-top: 30px;
}

        #footer_social_icons li{
display: inline-block;
margin: 0px 3px;
list-style: none;
}
</style>
</head>
<body>

    <div id="wrapper"  class="text-center">
        <div id="header" style="background: #363636;">

            <img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" style="outline: none;text-decoration: none;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;padding-top: 4px; width:142px;">
        </div><!-- #header -->

        <div id="content">
            <h2 style="text-align:center;color: #000000;font-family: Roboto;font-size: 24px;font-weight: 400;margin-top: 50px;margin-bottom: 0px;">Order Received</h2>
            <h2 class="attention" style="color: inherit;font-family: Helvetica, Arial, sans-serif;font-weight: 400;text-align: center;line-height: 1.3;word-wrap: normal;font-size: 14px;margin: 27px 0 10px;padding: 0;" align="center">Hello <b>'.$userInfo->name.'</b></h2>
            <p class="email-content" style="color: #666666;font-family: Helvetica, Arial, sans-serif;font-weight: normal;text-align: center;line-height: 1.3;font-size: 14px;margin: 0 0 10px;padding: 17px 43px;margin-bottom: 27px;" align="center">
                The Following order has been made </p>
                <table style="margin: 0 auto;text-align: left;">
                <tr>
                        <td><b>Custom Order Number</b></td>
                        <td style="padding-left: 20px;">
                            '.$customInformation[0]->user_id.'/'.$customInformation[0]->plan_no.'
                        </td>
                        </tr>
                ';
                $customInfo = '';
                for($i=0; $i < $customInformationCount; $i++){
                    $veg = '';

                    foreach (explode('+', $customInformation[$i]->vegcarb) as $key=>$vegcarb) {
                        if ($key == 0){
                            continue;
                        } elseif($key == 1){
                            $veg = $vegcarb;
                            continue;
                        }
                        $veg = $veg.' , '.$vegcarb;
                    }
                $customInfo .= '
                        <tr>
                        <td><b>Custom Orders</b></td>
                        <td style="padding-left: 20px;">
                            '.$customInformation[$i]->meal_name.'
                        </td>
                        <td style="padding-left: 20px;">
                            '.$customInformation[$i]->meal_quantity.'
                        </td>
                        <td style="padding-left: 20px;">
                            '.$veg.'
                        </td>
                    </tr>';
                    
                }       

                        $txt2 = '<tr><td><b>Price Detail</b></td><td style="
                                        padding-left: 20px;">£'.$custom_price.'</td></tr>
                                        <tr><td><b>Customer Name</b></td><td style="
                                        padding-left: 20px;">'.$userInfo->name.'</td></tr>
                                        <tr><td><b>Customer Phone Number</b></td><td style="
                                            padding-left: 20px;">'.$userInfo->phone_number.'</td></tr>
                                            <tr><td><b>Customer Address</b></td><td style="
                                                padding-left: 20px;">'.$userInfo->address.' '. $userInfo->addressline1. ' '. $userInfo->addressline2. ' '. $userInfo->postcode.'</td></tr>
                                                <tr><td><b>Customer Email</b></td><td style="
                                                    padding-left: 20px;">'.$userInfo->email.'</td></tr>

                                                </table>

                                            </div><!-- #content -->
                                            <div id="footer">

                                                <img src="http://rrg.nepgeeks.com/images/rrgimages/logo.png" alt="" />

                                                <p>7 RAILWAY STREET GRAVESEND, DA11 9DU <br />
                                                    <span id="footeremail" style="color:#fff;">info@rapidresultsgourmet.co.uk</span> | +44 7927 393481</p>


                                                    <ul id="footer_social_icons" style="padding-left: 0px;">
                                                        <li><a><img src="http://qodebox.com/images/social-facebook.jpg" alt="" /></a></li>
                                                        <li><a><img src="http://qodebox.com/images/social-gplus.jpg" alt="" /></a></li>
                                                        <li><a><img src="http://qodebox.com/images/social-twitter.jpg" alt="" /></a></li>
                                                        <li><a><img src="http://qodebox.com/images/social-linkedin.jpg" alt="" /></a></li>
                                                    </ul>
                                                </div><!-- #footer -->


                                            </div><!-- #wrapper -->
                                        </body>
                                        </html>
                                        ';
        $txt = $txt1.$customInfo.$txt2;

        $to = $userInfo->email;
        $subject = "Your RRG Order Summary";

        $headers = "From:rapidresultsorder@gmail.com\r\n";
	$headers .= "CC:".$orderInfo->email ."\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";


        


            mail($to,$subject,$txt,$headers);
        }
}
