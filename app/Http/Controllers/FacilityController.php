<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\MenuUploader;


class FacilityController extends Controller
{
    public function showmeals()
    {
        return view('homepage-pages.show_meal_redirect');

    }
    public function setMealPlan()
    {
      
      $setMealPlanInfo= DB::table('productcategories')
      ->where('meal_plan_type','=','s')->get();
      $menu = MenuUploader::orderBy('id', 'DSC')->first();

      if(!$menu){
        $menu = "No menu is uploaded yet!!!";
      }

      return view('homepage-pages.set_meal_plan_redirect', compact('setMealPlanInfo', 'menu'));
    }

    public function fitreatsAndSupplements(){
        $fitreats = DB::table('productcategories')
      ->select('name','body','price','short_intro','image_path','id')
      ->where('meal_plan_type','=','n')->get();

      $products = DB::table('products')->get();
        return view('homepage-pages.fitreats-and-supplements', compact('fitreats', 'products'));
    }
}