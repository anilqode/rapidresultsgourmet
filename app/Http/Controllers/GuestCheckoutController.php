<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Productcategory;
use App\Cart;
use App\Product;
use App\GuestCheckout;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class GuestCheckoutController extends Controller
{
  
public function guestCheckout()
{
    $plan_no = session()->get('plan_no');

 
    $setMealCount = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->get()->count();
    $setMealName = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->first();
    $orderDetail = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->get();
    $extra_snacks_detail = DB::table('extra_snack_plans')->where('plan_no', '=', $plan_no)->get();
    if ($setMealCount == 1) {
        if (isset($plan_no)) {
            $mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->where('set_meal_selected', '=', '1')->first()->vegan_price;
            $extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('extra_price');
            $sub_total = $mealPlanPrice + $extra_meal_price;
            $sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
        } else {
            $sub_total_in_decimal = '0.00';
        }
        $delivery_info = DB::table('productcategories')->where('name', $setMealName->plan_name)->get();
    } else {
        if (isset($plan_no)) {
            // $mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->first()->vegan_price;
            $extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('total_price');
            $sub_total = $extra_meal_price;
            $sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
        } else {
            $sub_total_in_decimal = '0.00';
        }

       

        $delivery_info = DB::table('productcategories')->where('name', 'Fitreats')->get();

    }

    $couponPercent=session()->get('coupon');
    if(isset($couponPercent)){

       
        $couponAmt= $couponPercent * 0.01 * $sub_total_in_decimal;

        if(DB::table('order_discounts')->where('plan_no',$plan_no)->exists()){

            DB::table('order_discounts')->where('plan_no',$plan_no)->update([
                'amount'=>$couponAmt
            ]);

        }
        else{
            DB::table('order_discounts')->insert(['plan_no'=>$plan_no,
            'amount'=>$couponAmt]); 
        }
    }
 

   
    return view('checkout.guest_checkout',compact('orderDetail', 'extra_snacks_detail', 'sub_total_in_decimal', 'delivery_info', 'setMealCount','plan_no'));
  
}

}
