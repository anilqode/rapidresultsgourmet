<?php

namespace App\Http\Controllers;


use App\Testinomial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userInfo=Auth::User();

        if($userInfo->role_id==2){
            return view('/admin-panel/Dashboard');
        }
        else{
            return view('/user-panel/user-dashboard');
        }

    }

    public function aboutRedirect(){
        return redirect('/about-us');
    }

    public function about()
    {
        $first=DB::table('testinomials')->select('id')->first()->id;
        $record=Testinomial::get();
        return view('homepage-pages.about-redirect',compact('record','first'));
    }

    public function affiliates() {
        return view('homepage-pages.affiliates');
    }

    public function courses()
    {
        $express=Fee::where('id','1')->first();
        $advance=Fee::where('id','3')->first();
        $master=Fee::where('id','4')->first();
        $classroom=Fee::where('id','5')->first();
        $first=DB::table('testinomials')->select('id')->first()->id;
        $record=Testinomial::get();
        $description_express=DB::table('descriptions')->where('fee_id',1)->get();
        $description_advance=DB::table('descriptions')->where('fee_id',3)->get();
        $description_master=DB::table('descriptions')->where('fee_id',4)->get();
        $description_class=DB::table('descriptions')->where('fee_id',5)->get();

        return view('homepage-pages.courses-redirect',compact('description_express','description_class','description_master','description_advance','record','first','express','advance','master','classroom'));
    }
    public function fee()
    {
        $first=DB::table('testinomials')->select('id')->first()->id;
        $record=Testinomial::get();
        return view('homepage-pages.fee-redirect',compact('record','first'));
    }
    public function testinomial()
    {
        $testinomial= DB::table('testinomials')->paginate(6);
        $id = DB::table('testinomials')->select('id')->count();
        return view('homepage-pages.testinomial-redirect',['testinomial' => $testinomial],compact('id'));
    }


}