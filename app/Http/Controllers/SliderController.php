<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('admin-panel.slider.add_slider_redirect');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $slider = new Slider();
        $slider->title = $request['title'];
        $slider->description = $request['description'];
        if(file_exists($request->image_path)){
            $filename = 'slide'.time().'.'.$request->image_path->getClientOriginalExtension();
            $location = public_path('/images/home');
            $request->image_path->move($location, $filename);
            $slider->image_path = $filename;
        }
        else{
            $slider->photo = 'https://static.thenounproject.com/png/17241-200.png';
        }
         $slider->save();
    

            return redirect('/add-slider')->with("success", "Home Slider Created Successfully");
    }





    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function displayslider()
    {
        $record=Slider::all();

        return view('admin-panel.slider.display_slider_redirect',compact('record'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider,$id)
    {
      $record=DB::table('sliders')->where('id',$id)->find($id);
        return view('admin-panel.slider.edit_slider_redirect',compact('record','slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

     /*   return $request->all();*/
        $slider = Slider::findorFail($id);
        $slider->title = $request['title'];
        $slider->description = $request['description'];
        if(file_exists($request->image_path)){
            $filename = 'slide'.time().'.'.$request->image_path->getClientOriginalExtension();
            $location = public_path('/images/home');
            $request->image_path->move($location, $filename);
            $slider->image_path = $filename;
        }
        else{
            $slider->photo = 'https://static.thenounproject.com/png/17241-200.png';
        }
        $slider->save();

        return redirect('/edit/' . $id . '/slider')->with("success", 'Your Slider Has Been Edited .');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function delete(Slider $slider,$id)
    {
        Slider::where('id', $id)->delete();
        return redirect('/display-slider');
    }
}
