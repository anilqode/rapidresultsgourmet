<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SebastianBergmann\Environment\Console;

class OrderController extends Controller
{
    //
    public function displayAllOrders()
    {

        $orderDetails = DB::table('setmealdata')->where('is_paid', '=', 1)
            ->where('setmealdata.user_id', '<>', '0')
            ->select('setmealdata.plan_no as id', 'users.name', 'users.phone_number', 'users.email', 'users.address', 'users.addressline1', 'users.addressline2', 'users.addressline3', 'users.postcode')
            ->join('users', 'users.id', 'setmealdata.user_id')
            ->distinct()
            ->get();

        $guestOrderDetails = DB::table('setmealdata')->where('setmealdata.is_paid', '=', 1)
            ->where('setmealdata.user_id', '=', '0')
            ->select('setmealdata.plan_no as id', 'guest_users.name', 'guest_users.phone_number', 'guest_users.email', 'guest_users.addressline1', 'guest_users.addressline2', 'guest_users.addressline3', 'guest_users.postcode')
            ->join('guest_users', 'guest_users.order_id', 'setmealdata.plan_no')
            ->distinct()
            ->get();

        //dd($guestOrderInfo);

        return view(
            'order-details.admin-order-detail-redirect',
            compact('orderDetails', 'guestOrderDetails')
        );
    }

    public function loadOrderDetail(Request $request)
    {

        //return($request->plan_no);
        if ($request->userType == '0') {
            $orderInfo = DB::table('setmealdata')
                ->leftjoin('guest_users', 'guest_users.order_id', 'setmealdata.plan_no')
                ->where('setmealdata.plan_no', $request->plan_no)
                ->select('setmealdata.id', 'plan_no', 'plan_name', 'vegan', 'extra_snacks', 'total_price', 'complementary_snacks', 'guest_users.name', 'guest_users.addressline3', 'guest_users.addressline1', 'guest_users.addressline2', 'guest_users.postcode', 'guest_users.phone_number', 'guest_users.email', 'guest_users.review')
                ->first();

            // dd($orderInfo);
        } else {
            $orderInfo = DB::table('setmealdata')
                ->leftjoin('users', 'users.id', 'setmealdata.user_id')
                ->where('setmealdata.plan_no', $request->plan_no)
                ->select('setmealdata.id', 'plan_no', 'plan_name', 'vegan', 'extra_snacks', 'total_price', 'complementary_snacks', 'users.name', 'users.addressline3', 'users.addressline1', 'users.addressline2', 'users.postcode', 'users.phone_number', 'users.email', 'review')
                ->first();
        }

        
        $extraSnacksInfo = DB::table('extra_snack_plans')
            ->where('plan_no', $request->plan_no)
            ->select('extra_snack', 'quantity')->get();
        $extra_snacks_set_meal = '';
        foreach ($extraSnacksInfo as $extrainfo) {
            $extra_snacks_set_meal .= $extrainfo->extra_snack . ' Quantity : ' . $extrainfo->quantity . ' | ';
        }

        //dd($extra_snacks_set_meal);


        $fitreatInfo = DB::table('setmealdata')
            ->where('setmealdata.plan_no', $request->plan_no)->where('set_meal_selected', '0')->where('plan_name', 'Fitreats')
            ->select('vegan', 'quantity')->get();


        $fitreats = '';
        foreach ($fitreatInfo as $fitinfo) {
            $fitreats .= $fitinfo->vegan . ' Quantity : ' . $fitinfo->quantity . ' | ';
        }

        //dd($fitreats);

        $complimentary_snacks = $orderInfo->complementary_snacks;
        if (!isset($complimentary_snacks)) {
            $complimentary_snacks = [];
        }
        $extra_snacks = $orderInfo->extra_snacks;

        if (!isset($complimentary_snacks)) {
            $complimentary_snacks = 'not available';
        }

        if (!isset($extra_snacks)) {
            $extra_snacks = 'not available';
        }

        //$this->sendEmail($orderInfo, $fitreats, $extra_snacks_set_meal, $complimentary_snacks, $extra_snacks);

        $html= view('order-details.orderPopupDetail', compact('orderInfo', 'fitreats', 'extra_snacks_set_meal', 'complimentary_snacks', 'extra_snacks'))->render();
   
        $response = response()->json(['success' => true, 'html' => $html]);
        return $response;
   
    }

    public function displayUserOrders($user_id)
    {

        $orderDetails = DB::table('setmealdata')->where('is_paid', '=', 1)
            ->select('setmealdata.plan_no as id')
            ->leftjoin('users', 'users.id', 'setmealdata.user_id')
            ->where('user_id', $user_id)
            ->distinct()
            ->get();

           // return $orderDetails;

        return view(
            'order-details.user-order-detail-redirect',
            compact('orderDetails')
        );
    }

    public function displayFitreatsDetail()
    {

        $fitreats_order_detail = DB::table('fitreats_cart')
            ->leftjoin('users', 'users.id', 'fitreats_cart.user_id')->where('fitreats_cart.is_paid', '=', 1)->select('fitreats_cart.order_id', 'fitreats_cart.orders', 'fitreats_cart.total_price', 'users.name', 'users.address', 'users.addressline1', 'users.addressline2', 'users.postcode', 'users.phone_number', 'users.email')->get();
        // dd($fitreats_order_detail);
        return view('order-details.fitreats-order-details', compact('fitreats_order_detail'));
    }

    public function displayFitreatUserOrders($user_id)
    {

        $fitreats_order_detail = DB::table('fitreats_cart')
            ->leftjoin('users', 'users.id', 'fitreats_cart.user_id')->where('fitreats_cart.is_paid', '=', 1)->select('fitreats_cart.order_id', 'fitreats_cart.orders', 'fitreats_cart.total_price', 'users.name', 'users.address', 'users.addressline1', 'users.addressline2', 'users.postcode', 'users.phone_number', 'users.email')->where('user_id', $user_id)->get();
        // dd($fitreats_order_detail);
        return view('order-details.fitreats-user-order-detail', compact('fitreats_order_detail'));
    }
}
