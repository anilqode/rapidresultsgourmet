<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WeekPlanController extends Controller
{
    public function index() {
    	$weeks = DB::table('weeks')->get();
    	return view('admin-panel.admin-week.index', compact('weeks'));
    }

    public function create() {

    	return view('admin-panel.admin-week.create');
    }

    public function store(Request $request) {
    	
    	$this->validate($request, [
            'week_plan' => 'required',
            'days' => 'required'
      ]);


    	DB::table('weeks')
            ->insert([
                'week' => $request->week_plan,
                'days' =>  $request->days,
            ]);

    	return redirect()->back()->with('store', 'Your data created successfully');
    }

    public function edit($id) {
    	$week = DB::table('weeks')->where('id', $id)->first();

    	return view('admin-panel.admin-week.edit', compact('week'));
    }

    public function update(Request $request, $id) {
    	$this->validate($request, [
            'week_plan' => 'required',
            'days' => 'required'
      ]);


    	DB::table('weeks')->where('id', $id)
            ->update([
                'week' => $request->week_plan,
                'days' =>  $request->days,
            ]);

    	return redirect()->back()->with('update', 'Your data updated successfully');
    }

    public function delete($id) {
    	
    	DB::table('weeks')->where('id', $id)->delete();
    	return redirect()->back()->with('delete', 'Your data deleted successfully!');
    }
}
