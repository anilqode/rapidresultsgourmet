<?php

namespace App\Http\Controllers;

use App\Coupon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupons = Coupon::all();
        return view('admin-panel.coupon.index', compact('coupons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin-panel.coupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Coupon::create($request->all());
        return redirect('/coupon');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);
        return view('admin-panel.coupon.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Coupon::findOrFail($id)->update($request->all());
        return redirect('/coupon');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {

        $coupon = Coupon::where('id',$coupon->id)->delete();
        return redirect('/coupon')->with("success", 'Your Coupon Has Been Deleted.');

    }

    public function applyCoupon(Request $request){
        $coupons = Coupon::all();
        foreach($coupons as $coupon){
            if($coupon->code == $request->coupon){
                if($coupon->used_times < $coupon->time_of_use){
                    if($coupon->status == 1){
                        // $coupon->update([
                        //     'used_times' => $coupon->used_times + 1
                        // ]);
                        session()->put('coupon', $coupon->value);
                        session()->put('coupon_type', $coupon->type);
                        session()->put('coupon_code', $coupon->code);

                        return response()->json(['status' => 1, 'message' => 'Coupon has been successfully applied!']);
                    }
                    return response()->json(['status' => 2, 'message' => 'Inactive Coupon has been used.']);
                }
                return response()->json(['status' => 3, 'message' => 'Coupon has been expired!']);
            }
        }
        return response()->json(['status' => 4, 'message' => 'Coupon is Invalid.']);
    }
}