<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function index()
    {
        return view('layouts.contact');

    }

    public function storing(Request $request)
    {
        $firstNum = (int)$request->firstNum;
        $secondNum = (int)$request->secondNum;
        $result = (int)$request->resultNum;
        $actualResult = $firstNum + $secondNum;
        if($result !== $actualResult){
            return redirect()->back()->with("alert", "You failed on captcha. Please try submitting again!!!");
        }
        else{
        $this->validate($request,[
            'email' => 'required|email']);
            Contact::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
            
                'message' => $request->message
            ]);

        $data= array(
            'email' => $request->email,
            'name' => $request->name,
            'phone_number' => $request->phone_number,
            'body_message' => $request->message
        );
 Mail::send('emails.contact',$data, function($message) use ($data){
 
            $message->from($data['email']);
            $message->to('nepgeeks@gmail.com');            
            $message->subject('New Contact Message for RRG');
       });

    return redirect('/contact')->with("success", " Your Contact Message send Successfully");
	}

}



    public function checked(Request $request)
    {
        Contact::where('id', $request->msgID)
            ->update([

                'checked' =>1,

            ]);
        return redirect('/contact-display');
    }

    public function unchecked(Request $request)
    {

        Contact::where('id', $request->unchkID)
            ->update([

                'checked' =>0,

            ]);
        return redirect('/contact-display');
    }
    public function replied(Request $request)
    {


        Contact::where('id', $request->repID)
            ->update([

                'replied' =>1,

            ]);
        return redirect('/contact-display');
    }
    public function priority(Request $request)
    {





        Contact::where('id', $request->prioID)
            ->update([

                'priority' =>1,

            ]);
        return redirect('/contact-display');
    }

    public function displaycontact()
    {

        $contact_infos = DB::table('contacts')->select('id', 'name', 'email', 'phone_number','subject','message','checked','replied','priority', 'created_at','updated_at')->get();

        return view('auth.contact_info_redirect',compact('contact_infos'));
    }


    }
