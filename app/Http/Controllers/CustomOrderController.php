<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CustomOrderController extends Controller
{
    //
    public function displayAllOrders(){

    	$orderDetails=DB::table('customplanusers')->where('is_paid','=', 1)->get();
        foreach($orderDetails as $orderDetail) {    
            if(Carbon::parse($orderDetail->created_at)->lt(Carbon::now())) {
                DB::table('customplanusers')->where('created_at', $orderDetail->created_at)->delete();
            }   
        }


    	return view('order-details.custom-admin-order-detail-redirect',
    		compact('orderDetails'));
    }

  public function displayUserOrders($user_id){

    	$orderDetails=DB::table('customplanusers')->where([['is_paid','=',1], ['user_id',$user_id]])->get();

    	return view('order-details.custom-user-order-detail-redirect',
    		compact('orderDetails'));
    }

}
