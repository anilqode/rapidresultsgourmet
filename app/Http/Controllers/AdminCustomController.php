<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminCustomController extends Controller
{
    public function index() {
    	$customplans = DB::table('customprices')->get();
    	return view('admin-panel.admin-custom.index', compact('customplans'));
    }

    public function create() {
    	
    	$weeks = DB::table('weeks')->get();
    	$meals = DB::table('meals')->get();

    	return view('admin-panel.admin-custom.create', compact('weeks', 'meals'));
    }

    public function store(Request $request) {
    	
    	$this->validate($request, [
            'week_plan' => 'required',
            'meal_plan' => 'required',
            'price' => 'required',
      ]);


    	DB::table('customprices')
            ->insert([
                'week_id' => $request->week_plan,
                'meal_id' =>  $request->meal_plan,
                'price' => $request->price,
            ]);

    	return redirect()->back()->with('store', 'Your data created successfully');
    }

    public function edit($id) {
    	$customplan = DB::table('customprices')->where('id', $id)->first();
    	$weeks = DB::table('weeks')->get();
    	$meals = DB::table('meals')->get();
    	$week = DB::table('weeks')->where('id', $customplan->week_id)->first();
    	$meal = DB::table('meals')->where('id', $customplan->meal_id)->first();

    	return view('admin-panel.admin-custom.edit', compact('customplan','weeks', 'meals', 'week', 'meal'));
    }

    public function update(Request $request, $id) {
    	$this->validate($request, [
            'week_plan' => 'required',
            'meal_plan' => 'required',
            'price' => 'required',
      	]);


    	DB::table('customprices')->where('id', $id)
            ->update([
                'week_id' => $request->week_plan,
                'meal_id' =>  $request->meal_plan,
                'price' => $request->price,
        ]);

    	return redirect()->back()->with('update', 'Your data updated successfully');
    }

    public function delete($id) {
    	
    	DB::table('customprices')->where('id', $id)->delete();
    	return redirect()->back()->with('delete', 'Your data deleted successfully!');
    }
}
