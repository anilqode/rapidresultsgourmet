<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use App\Productcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\MenuUploader;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Productcategory::pluck('name', 'id');
        $food = DB::table('foods')->pluck('food', 'id');
        $week = DB::table('weeks')->pluck('week', 'id');
        $meals = DB::table('meals')->pluck('meal', 'id');
        return view('admin-panel.product.create_product_redirect', compact('role', 'catag_id', 'status', 'categories', 'food', 'week', 'meals'));
    }

    public function getproductindex()
    {
        $products = Product::all();

        return   view('shop.shop_redirect');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (isset($request->instock)) {
            $instock = $request->instock;
        } else {
            $instock = 0;
        }

        if (isset($request->image)) {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            $featuredImg = $featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            $image_path = $destinationPathFeatured . '/' . $featuredImg;
            //save into the database
        } else {
            $image_path = '/images/3/blank-profile-picture-973460_960_720.png';
        }
        if ($request->price == null) {
            $price = 0;
        } else {
            $price = $request->price;
        }

        Product::create([

            'cat_id' => $request->category_id,
            'title' => $request->title,
            'price' => $price,
            'saleprice' => $request->saleprice,
            'price_per_meal' => $request->price_per_meal,
            'weekly_price' => $request->weekly_price,
            'quantity' => $request->qty,
            'instock' => $instock,
            'food_id' => $request->food_id,
            'week_id' => $request->week_id,
            'meal_id' => $request->meal_id,
            'discount' => $request->discount,
            'description' => $request->body,
            'price_qty_display' => $request->price_qty_display,
            'meta_desc' => $request->meta_description,
            'meta_keyword' => $request->meta_keywords,
            'imagePath' => $image_path,

        ]);

        return redirect('/create-product')->with("success", 'Your Product Has Been Created.');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product = Product::select('products.id', 'productcategories.name', 'title', 'cat_id', 'meals.meal as mealname', 'weeks.week')
            ->leftjoin('productcategories', 'productcategories.id', 'products.cat_id')
            ->leftjoin('meals', 'products.meal_id', 'meals.id')
            ->leftjoin('weeks', 'weeks.id', 'products.week_id')
            ->get();


        return view('admin-panel.product.display_product_redirect', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product, $id, $cat_id)
    {
        
        $product = Product::select('products.id as id', 'productcategories.name as name', 'title', 'cat_id', 'products.price', 'saleprice', 'price_per_meal', 'weekly_price', 'quantity', 'instock', 'discount', 'imagePath', 'description', 'meta_desc', 'meta_keyword','food_id')->where(['products.id' => $id, 'cat_id' => $cat_id])->leftjoin('productcategories', 'productcategories.id', 'products.cat_id')->first();
         return view('admin-panel.product.edit_product_redirect', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, $id)
    {

        if (isset($request->image)) {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            $featuredImg = $featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
            $image_path = $destinationPathFeatured . '/' . $featuredImg;
            //save into the database
        } else {
            $image_path = '/images/3/blank-profile-picture-973460_960_720.png';

            $featuredImg = Product::select('imagePath')->where('id', $id)->first();

            $image_path = $featuredImg['imagePath'];
        }

        if ($request->price == null) {
            $price = 0;
        } else {
            $price = $request->price;
        }

        //return $request->body;
        Product::where('id', $id)->update([
            'cat_id' => $request->cat_id,
            'title' => $request->title,
            'price' => $price,
            'saleprice' => $request->saleprice,
            'price_per_meal' => $request->price_per_meal,
            'weekly_price' => $request->weekly_price,
            'quantity' => $request->quantity,
            'instock' => $request->instock,
            'discount' => $request->discount,
            'description' => $request->body,
            'price_qty_display' => $request->price_qty_display,
            'meta_desc' => $request->meta_desc,
            'meta_keyword' => $request->meta_keyword,
            'imagePath' => $image_path,

        ]);

        return redirect('/edit/' . $id . '/product/' . $request->cat_id)->with("success", 'Your Product Has Been Created.');
    }
    public function shop()
    {
        $product = Product::get();

        return view('admin-panel.shop.shop_product_redirect', compact('product'));
    }
    public function getAddToCart(Request $request, $id)
    {

        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id, $request->qtty);

        Session::put('cart', $cart);
    }

    public function getCart()
    {
        if (!Session::has('cart')) {
            return view('admin-panel.shop.shopping_cart_redirect');
        }
        $oldCart = Session::get('cart');

        $cart = new Cart($oldCart);

        return view('admin-panel.shop.shopping_cart_redirect', ['product' => $cart->items, 'totalPrice' => $cart->totalPrice]);
    }

    public function deleteCart($id)
    {

        $product = Session::get('cart');
        return $product;
        Session::remove($product->items[$id]);

        return redirect()->back();
    }

    /**
     * Show the Custom Product and shop it
     */
    public function weightPlan($cat_id)
    {

        return view('admin-panel.weightplan.week_schedule_redirect', compact('cat_id'));
    }

    public function getAddVeganToCheckout($id)
    {


        session()->put('vegan_id', $id);
        $vegan_id = session()->get('vegan_id');


        $vegan = Product::where('id', $vegan_id)->first();

        $snacks = Product::where(['cat_id' => $vegan->cat_id, 'food_id' => 4])->get();
        //return $snacks;
        $cat_id = session()->get('prod_cat_id');
        $fitreatss = Productcategory::where('id', $cat_id)->first()->fit;

        session()->put('fitt', $fitreatss);

        return view('admin-panel.weightplan.free_snacks_paln_redirect', compact('vegan', 'vegan_id', 'snacks', 'fitreatss'));
    }

    public function getReviewOrder($id)
    {

        session()->put('freesnacks_id', $id);

        $cat_id = session()->get('prod_cat_id');
        // if($cat_id==14||$cat_id==15){

        if (session()->get('first_comp') == 0)
            session()->put('vegan_id', $id);
        // }

        $vegan_id = session()->get('vegan_id');
        // dd($vegan_id);
        $vegan = Product::where('id', $vegan_id)->first();

        $freesnacks = Product::where('id', $id)->first();


        $fitreatss = Productcategory::where('id', $cat_id)->first()->fit;

        session()->put('fitt', $fitreatss);

        $productCat = Productcategory::where('id', $cat_id)->first();

        return view('admin-panel.weightplan.review_order_paln_redirect', compact('vegan', 'freesnacks', 'fitreatss', 'productCat'));
    }

    public function getReviewOrderExcludingFirstComplimentary($id)
    {

        // session()->put('freesnacks_id',$id);

        $cat_id = session()->get('prod_cat_id');
        if ($cat_id == 14 || $cat_id == 15) {
            session()->put('vegan_id', $id);
        }

        $vegan_id = session()->get('vegan_id');
        $vegan = Product::where('id', $vegan_id)->first();

        // $freesnacks = Product::where('id', $id)->first();

        return view('admin-panel.weightplan.review_order_without_first_complimentary', compact('vegan'));
    }

    public function StoreReviewOrder(Request $request)
    {
        session()->put('review', $request->message);

        $cat_id = session()->get('prod_cat_id');



        $paid_dess = Productcategory::where('id', $cat_id)->first();
        $paid_dess_valu = $paid_dess->paid_dess;

        session()->put('second_comp', $paid_dess);


        if ($paid_dess_valu == 1) {
            return redirect('/add_free_snacks_to_checkout');
        } else {

            $vegan_id = Session::get('vegan_id');
            $vegan = Product::where('id', $vegan_id)->first();

            $price = 0;

            $cat_id = session()->get('prod_cat_id');

            $delivery_info = DB::table('productcategories')->where('id', $cat_id)->select('deliveries', 'delivery_rate', 'total_delivery_cost')->get();

            $freesnacks_id = session()->get('freesnacks_id');
            $freesnacks = Product::where('id', $freesnacks_id)->first();
            /* dd($freesnacks);*/

            $fitreatss = session()->get('fitt');
            /*dd($fitreats);*/
            return view('admin-panel.weightplan.add_to_checkout_package_without_paid_dessert_redirect', compact('vegan', 'cat_id', 'delivery_info', 'freesnacks', 'fitreatss'));
        }
    }
    public function getStoreReviewOrder()
    {

        $review = session()->get('review');
        return redirect('/add_free_snacks_to_checkout');
    }
    public function addSnacksToCheckout($id)
    {

        session()->put('vegan_id', $id);
        $cat_id = session()->get('prod_cat_id');

        $paid_dess = Productcategory::where('id', $cat_id)->first();
        $paid_dess_valu = $paid_dess->paid_dess;

        session()->put('second_comp', $paid_dess);

        $vegan_id = Session::get('vegan_id');
        $vegan = Product::where('id', $vegan_id)->first();

        $price = 0;

        $cat_id = session()->get('prod_cat_id');

        $delivery_info = DB::table('productcategories')->where('id', $cat_id)->select('deliveries', 'delivery_rate', 'total_delivery_cost')->get();

        $freesnacks_id = session()->get('freesnacks_id');
        $freesnacks = Product::where('id', $freesnacks_id)->first();
        /* dd($freesnacks);*/

        $fitreatss = session()->get('fitt');

        $freesnacks_id = session()->get('freesnacks_id');

        //return $freesnacks_id;

        $vegan_id = session()->get('vegan_id');

        $vegan = Product::where('id', $vegan_id)->first();
        $freesnacks = Product::where('id', $freesnacks_id)->first();
        /* $freesnacks= session()->put('freesnacks');*/
        /* dd($freesnacks);*/
        $snacks = Product::where(['cat_id' => $cat_id, 'food_id' => 5])->get();

        $plan = Productcategory::where('id', session()->get('prod_cat_id'))->first();
        $product = session()->get('freesnacks_id');

        return view('admin-panel.weightplan.extra_snacks_paln_redirect', compact('vegan', 'vegan_id', 'freesnacks_id', 'snacks', 'freesnacks', 'plan', 'product'));
    }

    public function deleteSetMeal($id)
    {
        DB::table('extra_snack_plans')->where('set_meal_id', $id)->delete();
        $result = DB::table('setmealdata')->where('id', $id)->delete();
        return $result;
    }

    public function getAddExtraSnacksToCheckout()
    {
        $freesnacks_id = session()->get('freesnacks_id');
        $vegan_id = session()->get('vegan_id');
        $extrasnacks_id = session()->get('extrasnacks_id');
        $vegan = Product::where('id', $vegan_id)->first();
        $freesnacks = Product::where('id', $freesnacks_id)->first();
        $gournment = Product::where(['cat_id' => $vegan->cat_id, 'week_id' => $vegan->week_id, 'food_id' => 3])->get();

        return view('admin-panel.weightplan.add_gournment_redirect', compact('vegan_id', 'freesnacks', 'vegan', 'freesnacks_id', 'extrasnacks_id', 'gournment'));
    }

    // public function storeDeliveryInfo() {
    //   return redirect()->route('paypalCheckout');
    // }


    public function redirectToRegister()
    {

        $freesnacks_id = session()->get('freesnacks_id');



        $vegan_id = session()->get('vegan_id');
        $extrasnacks_id = session()->get('extrasnacks_id');

        $cat_id = session()->get('prod_cat_id');

        $delivery_charge_info = DB::table('productcategories')->select('total_delivery_cost')->where('id', $cat_id)->first();

        $delivery_charge = $delivery_charge_info->total_delivery_cost;


        // return $extrasnacks_id;

        $gournment_id = session()->get('gournment_id');

        $vegan = Product::where('id', $vegan_id)->first();

        $freesnacks = Product::where('id', $freesnacks_id)->first();

        if (session()->get('first_comp') == 0) {
            $freesnackstitle = '';
        } else {
            $freesnackstitle = '';
            $freesnacks = Product::where('id', $freesnacks_id)->first();
            $freesnackstitle = $freesnacks->title;
        }


        $price = 0;
        $gourmentsnacks = '';
        if ($gournment_id) {
            foreach ($gournment_id as $id) {
                $gournment[] = \App\Product::where('id', $id)->first();
            }
            foreach ($gournment as $grou) {
                $price = $price + $grou->price;
                $gourmentsnacks = $gourmentsnacks . '+' . $grou->title;
            }
        }

        $tol_price = $price;

        $sum = 0;

        $paid_dess = Productcategory::where('id', $cat_id)->first()->paid_dess;
        session()->put('second_comp', $paid_dess);
        if (session()->get('second_comp') == 0) {
            $extra_snacks = '';
            /*dd('hello suresh');*/
        } else {
            $extra_snacks = '';
            if ($extrasnacks_id) {
                foreach ($extrasnacks_id as $snacks_id) {
                    $extrasnacks[] = \App\Product::where('id', $snacks_id)->first();
                }

                foreach ($extrasnacks as $extra) {
                    $sum = $sum + $extra->price;
                    $extra_snacks = $extra_snacks . '+' . $extra->title;
                }
            }
        }
        $tol_extra_snacks_price = $sum;



        $totalAmount = $vegan->price + $tol_extra_snacks_price + $tol_price + $delivery_charge;

        $mealTitleInfo = DB::table('productcategories')->select('name')
            ->where('id', '=', $vegan->cat_id)->first();

        $mealTitle = $mealTitleInfo->name;

        //setting session values
        $review = session()->get('review');
        $plan_name_reg = session()->put('mealTitle', $mealTitle);
        $vegan_reg = session()->put('vegan', $vegan->title);
        $vegan_price_reg = session()->put('vegan_price', $vegan->price);
        $complementary_snacks_reg = session()->put('complementary_snacks', $freesnackstitle);
        $extra_snacks_reg = session()->put('extra_snacks', $extra_snacks);
        $extra_price_reg = session()->put('extra_price', $tol_extra_snacks_price);
        $gourment_snacks_reg = session()->put('gourment_snacks', $gourmentsnacks);
        $gourment_price_reg = session()->put('gourment_price', $tol_price);
        $total_price_reg = session()->put('total_price', $totalAmount);
        //  $plan_no_reg=session()->put('plan_no',$plan_no);
        $review_reg = session()->put('review', $review);
        $total_delivery_cost_reg = session()->put('total_delivery_cost', $delivery_charge);




        if (Auth::user()) {

            //dd('hi anil00');
            $userId = $userId = Auth::user()->id;

            $review = session()->get('review');
            $planNoExist = DB::table('setmealdata')->where('user_id', $userId)->exists();
            //   return $planNoExist;
            $plan_no = 1;
            if ($planNoExist) {
                $last_plan_no = DB::table('setmealdata')->orderBy('plan_no', 'desc')->first();

                $plan_no += $last_plan_no->plan_no;
            }


            DB::table('setmealdata')->insert([
                'user_id' => $userId, 'plan_name' => $mealTitle, 'vegan' => $vegan->title, 'vegan_price' => $vegan->price, 'complementary_snacks' => $freesnackstitle, 'extra_snacks' => $extra_snacks, 'extra_price' => $tol_extra_snacks_price, 'gourment_snacks' => $gourmentsnacks, 'gourment_price' => $tol_price, 'total_price' => $totalAmount, 'plan_no' => $plan_no,
                'review' => $review, 'total_delivery_cost' => $delivery_charge
            ]);

            return redirect('/dashboard');
        } else {

            return redirect('/redirect_to_register_for_auth')->with('mealInfoForRegister');
        }
    }

    public function getAddToCheckout(Request $request)
    {

        $freesnacks_id = session()->get('freesnacks_id');
        $vegan_id = session()->get('vegan_id');
        $extrasnacks_id = session()->get('extrasnacks_id');

        $gournment_id = session()->get('gournment_id');
        $vegan = Product::where('id', $vegan_id)->first();

        $freesnacks = Product::where('id', $freesnacks_id)->first();

        $price = 0;
        if ($gournment_id) {
            foreach ($gournment_id as $id) {
                $gournment[] = \App\Product::where('id', $id)->first();
            }
            foreach ($gournment as $grou) {
                $price = $price + $grou->price;
            }
        }

        $tol_price = $price;

        $sum = 0;
        if ($extrasnacks_id) {
            foreach ($extrasnacks_id as $snacks_id) {
                $extrasnacks[] = \App\Product::where('id', $snacks_id)->first();
            }
            foreach ($extrasnacks as $extra) {
                $sum = $sum + $extra->price;
            }
        }

        $tol_extra_snacks_price = $sum;
        $cat_id = session()->get('prod_cat_id');
        $delivery_info = DB::table('productcategories')->where('id', $cat_id)->select('deliveries', 'delivery_rate', 'total_delivery_cost')->get();
        $plan = Productcategory::where('id', session()->get('prod_cat_id'))->first();
        $fitreatss = session()->get('fitt');
        $product = session()->get('freesnacks_id');
        return view('admin-panel.weightplan.add_to_checkout_package_redirect', compact('vegan', 'extrasnacks_id', 'freesnacks', 'gournment_id', 'tol_price', 'tol_extra_snacks_price', 'cat_id', 'delivery_info', 'plan', 'product'));
    }
    public function addVeganToCheckout(Request $request)
    {
        return $request->all();

        session()->put('vegan_id', $request->vegan);

        $vegan_id = $request->vegan;

        $vegan = Product::where('id', $vegan_id)->first();
        $snacks = Product::where(['cat_id' => $vegan->cat_id, 'week_id' => $vegan->week_id, 'food_id' => 2])->get();


        return view('admin-panel.weightplan.free_snacks_paln_redirect', compact('vegan_id', 'snacks'));
    }

    public function addFreeSnacksToCheckout(Request $request)
    {
        session()->put('freesnacks_id', $request->freesnacks_id);
        $freesnacks_id = $request->freesnacks_id;
        $vegan_id = $request->vegan_id;
        $vegan = Product::where('id', $vegan_id)->first();
        $snacks = Product::where(['cat_id' => $vegan->cat_id, 'week_id' => $vegan->week_id, 'food_id' => 2])->get();


        return view('admin-panel.weightplan.extra_snacks_paln_redirect', compact('vegan_id', 'freesnacks_id', 'snacks'));
    }
    public function addExtraSnacksToCheckout(Request $request)
    {

        session()->put('extrasnacks_id', $request['extrasnacks_id']);

        $freesnacks_id = $request->freesnacks_id;
        $vegan_id = $request->vegan_id;
        $extrasnacks_id = $request['extrasnacks_id'];
        $vegan = Product::where('id', $vegan_id)->first();
        $freesnacks = Product::where('id', $freesnacks_id)->first();

        $gournment = Product::where(['cat_id' => $vegan->cat_id, 'week_id' => $vegan->week_id, 'food_id' => 3])->get();

        return view('admin-panel.weightplan.add_gournment_redirect', compact('vegan_id', 'freesnacks', 'vegan', 'freesnacks_id', 'freesnacks', 'extrasnacks_id', 'gournment'));
    }


    public function detailsToCheckout(Request $request)
    {
        session()->put('gournment_id', $request['gourment_id']);



        return view('admin-panel.weightplan.add_details_to_checkout_redirect');
    }


    public function goToSetMealPlan()
    {
        $extra_snacks_reg = session()->put('extra_snacks', '');
        $extra_price_reg = session()->put('extra_price', '');
        return redirect('set-meal-plan');
    }


    public function saveMealPlanOrder($extrasnacks_id)
    {

        $freesnacks_id = session()->get('freesnacks_id');
        $vegan_id = session()->get('vegan_id');
        //$extrasnacks_id = session()->get('extrasnacks_id');
        $cat_id = session()->get('prod_cat_id');
        $delivery_charge_info = DB::table('productcategories')->select('total_delivery_cost')->where('id', $cat_id)->first();
        $delivery_charge = $delivery_charge_info->total_delivery_cost;
        // return $extrasnacks_id;
        $gournment_id = session()->get('gournment_id');

        $vegan = Product::where('id', $vegan_id)->first();
        //dd($vegan);

        $freesnacks = Product::where('id', $freesnacks_id)->first();
        if (session()->get('first_comp') == 0) {
            $freesnackstitle = '';
        } else {
            $freesnackstitle = '';
            $freesnacks = Product::where('id', $freesnacks_id)->first();
            $freesnackstitle = $freesnacks->title;
        }
        $price = 0;
        $gourmentsnacks = '';
        if ($gournment_id) {
            foreach ($gournment_id as $id) {
                $gournment[] = \App\Product::where('id', $id)->first();
            }
            foreach ($gournment as $grou) {
                $price = $price + $grou->price;
                $gourmentsnacks = $gourmentsnacks . '+' . $grou->title;
            }
        }

        $tol_price = $price;

        $sum = 0;
        $paid_dess = Productcategory::where('id', $cat_id)->first()->paid_dess;
        session()->put('second_comp', $paid_dess);
        if (session()->get('second_comp') == 0) {
            $extra_snacks = '';
            /*dd('hello suresh');*/
        } else {
            $extra_snacks = '';
            if ($extrasnacks_id) {
                //echo 'total snacks'.count($extrasnacks_id);
                foreach ($extrasnacks_id as $snacks_id) {
                    $extrasnacks[] = \App\Product::where('id', $snacks_id)->first();
                }

                foreach ($extrasnacks as $extra) {
                    $sum = $sum + $extra->price;
                    $extra_snacks = $extra_snacks . ' | ' . $extra->title;
                }
            }
        }
        $tol_extra_snacks_price = $sum;
        $totalAmount = $vegan->price + $tol_extra_snacks_price + $tol_price + $delivery_charge;

        $mealTitleInfo = DB::table('productcategories')->select('name')
            ->where('id', '=', $vegan->cat_id)->first();

        $mealTitle = $mealTitleInfo->name;



        //setting session values
        $review = session()->get('review');
        $plan_name_reg = session()->put('mealTitle', $mealTitle);
        $vegan_reg = session()->put('vegan', $vegan->title);
        $vegan_price_reg = session()->put('vegan_price', $vegan->price);
        $complementary_snacks_reg = session()->put('complementary_snacks', $freesnackstitle);
        $extra_snacks_reg = session()->put('extra_snacks', $extra_snacks);
        $extra_price_reg = session()->put('extra_price', $tol_extra_snacks_price);
        $gourment_snacks_reg = session()->put('gourment_snacks', $gourmentsnacks);
        $gourment_price_reg = session()->put('gourment_price', $tol_price);
        $total_price_reg = session()->put('total_price', $totalAmount);
        //$plan_no_reg=session()->put('plan_no',$plan_no);
        $review_reg = session()->put('review', $review);
        $total_delivery_cost_reg = session()->put('total_delivery_cost', $delivery_charge);

        if (Auth::user()) {


            $userId = $userId = Auth::user()->id;
         
            $review = session()->get('review');
            $planNoExist = DB::table('setmealdata')->exists();
            //   return $planNoExist;
            $plan_no = 1;
            if ($planNoExist) {
                $last_plan_no = DB::table('setmealdata')->orderBy('plan_no', 'desc')->first();

                $plan_no += $last_plan_no->plan_no;
            }
            DB::table('setmealdata')->where('user_id',$userId)->where('is_paid','0')->delete();
            DB::table('setmealdata')->insert([
                'user_id' => $userId, 'plan_name' => $mealTitle, 'vegan' => $vegan->title, 'vegan_price' => $vegan->price,
                'weekly_price' => $vegan->weekly_price,
                'price_per_meal' => $vegan->price_per_meal,
                'complementary_snacks' => $freesnackstitle,
                'extra_snacks' => $extra_snacks, 'extra_price' => $tol_extra_snacks_price, 'gourment_snacks' => $gourmentsnacks, 'gourment_price' => $tol_price, 'total_price' => $totalAmount, 'plan_no' => $plan_no,
                'review' => $review, 'total_delivery_cost' => $delivery_charge, 'set_meal_selected' => '1'
            ]);

           

           // $setMealCount = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->get()->count();
            

            session()->put('extrasnacks_id', $extrasnacks_id);
            $setMealIdLatest = DB::table('setmealdata')->orderBy('id', 'desc')->first()->id;
           
            
                $extra_snacks = '';
                if ($extrasnacks_id) {

                    

                    $i = 0;

                    foreach ($extrasnacks_id as $snacks_id) {
                        $extrasnackss[] = \App\Product::where('id', $snacks_id)->first();

                        $i = $i + 1;
                    }

                   
                        foreach ($extrasnackss as $extra) {

                            

                            DB::table('extra_snack_plans')->insert([
                                'extra_snack' => $extra->title,
                                'quantity' => '12',
                                'rate' => $extra->price,
                                'total_price' => $extra->price,
                                'plan_no' => $plan_no,
                                'set_meal_id' => $setMealIdLatest
                            ]);
                       
                    
                }
                session()->put('plan_no', $plan_no);
            }

       

            return redirect('/dashboard');
        } else {


            $userId = '';
            $review = session()->get('review');
            $plan_no = session()->get('plan_no');
            if (empty($plan_no)) {
                //  dd("hello anil, plan is empty");
                $last_plan_no = DB::table('setmealdata')->orderBy('id', 'desc')->first();
                if (empty($last_plan_no)) {
                    $plan_no = 1;
                } else {
                    $plan_no = $last_plan_no->plan_no + 1;
                }
                // dd( "plan number is" . $plan_no);

                $setMealCount = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->get()->count();

            

                if ($setMealCount == 0) {

                    DB::table('setmealdata')->insert([
                        'user_id' => $userId, 'plan_name' => $mealTitle,
                         'vegan' => $vegan->title,
                        'vegan_price' => $vegan->price, 'weekly_price' => $vegan->weekly_price,
                        'price_per_meal' => $vegan->price_per_meal,
                        'complementary_snacks' => $freesnackstitle,
                        'extra_snacks' => $extra_snacks,
                        'extra_price' => $tol_extra_snacks_price,
                        'gourment_snacks' => $gourmentsnacks, 'gourment_price' => $tol_price, 'total_price' => $totalAmount,
                        'plan_no' => $plan_no,
                        'review' => $review, 'total_delivery_cost' => $delivery_charge, 'set_meal_selected' => '1'
                    ]);
                }

                $setMealIdLatest = DB::table('setmealdata')->orderBy('id', 'desc')->first()->id;


                //adding extra snacks in extra snacks table

                if (session()->get('second_comp') == 0) {
                    $extra_snacks = '';
                    //dd('hello suresh');
                } else {
                    $extra_snacks = '';
                    if ($extrasnacks_id) {

                        $i = 0;

                        foreach ($extrasnacks_id as $snacks_id) {
                            $extrasnackss[] = \App\Product::where('id', $snacks_id)->first();

                            $i = $i + 1;
                        }
                        if ($setMealCount == 0) {
                            foreach ($extrasnacks as $extra) {

                                DB::table('extra_snack_plans')->insert([
                                    'extra_snack' => $extra->title,
                                    'quantity' => '12',
                                    'rate' => $extra->price,
                                    'total_price' => $extra->price,
                                    'plan_no' => $plan_no,
                                    'set_meal_id' => $setMealIdLatest
                                ]);
                            }
                        }
                    }
                }
                $paid_dess = Productcategory::where('id', $cat_id)->first()->paid_dess;
                session()->put('second_comp', $paid_dess);
                $plan_no = session()->put('plan_no', $plan_no);
            } else {

                $setMealCount = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->get()->count();



                if ($setMealCount == 0) {
                    DB::table('setmealdata')->insert([
                        'user_id' => $userId, 'plan_name' => $mealTitle, 'vegan' => $vegan->title, 'vegan_price' => $vegan->price, 'weekly_price' => $vegan->weekly_price, 'price_per_meal' => $vegan->price_per_meal,  'complementary_snacks' => $freesnackstitle, 'extra_snacks' => $extra_snacks, 'extra_price' => $tol_extra_snacks_price, 'gourment_snacks' => $gourmentsnacks, 'gourment_price' => $tol_price, 'total_price' => $totalAmount, 'plan_no' => $plan_no,
                        'review' => $review, 'total_delivery_cost' => $delivery_charge, 'set_meal_selected' => '1'
                    ]);
                }
                $setMealIdLatest = DB::table('setmealdata')->orderBy('id', 'desc')->first()->id;
                if (session()->get('second_comp') == 0) {
                    $extra_snacks = '';
                } else {
                    $extra_snacks = '';
                    if ($extrasnacks_id) {

                        $i = 0;

                        foreach ($extrasnacks_id as $snacks_id) {
                            $extrasnackss[] = \App\Product::where('id', $snacks_id)->first();

                            $i = $i + 1;
                        }
                        if ($setMealCount == 0) {
                            foreach ($extrasnacks as $extra) {

                                DB::table('extra_snack_plans')->insert([
                                    'extra_snack' => $extra->title,
                                    'quantity' => '12',
                                    'rate' => $extra->price,
                                    'total_price' => $extra->price,
                                    'plan_no' => $plan_no,
                                    'set_meal_id' => $setMealIdLatest
                                ]);
                            }
                        }
                    }
                }
                $paid_dess = Productcategory::where('id', $cat_id)->first()->paid_dess;
                session()->put('second_comp', $paid_dess);
                $plan_no = session()->put('plan_no', $plan_no);
            }



            $extra_snacks_reg = session()->put('extra_snacks', '');
            $extra_price_reg = session()->put('extra_price', '');
        }
    }

    public function clearBasket(Request $request)
    {
        $plan_no = session()->get('plan_no');
        DB::table('setmealdata')->where('plan_no', $plan_no)->delete();
        DB::table('extra_snack_plans')->where('plan_no', $plan_no)->delete();
        //$request->session()->flush();
        Session::forget('plan_no');

        return redirect('/shopping-basket');
    }

    public function fitreatsDetail(Request $request)
    {
        $fitreat = DB::table('productcategories')
            ->select('name', 'body', 'price', 'short_intro', 'image_path', 'id')
            ->where('meal_plan_type', '=', 'n')->get();
        $product = DB::table('products')->where('id', $request->fitreat_id)->first();
        return view('homepage-pages.fitreats_product', compact('product', 'fitreat'));
    }

    public function displayCheckout()
    {
        //display checkout when setmeal plan is only there 
        $plan_no = session()->get('plan_no');
        //return $plan_no;
        $setMealCount = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->get()->count();
        $setMealName = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->where('set_meal_selected', '1')->first();
        $orderDetail = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->get();
        //return $orderDetail;
        $extra_snacks_detail = DB::table('extra_snack_plans')->where('plan_no', '=', $plan_no)->get();

        //dd($extra_snacks_detail);
        if ($setMealCount == 1) {
            if (isset($plan_no)) {
                $mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->where('set_meal_selected', '=', '1')->first()->vegan_price;
                $extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('extra_price');
                $sub_total = $mealPlanPrice + $extra_meal_price;
                $sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
            } else {
                $sub_total_in_decimal = '0.00';
            }
            $delivery_info = DB::table('productcategories')->where('name', $setMealName->plan_name)->get();
        } else {
            if (isset($plan_no)) {
                // $mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->first()->vegan_price;
                $extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('total_price');
                $sub_total = $extra_meal_price;
                $sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
                $delivery_info = DB::table('productcategories')->where('name', 'Fitreats')->get();
            } else {
                $sub_total_in_decimal = '0.00';
            }
         
           // dd($delivery_info);
        }
        return view('admin-panel.weightplan.add_to_checkout_package_redirect', compact('orderDetail', 'extra_snacks_detail', 'sub_total_in_decimal', 'delivery_info', 'setMealCount'));


        // return view('admin-panel.weightplan.add_to_checkout_package_redirect',compact('vegan','extrasnacks_id','fitreatss','freesnacks','gournment_id','tol_price','tol_extra_snacks_price','delivery_info', 'product', 'plan'));

    }


    public function addToCheckoutanil(Request $request)
    {
        session()->put('gournment_id', $request['gourment_id']);
        $vegan_id = $request->vegan_id;
        $vegan = Product::where('id', $vegan_id)->first();

        //return $vegan;
        // $freesnacks_id = $request->freesnacks_id;
        // $freesnacks = Product::where('id', $freesnacks_id)->first();

        $extrasnacks_id = $request['extrasnacks_id'];
        session()->put('extrasnacks_id', $extrasnacks_id);
        $this->saveMealPlanOrder($extrasnacks_id);
        //vegan is second meal- page

        $plan_no = session()->get('plan_no');
        $orderDetail = DB::table('setmealdata')->where('plan_no', '=', $plan_no)->get();

        //return $orderDetail;

        $cat_id = session()->get('prod_cat_id');

        $delivery_info = DB::table('productcategories')->where('id', '=', $cat_id)->select('deliveries', 'delivery_rate', 'total_delivery_cost')->first();
        $price = 0;

        $cat_id = session()->get('prod_cat_id');

        $delivery_info = DB::table('productcategories')->where('id', $cat_id)->select('deliveries', 'delivery_rate', 'total_delivery_cost')->get();
        $gournment_id = $request['gourment_id'];
        if ($gournment_id) {
            foreach ($gournment_id as $id) {
                $gournment[] = \App\Product::where('id', $id)->first();
            }
            foreach ($gournment as $grou) {
                $price = $price + $grou->price;
            }
        }
        $tol_price = $price;
        $sum = 0;
        if ($extrasnacks_id) {
            foreach ($extrasnacks_id as $snacks_id) {
                $extrasnacks[] = \App\Product::where('id', $snacks_id)->first();
            }
            foreach ($extrasnacks as $extra) {
                $sum = $sum + $extra->price;
            }
        }
        $tol_extra_snacks_price = $sum;
        $fitreatss = session()->get('fitt');
        $product = session()->get('freesnacks_id');
        $plan = Productcategory::where('id', session()->get('prod_cat_id'))->first();
        $extra_snacks_detail = DB::table('extra_snack_plans')->where('plan_no', '=', $plan_no)->get();
        //dd($plan_no);
        $mealPlanPrice = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->where('set_meal_selected', '=', '1')->first()->vegan_price;
        $extra_meal_price = DB::table("setmealdata")->where('plan_no', '=', $plan_no)->get()->sum('extra_price');
        $sub_total = $mealPlanPrice + $extra_meal_price;
        $sub_total_in_decimal = number_format((float)$sub_total, 2, '.', '');
        return redirect('/shopping-basket');
        // return view('admin-panel.weightplan.add_to_checkout_package_redirect',compact('vegan','extrasnacks_id','fitreatss','freesnacks','gournment_id','tol_price','tol_extra_snacks_price','delivery_info', 'product', 'plan'));
    }

    public function addToCheckoutwithoutpaiddessert(Request $request)
    {


        session()->put('gournment_id', $request['gourment_id']);
        $vegan_id = $request->vegan_id;

        $vegan = Product::where('id', $vegan_id)->first();

        $cat_id = session()->get('prod_cat_id');

        $freesnacks_id = $request->freesnacks_id;
        $freesnacks = Product::where('id', $freesnacks_id)->first();

        $price = 0;

        $cat_id = session()->get('prod_cat_id');

        $delivery_info = DB::table('productcategories')->where('id', $cat_id)->select('deliveries', 'delivery_rate', 'total_delivery_cost')->get();

        // dd($delivery_info);


        $gournment_id = $request['gourment_id'];
        if ($gournment_id) {
            foreach ($gournment_id as $id) {
                $gournment[] = \App\Product::where('id', $id)->first();
            }
            foreach ($gournment as $grou) {
                $price = $price + $grou->price;
            }
        }

        $tol_price = $price;

        $sum = 0;

        $tol_extra_snacks_price = $sum;

        //return $tol_extra_snacks_price;

        return view('admin-panel.weightplan.add_to_checkout_package_without_paid_dessert_redirect', compact('vegan', 'extrasnacks_id', 'freesnacks', 'gournment_id', 'tol_price', 'tol_extra_snacks_price', 'delivery_info'));
    }





    public function weekPlan($cat_id, $week_id)
    {

        //session()->flush();

        $plan_no = session()->get('plan_no');


        if (DB::table('setmealdata')->where('plan_no', $plan_no)->where('set_meal_selected', '1')->exists()) {
            //return redirect('/set-meal-plan');
            return redirect('/set-meal-plan')->with("alert", 'Set Meal Plan Has Been Already Created');
        } else {
            session()->put('prod_cat_id', $cat_id);

            // $prod_cat_id= session()->get('prod_cat_id');
            // return $prod_cat_id;


            $product = Product::where(['cat_id' => $cat_id, 'food_id' => 1])->get();

            //return $product;

            //return $product;
            $pro = Product::where(['cat_id' => $cat_id, 'food_id' => 1])->first();

            $mealPlanInfo = DB::table('productcategories')
                ->select('name')
                ->where('id', '=', $cat_id)->first();




            $mealPlan = $mealPlanInfo->name;


            $comp_dess = Productcategory::where('id', $cat_id)->first()->comp_dess;


            session()->put('first_comp', $comp_dess);


            return view('admin-panel.weightplan.display_product_with_week_plan_redirect', compact('mealPlan', 'week_id', 'product', 'pro'));
        }
    }






    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */

    public function delete(Product $product, $id)
    {
        Product::where('id', $id)->delete();
        return redirect('/show-product-report');
    }
}
