<?php

namespace App\Http\Controllers;



use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use File;
use Illuminate\Support\Facades\Response;

class ProfileController extends Controller
{

    public function index()
    {

        //$roles = DB::table('roles')->select('id', 'role', 'created_at')->get();

        $role = DB::table('roles')->pluck( 'role','id');
        return view('auth.register', compact('role'));

    }
    public function userregister(){

        $role = DB::table('roles')->where('role','!=',1)->pluck( 'role','id');
        return view('user.register', compact('role'));
    }

    public function userRegisterCheckout(Request $request){
        
        $user=User::create([
               'name' => $request->name,
                'email' => $request->email,
                'role_id' => 2,
                'phone_number'=>$request->phone,
                'addressline1'=>$request->address,
                'addressline2'=>$request->town,
                'addressline3'=>$request->county,
                'postcode'=>$request->postcode
       ]);
 

     return redirect()->back()->with("success", "Register Successfully");
  
    }


    public function userregisterstore(Request $request)
    {


      $this->validate($request, [
            'name' => 'required|max:255',
            'phone' => 'required|min:10',
             'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
      ]);

      $api_token= str_random(30);
      $email_exists=DB::table('users')
        ->select('email')
        ->where('email','=',$request->email)->count();



        if($email_exists<=0)

        {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            if ($featuredImage != null) {

                $featuredImg = $featuredImage->getClientOriginalName();
                $featuredImage->move($destinationPathFeatured, $featuredImg);
                //save into the database

            }

            else{
                $featuredImg='/images/3/blank-profile-picture-973460_960_720.png';
            }



            $user=User::create([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => 2,
                'image_path' => $destinationPathFeatured . '/' . $featuredImg,
                'password' => bcrypt($request->password),
                'phone_number'=>$request->phone,
                'gender'=> $request->gender,
                'dob'=>$request->dob,
                'addressline1'=>$request->address,
                'addressline2'=>$request->town,
                'addressline3'=>$request->county,
                'postcode'=>$request->postcode,
                'user-description'=>$request->userdescription,
                'terms'=>$request->terms,
                'api_token' => 1
            ]);

            //save data for set meal plan 
            $mealTitle = $request->mealTitle;
            if($mealTitle != "NAN"){
                $userId = DB::table('users')->where('email', $request->email)->get();

                $planNoExist = DB::table('setmealdata')->where('user_id', $userId[0]->id)->exists();
                
                $plan_no = 1;
                if($planNoExist){
                    $last_plan_no = DB::table('setmealdata')->orderBy('plan_no', 'desc')->first();
                    $plan_no += $last_plan_no->plan_no;
                }
                $review = $request->review;
                $delivery_charge = $request->delivery_charge;
                DB::table('setmealdata')->insert([
                    'user_id' => $userId[0]->id, 'plan_name' => $mealTitle, 'vegan'=>$request->vegan , 'vegan_price'=>$request->vegan_price, 'complementary_snacks'=>$request->complementary_snacks, 'extra_snacks'=>$request->extra_snacks, 'extra_price'=>$request->extra_price, 'gourment_snacks'=>$request->gourment_snacks, 'gourment_price'=>$request->gourment_price, 'total_price'=> $request->total_price, 'plan_no' => $plan_no,'review'=>$review,'total_delivery_cost'=>$delivery_charge
                ]);
            }


            //save custom data
            $idFromBasket = $request->idFromBasket;
            if($idFromBasket != "NAN"){
                $basket_item_counts = DB::table('customtracktemp')->where('temp_user_id', $idFromBasket)->count();

                if($basket_item_counts){
                    $itemFromBaskets = DB::table('customtracktemp')->where('temp_user_id', $idFromBasket)->get();

                    $userId = DB::table('users')->where('email', $request->email)->get();

                    $planNoExist = DB::table('customplanusers')->where('user_id', $userId[0]->id)->exists();
                    
                    $plan_no = 1;
                    if($planNoExist){
                        $last_plan_no = DB::table('customplanusers')->orderBy('plan_no', 'desc')->first();
                        $plan_no += $last_plan_no->plan_no;
                    }


                    

                    foreach($itemFromBaskets as $itemFromBasket){
                        $noOfDelivery;
                        if($weekplanid ==1) {
                            $noOfDelivery = 8;
                        } elseif ($weekplanid ==2) {
                            $noOfDelivery = 12;
                        } elseif ($weekplanid ==3) {
                            $noOfDelivery = 16;
                        } 
                        DB::table('customplanusers')->insert(
                            ['user_id' => $userId[0]->id, 'weekplans_id' => $itemFromBasket->weekplans_id, 'meal_name' => $itemFromBasket->meal_name, 'meal_quantity' => $itemFromBasket->meal_quantity, 'vegcarb' => $itemFromBasket->vegcarb, 'plan_no' => $plan_no, 'noOfDelivery' => $noOfDelivery, 'meal_id'=>$itemFromBasket->meal_id ]
                        );
                    }

                    DB::table('customtracktemp')->where('temp_user_id', $userId)->delete();
                }
            }

           Auth::login($user);

           if($request->Normalid) {
                $url = '/custom-meal-plans/'.$request->week_id.'/'.$request->Normalid.'/'.Auth::user()->id;
                return redirect($url);
            }
            
            // return redirect('/user-register')->with("success", "user created");
            return redirect('/home');
        }
        else{
            return redirect('/user-register')->withErrors("User Exists Already, Use different email id and create once again");
        }



        //return redirect('/register')->with("success","Profile Created Successfully");


    }


    public function store(Request $request)
    {
        $email_exists=DB::table('users')
            ->select('email')
            ->where('email','=',$request->email)->first();



        if(count($email_exists)<=0)

        {
            $featuredImage = Input::file('image');
            $destinationPathFeatured = 'images/profileImages';
            if ($featuredImage != null) {

                $featuredImg = $featuredImage->getClientOriginalName();
                $featuredImage->move($destinationPathFeatured, $featuredImg);
                //save into the database
                /* return $featuredImg;*/
            }

            else{
                $featuredImg='/images/3/blank-profile-picture-973460_960_720.png';
            }



            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => $request->role_id,
                'image_path' => $destinationPathFeatured . '/' . $featuredImg,
                'password' => bcrypt($request->password),
                'api_token' => str_random(60)
            ]);
            return redirect('/create-user')->with("success", "Profile Created Successfully");
        }
        else{
            return redirect('/create-user')->withErrors("User Exists Already, Use different email id and create once again");
        }



        //return redirect('/register')->with("success","Profile Created Successfully");


    }


    public function displayUser()
    {
        $userInfo=Auth::User();
       
        if($userInfo->role_id==1){
            $user_infos = DB::table('users')->select('id', 'name', 'email', 'image_path','role_id', 'created_at')->get();
        }
        else{
            $user_infos = DB::table('users')->select('id', 'name', 'email', 'image_path','role_id', 'created_at')->where(['role_id'=>2,'email'=>$userInfo->email])->get();
        }


        return view('user_information.user_info',compact('user_infos','userInfo'));



    }

    public function edit($id)
    {
       /* $role = [
            '1' => 'Administrator',
            '2' => 'Student',
            '3' => 'Teacher',
            '4' => 'Parents'
        ];*/
        
        $role=DB::table('roles')->pluck('role','id');
       
        $userInfos = User::where('id', $id)->first();

        return view('user_information.user_info_edit', compact('userInfos', 'role'));
    }
    public function update(Request $request, $id)
    {

if(isset($request->image))
{
    $featuredImage = Input::file('image');
      $destinationPathFeatured = 'images/profileImages';
            $featuredImg = $featuredImage->getClientOriginalName();
            $featuredImage->move($destinationPathFeatured, $featuredImg);
              $image_path= $destinationPathFeatured . '/' . $featuredImg;
            //save into the database
}
else{
   $featuredImage = User::select('image_path')->where('id',$id)->first();
   $image_path=$featuredImage->image_path;
}






     

        User::where('id', $id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,

                'addressline1'=>$request->addressline1,
                'addressline2'=>$request->addressline2,
                'addressline3'=>$request->addressline3,
                'postcode'=>$request->postcode,

                'phone_number'=>$request->phone_number,
                'image_path' => $image_path,
               
            ]);
 
        return redirect('userinfo/' . $id . '/edit')->with("success", 'Your Profile Has Been Updated.');
    }
    public function delete($id)
    {
        User::where('id', $id)->delete();
        return redirect('userinfo/');
    }
    public function changePassword(){

     $userInfo=Auth::User();
   
    
            $user_infos = DB::table('users')->select('id','role_id', 'name','password', 'email', 'image_path', 'created_at')->where('id','=',$userInfo->id)->get();
      $user_info = DB::table('users')->select('id','role_id', 'name','password', 'email', 'image_path', 'created_at')->where('id','=',$userInfo->id)->first();
  

        return view('user_information.change-password',compact('user_infos','user_info'));
    }
    public function updatePassword(Request $request){
        
        
        if($request->password==$request->password_confirmation)
        {
             User::where('id', $request->id)
            ->update([
                'password' => bcrypt($request->password),
            ]);
              return redirect('/logout')->with("success", 'Password Has Been Chnaged.');
             
        }
else
{
    return redirect('change-password/')->with("success", 'Password Confirmation doesnot Matched.');
}
       


       


    }
    public function confirm($api_token,$email)
    {

        if( !$api_token)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::where('email',$email)->count();

        if ($user==0)
        {
            throw new InvalidApiTokenException;
        }
else
{
        User::where('email', $email)
            ->update([
                'api_token' => 1,
            ]);
$data= array(
            'email' => $email,
            'subject'=>'Account Register'
             );
    Mail::send('emails.userregister',$data, function($message) use ($data){
$message->from($data['email']);
            $message->to('admin@progressivenaatiii.com.au');
            $message->subject($data['subject']);
        });

  

}
  return redirect('/login')->with("success", "Profile Created Sucessfully !! Login with your Username and Password");

      

           }
           
           public function exportCSV(){
        $userInfo=Auth::User();
//dd($userInfo);
        if($userInfo->role_id==1){
            $user_infos = DB::table('users')->get();
            // dd($user_infos);
        
        

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=file.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );


        $columns = array('S.N.', 'Name', 'Email', 'Gender', 'Phone Number', 'Address', 'Postal Code', 'Created Date');

        $callback = function() use ($user_infos, $columns)
        {
$count = 1;
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($user_infos as $user_info) {
                fputcsv($file, array(
                    $count++, 
                    $user_info->name, 
                    $user_info->email, 
                    $user_info->gender, 
$user_info->phone_number, 
                    $user_info->addressline1 . ', ' . $user_info->addressline2 . ', ' . $user_info->addressline3, 
                    $user_info->postcode, 
                    $user_info->created_at
                ));
            }
            fclose($file);
        };
        return Response::stream($callback, 200, $headers);
        }
        else{
            return false;
        }
    }
}
