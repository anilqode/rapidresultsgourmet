<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productcategory extends Model
{
    protected $guarded = [];

    protected $fillable = [
        'name', 'body', 'slug', 'meal_plan_type', 'price', 'short_intro', 'deliveries', 'delivery_rate', 'paidDes',
        'total_delivery_cost', 'total_delivery_cost_fit', 'fit', 'comp_dess', 'paid_dess', 'image_path', 'best_seller', 'most_popular'
    ];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}