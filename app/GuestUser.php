<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestUser extends Model
{
    protected $fillable=[
             'name' ,
			 'email' ,
			 'phone_number',
			 'addressline1',
			 'addressline2',
			 'addressline3',
             'postcode',
             'order_id',
             'is_paid',
             'review'
    ];
}
