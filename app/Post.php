<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'author_id','catag_id','role_id', 'title','body','meta_description','meta_keywords','image', 'slug','status',
    ];
}
